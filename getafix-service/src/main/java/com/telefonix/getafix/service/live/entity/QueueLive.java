package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.asterisk.service.live.entity.QueueParams;
import com.telefonix.getafix.asterisk.service.live.entity.QueueSummary;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-06-24.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"queue"}, callSuper = true)
public class QueueLive extends Live {

    private String queueId;

    private Queue queue;

    private String companyId;

    private QueueParams queueParams;

    private QueueSummary queueSummary;

    private Map<String, AgentLive> agentLiveMap = new ConcurrentHashMap<>();

    private Map<String, QueueChannelLive> channelLiveMap = new ConcurrentHashMap<>();

    private VoiceMailLive voiceMailLive;

    public QueueLive(Queue queue, VoiceMailLive voiceMailLive) {
        super(queue.getId());
        this.queue = queue;
        this.companyId = queue.getCompany().getCompanyId();
        this.queueId = TelephonyUtils.getUniqueQueueId(companyId, queue.getQueueId());
        this.voiceMailLive = voiceMailLive;
    }

    @Override
    public String toString() {
        return "QueueLive{" +
            "queue=" + queueId +
            ", companyId=" + companyId +
            ", queueName=" + queue.getQueueName() +
            ", agentLiveMap=" + agentLiveMap.keySet() +
            ", channelLiveMap=" + channelLiveMap.keySet() +
            ", params=" + queueParams +
            ", queueSummary=" + queueSummary +
            ", voiceMailLive=" + voiceMailLive +
            '}';
    }
}
