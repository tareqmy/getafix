package com.telefonix.getafix.service;

import com.telefonix.getafix.service.live.dto.*;
import reactor.core.publisher.Flux;

/**
 * Created by tareqmy on 2019-05-27.
 */
public interface ServerSentEventService {

    Flux<ServerTimeDTO> streamTime();

    Flux<SystemLiveDTO> streamSystemLive();

    Flux<CallCenterLiveDTO> streamCallCenterLive();

    Flux<QueueLiveDTO> streamQueuesLive(String uniqueQueueId);

    Flux<AgentLiveDTO> streamAgentsLive();
}
