package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getfix.dto.CompanyReportDTO;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Created by tareqmy on 24/10/19.
 */
public interface CompanyReportService {

    CompanyReportDTO getCompanyReport(Company company, LocalDate from, LocalDate to, ZoneId zoneId);
}
