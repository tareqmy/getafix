package com.telefonix.getafix.service.live.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZoneId;

/**
 * Created by tareqmy on 24/9/19.
 */
@Getter
@Setter
@ToString
public class ServerTimeDTO extends LiveDTO {

    public ServerTimeDTO(ZoneId zoneId) {
        super(zoneId);
    }
}
