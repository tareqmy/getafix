package com.telefonix.getafix.service.live.entity;

import ch.loway.oss.ari4java.generated.Channel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by tareqmy on 2019-08-22.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"sourceChannelId"})
@ToString
public class OutboundQueueCall {

    private String companyId;

    private String queueId;

    private String sourceChannelId;

    private String sourceChannelName;

    public OutboundQueueCall(String companyId, String queueId, Channel sourceChannel) {
        this.companyId = companyId;
        this.queueId = queueId;
        this.sourceChannelId = sourceChannel.getId();
        this.sourceChannelName = sourceChannel.getName();
    }
}
