package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.System;
import com.telefonix.getafix.repository.SystemRepository;
import com.telefonix.getafix.service.CompanyService;
import com.telefonix.getafix.service.SystemService;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Slf4j
@Service
@Transactional
public class SystemServiceImpl implements SystemService {

    //NOTE: this should never be edited.
    private Long systemId = 1L;

    private SystemRepository systemRepository;

    private CompanyService companyService;

    @Autowired
    public SystemServiceImpl(SystemRepository systemRepository, CompanyService companyService) {
        this.systemRepository = systemRepository;
        this.companyService = companyService;
    }

    @Override
    public System getSystem() {
        return systemRepository.findById(systemId).orElseThrow(
            () -> new GenericGetafixException("Invalid system id " + systemId)
        );
    }

    @PostConstruct
    public void executeAction() {
        System system = getSystem();
        if (system.isRebuildDialplan()) {
            companyService.rebuildAllDialplan();
            system.setRebuildDialplan(false);
            systemRepository.save(system);
        }
    }
}
