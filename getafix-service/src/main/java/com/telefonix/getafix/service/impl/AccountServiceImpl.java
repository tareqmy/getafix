package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.*;
import com.telefonix.getfix.dto.ChangePassword;
import com.telefonix.getfix.dto.ExtensionReportDTO;
import com.telefonix.getfix.dto.UserDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tmyousuf on 6/9/18.
 */
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    private AuthenticationManager authenticationManager;

    private UserService userService;

    private ExtensionService extensionService;

    private PhoneService phoneService;

    private ReportService reportService;

    @Autowired
    public AccountServiceImpl(AuthenticationManager authenticationManager,
                              UserService userService,
                              ExtensionService extensionService,
                              PhoneService phoneService,
                              ReportService reportService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.extensionService = extensionService;
        this.phoneService = phoneService;
        this.reportService = reportService;
    }

    @Override
    public Authentication authenticate(UsernamePasswordAuthenticationToken authenticationToken) {
        try {
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication;
        } catch (AuthenticationException ae) {
            log.info("AuthenticationException {}", ae.getLocalizedMessage());
        }
        return null;
    }

    @Override
    public User findCurrentLoggedInUser() {
        return userService.getUser(SecurityUtils.getCurrentUserName());
    }

    @Override
    public User updateAccount(UserDTO userDTO) {
        return userService.updateUserInformation(userDTO);
    }

    @Override
    public void changePassword(ChangePassword changePassword) {
        String email = SecurityUtils.getCurrentUserName();
        userService.verifyOldPassword(email, changePassword.getOldPassword());
        userService.changeUserPassword(email, changePassword.getNewPassword());
    }

    @Override
    public void initiateEmailConfirm() {
        String email = SecurityUtils.getCurrentUserName();
        userService.initiateEmailConfirm(email);
    }

    @Override
    public boolean completeEmailConfirm(String token) {
        String email = SecurityUtils.getCurrentUserName();
        return userService.completeEmailConfirm(token, email);
    }

    @Override
    public List<Extension> getExtensionsForAccount() {
        User oneByEmail = findCurrentLoggedInUser();
        return extensionService.getExtensions(oneByEmail);
    }

    @Override
    public List<ExtensionReportDTO> getExtensionReports(Long id) {
        User oneByEmail = findCurrentLoggedInUser();
        if (oneByEmail.getExtensions().stream().noneMatch(extension -> extension.getId().equals(id))) {
            throw new BusinessRuleViolationException("Invalid extension id");
        }
        return reportService.getExtensionReports(oneByEmail, extensionService.getExtension(id));
    }

    @Override
    public List<Phone> getPhonesForAccount() {
        User oneByEmail = findCurrentLoggedInUser();
        return phoneService.getPhones(oneByEmail);
    }
}
