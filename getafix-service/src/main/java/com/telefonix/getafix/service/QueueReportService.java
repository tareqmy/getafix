package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Queue;
import com.telefonix.getfix.dto.QueueReportDTO;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Created by tareqmy on 4/9/19.
 */
public interface QueueReportService {

    QueueReportDTO getQueueReport(Queue queue, LocalDate from, LocalDate to, ZoneId zoneId);
}
