package com.telefonix.getafix.service.live;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.repository.UserRepository;
import com.telefonix.getafix.service.AgentLogService;
import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.AgentStatus;
import com.telefonix.getafix.service.live.entity.CallCenterLive;
import com.telefonix.getafix.service.live.entity.QueueLive;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-07-14.
 */
@Slf4j
@Component
public class CallCenterLiveRepository {

    private Map<String, CallCenterLive> callCenterLiveMap;

    private QueueLiveRepository queueLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    private UserLiveRepository userLiveRepository;

    private UserRepository userRepository;

    private AgentLogService agentLogService;

    private ScheduledExecutorService scheduleExecutor;

    private List<CallCenterLiveEventListener> callCenterLiveEventListeners;

    @Autowired
    public CallCenterLiveRepository(QueueLiveRepository queueLiveRepository,
                                    AgentLiveRepository agentLiveRepository,
                                    UserLiveRepository userLiveRepository,
                                    UserRepository userRepository,
                                    AgentLogService agentLogService,
                                    ScheduledExecutorService scheduleExecutor) {
        this.queueLiveRepository = queueLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.userLiveRepository = userLiveRepository;
        this.userRepository = userRepository;
        this.agentLogService = agentLogService;
        this.scheduleExecutor = scheduleExecutor;

        this.callCenterLiveMap = new ConcurrentHashMap<>();
        this.callCenterLiveEventListeners = new ArrayList<>();
    }

    public void addCallCenterLiveEventListener(CallCenterLiveEventListener callCenterLiveEventListener) {
        callCenterLiveEventListeners.add(callCenterLiveEventListener);
    }

    private void notifyListeners(CallCenterLive callCenterLive) {
        if (callCenterLive == null) {
            log.info("Request for null live notify!");
            return;
        }
        callCenterLiveEventListeners.forEach(callCenterLiveEventListener -> {
            callCenterLiveEventListener.onEvent(callCenterLive);
        });
    }

    public Collection<CallCenterLive> getCallCenterLiveMapValues() {
        return callCenterLiveMap.values();
    }

    // ----- live map manipulators -----

    public void initCallCenterLive(Company company) {
        if (company == null) {
            throw new IllegalArgumentException("Whats that?");
        }
        CallCenterLive callCenterLive = new CallCenterLive(company);
        callCenterLiveMap.put(company.getCompanyId(), callCenterLive);
    }

    public synchronized void updateCallCenterLive(Company company) {
        CallCenterLive callCenterLive = callCenterLiveMap.get(company.getCompanyId());
        if (callCenterLive == null) {
            callCenterLive = new CallCenterLive(company);
            callCenterLiveMap.put(company.getCompanyId(), callCenterLive);
        } else {
            //NOTE: cant just create and replace a new live because it contains agent/queue map inside which is updated separately
            callCenterLive.setCompany(company);
        }
    }

    public synchronized void removeCallCenterLive(Company company) {
        callCenterLiveMap.remove(company.getCompanyId());
    }

    // -----

    //NOTE: this is should never return null
    public synchronized CallCenterLive getCallCenterLive(String companyId) {
        return callCenterLiveMap.get(companyId);
    }

    //NOTE: this is should never return null
    private CallCenterLive getCallCenterLive(Company company) {
        return getCallCenterLive(company.getCompanyId());
    }

    // ----- agent actions -----

    //this is called when user tries to join
    public synchronized void join(User user, PhoneLine phoneLine) {
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());

        if (callCenterLive.getAgentLiveMap().containsKey(user)) {
            throw new BusinessRuleViolationException("Please log out first!");
        }
        AgentLive agentLive = agentLiveRepository.initAgentLive(user, phoneLine);

        user.setJoinedPhoneLine(phoneLine);
        userLiveRepository.putUserLive(userRepository.save(user));
        callCenterLive.getAgentLiveMap().put(user, agentLive);

        agentLogService.onJoinCallCenter(user, phoneLine);
        notifyListeners(callCenterLive);
    }

    public synchronized void leave(User user) {
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());

        AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
        if (agentLive == null) {
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }

        //before agent leave queue check agent has active queue calls
        if (agentLive.getChannelLiveMap().size() > 0) {
            throw new BusinessRuleViolationException("You must end all active calls first!");
        }

        //this is needed because otherwise the next join callcenter leaves agent in paused state.??
        if (agentLive.isPaused()) {
            agentLiveRepository.resumeAgent(agentLive, user);
        }

        agentLive.getStatusMap().keySet().forEach(queueName -> {
            queueLiveRepository.leaveQueue(queueName, agentLive.getPhoneLine());
        });

        scheduleExecutor.schedule(() -> {
            callCenterLive.getAgentLiveMap().remove(user);
            agentLiveRepository.removeAgentLive(user.getJoinedPhoneLine());
            agentLogService.onLeaveCallCenter(user, user.getJoinedPhoneLine());
            user.setJoinedPhoneLine(null);
            userLiveRepository.putUserLive(userRepository.save(user));
            notifyListeners(callCenterLive);
        }, 1L, TimeUnit.SECONDS);
    }

    public synchronized void pause(User user) {
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());

        AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
        if (agentLive == null) {
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }

        if (agentLive.isPaused()) {
            throw new BusinessRuleViolationException("You are already paused!");
        }

        agentLiveRepository.pauseAgent(agentLive, user);

        agentLive.getStatusMap().keySet().forEach(queueName -> {
            queueLiveRepository.pauseQueueMember(queueName, agentLive.getPhoneLine());
        });
        notifyListeners(callCenterLive);

        scheduleResume(callCenterLive, user);
    }

    private void scheduleResume(CallCenterLive callCenterLive, User user) {
        scheduleExecutor.schedule(() -> {
            AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
            if (agentLive == null) {
                log.debug("The agent is not long logged in anymore!");
                return;
            }
            if (agentLive.isPaused()) {
                log.debug("Time up! Resume the agent {}", agentLive);
                agentLive.getStatusMap().keySet().forEach(queueName -> {
                    queueLiveRepository.resumeQueueMember(queueName, agentLive.getPhoneLine());
                });

                agentLiveRepository.resumeAgent(agentLive, user);
                notifyListeners(callCenterLive);
            }
        }, 5L, TimeUnit.MINUTES);
    }

    public synchronized void resume(User user) {
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());

        AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
        if (agentLive == null) {
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }

        if (!agentLive.isPaused()) {
            throw new BusinessRuleViolationException("You are not paused!");
        }

        agentLive.getStatusMap().keySet().forEach(queueName -> {
            queueLiveRepository.resumeQueueMember(queueName, agentLive.getPhoneLine());
        });

        agentLiveRepository.resumeAgent(agentLive, user);
        notifyListeners(callCenterLive);
    }

    // -----

    public synchronized void initQueueLive(Queue queue) { //why is it not done through queuerepo?
        if (queue == null) {
            throw new IllegalArgumentException("Whats that?");
        }
        QueueLive queueLive = queueLiveRepository.initQueueLive(queue);
        CallCenterLive callCenterLive = getCallCenterLive(queue.getCompany());
        String uniqueQueueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        callCenterLive.getQueueLiveMap().put(uniqueQueueName, queueLive);
    }

    public synchronized void updateQueueLive(Queue queue) { //why is it not done through queuerepo?
        QueueLive queueLive = queueLiveRepository.updateQueueLive(queue);
        CallCenterLive callCenterLive = getCallCenterLive(queue.getCompany());
        String uniqueQueueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        callCenterLive.getQueueLiveMap().put(uniqueQueueName, queueLive);
    }

    public synchronized void deleteQueueLive(Queue queue) { //why is it not done through queuerepo?
        QueueLive queueLive = queueLiveRepository.removeQueueLive(queue);
        if (queueLive != null) {
            CallCenterLive callCenterLive = getCallCenterLive(queue.getCompany());
            String uniqueQueueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
            callCenterLive.getQueueLiveMap().remove(uniqueQueueName);
        }
    }

    // -----

    //why is it not done through agentrepo! otherwise how would callcenter will have it in its personal map!
    synchronized void initAgentLive(User user) {
        if (user == null || user.getJoinedPhoneLine() == null) {
            throw new IllegalArgumentException("Whats that?");
        }
        PhoneLine phoneLine = user.getJoinedPhoneLine();
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());
        AgentLive agentLive = agentLiveRepository.initAgentLive(user, phoneLine);

        if (agentLive.getStatusMap().values().stream().anyMatch(agentStatus -> agentStatus.equals(AgentStatus.PAUSED))) {

            agentLive.getStatusMap().forEach((queueName, agentStatus) -> {
                log.debug("{}/{}", queueName, agentStatus);
                if (agentStatus.equals(AgentStatus.PAUSED)) {
                    //incase of application restart and the scheduled resume was not executed
                    queueLiveRepository.resumeQueueMember(queueName, phoneLine);
                }
            });
            agentLiveRepository.resumeAgent(agentLive, user);
        }

        callCenterLive.getAgentLiveMap().put(user, agentLive);
        notifyListeners(callCenterLive);
    }

    //why is it not done through agentrepo! otherwise how would callcenter will have it removed from its personal map!
    public synchronized void deleteAgentLive(User user) {
        CallCenterLive callCenterLive = getCallCenterLive(user.getCompany());

        AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
        if (agentLive == null) {
            return;
        }

        agentLive.getStatusMap().keySet().forEach(queueName -> {
            queueLiveRepository.leaveQueue(queueName, agentLive.getPhoneLine());
        });

        callCenterLive.getAgentLiveMap().remove(user);
        //note: dont need to update the user because it will be deleted
    }

    // -----

    //this is needed for callcenterlive listeners when any change happens in queue through events
    void onQueueLiveEvent(QueueLive queueLive) {
        String companyId = queueLive.getCompanyId();
        CallCenterLive callCenterLive = getCallCenterLive(companyId);
        notifyListeners(callCenterLive); //how to notify?? if this method is removed
    }
}
