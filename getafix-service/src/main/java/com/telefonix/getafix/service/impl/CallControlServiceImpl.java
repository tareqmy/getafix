package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.ari.enums.HangupReason;
import com.telefonix.getafix.asterisk.service.ManagerService;
import com.telefonix.getafix.asterisk.service.ResourceService;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.service.AccountService;
import com.telefonix.getafix.service.CallControlService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-07-11.
 */
@Slf4j
@Service
@Transactional
public class CallControlServiceImpl implements CallControlService {

    @Value(value = "${getafix.asterisk.ari.applicationName}")
    private String stasisApplicationName;

    private final String applicationName = "Stasis";

    private final String localChannel = "Local";

    private ResourceService resourceService;

    private ManagerService managerService;

    private ChannelLiveRepository channelLiveRepository;

    private CallCenterLiveRepository callCenterLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    private GeneralService generalService;

    private AccountService accountService;

    @Autowired
    public CallControlServiceImpl(ResourceService resourceService,
                                  ChannelLiveRepository channelLiveRepository,
                                  CallCenterLiveRepository callCenterLiveRepository,
                                  AgentLiveRepository agentLiveRepository,
                                  ManagerService managerService,
                                  GeneralService generalService,
                                  AccountService accountService) {
        this.resourceService = resourceService;
        this.channelLiveRepository = channelLiveRepository;
        this.callCenterLiveRepository = callCenterLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.managerService = managerService;
        this.generalService = generalService;
        this.accountService = accountService;
    }

    @Override
    public void dial(String name, String number, String fromExtension) {
        log.debug("number to dial {}", number);
        String companyId = validateDial(number, fromExtension);
        String channel = localChannel + "/" + fromExtension + "@" + companyId;
        String callerId = TelephonyUtils.getCallerId(name, number);
        String data = stasisApplicationName + "," + CallControlProperties.click2dial_arg;
        Map<String, String> variables = new HashMap<>();
        variables.put(CallControlProperties.SERVER_GENERATED, CallControlProperties.yes);
        variables.put(CallControlProperties.GX_NUMBER, number);
        managerService.originate(channel, callerId, applicationName, data, variables);
    }

    @Override
    public void dial(String name, String number, String destinationCallerId, String fromExtension, String queueId) {
        log.debug("number to dial {}", number);
        String companyId = validateDial(number, fromExtension);
        String channel = localChannel + "/" + fromExtension + "@" + companyId;
        String callerId = TelephonyUtils.getCallerId(name, number);
        String data = stasisApplicationName + "," + CallControlProperties.click2dial_arg;
        Map<String, String> variables = new HashMap<>();
        variables.put(CallControlProperties.SERVER_GENERATED, CallControlProperties.yes);
        variables.put(CallControlProperties.GX_NUMBER, number);
        variables.put(CallControlProperties.GX_CALLERID, destinationCallerId);
        variables.put(CallControlProperties.GX_QUEUEID, queueId);
        managerService.originate(channel, callerId, applicationName, data, variables);
    }

    @Override
    public void transfer(String channelId, Long userId) {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        ChannelLive channelLive = verifyAccess(channelId, currentLoggedInUser, true);
        User user = generalService.getUser(userId);
        if (verifyUserAction(currentLoggedInUser, user)) {
            blindTransfer(channelLive, user.getJoinedPhoneLine().getExtension().getBaseExtension());
        } else {
            throw new BusinessRuleViolationException("Invalid agent id!");
        }
    }

    private boolean verifyUserAction(User currentLoggedInUser, User user) {
        if (!user.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid user id");
        }
        return agentLiveRepository.isLiveAgent(user);
    }

    @Override
    public void escalate(String channelId, Long userId) {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        ChannelLive channelLive = verifyAccess(channelId, currentLoggedInUser, true);
        User user = generalService.getUser(userId);
        if (user.getUserRole().getRole().equals(Role.ROLE_USER)) {
            throw new BusinessRuleViolationException("Invalid supervisor id");
        }
        if (verifyUserAction(currentLoggedInUser, user)) {
            blindTransfer(channelLive, user.getJoinedPhoneLine().getExtension().getBaseExtension());
        } else {
            throw new BusinessRuleViolationException("Invalid supervisor id");
        }
    }

    @Override
    public void moveToQueue(String channelId, Long queueId) {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        ChannelLive channelLive = verifyAccess(channelId, currentLoggedInUser, true);
        Queue queue = generalService.getQueue(queueId);
        if (!queue.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid queue id");
        }
        blindTransfer(channelLive, queue.getExtension().getBaseExtension());
    }

    private void blindTransfer(ChannelLive channelLive, String extension) {
//        cant be done because it needs the channel to be in stasis application
//        resourceService.continueChannelInDialplan(channelId, channelLive.getContext(), extension, 1);
        managerService.blindTransfer(channelLive.getChannel(), channelLive.getContext(), extension);
    }

    @Override
    public void hangup(String channelId) {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        verifyAccess(channelId, currentLoggedInUser, false);
        resourceService.hangupChannel(channelId, HangupReason.normal.toString());
    }

    private ChannelLive verifyAccess(String channelId, User currentLoggedInUser, boolean queueOnlyAction) {
        //1. does this channel exist?
        ChannelLive channelLive = channelLiveRepository.getChannelLive(channelId);
        if (channelLive == null) {
            throw new BusinessRuleViolationException("Invalid channel id");
        }

        //2. is this user a joined agent?
        if (!agentLiveRepository.isLiveAgent(currentLoggedInUser)) {
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }

        //3. is this channel assigned to this user?
        channelLive = agentLiveRepository.getAgentChannel(currentLoggedInUser, channelId);
        if (channelLive == null) {
            throw new BusinessRuleViolationException("Invalid channel id");
        }

        //4. is this action a queue only action where the channel is personal??
        if (queueOnlyAction && channelLive.getCallType().equals(Constants.personalCallType)) {
            throw new BusinessRuleViolationException("Invalid action for this personal call");
        }
        return channelLive;
    }

    private String validateDial(String number, String fromExtension) {
        if (StringUtils.isEmpty(number) || !number.matches("^(\\d{2}|\\+?\\d{1,15})$")) {
            throw new BusinessRuleViolationException("Allowed number \\d{2} or \\+?\\d{1,15}");
        }
        if (StringUtils.isEmpty(fromExtension) || !fromExtension.matches("^(\\d{2})$")) {
            throw new BusinessRuleViolationException("Allowed extension XX.");
        }

        User user = accountService.findCurrentLoggedInUser();
        if (user == null) {
            throw new BusinessRuleViolationException("User must be signed-in to originate call.");
        }
        if (CollectionUtils.isEmpty(user.getExtensions())) {
            throw new BusinessRuleViolationException("You have no extensions!");
        }
        if (user.getExtensions().stream().map(Extension::getBaseExtension)
            .noneMatch(s -> s.equals(fromExtension))) {
            throw new BusinessRuleViolationException("You dont own this extension!");
        }
        return user.getCompany().getCompanyId();
    }
}
