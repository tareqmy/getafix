package com.telefonix.getafix.service;

import java.util.List;
import java.util.Map;

/**
 * Created by tareqmy on 20/12/18.
 */
public interface UtilityService {

    List<String> getAllZoneIds();

    Map<String, String> getAllZonesMap();

    Map<String, String> getAllRolesMap();
}
