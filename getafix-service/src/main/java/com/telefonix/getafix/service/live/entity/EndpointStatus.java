package com.telefonix.getafix.service.live.entity;

public enum EndpointStatus {
    unknown,
    offline,
    online
}
