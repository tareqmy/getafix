package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.service.UtilityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tareqmy on 20/12/18.
 */
@Slf4j
@Service
@Transactional
public class UtilityServiceImpl implements UtilityService {

    @Value("#{'${getafix.timeZones}'.split(',')}")
    private List<String> timeZones;

    @Autowired
    public UtilityServiceImpl() {
    }

    @Override
    public List<String> getAllZoneIds() {
        return timeZones;
    }

    @Override
    public Map<String, String> getAllZonesMap() {
        Instant now = Instant.now();
        Map<String, String> map = new HashMap<>();
        for (String id : timeZones) {
            ZoneId zoneId = ZoneId.of(id);
            ZoneOffset offset = zoneId.getRules().getOffset(now);
            map.put(id, "(GMT" + offset.toString() + ") " + id);
        }
        return map;
    }

    @Override
    public Map<String, String> getAllRolesMap() {
        Map<String, String> map = new HashMap<>();
        map.put("ROLE_SYSTEM_ADMIN", "System Admin");
        map.put("ROLE_GETAFIX_ADMIN", "Getafix Admin");
        map.put("ROLE_ADMIN", "Admin");
        map.put("ROLE_USER", "User");
        return map;
    }
}
