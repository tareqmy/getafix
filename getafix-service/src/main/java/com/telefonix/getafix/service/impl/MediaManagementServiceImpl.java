package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.service.FileService;
import com.telefonix.getafix.service.MediaManagementService;
import com.telefonix.getafix.service.ObjectStorageService;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by tareqmy on 2019-05-22.
 */
@Slf4j
@Service
@Transactional
public class MediaManagementServiceImpl implements MediaManagementService {

    private ObjectStorageService objectStorageService;

    private FileService fileService;

    private FFmpegExecutor executor;

    private String temp;

    public MediaManagementServiceImpl(ObjectStorageService objectStorageService, FileService fileService,
                                      @Value(value = "${getafix.mediaConverter.temp}") String temp,
                                      @Value(value = "${getafix.mediaConverter.ffmpeg}") String ffmpeg,
                                      @Value(value = "${getafix.mediaConverter.ffprobe}") String ffprobe) {
        this.objectStorageService = objectStorageService;
        this.fileService = fileService;
        this.temp = temp;
        try {
            this.executor = new FFmpegExecutor(new FFmpeg(ffmpeg), new FFprobe(ffprobe));
        } catch (IOException e) {
            log.error("FFmpeg init failed");
        }
    }

    @Override
    public void upload(String companyId, String name, MultipartFile file) {
        Path input = Paths.get(temp, file.getOriginalFilename());
        Path output = Paths.get(temp, name);
        try {
            store(file, input);
            convert(input, output);

            objectStorageService.upload(companyId, temp, name);
            fileService.upload(companyId, name, output.toFile());
        } catch (Exception e) {
            throw new GenericGetafixException("Failed convert media");
        } finally {
            cleanup(input, output);
        }
    }

    @Override
    public void uploadMoh(String companyId, String mohId, String name, MultipartFile file) {
        Path input = Paths.get(temp, file.getOriginalFilename());
        Path output = Paths.get(temp, name);
        try {
            store(file, input);
            convert(input, output);

            objectStorageService.upload(companyId, temp, name);
            fileService.upload(companyId + "/moh/" + mohId, name, output.toFile());
        } catch (Exception e) {
            throw new GenericGetafixException("Failed convert media");
        } finally {
            cleanup(input, output);
        }
    }

    @Override
    public void delete(String companyId, String name) {
        try {
            objectStorageService.delete(companyId, name);
            fileService.deleteFile(companyId, name);
        } catch (Exception e) {
            throw new GenericGetafixException("Failed delete media");
        }
    }

    @Override
    public void deleteMoh(String companyId, String mohId, String name) {
        try {
            objectStorageService.delete(companyId, name);
            fileService.deleteFile(companyId + "/moh/" + mohId, name);
        } catch (Exception e) {
            throw new GenericGetafixException("Failed delete media");
        }
    }

    private void store(MultipartFile file, Path name) throws IOException {
        file.transferTo(name.toFile());
    }

    private void convert(Path input, Path output) {
        FFmpegBuilder builder = new FFmpegBuilder();
        builder.setInput(input.toString())
            .addOutput(output.toString())
            .setAudioChannels(FFmpeg.AUDIO_MONO)
            .setAudioSampleRate(FFmpeg.AUDIO_SAMPLE_8000)
            .setAudioSampleFormat(FFmpeg.AUDIO_FORMAT_S16)
            .done();
        executor.createJob(builder).run();
    }

    private void cleanup(Path input, Path output) {
        try {
            Files.delete(input);
            Files.delete(output);
        } catch (Exception e) {
            log.warn("Unable to cleanup input/output");
        }
    }
}
