package com.telefonix.getafix.service.live;

import com.telefonix.getafix.service.live.entity.VoiceMailLive;

/**
 * Created by tareqmy on 2019-08-05.
 */
@FunctionalInterface
public interface VoiceMailLiveEventListener {

    void onVoiceMailLiveEvent(VoiceMailLive voiceMailLive);
}
