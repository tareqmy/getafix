package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Variable;
import ch.loway.oss.ari4java.tools.RestException;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.ari.enums.HangupReason;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-08-22.
 */
@Slf4j
abstract class AbstractCallControlServiceImpl {

    @Value(value = "${getafix.asterisk.dialplan.context.main}")
    public String getafix_context;

    @Value(value = "${getafix.asterisk.sip.technology}")
    public String TECHNOLOGY;

    protected AsteriskResourceService asteriskResourceService;

    AbstractCallControlServiceImpl(AsteriskResourceService asteriskResourceService) {
        this.asteriskResourceService = asteriskResourceService;
    }

    CallEventListener hangupChannel(Channel channel, String accountCode, String context) {
        try {
            asteriskResourceService.hangupChannel(channel.getId(), HangupReason.no_answer.toString());
        } catch (RestException e) {
            log.warn("Failed to hangup channel {}", channel.getId());
            return new TerminalCall(accountCode, CallControlProperties.invalid_exten, context, null, null, asteriskResourceService);
        }
        return null;
    }

    CallEventListener busyTreatCall(String accountCode) {
        return new TerminalCall(accountCode, CallControlProperties.busy_treat_exten, getafix_context, null, null, asteriskResourceService);
    }

    CallEventListener playMessageAndHangup(String accountCode, String messageFileName) {
        Map<String, String> vMap = new HashMap<>();
        //set the call in the group
        vMap.put(CallControlProperties.GX_END_MESSAGE, messageFileName);
        return new TerminalCall(accountCode, CallControlProperties.end_with_message_exten, getafix_context, null, vMap, asteriskResourceService);
    }

    void setChannelVariable(Channel channel, String varName, String varValue) {
        try {
            log.debug("Set variable {}={}", varName, varValue);
            asteriskResourceService.setChannelVar(channel.getId(), varName, varValue);
        } catch (RestException e) {
            log.info("failed to set variable {}", e.getLocalizedMessage());
        }
    }

    String getChannelVariable(Channel channel, String varName) {
        String value = null;
        try {
            Variable channelVar = asteriskResourceService.getChannelVar(channel.getId(), varName);
            if (!StringUtils.isEmpty(channelVar.getValue())) {
                value = channelVar.getValue();
            }
            log.debug("channel variable {} = {}", varName, value);
        } catch (RestException e) {
            //exception means no variable
            log.info("{} {} {}", channel.getName(), varName, e.getLocalizedMessage());
        }
        return value;
    }
}
