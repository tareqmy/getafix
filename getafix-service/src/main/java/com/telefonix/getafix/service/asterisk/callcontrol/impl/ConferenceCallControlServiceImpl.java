package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.entity.Conference;
import com.telefonix.getafix.service.ConferenceService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.asterisk.callcontrol.ConferenceCallControlService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Slf4j
@Service
@Transactional
public class ConferenceCallControlServiceImpl extends AbstractCallControlServiceImpl implements ConferenceCallControlService {

    private ConferenceService conferenceService;

    @Autowired
    public ConferenceCallControlServiceImpl(AsteriskResourceService asteriskResourceService,
                                            ConferenceService conferenceService) {
        super(asteriskResourceService);
        this.conferenceService = conferenceService;
    }

    @Override
    public CallEventListener handleConferenceCall(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put(CallControlProperties.GX_CONTEXT, context);
        return new TerminalCall(accountCode, CallControlProperties.readpin_exten, getafix_context, null, vMap, asteriskResourceService);
    }

    @Override
    public CallEventListener onReadPin(Channel channel, String accountCode, String exten, String context) {
        String pin = getChannelVariable(channel, CallControlProperties.PIN);
        String gxContext = getChannelVariable(channel, CallControlProperties.GX_CONTEXT);

        //failed to get any pin
        if (StringUtils.isEmpty(pin)) {
            return onInvalidPin(pin, accountCode);
        } else {
            Optional<Conference> any = conferenceService.findByCompany(gxContext)
                .stream()
                .filter(conference -> conference.getLeaderCode().equals(pin) || conference.getParticipantCode().equals(pin))
                .findAny();
            if (any.isPresent()) {
                return joinConference(any.get(), accountCode, context);
            }
        }

        return onInvalidPin(pin, accountCode);
    }

    private CallEventListener joinConference(Conference conference, String accountCode, String context) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put(CallControlProperties.GX_CONF_ID, conference.getId().toString());
        vMap.put(CallControlProperties.GX_MOH_CLASS, "default");
        vMap.put(CallControlProperties.GX_ANNOUNCE, CallControlProperties.no);
        vMap.put(CallControlProperties.GX_ANNOUNCE_ONLY, CallControlProperties.yes);
        vMap.put(CallControlProperties.GX_ADMIN, CallControlProperties.no);
        vMap.put(CallControlProperties.GX_MARKED, CallControlProperties.no);
        vMap.put(CallControlProperties.GX_WAIT_MARKED, CallControlProperties.no);
        vMap.put(CallControlProperties.GX_END_MARKED, CallControlProperties.no);
        vMap.put(CallControlProperties.GX_START_MUTED, CallControlProperties.no);
        return new TerminalCall(accountCode, CallControlProperties.conference_exten, getafix_context, null, vMap, asteriskResourceService);
    }

    private CallEventListener onInvalidPin(String pin, String accountCode) {
        log.debug("{} is an invalid pin", pin);
        return new TerminalCall(accountCode, CallControlProperties.invalidpin_exten, getafix_context, null, null, asteriskResourceService);
    }
}
