package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * Created by tareqmy on 2019-08-11.
 */
public interface AgentLogService {

    void onJoinCallCenter(User user, PhoneLine phoneLine);

    void onLeaveCallCenter(User user, PhoneLine joinedPhoneLine);

    void onPauseAgent(User user, PhoneLine phoneLine);

    void onResumeAgent(User user, PhoneLine phoneLine);

    void onJoinQueue(User user, PhoneLine phoneLine, Queue queue);

    void onLeaveQueue(User user, PhoneLine phoneLine, Queue queue);

    List<AgentLog> getAgentLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);
}
