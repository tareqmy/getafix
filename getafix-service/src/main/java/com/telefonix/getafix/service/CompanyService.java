package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.*;
import com.telefonix.getfix.dto.CompanyDTO;
import com.telefonix.getfix.dto.CompanyReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 3/1/19.
 */
public interface CompanyService {

    Company createCompany(CompanyDTO companyDTO);

    Company updateCompany(CompanyDTO companyDTO);

    List<Company> findAll();

    Page<Company> findAll(Pageable pageable);

    Optional<Company> findOne(String companyId);

    Optional<Company> findOne(Long id);

    Company getCompany(Long id);

    void delete(Long id);

    List<User> getUsers(Long id);

    List<Extension> getExtensions(Long id, boolean unAssigned);

    List<Phone> getPhones(Long id);

    List<PhoneNumber> getPhoneNumbers(Long id);

    List<Queue> getQueues(Long id);

    List<AutoAttendant> getAutoAttendants(Long id);

    List<Conference> getConferences(Long id);

    void rebuildAllDialplan();

    CompanyReportDTO getCompanyReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId);
}
