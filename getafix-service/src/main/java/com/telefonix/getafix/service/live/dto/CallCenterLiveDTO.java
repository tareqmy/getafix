package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.service.live.entity.CallCenterLive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-08-05.
 */
@Getter
@Setter
@ToString
public class CallCenterLiveDTO extends LiveDTO {

    private int queueCount;

    private List<QueueLiveDTO> queues;

    public CallCenterLiveDTO(CallCenterLive callCenterLive, ZoneId zoneId) {
        super(zoneId);
        List<QueueLiveDTO> queues = callCenterLive.getQueueLiveMap().values().stream()
            .map(queueLive -> new QueueLiveDTO(queueLive, zoneId, false))
            .collect(Collectors.toList());
        setQueueCount(queues.size());
        setQueues(queues);
    }
}
