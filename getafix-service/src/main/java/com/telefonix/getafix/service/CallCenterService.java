package com.telefonix.getafix.service;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;

import java.util.Map;

/**
 * Created by tareqmy on 2019-07-14.
 */
public interface CallCenterService {

    String getAgentSipUserName();

    void join(Long phoneLineId);

    void leave();

    void pause();

    void resume();

    Map<Long, String> getQueuesMap();

    Map<Long, String> getAgentsMap();

    Map<Long, String> getSupervisorsMap();

    void joinQueue(Long id);

    void leaveQueue(Long id);

    void callQueue(Long id, String destinationNumber);

    void kickOutAgentQueue(Long id, Long phoneLineId);

    AXVoiceMessage getVoiceMailMessageInfo(Long id, String msgId);

    void callbackVoiceMailMessage(Long id, String msgId);

    void deleteVoiceMailMessage(Long id, String msgId);
}
