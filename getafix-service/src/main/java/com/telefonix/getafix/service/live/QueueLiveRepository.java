package com.telefonix.getafix.service.live;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ManagerService;
import com.telefonix.getafix.asterisk.service.live.*;
import com.telefonix.getafix.asterisk.service.live.entity.*;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.repository.QueueRepository;
import com.telefonix.getafix.service.QueueChannelLogService;
import com.telefonix.getafix.service.live.entity.*;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-06-24.
 * NOTE to self: can this behemoth be subdued?
 */
@Slf4j
@Component
public class QueueLiveRepository implements AXQueueLiveEventListener,
    AXQueueMemberLiveEventListener,
    ChannelLiveEventListener,
    VoiceMailLiveEventListener {

    @Value(value = "${getafix.asterisk.sip.technology}")
    private String technology;

    private Map<String, QueueLive> queueLiveMap;

    private Map<String, OutboundQueueCall> outboundQueueCallMap;

    private AXQueueLiveRepository axQueueLiveRepository;

    private VoiceMailLiveRepository voiceMailLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    private ChannelLiveRepository channelLiveRepository;

    private QueueRepository queueRepository;

    private QueueChannelLogService queueChannelLogService;

    private ManagerService managerService;

    private ScheduledExecutorService scheduleExecutor;

    private List<QueueLiveEventListener> queueLiveEventListeners;

    @Autowired
    public QueueLiveRepository(AXQueueLiveRepository axQueueLiveRepository,
                               VoiceMailLiveRepository voiceMailLiveRepository,
                               AgentLiveRepository agentLiveRepository,
                               ChannelLiveRepository channelLiveRepository,
                               QueueRepository queueRepository,
                               QueueChannelLogService queueChannelLogService,
                               ManagerService managerService,
                               ScheduledExecutorService scheduleExecutor) {
        axQueueLiveRepository.addAXQueueLiveEventListener(this);
        axQueueLiveRepository.addAXQueueMemberLiveEventListener(this);
        voiceMailLiveRepository.addVoiceMailLiveEventListener(this);
        channelLiveRepository.addChannelLiveEventListener(this);

        this.axQueueLiveRepository = axQueueLiveRepository;
        this.voiceMailLiveRepository = voiceMailLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.channelLiveRepository = channelLiveRepository;
        this.queueRepository = queueRepository;
        this.queueChannelLogService = queueChannelLogService;
        this.managerService = managerService;
        this.scheduleExecutor = scheduleExecutor;

        this.queueLiveMap = new ConcurrentHashMap<>();
        this.outboundQueueCallMap = new ConcurrentHashMap<>();
        this.queueLiveEventListeners = new ArrayList<>();
    }

    public void addQueueLiveEventListener(QueueLiveEventListener queueLiveEventListener) {
        queueLiveEventListeners.add(queueLiveEventListener);
    }

    private void notifyListeners(QueueLive queueLive) {
        if (queueLive == null) {
            log.info("Request for null live notify!");
            return;
        }
        queueLiveEventListeners.forEach(queueLiveEventListener -> {
            queueLiveEventListener.onEvent(queueLive);
        });
    }

    public Collection<QueueLive> getQueueLiveMapValues() {
        return queueLiveMap.values();
    }

    // ----- live map manipulators -----

    synchronized QueueLive initQueueLive(Queue queue) {
        if (queue == null) {
            throw new IllegalArgumentException("Whats that?");
        }
        return createQueueLive(queue);
    }

    synchronized QueueLive updateQueueLive(Queue queue) {
        String uniqueQueueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        QueueLive queueLive = queueLiveMap.get(uniqueQueueName);
        if (queueLive == null) {
            log.error("Updating queue live didnt find queue live for {}!!!!", queue);
            queueLive = createQueueLive(queue);
        } else {
            queueLive.setQueue(queue);
        }
        return queueLive;
    }

    private QueueLive createQueueLive(Queue queue) {
        String companyId = queue.getCompany().getCompanyId();
        String uniqueQueueName = TelephonyUtils.getUniqueQueueId(companyId, queue.getQueueId());
        QueueLive queueLive = queueLiveMap.get(uniqueQueueName);
        if (queueLive == null) {
            String mailBox = TelephonyUtils.getMailBox(queue.getExtension().getBaseExtension(), companyId);
            VoiceMailLive voiceMailLive = voiceMailLiveRepository.getVoiceMailLive(mailBox);
            voiceMailLive.setQueue(queue); //some voicemail live belongs to a queue extension.
            queueLive = new QueueLive(queue, voiceMailLive);
            log.trace("createQueueLive {}", queueLive);
            queueLiveMap.put(uniqueQueueName, queueLive);
        }
        return queueLive;
    }

    synchronized QueueLive removeQueueLive(Queue queue) {
        String uniqueQueueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        QueueLive queueLive = queueLiveMap.get(uniqueQueueName);
        if (queueLive != null) {
            if (queueLive.getChannelLiveMap().size() > 0) {
                throw new BusinessRuleViolationException("It is not safe to delete the queue. Active calls in queue!");
            }

            if (queueLive.getVoiceMailLive().getAxVoiceMailLive().getAxVoiceMessageMap().size() > 0) {
                throw new BusinessRuleViolationException("It is not safe to delete the queue. There are unhandled voicemails!");
            }

            //remove joined agents
            queueLive.getAgentLiveMap().values().forEach(agentLive -> {
                if (agentLive.isPaused()) {
                    resumeQueueMember(uniqueQueueName, agentLive.getPhoneLine());
                }
                leaveQueue(uniqueQueueName, agentLive.getPhoneLine());
            });
        }
        queueLiveMap.remove(uniqueQueueName);
        axQueueLiveRepository.removeAXQueueLive(uniqueQueueName);
        return queueLive;
    }

    // -----

    public synchronized QueueLive getQueueLive(String uniqueQueueName) {
        QueueLive queueLive = queueLiveMap.get(uniqueQueueName);
        if (queueLive == null) { //although it is created on entity creation, what if events come before added in map!!! is it a possibility?
            log.warn("Trying to find out whether this ever happens or not {}", uniqueQueueName);
            String companyId = TelephonyUtils.getCompanyIdFromQueueName(uniqueQueueName);
            String queueId = TelephonyUtils.getQueueIdFromQueueName(uniqueQueueName);
            Optional<Queue> queue = queueRepository.findByCompanyIdAndQueueId(companyId, queueId);
            if (queue.isPresent()) {
                queueLive = createQueueLive(queue.get());
            } else {
                log.warn("A queue live event when there is no such entity!"); //sometimes happen after queue deletion... residue from asterisk-java?
            }
        }
        return queueLive;
    }

    //----------------- Queue Live Event handlers ----------------

    @Async
    @Override
    public void onQueueSummary(AXQueueLive axQueueLive, QueueSummary queueSummary) {
        log.trace("onQueueSummary - {}", queueSummary);
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        queueLive.setQueueSummary(queueSummary);
        notifyListeners(queueLive);
    }

    @Async
    @Override
    public void onQueueParams(AXQueueLive axQueueLive, QueueParams queueParams) {
        log.trace("onQueueParams - {}", queueParams);
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        queueLive.setQueueParams(queueParams);
        notifyListeners(queueLive);
    }

    //----------------- Queue Live Caller Event handlers ----------------

    @Async
    @Override
    public void onQueueCallerJoin(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        log.trace("onQueueCallerJoin - {}", axQueueCallerLive);
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(axQueueCallerLive.getUniqueId());
        if (queueChannelLive == null) {
            //NOTE: generally this queueChannelLive is entered here
            //This is needed for the position
            ChannelLive channelLive = channelLiveRepository.getChannelLive(axQueueCallerLive.getUniqueId());
            queueChannelLive = new QueueChannelLive(queueLive.getCompanyId(), TelephonyUtils.getQueueIdFromQueueName(queueLive.getQueueId()), channelLive, false);
            queueLive.getChannelLiveMap().put(axQueueCallerLive.getUniqueId(), queueChannelLive);
        }
        queueChannelLive.setPosition(axQueueCallerLive.getPosition());
        notifyListeners(queueLive);
        queueChannelLogService.onQueueChannelStartQueue(queueChannelLive);
    }

    //caller leaves queued when answered by agent or hangup by caller
    @Async
    @Override
    public void onQueueCallerLeave(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        log.trace("onQueueCallerLeave - {}", axQueueCallerLive);
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(axQueueCallerLive.getUniqueId());
        if (queueChannelLive != null) {
            queueChannelLive.setPosition(axQueueCallerLive.getPosition());
            queueChannelLive.setLeft(true);

            log.debug("onQueueCallerLeave {}", queueLive);
            scheduleExecutor.schedule(() -> {

                //change queue position of other calls?
                queueLive.getChannelLiveMap().values().forEach(qCL -> {
                    if (queueChannelLive.getPosition() < qCL.getPosition()) {
                        qCL.setPosition(qCL.getPosition() - 1);
                    }
                });
                notifyListeners(queueLive);
                queueChannelLogService.onQueueChannelLeave(queueChannelLive);
            }, 1L, TimeUnit.SECONDS);
        }
    }

    @Async
    @Override
    public void onQueueCallerAbandoned(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        log.trace("onQueueCallerAbandoned - {}", axQueueCallerLive);
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(axQueueCallerLive.getUniqueId());
        if (queueChannelLive != null) {
            queueChannelLive.setPosition(axQueueCallerLive.getPosition());
            queueChannelLive.setAbandoned(true);
            log.debug("onQueueCallerAbandoned {}", queueLive);
            notifyListeners(queueLive);
            queueChannelLogService.onQueueChannelAbandoned(queueChannelLive);
        }
    }

    //----------------- Queue Member Live Event handlers ----------------

    @Async
    @Override
    public void onQueueMemberAdded(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        String sipUserName = TelephonyUtils.getSipUserName(axQueueMemberLive.getInterFace());
        if (StringUtils.isNotEmpty(sipUserName)) {
            AgentLive agentLive = agentLiveRepository.onQueueMemberAdded(sipUserName, queueLive,
                axQueueMemberLive.getPaused(), axQueueMemberLive.getStatus());
            if (agentLive != null) {
                queueLive.getAgentLiveMap().put(sipUserName, agentLive);
                notifyListeners(queueLive);
            }
        }
    }

    @Async
    @Override
    public void onQueueMemberRemoved(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        if (axQueueMemberLive == null) {
            //should not happen again. this is handled
            log.error("Null member is removed from {}", axQueueLive);
            return;
        }
        String sipUserName = TelephonyUtils.getSipUserName(axQueueMemberLive.getInterFace());
        if (StringUtils.isNotEmpty(sipUserName)) {
            AgentLive agentLive = agentLiveRepository.onQueueMemberRemoved(sipUserName, queueLive, axQueueMemberLive.getStatus());
            if (agentLive != null) {
                queueLive.getAgentLiveMap().remove(sipUserName);
                notifyListeners(queueLive);
            }
        }
    }

    @Async
    @Override
    public void onQueueMemberRemoved(AXQueueLive axQueueLive, String interFace) {
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        String sipUserName = TelephonyUtils.getSipUserName(interFace);
        if (StringUtils.isNotEmpty(sipUserName)) {
            AgentLive agentLive = agentLiveRepository.onQueueMemberRemoved(sipUserName, queueLive);
            if (agentLive != null) {
                queueLive.getAgentLiveMap().remove(sipUserName);
                notifyListeners(queueLive);
            }
        }
    }

    @Async
    @Override
    public void onQueueMemberPause(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        String sipUserName = TelephonyUtils.getSipUserName(axQueueMemberLive.getInterFace());
        if (StringUtils.isNotEmpty(sipUserName)) {
            AgentLive agentLive = agentLiveRepository.onQueueMemberPause(sipUserName, queueLive.getQueueId(),
                axQueueMemberLive.getPaused(), axQueueMemberLive.getStatus());
            if (agentLive != null) {
                notifyListeners(queueLive);
            }
        }
    }

    @Async
    @Override
    public void onQueueMemberStatus(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        QueueLive queueLive = getQueueLive(axQueueLive.getAxQueue().getName());
        String sipUserName = TelephonyUtils.getSipUserName(axQueueMemberLive.getInterFace());
        if (StringUtils.isNotEmpty(sipUserName)) {
            AgentLive agentLive = agentLiveRepository.onQueueMemberStatus(sipUserName, axQueueMemberLive.getStatus());
            if (agentLive != null) {
                notifyListeners(queueLive);
            }
        }
    }

    //----------------- Queue Channel Live Event handlers ----------------

    public void anticipateOutboundQueueCall(String companyId, String queueId, Channel channel) {
        OutboundQueueCall outboundQueueCall = new OutboundQueueCall(companyId, queueId, channel);
        outboundQueueCallMap.put(channel.getId(), outboundQueueCall);
        scheduleExecutor.schedule(() -> {
            OutboundQueueCall failedOutboundCall = outboundQueueCallMap.remove(channel.getId());
            if (failedOutboundCall != null) {
                log.error("Outbound queue call didnt come here! {}", failedOutboundCall);
            }
        }, 3L, TimeUnit.SECONDS);
    }

    @Async
    @Override
    public void onChannelStart(ChannelLive channelLive) {
        OutboundQueueCall outboundQueueCall = outboundQueueCallMap.remove(channelLive.getLinkedID());
        if (outboundQueueCall != null) {
            log.debug("onChannelStart {}/{}", outboundQueueCall, channelLive);
            outboundQueueCallMap.put(channelLive.getUniqueID(), outboundQueueCall);
            String uniqueQueueId = TelephonyUtils.getUniqueQueueId(outboundQueueCall.getCompanyId(), outboundQueueCall.getQueueId());
            QueueLive queueLive = getQueueLive(uniqueQueueId);
            QueueChannelLive queueChannelLive = new QueueChannelLive(queueLive.getCompanyId(), TelephonyUtils.getQueueIdFromQueueName(queueLive.getQueueId()), channelLive, true);
            queueLive.getChannelLiveMap().put(channelLive.getUniqueID(), queueChannelLive);
            notifyListeners(queueLive);
            queueChannelLogService.onQueueChannelStartQueue(queueChannelLive);
        }
    }

    @Async
    @Override
    public void onChannelAnswered(ChannelLive channelLive) {
        OutboundQueueCall outboundQueueCall = outboundQueueCallMap.get(channelLive.getUniqueID());
        if (outboundQueueCall != null) {
            log.debug("onChannelAnswered {}/{}", outboundQueueCall, channelLive);
            String uniqueQueueId = TelephonyUtils.getUniqueQueueId(outboundQueueCall.getCompanyId(), outboundQueueCall.getQueueId());
            QueueLive queueLive = getQueueLive(uniqueQueueId);
            QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(channelLive.getUniqueID());
            if (queueChannelLive != null) {
                notifyListeners(queueLive);
                queueChannelLogService.onQueueChannelLeave(queueChannelLive);
            }
        }
    }

    @Async
    @Override
    public void onChannelAppStart(ChannelLive channelLive) {
        if (channelLive.getApp().equals(ChannelLiveRepository.queue)) {
            log.trace("onChannelAppStart - {}", channelLive);
            String queueName = channelLive.getAppData().split(",")[0];
            QueueLive queueLive = getQueueLive(queueName);
            QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(channelLive.getUniqueID());
            if (queueChannelLive == null) {
                //NOTE: generally this queueChannelLive is already entered by onQueueCallerJoin event.
                //NOTE: at this moment position is unknown --- what is the purpose of this then!!!!
                queueChannelLive = new QueueChannelLive(queueLive.getCompanyId(), TelephonyUtils.getQueueIdFromQueueName(queueLive.getQueueId()), channelLive, false);
                queueLive.getChannelLiveMap().put(channelLive.getUniqueID(), queueChannelLive);
            }
            notifyListeners(queueLive);
        } else if (channelLive.getApp().equals(ChannelLiveRepository.appQueue)) {
            if (channelLive.getLinkedChannel().getApp().equals(ChannelLiveRepository.queue)) {
                String queueName = channelLive.getLinkedChannel().getAppData().split(",")[0];
                log.debug("Queue call to agent started > {} > {} > {}", channelLive.getLinkedID(), queueName, channelLive.getChannel());
                QueueLive queueLive = getQueueLive(queueName);
                QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(channelLive.getLinkedID());
                if (queueChannelLive != null) {
                    queueChannelLive.getAgentChannelLives().add(channelLive);
                } else {
                    log.error("could not find queue channel live for this call {}", channelLive.getUniqueID());
                }
            }
        }
    }

    @Async
    @Override
    public void onChannelAppStop(ChannelLive channelLive) {
        if (channelLive.getApp().equals(ChannelLiveRepository.queue)) {
            log.trace("onChannelAppStop - {}", channelLive);
            String queueName = channelLive.getAppData().split(",")[0];
            QueueLive queueLive = getQueueLive(queueName);
            log.debug("onChannelAppStop {}", queueLive);
            QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(channelLive.getUniqueID());
            queueChannelLogService.onQueueChannelEndQueue(queueChannelLive);
            queueLive.getChannelLiveMap().remove(channelLive.getUniqueID());
            notifyListeners(queueLive);
        } else if (channelLive.getApp().equals(ChannelLiveRepository.appQueue)) {
            if (channelLive.getLinkedChannel().getApp().equals(ChannelLiveRepository.queue)) {
                String queueName = channelLive.getLinkedChannel().getAppData().split(",")[0];
                log.debug("Queue call to agent stopped > {} > {} > {} --- {}", channelLive.getLinkedID(), queueName, channelLive.getChannel(), channelLive.getAnswerInstant());
                QueueLive queueLive = getQueueLive(queueName);
                //on answered call it was found null :( because sometimes the above case happens before... need synchronization!?
                //fortunately, so far, we only need the queue channel live when this call was not handled
                QueueChannelLive queueChannelLive = queueLive.getChannelLiveMap().get(channelLive.getLinkedID());
                if (queueChannelLive != null && channelLive.getAnswerInstant() == null) {
                    //NOTE: no need to remove from queue channel agent channel live list
                    log.info("Number of agents didnt answer queue call {}", queueChannelLive.getAgentChannelLives().size());
                    //what if the caller abandoned the call during?
                    if (!queueChannelLive.isAbandoned()) { //other than the case of RINGCANCELED this agnets failed to answer the queue call.
                        queueChannelLive.getAgentChannelLives().forEach(cl -> {
                            log.info("No answer by > {}", cl.getChannel());
                        });
                    }
                }
            }
        }
    }

    @Async
    @Override
    public void onChannelEnd(ChannelLive channelLive) {
        OutboundQueueCall outboundQueueCall = outboundQueueCallMap.remove(channelLive.getUniqueID());
        if (outboundQueueCall != null) {
            log.debug("onChannelEnd {}/{}", outboundQueueCall, channelLive);
            String uniqueQueueId = TelephonyUtils.getUniqueQueueId(outboundQueueCall.getCompanyId(), outboundQueueCall.getQueueId());
            QueueLive queueLive = getQueueLive(uniqueQueueId);
            scheduleExecutor.schedule(() -> {
                queueLive.getChannelLiveMap().remove(channelLive.getUniqueID());
                notifyListeners(queueLive);
            }, 1L, TimeUnit.SECONDS);
        }
        queueChannelLogService.onQueueChannelEnd(channelLive);
    }

    //----------------- Queue VoiceMail Live Event handlers ----------------

    @Override
    public void onVoiceMailLiveEvent(VoiceMailLive voiceMailLive) {
        Queue queue = voiceMailLive.getQueue();
        log.trace("onVoiceMailLiveEvent {}", queue);
        if (queue != null) {
            String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
            QueueLive queueLive = getQueueLive(queueName);
            log.trace("onVoiceMailLiveEvent {}", queueLive);
            notifyListeners(queueLive);
        }
    }

    //----------------- Queue Live Actions methods ----------------

    public void joinQueue(Queue queue, PhoneLine phoneLine) {
        log.info("joinQueue {}/{}", queue.getQueueId(), phoneLine.getSipUserName());
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        joinQueue(queueName, phoneLine);
    }

    private void joinQueue(String queueName, PhoneLine phoneLine) {
        String memberChannel = technology + "/" + phoneLine.getSipUserName();
        managerService.queueAdd(queueName, memberChannel, phoneLine.getSipUserName());
    }

    public void leaveQueue(Queue queue, PhoneLine phoneLine) {
        log.info("leaveQueue {}/{}", queue.getQueueId(), phoneLine.getSipUserName());
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        leaveQueue(queueName, phoneLine);
    }

    void leaveQueue(String queueName, PhoneLine phoneLine) {
        String memberChannel = technology + "/" + phoneLine.getSipUserName();
        managerService.queueRemove(queueName, memberChannel);
    }

    void pauseQueueMember(String queueName, PhoneLine phoneLine) {
        String memberChannel = technology + "/" + phoneLine.getSipUserName();
        managerService.queuePause(queueName, memberChannel);
    }

    void resumeQueueMember(String queueName, PhoneLine phoneLine) {
        String memberChannel = technology + "/" + phoneLine.getSipUserName();
        managerService.queueUnpause(queueName, memberChannel);
    }

    //-------

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void garbageCollection() {
        for (Map.Entry<String, QueueLive> nextQ : queueLiveMap.entrySet()) {
            QueueLive queueLive = nextQ.getValue();
            Iterator<Map.Entry<String, QueueChannelLive>> iterator = queueLive.getChannelLiveMap().entrySet().iterator();
            while (iterator.hasNext()) {
                ChannelLive channelLive = iterator.next().getValue().getChannelLive();
                ChannelLive live = channelLiveRepository.getChannelLive(channelLive.getUniqueID());
                if (live == null) {
                    queueLive.getChannelLiveMap().remove(channelLive.getUniqueID());
                    outboundQueueCallMap.remove(channelLive.getUniqueID());
                    queueChannelLogService.onQueueChannelEnd(channelLive);
                    notifyListeners(queueLive);
                }
            }
        }
    }
}
