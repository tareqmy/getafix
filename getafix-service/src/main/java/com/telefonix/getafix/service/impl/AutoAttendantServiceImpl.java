package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.AutoAttendant;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.repository.AutoAttendantRepository;
import com.telefonix.getafix.service.AutoAttendantService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.MediaManagementService;
import com.telefonix.getafix.service.ObjectStorageService;
import com.telefonix.getfix.dto.AutoAttendantDTO;
import com.telefonix.getfix.dto.SoundFile;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-05.
 */
@Slf4j
@Service
@Transactional
public class AutoAttendantServiceImpl implements AutoAttendantService {

    private AutoAttendantRepository autoAttendantRepository;

    private GeneralService generalService;

    private MediaManagementService mediaManagementService;

    private ObjectStorageService objectStorageService;

    @Autowired
    public AutoAttendantServiceImpl(AutoAttendantRepository autoAttendantRepository,
                                    GeneralService generalService,
                                    MediaManagementService mediaManagementService,
                                    ObjectStorageService objectStorageService) {
        this.autoAttendantRepository = autoAttendantRepository;
        this.generalService = generalService;
        this.mediaManagementService = mediaManagementService;
        this.objectStorageService = objectStorageService;
    }

    private void verifyUniqueId(String uniqueId, Company company) {
        if (autoAttendantRepository.findByCompany(company)
            .stream()
            .map(AutoAttendant::getAutoAttendantId)
            .anyMatch(s -> s.equals(uniqueId))) {
            throw new BusinessRuleViolationException("This id is taken " + uniqueId);
        }
    }

    private void verifyCompany(Extension extension, Company company) {
        if (!extension.getCompany().equals(company)) {
            throw new RuntimeException("Assigned extension must belong to same company");
        }
    }

    private void verifyExtension(AutoAttendant autoAttendant, Extension extension) {
        if (extension.getAutoAttendant() != null && !extension.getAutoAttendant().equals(autoAttendant)) {
            throw new RuntimeException("This extension is already assigned to another auto attendant");
        }

        if (extension.getQueue() != null) {
            throw new RuntimeException("This extension is already assigned to a queue");
        }

        if (extension.getPhoneLine() != null) {
            throw new RuntimeException("This extension is already assigned to a phone line");
        }
    }

    @Override
    public AutoAttendant createAutoAttendant(AutoAttendantDTO autoAttendantDTO) {
        Company company = generalService.getCompany(autoAttendantDTO.getCompanyId());
        Extension extension = generalService.getExtension(autoAttendantDTO.getExtensionId());
        AutoAttendant autoAttendant = new AutoAttendant();
        verifyCompany(extension, company);
        verifyUniqueId(autoAttendantDTO.getAutoAttendantId(), company);
        verifyExtension(autoAttendant, extension);
        autoAttendant.setAutoAttendantId(autoAttendantDTO.getAutoAttendantId().toLowerCase());
        autoAttendant.setAutoAttendantName(autoAttendantDTO.getAutoAttendantName());
        autoAttendant.setCompany(company);
        autoAttendant.setExtension(extension);
        autoAttendant.setPromptUploaded(false);
        return processOptionsMap(autoAttendantDTO, autoAttendant);
    }

    private AutoAttendant processOptionsMap(AutoAttendantDTO autoAttendantDTO, AutoAttendant autoAttendant) {
        autoAttendant.setOptionsMap(autoAttendantDTO.getOptionsMap());
        if (MapUtils.isNotEmpty(autoAttendant.getOptionsMap())) {
            autoAttendant.getOptionsMap().entrySet()
                .removeIf(stringStringEntry -> StringUtils.isEmpty(stringStringEntry.getValue()));
            autoAttendant.getOptionsMap().forEach((key, value) -> {
                if (!key.matches("^[0-9]$")) {
                    throw new RuntimeException("Invalid destination pattern. Must be a 0-9!");
                }

                if (!value.matches("^\\*?[0-9]{2}$")) {
                    throw new RuntimeException("Invalid destination pattern. Must be a 2 digit extension!");
                }
            });
        }
        return autoAttendantRepository.save(autoAttendant);
    }

    @Override
    public AutoAttendant updateAutoAttendant(AutoAttendantDTO autoAttendantDTO) {
        AutoAttendant autoAttendant = getAutoAttendant(autoAttendantDTO.getId());
        Extension extension = generalService.getExtension(autoAttendantDTO.getExtensionId());
        verifyCompany(extension, autoAttendant.getCompany());
        verifyExtension(autoAttendant, extension);
        autoAttendant.setAutoAttendantName(autoAttendantDTO.getAutoAttendantName());
        autoAttendant.setExtension(extension);
        return processOptionsMap(autoAttendantDTO, autoAttendant);
    }

    @Override
    public List<AutoAttendant> findAll() {
        return autoAttendantRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<AutoAttendant> findOne(Long id) {
        return autoAttendantRepository.findById(id);
    }

    @Override
    public AutoAttendant getAutoAttendant(Long id) {
        return findOne(id).orElseThrow(
            () -> new RuntimeException("Invalid autoAttendant id " + id)
        );
    }

    @Override
    public List<AutoAttendant> getAutoAttendants(Company company) {
        return autoAttendantRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getAutoAttendants().forEach(this::delete);
    }

    private void delete(AutoAttendant autoAttendant) {
        if (autoAttendant.isPromptUploaded()) {
            mediaManagementService.delete(autoAttendant.getCompany().getCompanyId(), autoAttendant.getAutoAttendantId() + "_prompt" + ".wav");
        }
        autoAttendantRepository.delete(autoAttendant);
        log.debug("Deleted autoAttendant: {}", autoAttendant);
    }

    @Override
    public AutoAttendant createPrompt(Long id, MultipartFile file) {
        AutoAttendant autoAttendant = getAutoAttendant(id);
        mediaManagementService.upload(autoAttendant.getCompany().getCompanyId(),
            autoAttendant.getAutoAttendantId() + "_prompt" + ".wav", file);
        autoAttendant.setPromptUploaded(true);
        return autoAttendantRepository.save(autoAttendant);
    }

    @Override
    public SoundFile downloadPrompt(Long id) {
        AutoAttendant autoAttendant = getAutoAttendant(id);
        String key = autoAttendant.getAutoAttendantId() + "_prompt" + ".wav";
        return objectStorageService.getObject(autoAttendant.getCompany().getCompanyId(), key);
    }
}
