package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.service.AgentReportService;
import com.telefonix.getafix.service.CompanyReportService;
import com.telefonix.getafix.service.QueueReportService;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.dto.AgentReportDTO;
import com.telefonix.getfix.dto.CompanyReportDTO;
import com.telefonix.getfix.dto.QueueReportDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Created by tareqmy on 24/10/19.
 */
@Slf4j
@Service
public class CompanyReportServiceImpl implements CompanyReportService {

    private QueueReportService queueReportService;

    private AgentReportService agentReportService;

    private DecimalFormat decimalFormat = new DecimalFormat();

    @Autowired
    public CompanyReportServiceImpl(QueueReportService queueReportService, AgentReportService agentReportService) {
        this.queueReportService = queueReportService;
        this.agentReportService = agentReportService;
        decimalFormat.setMaximumFractionDigits(2);
    }

    @Override
    public CompanyReportDTO getCompanyReport(Company company, LocalDate from, LocalDate to, ZoneId zoneId) {
        StopWatch watch = new StopWatch();
        watch.start();
        CompanyReportDTO companyReport = new CompanyReportDTO();

        company.getQueues().forEach(queue -> {
            QueueReportDTO queueReport = queueReportService.getQueueReport(queue, from, to, zoneId);
            companyReport.getQueueReports().add(queueReport);
        });

        company.getUsers().forEach(user -> {
            AgentReportDTO agentReport = agentReportService.getAgentReport(user, from, to, zoneId);
            if (agentReport.getAgentSessions().size() > 0) {
                companyReport.getAgentReports().add(agentReport);
            }
        });

        companyReport.setTotalCalls(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getTotal).sum());
        companyReport.setTotalOverflowed(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getOverflowed).sum());
        companyReport.setTotalReceived(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getReceived).sum());
        companyReport.setTotalAnswered(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getAnswered).sum());
        companyReport.setTotalTransferred(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getTransferred).sum());
        companyReport.setTotalUnanswered(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getUnanswered).sum());
        companyReport.setTotalAbandoned(companyReport.getQueueReports().stream().mapToLong(QueueReportDTO::getAbandoned).sum());
        if (companyReport.getTotalCalls() > 0) {
            companyReport.setPercentAbandoned(formatDecimal(((float) companyReport.getTotalAbandoned() / companyReport.getTotalCalls()) * 100));
        }

        companyReport.setTotalAgents(companyReport.getAgentReports().size());
        companyReport.setTotalSessions(companyReport.getAgentReports().stream().mapToLong(AgentReportDTO::getSessions).sum());

        companyReport.setShortestSessionDuration(companyReport.getAgentReports()
            .stream().mapToLong(value -> value.getAgentSessions()
                .stream().mapToLong(session -> session.getSession().getDuration())
                .min().orElse(0))
            .min().orElse(0));

        companyReport.setLongestSessionDuration(companyReport.getAgentReports()
            .stream().mapToLong(value -> value.getAgentSessions()
                .stream().mapToLong(session -> session.getSession().getDuration())
                .max().orElse(0))
            .max().orElse(0));

        companyReport.setTotalSessionDuration(companyReport.getAgentReports().stream().mapToLong(AgentReportDTO::getTotalSessionDuration).sum());

        if (companyReport.getTotalSessions() > 0) {
            companyReport.setAverageSessionDuration(companyReport.getTotalSessionDuration() / companyReport.getTotalSessions());
        }

        applyStats(companyReport);
        watch.stop();
        log.debug("calculated company report in {} ms", watch.getTotalTimeMillis());
        return companyReport;
    }

    private void applyStats(CompanyReportDTO report) {
        report.setShortestSessionTime(formatDuration(report.getShortestSessionDuration()));
        report.setLongestSessionTime(formatDuration(report.getLongestSessionDuration()));
        report.setTotalSessionTime(formatDuration(report.getTotalSessionDuration()));
        report.setAverageSessionTime(formatDuration(report.getAverageSessionDuration()));
    }

    private String formatDuration(long durationSecs) {
        if (durationSecs < 0) {
            log.error("Format requested for negative {}", durationSecs);
            return Constants.zeroDurationFormat;
        }
        return DurationFormatUtils.formatDuration(durationSecs * 1000, Constants.durationFormat);
    }

    private String formatDecimal(float decimal) {
        if(decimal <= 0) {
            return "0";
        }
        return decimalFormat.format(decimal);
    }
}
