package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Created by tareqmy on 2019-07-03.
 */
@Getter
@Setter
@ToString
public class ChannelLiveDTO extends LiveDTO {

    private String uniqueID;

    private String channel;

    private String accountCode;

    private String callerIdNum;

    private String callerIdName;

    private String destination;

    private String linkedID;

    private Instant startInstant;

    private String startTime;

    private Instant answerInstant;

    private String answerTime;

    private boolean answered;

    private Instant endInstant;

    private String endTime;

    private Long duration;

    private String durationString;

    private String bridgeId;

    private String app;

    private String direction;

    private String callType;

    public ChannelLiveDTO(ChannelLive channelLive, ZoneId zoneId) {
        super(zoneId);
        setUniqueID(channelLive.getUniqueID());
        setChannel(channelLive.getChannel());
        setAccountCode(channelLive.getAccountCode());
        if (channelLive.isCaller()) {
            setCallerIdName(channelLive.getCallerIdName());
            setCallerIdNum(channelLive.getCallerIdNum());
        } else {
            setCallerIdName(channelLive.getLinkedChannel().getCallerIdName());
            setCallerIdNum(channelLive.getLinkedChannel().getCallerIdNum());
        }
        setDestination(channelLive.getCalledNumber());
        setLinkedID(channelLive.getLinkedID());
        setStartInstant(channelLive.getStartInstant());
        setStartTime(channelLive.getStartInstant()
            .atZone(zoneId)
            .format(DateTimeFormatter.ofPattern(Constants.durationFormat)));
        Duration between = MiscUtils.getDuration(channelLive.getStartInstant(), Instant.now());
        setDuration(between.toSeconds());
        setDurationString(DurationFormatUtils.formatDuration(between.toMillis(), Constants.durationFormat));
        setAnswerInstant(channelLive.getAnswerInstant());
        if (channelLive.getAnswerInstant() != null) {
            setAnswerTime(channelLive.getAnswerInstant()
                .atZone(zoneId)
                .format(DateTimeFormatter.ofPattern(Constants.durationFormat)));
            setAnswered(true);
        }
        setEndInstant(channelLive.getEndInstant());
        if (channelLive.getEndInstant() != null) {
            setEndTime(channelLive.getEndInstant()
                .atZone(zoneId)
                .format(DateTimeFormatter.ofPattern(Constants.durationFormat)));
        }
        setApp(channelLive.getApp());
        if (channelLive.isCaller() || channelLive.isCallInitiator()) {
            setDirection(Constants.outgoingCall);
        } else {
            setDirection(Constants.incomingCall);
        }
        setCallType(channelLive.getCallType());
    }
}
