package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.ami.AsteriskManagerCommandService;
import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.asterisk.service.AXVoiceMessageService;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.service.CallControlService;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.VoiceMailMessageService;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-07-28.
 */
@Slf4j
@Service
@Transactional
public class VoiceMailMessageServiceImpl implements VoiceMailMessageService {

    private final String GREETINGS_MSG_NUM = "-1";

    private AXVoiceMessageService axVoiceMessageService;

    private ExtensionService extensionService;

    private AsteriskManagerCommandService asteriskManagerCommandService;

    private CallControlService callControlService;

    @Autowired
    public VoiceMailMessageServiceImpl(AXVoiceMessageService axVoiceMessageService,
                                       ExtensionService extensionService,
                                       AsteriskManagerCommandService asteriskManagerCommandService,
                                       CallControlService callControlService) {
        this.axVoiceMessageService = axVoiceMessageService;
        this.extensionService = extensionService;
        this.asteriskManagerCommandService = asteriskManagerCommandService;
        this.callControlService = callControlService;
    }

    @Override
    public List<AXVoiceMessage> getAllVoiceMailMessageInfo(Long extensionId) {
        Extension extension = extensionService.getExtension(extensionId);
        return axVoiceMessageService.findAllVoiceMailMessageInfo(extension.getBaseExtension(), extension.getCompany().getCompanyId())
            .stream()
            .filter(axVoiceMessage -> !axVoiceMessage.getAxVoiceMessageId().getMsgNum().equals(GREETINGS_MSG_NUM))
            .collect(Collectors.toList());
    }

    @Override
    public List<AXVoiceMessage> getAllVoiceMailMessageInfo(Long extensionId, String dir) {
        Extension extension = extensionService.getExtension(extensionId);
        return axVoiceMessageService.getAllVoiceMailMessageInfo(extension.getBaseExtension(), extension.getCompany().getCompanyId(), dir)
            .stream()
            .filter(axVoiceMessage -> !axVoiceMessage.getAxVoiceMessageId().getMsgNum().equals(GREETINGS_MSG_NUM))
            .collect(Collectors.toList());
    }

    @Override
    public AXVoiceMessage getVoiceMailMessageInfo(Long extensionId, String msgId) {
        Extension extension = extensionService.getExtension(extensionId);
        return axVoiceMessageService.getVoiceMailMessageInfo(extension.getBaseExtension(), extension.getCompany().getCompanyId(), msgId).orElseThrow(
            () -> new GenericGetafixException("Invalid voicemail message id " + msgId)
        );
    }

    @Override
    public void deleteVoiceMailMessageInfo(Long extensionId, String msgId) {
        Extension extension = extensionService.getExtension(extensionId);
        axVoiceMessageService.deleteVoiceMailMessage(extension.getBaseExtension(), extension.getCompany().getCompanyId(), msgId);
        asteriskManagerCommandService.voicemailRefresh(extension.getCompany().getCompanyId(), extension.getBaseExtension());
    }

    @Override
    public void callbackVoiceMailMessage(Long extensionId, String msgId) {
        Extension extension = extensionService.getExtension(extensionId);
        AXVoiceMessage axVoiceMessage = axVoiceMessageService.getVoiceMailMessageInfo(extension.getBaseExtension(), extension.getCompany().getCompanyId(), msgId).orElseThrow(
            () -> new GenericGetafixException("Invalid voicemail message id " + msgId)
        );
        String callerName = TelephonyUtils.getCallerName(axVoiceMessage.getCallerId());
        String callerNumber = TelephonyUtils.getCallerNumber(axVoiceMessage.getCallerId());
        callControlService.dial(callerName, callerNumber, extension.getBaseExtension());
    }
}
