package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.EndpointLog;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.User;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * Created by tareqmy on 2019-08-20.
 */
public interface EndpointLogService {

    void onEndpointUpdate(String resource, String endpointStatus, String deviceStatus, PhoneLine phoneLine, User user);

    List<EndpointLog> getEndpointLogs(String sipUserName, LocalDate from, LocalDate to, ZoneId zoneId);

    List<EndpointLog> getEndpointLogs(List<String> sipUserNames, LocalDate from, LocalDate to, ZoneId zoneId);

    List<EndpointLog> getEndpointLogs(Long userId, LocalDate from, LocalDate to, ZoneId zoneId);
}
