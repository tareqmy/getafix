package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.AutoAttendant;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getfix.dto.AutoAttendantDTO;
import com.telefonix.getfix.dto.SoundFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-05.
 */
public interface AutoAttendantService {

    AutoAttendant createAutoAttendant(AutoAttendantDTO autoAttendantDTO);

    AutoAttendant updateAutoAttendant(AutoAttendantDTO autoAttendantDTO);

    List<AutoAttendant> findAll();

    Optional<AutoAttendant> findOne(Long id);

    AutoAttendant getAutoAttendant(Long id);

    List<AutoAttendant> getAutoAttendants(Company company);

    void delete(Long id);

    void delete(Company company);

    AutoAttendant createPrompt(Long id, MultipartFile file);

    SoundFile downloadPrompt(Long id);
}
