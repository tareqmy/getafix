package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.entity.enums.QueueLogEvent;
import com.telefonix.getafix.asterisk.repository.AXQueueLogRepository;
import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getafix.entity.EndpointLog;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.AgentEvent;
import com.telefonix.getafix.repository.AgentLogRepository;
import com.telefonix.getafix.repository.ChannelLogRepository;
import com.telefonix.getafix.repository.EndpointLogRepository;
import com.telefonix.getafix.service.AgentReportService;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.dto.AgentReportDTO;
import com.telefonix.getfix.dto.AgentSessionDTO;
import com.telefonix.getfix.dto.SessionDTO;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.text.DecimalFormat;
import java.time.*;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 29/8/19.
 */
@Slf4j
@Service
public class AgentReportServiceImpl implements AgentReportService {

    private AgentLogRepository agentLogRepository;

    private EndpointLogRepository endpointLogRepository;

    private ChannelLogRepository channelLogRepository;

    private AXQueueLogRepository axQueueLogRepository;

    private DecimalFormat decimalFormat = new DecimalFormat();

    @Autowired
    public AgentReportServiceImpl(AgentLogRepository agentLogRepository,
                                  EndpointLogRepository endpointLogRepository,
                                  ChannelLogRepository channelLogRepository,
                                  AXQueueLogRepository axQueueLogRepository) {
        this.agentLogRepository = agentLogRepository;
        this.endpointLogRepository = endpointLogRepository;
        this.channelLogRepository = channelLogRepository;
        this.axQueueLogRepository = axQueueLogRepository;
        decimalFormat.setMaximumFractionDigits(2);
    }

    @Override
    public AgentReportDTO getAgentReport(User user, LocalDate from, LocalDate to, ZoneId zoneId) {
        StopWatch watch = new StopWatch();
        watch.start();
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();

        List<ChannelLog> channelLogs = channelLogRepository.findByUserIdAndStartBetween(user.getId(), fromDate, toDate);
        List<EndpointLog> endpointLogs = endpointLogRepository.findByUserIdAndStartBetween(user.getId(), fromDate, toDate);
        List<AgentLog> agentLogs = agentLogRepository.findByUserIdAndEventInstantBetweenOrderByEventInstantAsc(user.getId(), fromDate, toDate);

        AgentReportDTO agentReport = initialize(user, fromDate, toDate, zoneId);

        processAgentLogs(agentReport, agentLogs, fromDate, zoneId);
        processEndpointLogs();
        processChannelLogs(agentReport, channelLogs);
        processQueueLogs(agentReport, zoneId);

        applyStats(agentReport);

        watch.stop();
        log.debug("calculated report in {} ms", watch.getTotalTimeMillis());
        return agentReport;
    }

    private AgentReportDTO initialize(User user, Instant startInstant, Instant endInstant, ZoneId zoneId) {
        AgentReportDTO report = new AgentReportDTO();
        report.setUserId(user.getId());
        report.setUserName(user.getFullName());

        report.setZoneId(zoneId);
        report.setFrom(LocalDateTime.ofInstant(startInstant, zoneId));
        report.setTo(LocalDateTime.ofInstant(endInstant, zoneId));

        report.setPeriod(Period.between(LocalDate.ofInstant(startInstant, zoneId), LocalDate.ofInstant(endInstant, zoneId)));
        report.setDuration(MiscUtils.getDuration(startInstant, endInstant));
        return report;
    }

    private void processQueueLogs(AgentReportDTO report, ZoneId zoneId) {
        report.getAgentSessions().forEach(agentSession -> {
            if (!StringUtils.isEmpty(agentSession.getSipUserName())) {
                List<AXQueueLog> queueLogs = axQueueLogRepository.findAllByAgentAndTimeBetweenOrderByTimeDesc(agentSession.getSipUserName(),
                    agentSession.getSession().getStart().atZone(zoneId).toInstant(),
                    agentSession.getSession().getEnd().atZone(zoneId).toInstant());

                agentSession.setAcdCallsFailed(queueLogs.stream()
                    .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.RINGNOANSWER)).count());

                agentSession.setAcdCallsAnswered(queueLogs.stream()
                    .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.CONNECT)).count());

                agentSession.setAcdCallsEndByAgent(queueLogs.stream()
                    .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.COMPLETEAGENT)).count());

                agentSession.setAcdCallsEndByCaller(queueLogs.stream()
                    .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.COMPLETECALLER)).count());

                agentSession.setAcdCallsTransferred(queueLogs.stream()
                    .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.BLINDTRANSFER)).count());
            }
        });
    }

    private void processEndpointLogs() {
    }

    private void processChannelLogs(AgentReportDTO report, List<ChannelLog> channelLogs) {
        List<ChannelLog> queueCalls = channelLogs.stream()
            .filter(channelLog -> !channelLog.getCallType().equals(Constants.personalCallType))
            .collect(Collectors.toList());

        List<ChannelLog> queueAnswered = queueCalls.stream()
            .filter(channelLog -> channelLog.getAnswer() != null)
            .collect(Collectors.toList());

        report.setTotalCalls(channelLogs.size());
        report.setTotalQueueCalls(queueCalls.size());
        report.setTotalQueueCallsInbound(queueCalls.stream()
            .filter(channelLog -> !channelLog.isCallInitiator()).count());
        report.setTotalQueueCallsOutbound(queueCalls.stream()
            .filter(ChannelLog::isCallInitiator).count());
        report.setTotalQueueCallsAnswered(queueAnswered.size());
        report.setTotalQueueCallsUnanswered(report.getTotalQueueCalls() - report.getTotalQueueCallsAnswered());

        report.setTotalCallDuration(channelLogs.stream()
            .mapToLong(ChannelLog::getDuration)
            .sum());

        report.setTotalQueueCallRingDuration(queueCalls.stream()
            .mapToLong(channelLog -> {
                if (channelLog.getAnswer() != null) {
                    return MiscUtils.getDuration(channelLog.getStart(), channelLog.getAnswer()).getSeconds();
                }
                return MiscUtils.getDuration(channelLog.getStart(), channelLog.getEnd()).getSeconds();
            })
            .sum());

        report.setTotalQueueCallTalkDuration(queueAnswered.stream()
            .filter(channelLog -> channelLog.getEnd() != null)
            .mapToLong(channelLog -> MiscUtils.getDuration(channelLog.getAnswer(), channelLog.getEnd()).getSeconds())
            .sum());
    }

    private void processAgentLogs(AgentReportDTO report, List<AgentLog> agentLogs, Instant startInstant, ZoneId zoneId) {
        AgentSessionDTO session = initializeAgentSession(report, startInstant, zoneId);

        for (AgentLog agentLog : agentLogs) {
            switch (agentLog.getAgentEvent()) {
                case JOIN_CALL_CENTER:
                    session.getSession().setStart(LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId));
                    session.setSipUserName(agentLog.getSipUserName());
                    report.getAgentSessions().add(session);
                    break;
                case LEAVE_CALL_CENTER:
                    endSession(session.getSession(), LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId));
                    session = new AgentSessionDTO();
                    break;
                case PAUSE_AGENT:
                    if (!session.getPauses().isEmpty()) {
                        SessionDTO lastPaused = session.getPauses().get(session.getPauses().size() - 1);
                        if (lastPaused.getEnd() == null) {
                            //to avoid a paused session that has no end because two consecutive pauseagent event received
                            break;
                        }
                    }
                    session.getPauses().add(startSession(LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId)));
                    break;
                case RESUME_AGENT:
                    //resume when no started pause then add from start
                    if (session.getPauses().isEmpty()) {
                        session.getPauses().add(startSession(LocalDateTime.ofInstant(startInstant, zoneId)));
                    }
                    endSession(session.getPauses().get(session.getPauses().size() - 1), LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId));
                    break;
                case JOIN_QUEUE:
                    if (!session.getJoined().isEmpty()) {
                        SessionDTO lastJoined = session.getJoined().get(session.getJoined().size() - 1);
                        if (lastJoined.getEnd() == null) {
                            //to avoid a joined session that has no end because two consecutive joinqueue event received
                            break;
                        }
                    }
                    session.getJoined().add(startSession(LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId)));
                    break;
                case LEAVE_QUEUE:
                    if (session.getJoined().isEmpty()) {
                        session.getJoined().add(startSession(LocalDateTime.ofInstant(startInstant, zoneId)));
                    }
                    endSession(session.getJoined().get(session.getJoined().size() - 1), LocalDateTime.ofInstant(agentLog.getEventInstant(), zoneId));
                    break;
                default:
                    break;
            }
        }

        closeSessions(session, zoneId);
    }

    private AgentSessionDTO initializeAgentSession(AgentReportDTO report, Instant startInstant, ZoneId zoneId) {
        AgentSessionDTO session = new AgentSessionDTO();
        AgentLog lastAgentLog = agentLogRepository.getTopByUserIdAndEventInstantBeforeOrderByEventInstantDesc(report.getUserId(), startInstant);
        if (isContinuedSession(lastAgentLog)) {
            session.getSession().setStart(LocalDateTime.ofInstant(startInstant, zoneId));
            session.setSipUserName(lastAgentLog.getSipUserName());
            report.getAgentSessions().add(session);
            if (isContinuedJoined(report.getUserId(), lastAgentLog.getEventInstant())) {
                session.getJoined().add(startSession(LocalDateTime.ofInstant(startInstant, zoneId)));
            }
        }
        return session;
    }

    private boolean isContinuedSession(AgentLog lastAgentLog) {
        return lastAgentLog != null
            && !lastAgentLog.getAgentEvent().equals(AgentEvent.LEAVE_CALL_CENTER);
    }

    //when a session is continued
    private boolean isContinuedJoined(Long userId, Instant lastEventInstant) {
        //find the last join callcenter before the lasteventinstant
        AgentLog joinCallCenterLog = agentLogRepository.getTopByUserIdAndAgentEventAndEventInstantBeforeOrderByEventInstantDesc(userId,
            AgentEvent.JOIN_CALL_CENTER, lastEventInstant);

        //get all the queue events in between
        List<AgentLog> agentQueueLogs = agentLogRepository.findByUserIdAndQueueNameNotNullAndEventInstantBetweenOrderByEventInstantDesc(userId,
            joinCallCenterLog.getEventInstant(), lastEventInstant);

        AtomicBoolean joined = new AtomicBoolean(false);
        if (agentQueueLogs.isEmpty()) {
            return joined.get();
        }

        //find each individual queues and check the last event for that queue is join then joined
        for (String queueName : agentQueueLogs.stream().map(AgentLog::getQueueName).collect(Collectors.toSet())) {
            agentQueueLogs.stream().filter(agentLog -> agentLog.getQueueName().equals(queueName)).findFirst()
                .ifPresent(agentLog -> joined.set(agentLog.getAgentEvent().equals(AgentEvent.JOIN_QUEUE)));
            if (joined.get()) {
                break;
            }
        }

        return joined.get();
    }

    private SessionDTO startSession(LocalDateTime start) {
        SessionDTO session = new SessionDTO();
        session.setStart(start);
        return session;
    }

    private void closeSessions(AgentSessionDTO session, ZoneId zoneId) {
        //end the session if not ended
        if (session.getSession().getStart() != null && session.getSession().getEnd() == null) {
            endSession(session.getSession(), LocalDateTime.ofInstant(Instant.now(), zoneId));
        }

        //end the last pause if not ended
        if (!session.getPauses().isEmpty()) {
            SessionDTO paused = session.getPauses().get(session.getPauses().size() - 1);
            if (paused.getEnd() == null) {
                endSession(paused, LocalDateTime.ofInstant(Instant.now(), zoneId));
            }
        }

        //end the last joined if not ended
        if (!session.getJoined().isEmpty()) {
            SessionDTO joined = session.getJoined().get(session.getJoined().size() - 1);
            if (joined.getEnd() == null) {
                endSession(joined, LocalDateTime.ofInstant(Instant.now(), zoneId));
            }
        }
    }

    private void endSession(SessionDTO sessionDTO, LocalDateTime end) {
        sessionDTO.setEnd(end);
        Duration between = Duration.between(sessionDTO.getStart(), sessionDTO.getEnd());
        sessionDTO.setDuration(between.getSeconds());
        sessionDTO.setTime(formatDuration(between.getSeconds()));
    }

    private void applyStats(AgentReportDTO report) {
        List<AgentSessionDTO> agentSessions = report.getAgentSessions();
        report.setSessions(agentSessions.size());

        agentSessions.forEach(session -> {
            session.setJoinedDuration(session.getJoined()
                .stream().mapToLong(SessionDTO::getDuration).sum());

            session.setUnJoinedDuration(session.getSession().getDuration() - session.getJoinedDuration());

            session.setPausedDuration(session.getPauses()
                .stream().mapToLong(SessionDTO::getDuration).sum());
        });

        List<SessionDTO> pauses = agentSessions.stream()
            .map(AgentSessionDTO::getPauses)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());

        List<SessionDTO> joined = agentSessions.stream()
            .map(AgentSessionDTO::getJoined)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());

        report.setJoined(joined.size());
        report.setPauses(pauses.size());

        report.setTotalSessionDuration(agentSessions.stream()
            .mapToLong(agentSessionDTO -> agentSessionDTO.getSession().getDuration())
            .sum());

        report.setTotalJoinedDuration(agentSessions.stream()
            .mapToLong(AgentSessionDTO::getJoinedDuration)
            .sum());

        report.setTotalUnJoinedDuration(agentSessions.stream()
            .mapToLong(AgentSessionDTO::getUnJoinedDuration)
            .sum());

        report.setTotalPausedDuration(agentSessions.stream()
            .mapToLong(AgentSessionDTO::getPausedDuration)
            .sum());

        //averages
        if (!agentSessions.isEmpty()) {
            report.setAverageSessionDuration((float) report.getTotalSessionDuration() / agentSessions.size());
        }

        if (!joined.isEmpty()) {
            report.setAverageJoinedDuration((float) report.getTotalJoinedDuration() / joined.size());
        }

        if (!pauses.isEmpty()) {
            report.setAveragePausedDuration((float) report.getTotalPausedDuration() / pauses.size());
        }

        if (report.getTotalQueueCallsAnswered() > 0) {
            report.setAverageHandleDuration((float) report.getTotalQueueCallTalkDuration() / report.getTotalQueueCallsAnswered());
        }

        //percentages
        if (report.getTotalSessionDuration() > 0) {
            report.setPercentJoined(formatDecimal(((float) report.getTotalJoinedDuration() / report.getTotalSessionDuration()) * 100));
        }

        if (report.getTotalJoinedDuration() > 0) {
            report.setPercentPaused(formatDecimal(((float) report.getTotalPausedDuration() / report.getTotalJoinedDuration()) * 100));
        }

        if (report.getDuration().getSeconds() > 0) {
            report.setPercentSession(formatDecimal(((float) report.getTotalSessionDuration() / report.getDuration().getSeconds()) * 100));
        }

        if (report.getSessions() > 0) {
            report.setPausesPerSession(formatDecimal(((float) report.getPauses() / report.getSessions())));
        }

        report.setTotalIdleDuration(report.getTotalJoinedDuration()
            - (report.getTotalQueueCallRingDuration()
            + report.getTotalQueueCallTalkDuration()
            + report.getTotalPausedDuration()));

        report.setTotalACDCallsAnswered(report.getAgentSessions().stream()
            .mapToLong(AgentSessionDTO::getAcdCallsAnswered).sum());

        report.setTotalACDCallsFailed(report.getAgentSessions().stream()
            .mapToLong(AgentSessionDTO::getAcdCallsFailed).sum());

        report.setTotalACDCallsEndByAgent(report.getAgentSessions().stream()
            .mapToLong(AgentSessionDTO::getAcdCallsEndByAgent).sum());

        report.setTotalACDCallsEndByCaller(report.getAgentSessions().stream()
            .mapToLong(AgentSessionDTO::getAcdCallsEndByCaller).sum());

        report.setTotalACDCallsTransferred(report.getAgentSessions().stream()
            .mapToLong(AgentSessionDTO::getAcdCallsTransferred).sum());

        //time format
        report.setTotalSessionTime(formatDuration(report.getTotalSessionDuration()));
        report.setTotalJoinedTime(formatDuration(report.getTotalJoinedDuration()));
        report.setTotalUnJoinedTime(formatDuration(report.getTotalUnJoinedDuration()));
        report.setTotalPausedTime(formatDuration(report.getTotalPausedDuration()));
        report.setTotalIdleTime(formatDuration(report.getTotalIdleDuration()));

        report.setAverageSessionTime(formatDuration((long) (report.getAverageSessionDuration())));
        report.setAverageJoinedTime(formatDuration((long) (report.getAverageJoinedDuration())));
        report.setAveragePausedTime(formatDuration((long) (report.getAveragePausedDuration())));
        report.setAverageHandleTime(formatDuration((long) (report.getAverageHandleDuration())));

        report.setTotalQueueCallRingTime(formatDuration(report.getTotalQueueCallRingDuration()));
        report.setTotalQueueCallTalkTime(formatDuration(report.getTotalQueueCallTalkDuration()));
        report.setTotalCallTime(formatDuration(report.getTotalCallDuration()));
    }

    private String formatDuration(long durationSecs) {
        if (durationSecs < 0) {
            log.error("Format requested for negative {}", durationSecs);
            return Constants.zeroDurationFormat;
        }
        return DurationFormatUtils.formatDuration(durationSecs * 1000, Constants.durationFormat);
    }

    private String formatDecimal(float decimal) {
        if(decimal <= 0) {
            return "0";
        }
        return decimalFormat.format(decimal);
    }
}
