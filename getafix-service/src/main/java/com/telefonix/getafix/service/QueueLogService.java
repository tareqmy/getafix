package com.telefonix.getafix.service;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-06-23.
 */
public interface QueueLogService {

    List<AXQueueLog> findAll();

    Optional<AXQueueLog> findOne(Long id);

    AXQueueLog getAXQueueLog(Long id);

    List<AXQueueLog> findAllByQueueName(String queueName, LocalDate from, LocalDate to, ZoneId zoneId);
}
