package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-07-10.
 */
@Getter
@Setter
@ToString
public class SystemLive extends Live {

    //not yet used
    private Map<String, PSEndpointLive> psEndpointLiveMap = new ConcurrentHashMap<>();

    private Map<String, ChannelLive> channelLiveMap = new ConcurrentHashMap<>();

    public SystemLive() {
        super(0L); //because system is not a database entity
    }
}
