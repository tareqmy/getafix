package com.telefonix.getafix.service.asterisk.impl;

import com.telefonix.getafix.asterisk.dto.AXVoiceMailDTO;
import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import com.telefonix.getafix.asterisk.entity.PSAor;
import com.telefonix.getafix.asterisk.entity.PSAuth;
import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import com.telefonix.getafix.asterisk.entity.enums.Strategy;
import com.telefonix.getafix.asterisk.service.*;
import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tareqmy on 2019-01-24.
 */
@Slf4j
@Service
@Transactional
public class EntityServiceImpl implements EntityService {

    @Value(value = "${getafix.obelix.path}")
    private String path;

    private PSAorService psAorService;

    private PSAuthService psAuthService;

    private PSEndpointService psEndpointService;

    private PSRegistrationService psRegistrationService;

    private PSEndpointIdentifyService psEndpointIdentifyService;

    private AXQueueService axQueueService;

    private AXVoiceMailService axVoiceMailService;

    @Autowired
    public EntityServiceImpl(PSAorService psAorService,
                             PSAuthService psAuthService,
                             PSEndpointService psEndpointService,
                             PSRegistrationService psRegistrationService,
                             PSEndpointIdentifyService psEndpointIdentifyService,
                             AXQueueService axQueueService,
                             AXVoiceMailService axVoiceMailService) {
        this.psAorService = psAorService;
        this.psAuthService = psAuthService;
        this.psEndpointService = psEndpointService;
        this.psRegistrationService = psRegistrationService;
        this.psEndpointIdentifyService = psEndpointIdentifyService;
        this.axQueueService = axQueueService;
        this.axVoiceMailService = axVoiceMailService;
    }

    private void createSipEntity(Extension extension, String sipEntityId, String password, String callerName, String callerNumber) {
        String companyId = extension.getCompany().getCompanyId();
        String mailBox = null;
        if (extension.getVoiceMail().isEnabled()) {
            mailBox = TelephonyUtils.getMailBox(extension.getBaseExtension(), companyId);
        }

        //1. create psaor
        PSAor psAor = psAorService.create(sipEntityId, mailBox, 1);

        //2. create psauth
        PSAuth psAuth = psAuthService.create(sipEntityId, password);

        //3. create psendpoint
        PSEndpoint psEndpoint = psEndpointService.create(sipEntityId, companyId, TelephonyUtils.getCallerId(callerName, callerNumber), true);
    }

    @Override
    public void createSipEntity(PhoneLine phoneLine) {
        String sipEntityId = getSipEntityId(phoneLine);
        Extension extension = phoneLine.getExtension();
        createSipEntity(extension, sipEntityId, phoneLine.getSipPassword(), phoneLine.getCallerName(), extension.getBaseExtension());
    }

    @Override
    public void createSipEntity(Queue queue) {
        //1. create queue
        String uniqueQueueId = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        String context = null;
        if (queue.isZeroEscapeEnabled()) {
            context = Constants.zeroEscapeContext;
        }
        axQueueService.create(uniqueQueueId, Strategy.valueOf(queue.getStrategy().name()),
            queue.getWeight(), queue.getServiceLevel(), queue.getMaxLength(), context);
    }

    @Override
    public void createSipEntity(Provider provider) {

        psEndpointIdentifyService.create(provider.getProviderId(), provider.getHost());

        psEndpointService.createProviderEndpoint(provider.getProviderId());

        //this will be used when phone number uses number@provider to dial
        psAorService.create(provider.getProviderId(), "sip:" + provider.getHost() + ":5060");
    }

    @Override
    public void createSipEntity(PhoneNumber phoneNumber) {

        psRegistrationService.create(phoneNumber.getNumber(), phoneNumber.getProvider().getHost());

        psEndpointService.createPhoneNumberEndpoint(phoneNumber.getNumber(), phoneNumber.getProvider().getProviderId(),
            TelephonyUtils.getCallerId(phoneNumber.getCompany().getCompanyName(), phoneNumber.getNumber()));

//        id;auth_type;username;password
//        support_auth;userpass;09614500737;8dw307vs5x8y95pg
        psAuthService.create(phoneNumber.getNumber(), phoneNumber.getPassword());
    }

    @Override
    public void createSipEntity(VoiceMail voiceMail) {

        Extension extension = voiceMail.getExtension();
        String companyId = extension.getCompany().getCompanyId();

        AXVoiceMail axVoiceMail = axVoiceMailService.findOne(extension.getCompany().getCompanyId(), extension.getBaseExtension()).orElseGet(() -> {
            AXVoiceMail ax = new AXVoiceMail();
            ax.setPassword(RandomStringUtils.randomNumeric(10));
            ax.setContext(companyId);
            ax.setMailbox(extension.getBaseExtension());
            return ax;
        });
        AXVoiceMailDTO axVoiceMailDTO = new AXVoiceMailDTO();
        axVoiceMailDTO.setUniqueid(axVoiceMail.getUniqueid());
        axVoiceMailDTO.setContext(companyId);
        axVoiceMailDTO.setMailbox(extension.getBaseExtension());
        axVoiceMailDTO.setPassword(axVoiceMail.getPassword());
        axVoiceMailDTO.setEmail(voiceMail.getEmail());
        axVoiceMailDTO.setFullName(voiceMail.getFullName());
        axVoiceMailService.create(axVoiceMailDTO);

        //update ps aors where the extension belongs to a phoneline
        if (extension.getPhoneLine() != null) {
            String mailBox = TelephonyUtils.getMailBox(extension.getBaseExtension(), companyId);
            String sipEntityId = getSipEntityId(extension.getPhoneLine());
            psAorService.create(sipEntityId, mailBox, 1);
        }
    }

    @Override
    public void updateSipEntity(VoiceMail voiceMail) {
        axVoiceMailService.update(voiceMail.getExtension().getCompany().getCompanyId(),
            voiceMail.getExtension().getBaseExtension(),
            voiceMail.getFullName(), voiceMail.getEmail());
    }

    @Override
    public void updateSipEntity(VoiceMail voiceMail, String password) {
        axVoiceMailService.changePassword(voiceMail.getExtension().getCompany().getCompanyId(),
            voiceMail.getExtension().getBaseExtension(), password);

        axVoiceMailService.update(voiceMail.getExtension().getCompany().getCompanyId(),
            voiceMail.getExtension().getBaseExtension(),
            voiceMail.getFullName(), voiceMail.getEmail());
    }

    @Override
    public void updateSipEntity(Queue queue) {
        String uniqueQueueId = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());

        String context = null;
        String musicclass = null;
        String comfortmessage = null;
        if (queue.isPlayMusicOnHold()) {
            musicclass = uniqueQueueId;
        }
        if (queue.isPlayComfortMessage() &&
            !StringUtils.isEmpty(queue.getComfortMessageFileName())) {
            comfortmessage = path + "/" + queue.getCompany().getCompanyId() + "/" + StringUtils.removeEnd(queue.getComfortMessageFileName(), ".wav");
        }
        if (queue.isZeroEscapeEnabled()) {
            context = Constants.zeroEscapeContext;
        }
        axQueueService.update(uniqueQueueId, Strategy.valueOf(queue.getStrategy().name()),
            queue.getWeight(), queue.getServiceLevel(), queue.getMaxLength(), context,
            musicclass, comfortmessage, queue.getComfortMessageInterval());
    }

    private String getSipEntityId(PhoneLine phoneLine) {
        Extension extension = phoneLine.getExtension();
        if (extension == null) {
            throw new RuntimeException("A extension must be assigned to the device line to create a sip entity.");
        }
        if (phoneLine.getSipUserName() == null) {
            throw new RuntimeException("Device line must have a sip username to create sip entity!");
        }
        return phoneLine.getSipUserName();
    }

    @Override
    public void deleteSipEntity(PhoneLine phoneLine) {
        String sipEntityId = getSipEntityId(phoneLine);
        psEndpointService.delete(sipEntityId);
        psAuthService.delete(sipEntityId);
        psAorService.delete(sipEntityId);
    }

    @Override
    public void deleteSipEntity(Queue queue) {
        String uniqueQueueId = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        axQueueService.delete(uniqueQueueId);
    }

    @Override
    public void deleteSipEntity(Provider provider) {
        psEndpointIdentifyService.delete(provider.getProviderId());
        psAorService.delete(provider.getProviderId());
        psEndpointService.delete(provider.getProviderId());
    }

    @Override
    public void deleteSipEntity(PhoneNumber phoneNumber) {
        psRegistrationService.delete(phoneNumber.getNumber());
        psAuthService.delete(phoneNumber.getNumber());
        psEndpointService.delete(phoneNumber.getNumber());
    }

    @Override
    public void deleteSipEntity(VoiceMail voiceMail) {
        axVoiceMailService.delete(voiceMail.getExtension().getCompany().getCompanyId(), voiceMail.getExtension().getBaseExtension());
        if (voiceMail.getExtension().getPhoneLine() != null) {
            String sipEntityId = getSipEntityId(voiceMail.getExtension().getPhoneLine());
            psAorService.create(sipEntityId, null, 1);
        }
    }
}
