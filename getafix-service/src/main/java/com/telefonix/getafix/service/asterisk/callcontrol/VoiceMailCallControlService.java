package com.telefonix.getafix.service.asterisk.callcontrol;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;

/**
 * Created by tareqmy on 2019-04-30.
 */
public interface VoiceMailCallControlService {

    CallEventListener handleVoiceMailDrop(Channel channel, String accountCode, String exten, String context);

    CallEventListener handleVoiceMailCheck(Channel channel, String accountCode, String exten, String context);
}
