package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.asterisk.service.live.entity.AXVoiceMailLive;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.VoiceMail;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by tareqmy on 2019-08-01.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"voiceMail"}, callSuper = true)
@ToString
public class VoiceMailLive extends Live {

    private VoiceMail voiceMail;

    private Extension extension;

    private Queue queue;

    private AXVoiceMailLive axVoiceMailLive;

    public VoiceMailLive(VoiceMail voiceMail, AXVoiceMailLive axVoiceMailLive) {
        super(voiceMail.getId());
        this.voiceMail = voiceMail;
        this.extension = voiceMail.getExtension();
        this.axVoiceMailLive = axVoiceMailLive;
    }
}
