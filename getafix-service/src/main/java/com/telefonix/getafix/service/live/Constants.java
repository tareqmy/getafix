package com.telefonix.getafix.service.live;

/**
 * Created by tareqmy on 27/8/19.
 */
public class Constants {

    public static final String incomingCall = "Incoming";

    public static final String outgoingCall = "Outgoing";

    public static final String personalCallType = "Personal";

    public static final String queueCallTypePrefix = "Q: ";

    public static final String zeroDurationFormat = "00:00:00";

    public static final String durationFormat = "HH:mm:ss";

    public static final String zeroEscapeContext = "queueexitcontext";
}
