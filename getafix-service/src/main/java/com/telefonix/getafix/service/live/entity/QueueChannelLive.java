package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.service.live.Constants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-07-17.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"channelLive"})
@ToString
public class QueueChannelLive {

    private String companyId;

    private String queueId;

    private int position;

    private boolean left;

    private boolean abandoned;

    private Instant queuedInstant;

    private ChannelLive channelLive;

    private boolean outbound;

    private String queueCallDirection;

    private List<ChannelLive> agentChannelLives = new ArrayList<>();

    public QueueChannelLive(String companyId, String queueId, ChannelLive channelLive, boolean outbound) {
        setCompanyId(companyId);
        setQueueId(queueId);
        setQueuedInstant(Instant.now());
        if (outbound) {
            setPosition(0);
            setLeft(true);
        } else {
            setLeft(false);
        }
        setOutbound(outbound);
        setAbandoned(false);

        setChannelLive(channelLive);
        if (outbound) {
            setQueueCallDirection(Constants.outgoingCall);
        } else {
            setQueueCallDirection(Constants.incomingCall);
        }
    }
}
