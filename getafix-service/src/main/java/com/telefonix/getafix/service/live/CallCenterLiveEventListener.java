package com.telefonix.getafix.service.live;

import com.telefonix.getafix.service.live.entity.CallCenterLive;

/**
 * Created by tareqmy on 2019-08-05.
 */
@FunctionalInterface
public interface CallCenterLiveEventListener {

    void onEvent(CallCenterLive callCenterLive);
}
