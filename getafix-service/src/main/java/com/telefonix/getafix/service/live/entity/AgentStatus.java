package com.telefonix.getafix.service.live.entity;

public enum AgentStatus {
    UNKNOWN,
    JOINED,
    PAUSED,
    UNJOINED
}
