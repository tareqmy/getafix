package com.telefonix.getafix.service.live;

import com.telefonix.getafix.service.live.entity.QueueLive;

/**
 * Created by tareqmy on 2019-06-25.
 */
@FunctionalInterface
public interface QueueLiveEventListener {

    void onEvent(QueueLive queueLive);
}
