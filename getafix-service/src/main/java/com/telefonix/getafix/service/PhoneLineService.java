package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getfix.dto.PhoneLineDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-03-25.
 */
public interface PhoneLineService {

    PhoneLine createPhoneLine(PhoneLineDTO phoneLineDTO);

    PhoneLine updatePhoneLine(PhoneLineDTO phoneLineDTO);

    List<PhoneLine> findAll();

    Optional<PhoneLine> findOne(Long id);

    Optional<PhoneLine> findBySipUserName(String sipUserName);

    PhoneLine getPhoneLine(Long id);

    List<PhoneLine> getPhoneLines(Extension extension);

    List<PhoneLine> getPhoneLines(Phone phone);

    List<PhoneLine> findByJoinedUserNotNull();

    void delete(Long id);

    void delete(Phone phone);
}
