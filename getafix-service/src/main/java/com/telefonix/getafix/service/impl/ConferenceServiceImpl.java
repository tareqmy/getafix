package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Conference;
import com.telefonix.getafix.repository.ConferenceRepository;
import com.telefonix.getafix.service.ConferenceService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getfix.dto.ConferenceDTO;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Slf4j
@Service
@Transactional
public class ConferenceServiceImpl implements ConferenceService {

    private ConferenceRepository conferenceRepository;

    private GeneralService generalService;

    @Autowired
    public ConferenceServiceImpl(ConferenceRepository conferenceRepository, GeneralService generalService) {
        this.conferenceRepository = conferenceRepository;
        this.generalService = generalService;
    }

    @Override
    public Conference createConference(ConferenceDTO conferenceDTO) {
        Company company = generalService.getCompany(conferenceDTO.getCompanyId());
        Conference conference = new Conference();
        conference.setName(conferenceDTO.getName());
        conference.setCompany(company);
        conference.setLeaderCode(RandomStringUtils.randomNumeric(6));
        conference.setParticipantCode(RandomStringUtils.randomNumeric(6));
        return conferenceRepository.save(conference);
    }

    @Override
    public Conference updateConference(ConferenceDTO conferenceDTO) {
        Conference conference = getConference(conferenceDTO.getId());
        conference.setLeaderCode(RandomStringUtils.randomNumeric(6));
        conference.setParticipantCode(RandomStringUtils.randomNumeric(6));
        return conferenceRepository.save(conference);
    }

    @Override
    public List<Conference> findAll() {
        return conferenceRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<Conference> findByCompany(String companyId) {
        return conferenceRepository.findByCompanyId(companyId)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<Conference> getConference(Company company) {
        return conferenceRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Conference> findOne(Long id) {
        return conferenceRepository.findById(id);
    }

    @Override
    public Conference getConference(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid conference id " + id)
        );
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getConferences().forEach(this::delete);
    }

    private void delete(Conference conference) {
        conferenceRepository.delete(conference);
        log.debug("Deleted conference: {}", conference);
    }
}
