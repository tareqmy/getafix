package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.ServerSentEventService;
import com.telefonix.getafix.service.live.*;
import com.telefonix.getafix.service.live.dto.*;
import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.CallCenterLive;
import com.telefonix.getafix.service.live.entity.QueueLive;
import com.telefonix.getafix.service.live.entity.SystemLive;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

/**
 * Created by tareqmy on 2019-05-27.
 */
@Slf4j
@Service
//@Transactional NOTE: this makes the simplest of methods, where no db query is made, create a db connection
public class ServerSentEventServiceImpl implements ServerSentEventService, SystemLiveEventListener, CallCenterLiveEventListener,
    QueueLiveEventListener, AgentLiveEventListener, UserLiveEventListener {

    private List<LiveConsumer<ServerTimeDTO>> serverTimeLiveConsumers = new CopyOnWriteArrayList<>();

    private List<LiveConsumer<SystemLiveDTO>> systemLiveConsumers = new CopyOnWriteArrayList<>();

    private Map<String, List<LiveConsumer<CallCenterLiveDTO>>> companyQueueLiveConsumers = new ConcurrentHashMap<>();

    private Map<String, List<LiveConsumer<QueueLiveDTO>>> queueLiveConsumers = new ConcurrentHashMap<>();

    //list of consumers because the same user might login from different browsers?
    private Map<String, List<LiveConsumer<AgentLiveDTO>>> agentLiveConsumers = new ConcurrentHashMap<>();

    private UserLiveRepository userLiveRepository;

    private SystemLiveRepository systemLiveRepository;

    private CallCenterLiveRepository callCenterLiveRepository;

    private QueueLiveRepository queueLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    @Autowired
    public ServerSentEventServiceImpl(UserLiveRepository userLiveRepository,
                                      SystemLiveRepository systemLiveRepository,
                                      CallCenterLiveRepository callCenterLiveRepository,
                                      QueueLiveRepository queueLiveRepository,
                                      AgentLiveRepository agentLiveRepository) {
        this.userLiveRepository = userLiveRepository;
        this.systemLiveRepository = systemLiveRepository;
        this.callCenterLiveRepository = callCenterLiveRepository;
        this.queueLiveRepository = queueLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.queueLiveRepository.addQueueLiveEventListener(this);
        this.agentLiveRepository.addAgentLiveEventListener(this);
        this.systemLiveRepository.addSystemLiveEventListener(this);
        this.callCenterLiveRepository.addCallCenterLiveEventListener(this);
        this.userLiveRepository.addUserLiveEventListener(this);
    }

    private User getUser(String email) {
        User user = userLiveRepository.getUserLive(email);
        if (user == null) {
            log.error("Requested user is not in live repository {}", email);
            throw new GenericGetafixException("Requested user is not in live repository!");
        }
        return user;
    }

    //=== register for live events =====

    public Flux<ServerTimeDTO> streamTime() {
        return Flux.create(emitter -> {
            User user = getUser(SecurityUtils.getCurrentUserName());
            Consumer<ServerTimeDTO> consumer = emitter::next;
            registerServerTimeLive(user, consumer, emitter);
            emitter.onDispose(() -> unregisterServerTimeLive(consumer));
        }, FluxSink.OverflowStrategy.LATEST);
    }

    @Override
    public Flux<SystemLiveDTO> streamSystemLive() {
        return Flux.create(emitter -> {
            User user = getUser(SecurityUtils.getCurrentUserName());
            Consumer<SystemLiveDTO> consumer = emitter::next;
            registerSystemLive(user, consumer, emitter);
            emitter.onDispose(() -> unregisterSystemLive(consumer));
        }, FluxSink.OverflowStrategy.LATEST);
    }

    @Override
    public Flux<CallCenterLiveDTO> streamCallCenterLive() {
        return Flux.create(emitter -> {
            User user = getUser(SecurityUtils.getCurrentUserName());
            String companyId = user.getCompany().getCompanyId();
            CallCenterLive callCenterLive = callCenterLiveRepository.getCallCenterLive(companyId);
            Consumer<CallCenterLiveDTO> consumer = emitter::next;
            registerCallCenterLive(user, callCenterLive, consumer, emitter);
            emitter.onDispose(() -> unregisterCallCenterLive(callCenterLive, consumer));
        }, FluxSink.OverflowStrategy.LATEST);
    }

    @Override
    public Flux<QueueLiveDTO> streamQueuesLive(String uniqueQueueId) {
        return Flux.create(emitter -> {
            User user = getUser(SecurityUtils.getCurrentUserName());
            QueueLive queueLive = queueLiveRepository.getQueueLive(uniqueQueueId);
            if (queueLive == null || !queueLive.getCompanyId().equals(user.getCompany().getCompanyId())) {
                throw new BusinessRuleViolationException("Invalid queue id");
            }
            Consumer<QueueLiveDTO> consumer = emitter::next;
            registerQueueLive(user, queueLive, consumer, emitter);
            emitter.onDispose(() -> unregisterQueueLive(queueLive, consumer));
        }, FluxSink.OverflowStrategy.LATEST);
    }

    @Override
    public Flux<AgentLiveDTO> streamAgentsLive() {
        return Flux.create(emitter -> {
            User user = getUser(SecurityUtils.getCurrentUserName());
            AgentLive agentLive = agentLiveRepository.getAgentLive(user);
            if (agentLive == null) {
                throw new BusinessRuleViolationException("You are not logged in this callcenter!");
            }
            Consumer<AgentLiveDTO> consumer = emitter::next;
            registerAgentLive(user, agentLive, consumer, emitter);
            emitter.onDispose(() -> unregisterAgentLive(agentLive, consumer));
        }, FluxSink.OverflowStrategy.LATEST);
    }

    //=== register for live events =====

    private void registerServerTimeLive(User user, Consumer<ServerTimeDTO> consumer, FluxSink<ServerTimeDTO> emitter) {
        LiveConsumer<ServerTimeDTO> liveConsumer = new LiveConsumer<>(user, consumer, emitter);
        serverTimeLiveConsumers.add(liveConsumer);
        pushTime(liveConsumer);
    }

    private void unregisterServerTimeLive(Consumer<ServerTimeDTO> consumer) {
        serverTimeLiveConsumers.removeIf(liveConsumer -> liveConsumer.getConsumer().equals(consumer));
    }

    private void registerSystemLive(User user, Consumer<SystemLiveDTO> consumer, FluxSink<SystemLiveDTO> emitter) {
        LiveConsumer<SystemLiveDTO> liveConsumer = new LiveConsumer<>(user, consumer, emitter);
        systemLiveConsumers.add(liveConsumer);
        pushSystemLive(systemLiveRepository.getSystemLive(), liveConsumer);
    }

    private void unregisterSystemLive(Consumer<SystemLiveDTO> consumer) {
        systemLiveConsumers.removeIf(liveConsumer -> liveConsumer.getConsumer().equals(consumer));
    }

    private void registerCallCenterLive(User user, CallCenterLive callCenterLive, Consumer<CallCenterLiveDTO> consumer, FluxSink<CallCenterLiveDTO> emitter) {
        LiveConsumer<CallCenterLiveDTO> liveConsumer = new LiveConsumer<>(user, consumer, emitter);
        companyQueueLiveConsumers.computeIfAbsent(callCenterLive.getCompany().getCompanyId(), s -> new CopyOnWriteArrayList<>())
            .add(liveConsumer);
        pushCallCenterLive(callCenterLive, liveConsumer);
    }

    private void unregisterCallCenterLive(CallCenterLive callCenterLive, Consumer<CallCenterLiveDTO> consumer) {
        companyQueueLiveConsumers.computeIfAbsent(callCenterLive.getCompany().getCompanyId(), s -> new CopyOnWriteArrayList<>())
            .removeIf(liveConsumer -> liveConsumer.getConsumer().equals(consumer));
    }

    private void registerQueueLive(User user, QueueLive queueLive, Consumer<QueueLiveDTO> consumer, FluxSink<QueueLiveDTO> emitter) {
        LiveConsumer<QueueLiveDTO> liveConsumer = new LiveConsumer<>(user, consumer, emitter);
        queueLiveConsumers.computeIfAbsent(queueLive.getQueueId(), s -> new CopyOnWriteArrayList<>())
            .add(liveConsumer);
        pushQueueLive(queueLive, liveConsumer);
    }

    private void unregisterQueueLive(QueueLive queueLive, Consumer<QueueLiveDTO> consumer) {
        queueLiveConsumers.computeIfAbsent(queueLive.getQueueId(), s -> new CopyOnWriteArrayList<>())
            .removeIf(liveConsumer -> liveConsumer.getConsumer().equals(consumer));
    }

    private void registerAgentLive(User user, AgentLive agentLive, Consumer<AgentLiveDTO> consumer, FluxSink<AgentLiveDTO> emitter) {
        String sipUserName = agentLive.getPhoneLine().getSipUserName();
        LiveConsumer<AgentLiveDTO> liveConsumer = new LiveConsumer<>(user, consumer, emitter);
        agentLiveConsumers.computeIfAbsent(sipUserName, s -> new CopyOnWriteArrayList<>())
            .add(liveConsumer);
        pushAgentLive(agentLive, liveConsumer);
    }

    private void unregisterAgentLive(AgentLive agentLive, Consumer<AgentLiveDTO> consumer) {
        String sipUserName = agentLive.getPhoneLine().getSipUserName();
        agentLiveConsumers.computeIfAbsent(sipUserName, s -> new CopyOnWriteArrayList<>())
            .removeIf(liveConsumer -> liveConsumer.getConsumer().equals(consumer));
    }


    //=== Event distribution =====

    private void pushTime(LiveConsumer<ServerTimeDTO> consumer) {
        consumer.getConsumer().accept(new ServerTimeDTO(consumer.getZoneId()));
    }

    private void pushSystemLive(SystemLive systemLive, LiveConsumer<SystemLiveDTO> consumer) {
        consumer.getConsumer().accept(new SystemLiveDTO(systemLive, consumer.getZoneId()));
    }

    private void pushCallCenterLive(CallCenterLive callCenterLive, LiveConsumer<CallCenterLiveDTO> consumer) {
        consumer.getConsumer().accept(new CallCenterLiveDTO(callCenterLive, consumer.getZoneId()));
    }

    private void pushQueueLive(QueueLive queueLive, LiveConsumer<QueueLiveDTO> consumer) {
        consumer.getConsumer().accept(new QueueLiveDTO(queueLive, consumer.getZoneId(), true));
    }

    private void pushAgentLive(AgentLive agentLive, LiveConsumer<AgentLiveDTO> consumer) {
        consumer.getConsumer().accept(new AgentLiveDTO(agentLive, consumer.getZoneId()));
    }

    //=== event distribution on update =====

    @Async
    @Override
    public void onEvent(SystemLive systemLive) {
        systemLiveConsumers.forEach(consumer -> pushSystemLive(systemLive, consumer));
    }

    @Async
    @Override
    public void onEvent(CallCenterLive callCenterLive) {
        String companyId = callCenterLive.getCompany().getCompanyId();
        companyQueueLiveConsumers.computeIfAbsent(companyId, s -> new CopyOnWriteArrayList<>())
            .forEach(consumer -> pushCallCenterLive(callCenterLive, consumer));
    }

    @Async
    @Override
    public void onEvent(QueueLive queueLive) {
        String queueId = queueLive.getQueueId();
        queueLiveConsumers.computeIfAbsent(queueId, s -> new CopyOnWriteArrayList<>())
            .forEach(consumer -> pushQueueLive(queueLive, consumer));
    }

    @Async
    @Override
    public void onEvent(AgentLive agentLive) {
        String sipUserName = agentLive.getPhoneLine().getSipUserName();
        agentLiveConsumers.computeIfAbsent(sipUserName, s -> new CopyOnWriteArrayList<>())
            .forEach(consumer -> pushAgentLive(agentLive, consumer));
    }

    //=== user live events =====

    @Override
    public void onUserLeftContactCenter(User user) {
        if (user.getJoinedPhoneLine() != null) {
            List<LiveConsumer<AgentLiveDTO>> liveConsumers = agentLiveConsumers.remove(user.getJoinedPhoneLine().getSipUserName());
            if (!CollectionUtils.isEmpty(liveConsumers)) {
                liveConsumers.forEach(liveConsumer -> liveConsumer.getEmitter().complete());
            }
        }
    }

    //=== Scheduled event distribution =====

    @Scheduled(fixedRate = 3000)
    public void pushTime() {
        serverTimeLiveConsumers.forEach(this::pushTime);
    }

    @Scheduled(fixedRate = 30000)
    public void pushSystemLive() {
        onEvent(systemLiveRepository.getSystemLive());
    }

    @Scheduled(fixedRate = 30000)
    public void pushCallCenterLive() {
        callCenterLiveRepository.getCallCenterLiveMapValues().forEach(this::onEvent);
    }

    @Scheduled(fixedRate = 30000)
    public void pushQueueLive() {
        queueLiveRepository.getQueueLiveMapValues().forEach(this::onEvent);
    }

    @Scheduled(fixedRate = 30000)
    public void pushAgentLive() {
        agentLiveRepository.getAgentLiveMapValues().forEach(this::onEvent);
    }
}
