package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.UserRole;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.repository.UserRoleRepository;
import com.telefonix.getafix.service.UserRoleService;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tmyousuf on 2/15/16.
 * Provide service related to userRoles
 */
@Slf4j
@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserRole getUserRole(Role role) {
        log.debug("getUserRole() {}", role);
        return userRoleRepository.findOneByRole(role)
            .orElseThrow(() -> new BusinessRuleViolationException("No such user role: " + role));
    }

    @Override
    public UserRole getUserRole(String roleValue) {
        log.debug("getUserRole() {}", roleValue);
        return getUserRole(getRole(roleValue));
    }

    private Role getRole(String roleValue) {
        Role type;
        try {
            type = Role.valueOf(roleValue);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Invalid role: " + roleValue);
        }
        return type;
    }
}
