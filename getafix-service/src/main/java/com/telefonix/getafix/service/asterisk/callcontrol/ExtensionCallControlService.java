package com.telefonix.getafix.service.asterisk.callcontrol;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;

/**
 * Created by tareqmy on 2019-04-30.
 */
public interface ExtensionCallControlService {

    CallEventListener handleExtensionCall(Channel channel, String accountCode, String exten, String context);
}
