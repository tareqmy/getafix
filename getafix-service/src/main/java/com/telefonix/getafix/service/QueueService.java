package com.telefonix.getafix.service;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getfix.dto.QueueDTO;
import com.telefonix.getfix.dto.QueueReportDTO;
import com.telefonix.getfix.dto.SoundFile;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-28.
 */
public interface QueueService {

    Queue createQueue(QueueDTO queueDTO);

    Queue updateQueue(QueueDTO queueDTO);

    List<Queue> findAll();

    Optional<Queue> findOne(Long id);

    Queue getQueue(Long id);

    List<Queue> getQueues(Company company);

    void delete(Long id);

    void delete(Company company);

    List<AXQueueLog> getQueueLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    List<QueueChannelQueueLog> getQueueChannelLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    QueueReportDTO getQueueReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    Queue uploadInitialMessage(Long id, MultipartFile file);

    Queue deleteInitialMessage(Long id);

    Queue uploadMusicOnHold(Long id, MultipartFile file);

    Queue deleteMusicOnHold(Long id);

    Queue uploadComfortMessage(Long id, MultipartFile file);

    Queue deleteComfortMessage(Long id);

    SoundFile downloadInitialMessage(Long id);

    SoundFile downloadMusicOnHold(Long id);

    SoundFile downloadComfortMessage(Long id);
}
