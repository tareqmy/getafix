package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.AgentEvent;
import com.telefonix.getafix.repository.AgentLogRepository;
import com.telefonix.getafix.service.AgentLogService;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-08-11.
 */
@Slf4j
@Service
@Transactional
public class AgentLogServiceImpl implements AgentLogService {

    private AgentLogRepository agentLogRepository;

    private Map<String, AgentLog> latestAgentLogMap;

    @Autowired
    public AgentLogServiceImpl(AgentLogRepository agentLogRepository) {
        this.agentLogRepository = agentLogRepository;
        this.latestAgentLogMap = new ConcurrentHashMap<>();
    }

    private String getAgentLogIdentifier(AgentLog agentLog) {
        if (agentLog == null || StringUtils.isEmpty(agentLog.getSipUserName())) {
            return null;
        }
        if (StringUtils.isEmpty(agentLog.getQueueName())) {
            return agentLog.getSipUserName();
        }
        return agentLog.getSipUserName() + "_" + agentLog.getQueueName();
    }

    private Optional<AgentLog> getLastLog(AgentLog agentLog) {
        if (agentLog == null || StringUtils.isEmpty(agentLog.getSipUserName())) {
            return Optional.empty();
        }
        if (StringUtils.isEmpty(agentLog.getQueueName())) {
            return agentLogRepository.findTopBySipUserNameOrderByEventInstantDesc(agentLog.getSipUserName());
        }
        return agentLogRepository.findTopBySipUserNameAndQueueNameOrderByEventInstantDesc(agentLog.getSipUserName(), agentLog.getQueueName());
    }

    private synchronized boolean isDuplicateEvent(AgentLog agentLog) {
        boolean duplicate = false;
        if (agentLog == null || StringUtils.isEmpty(agentLog.getSipUserName())) {
            log.error("asking for a duplicate with a weird log {}", agentLog);
            return duplicate;
        }
        AgentLog cached = latestAgentLogMap.get(getAgentLogIdentifier(agentLog));
        if (cached == null) {
            Optional<AgentLog> latest = getLastLog(agentLog);
            if (latest.isPresent()) {
                latestAgentLogMap.put(getAgentLogIdentifier(agentLog), latest.get());
                cached = latest.get();
            }
        }

        if (cached != null) {
            duplicate = cached.getAgentEvent().equals(agentLog.getAgentEvent());

            //for queue join and leave needs to match the queue too!
            if (duplicate && (cached.getAgentEvent().equals(AgentEvent.JOIN_QUEUE)
                || cached.getAgentEvent().equals(AgentEvent.LEAVE_QUEUE))) {
                duplicate = cached.getQueueName().equals(agentLog.getQueueName());
            }
        }
        if (!duplicate) {
            latestAgentLogMap.put(getAgentLogIdentifier(agentLog), agentLog);
        }
        return duplicate;
    }

    @Override
    public void onJoinCallCenter(User user, PhoneLine phoneLine) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.JOIN_CALL_CENTER);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    @Override
    public void onLeaveCallCenter(User user, PhoneLine phoneLine) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.LEAVE_CALL_CENTER);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    @Override
    public void onPauseAgent(User user, PhoneLine phoneLine) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.PAUSE_AGENT);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    @Override
    public void onResumeAgent(User user, PhoneLine phoneLine) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.RESUME_AGENT);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    //also called when application restarts. causing duplicate joins! we dont mind
    @Override
    public void onJoinQueue(User user, PhoneLine phoneLine, Queue queue) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.JOIN_QUEUE);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        agentLog.setQueueId(queue.getId());
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        agentLog.setQueueName(queueName);
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    @Override
    public void onLeaveQueue(User user, PhoneLine phoneLine, Queue queue) {
        AgentLog agentLog = new AgentLog();
        agentLog.setAgentEvent(AgentEvent.LEAVE_QUEUE);
        agentLog.setEventInstant(Instant.now());
        agentLog.setCompanyId(user.getCompany().getCompanyId());
        agentLog.setUserId(user.getId());
        agentLog.setUserName(user.getFullName());
        agentLog.setPhoneLineId(phoneLine.getId());
        agentLog.setSipUserName(phoneLine.getSipUserName());
        agentLog.setQueueId(queue.getId());
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        agentLog.setQueueName(queueName);
        if (isDuplicateEvent(agentLog)) {
            return;
        }
        agentLogRepository.save(agentLog);
    }

    @Override
    public List<AgentLog> getAgentLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return agentLogRepository.findByUserIdAndEventInstantBetweenOrderByEventInstantDesc(id, fromDate, toDate);
    }
}
