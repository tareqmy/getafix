package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Provider;
import com.telefonix.getfix.dto.ProviderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-01.
 */
public interface ProviderService {

    Provider createProvider(ProviderDTO providerDTO);

    Provider updateProvider(ProviderDTO providerDTO);

    List<Provider> findAll();

    Page<Provider> findAll(Pageable pageable);

    Optional<Provider> findOne(Long id);

    Optional<Provider> findOne(String providerId);

    Provider getProvider(Long id);

    void delete(Long id);
}
