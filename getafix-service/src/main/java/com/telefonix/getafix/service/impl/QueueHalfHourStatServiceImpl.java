package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.entity.enums.QueueLogEvent;
import com.telefonix.getafix.asterisk.repository.AXQueueLogRepository;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getafix.entity.QueueHalfHourStat;
import com.telefonix.getafix.repository.QueueChannelQueueLogRepository;
import com.telefonix.getafix.repository.QueueHalfHourStatRepository;
import com.telefonix.getafix.service.QueueHalfHourStatService;
import com.telefonix.getafix.service.live.QueueLiveEventListener;
import com.telefonix.getafix.service.live.QueueLiveRepository;
import com.telefonix.getafix.service.live.entity.QueueLive;
import com.telefonix.getfix.utils.InstantUtils;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 1/10/19.
 */
@Slf4j
@Service
@Transactional
public class QueueHalfHourStatServiceImpl implements QueueHalfHourStatService, QueueLiveEventListener {

    private QueueHalfHourStatRepository queueHalfHourStatRepository;

    private QueueLiveRepository queueLiveRepository;

    private AXQueueLogRepository axQueueLogRepository;

    private QueueChannelQueueLogRepository queueChannelQueueLogRepository;

    private Map<String, QueueHalfHourStat> currentQueueHalfHourStatMap;

    @Autowired
    public QueueHalfHourStatServiceImpl(QueueHalfHourStatRepository queueHalfHourStatRepository,
                                        QueueLiveRepository queueLiveRepository,
                                        AXQueueLogRepository axQueueLogRepository,
                                        QueueChannelQueueLogRepository queueChannelQueueLogRepository) {
        queueLiveRepository.addQueueLiveEventListener(this);

        this.queueHalfHourStatRepository = queueHalfHourStatRepository;
        this.queueLiveRepository = queueLiveRepository;
        this.axQueueLogRepository = axQueueLogRepository;
        this.queueChannelQueueLogRepository = queueChannelQueueLogRepository;

        this.currentQueueHalfHourStatMap = new ConcurrentHashMap<>();
    }

    @Scheduled(cron = "0 0/30 * * * *")
    public synchronized void startStat() {
        queueLiveRepository.getQueueLiveMapValues()
            .forEach(this::getCurrentQueueHalfHourStat);
    }

    @Async
    @Override
    public synchronized void onQueueCreated(Queue queue) {
        //create a stat when a queue is created... so ensuring it always finds one saved...
        Instant currentFloorHalfHour = InstantUtils.getCurrentFloorHalfHour();
        QueueHalfHourStat queueHalfHourStat = new QueueHalfHourStat();
        queueHalfHourStat.setStart(currentFloorHalfHour);
        queueHalfHourStat.setCompanyId(queue.getCompany().getCompanyId());
        queueHalfHourStat.setQueueId(queue.getId());
        queueHalfHourStat.setQueueName(queue.getQueueName());
        queueHalfHourStatRepository.save(queueHalfHourStat);
        String uniqueQueueId = TelephonyUtils.getUniqueQueueId(queueHalfHourStat.getCompanyId(), queue.getQueueId());
        currentQueueHalfHourStatMap.put(uniqueQueueId, queueHalfHourStat);
    }

    @Async
    @Override
    public synchronized void onEvent(QueueLive queueLive) {
        int agents = queueLive.getAgentLiveMap().size();
        QueueHalfHourStat currentQueueHalfHourStat = getCurrentQueueHalfHourStat(queueLive);
        if (currentQueueHalfHourStat.getMaxJoinedAgents() < agents) {
            currentQueueHalfHourStat.setMaxJoinedAgents(agents);
        }
        int callers = queueLive.getChannelLiveMap().size();
        if (currentQueueHalfHourStat.getMaxCallers() < callers) {
            currentQueueHalfHourStat.setMaxCallers(callers);
        }
        queueHalfHourStatRepository.save(currentQueueHalfHourStat);
    }

    private QueueHalfHourStat getCurrentQueueHalfHourStat(QueueLive queueLive) {
        Instant floorHalfHour = InstantUtils.getCurrentFloorHalfHour();
        QueueHalfHourStat queueHalfHourStat = currentQueueHalfHourStatMap.get(queueLive.getQueueId());
        if (queueHalfHourStat == null) { //this means the application just started
            Optional<QueueHalfHourStat> current = queueHalfHourStatRepository.findByQueueIdAndStart(queueLive.getId(), floorHalfHour);
            if (current.isPresent()) { //application was restarted within this half hour
                queueHalfHourStat = current.get();
            } else { //application was stop atleast within this half hour
                QueueHalfHourStat immediatePrevious = processPreviousMissingStats(queueLive, floorHalfHour);
                queueHalfHourStat = createQueueHalfHourStat(queueLive, immediatePrevious, floorHalfHour);
            }
        } else { //application is in running
            if (!queueHalfHourStat.getStart().equals(floorHalfHour)) { //application is in running atleast since last half hour
                //NOTE: if floorHalfHour is not at 30 minutes start need to query axqueuelog
                QueueHalfHourStat immediatePrevious = queueHalfHourStat;
                if (!floorHalfHour.minus(30, ChronoUnit.MINUTES).equals(immediatePrevious.getStart())) {
                    //if the current is not immediate previous than process the missing first.
                    //it should not never be the case but added this logic just in case
                    //only maybe the previous iteration faced exception and nobody called a get
                    log.warn("A use case which shouldn't have happened!! {}/{}", floorHalfHour, immediatePrevious);
                    immediatePrevious = processPreviousMissingStats(queueLive, floorHalfHour);
                }
                queueHalfHourStat = createQueueHalfHourStat(queueLive, immediatePrevious, floorHalfHour);
            }
        }
        currentQueueHalfHourStatMap.put(queueLive.getQueueId(), queueHalfHourStat);
        return queueHalfHourStat;
    }

    //should be called when current floor is missing
    //basically find the last period that was logged and executed sequentially and create all the missing periods so that
    //it is a continuous statistics
    private QueueHalfHourStat processPreviousMissingStats(QueueLive queueLive, Instant currentHalfHourFloor) {
        AtomicReference<QueueHalfHourStat> immediatePrevious = new AtomicReference<>();
        queueHalfHourStatRepository.findTopByQueueIdOrderByStartDesc(queueLive.getId())
            .ifPresentOrElse(queueHalfHourStat -> {
                    immediatePrevious.set(processPreviousMissingStats(queueLive, queueHalfHourStat,
                        queueHalfHourStat.getStart(),
                        currentHalfHourFloor));
                },
                () -> {
                    log.debug("Didn't find any half hour... checking for queue log");
                    axQueueLogRepository.findTopByQueueNameOrderByTimeAsc(queueLive.getQueueId())
                        .ifPresentOrElse(axQueueLog -> {
                            log.debug("First queue log event {}", axQueueLog);
                            immediatePrevious.set(processPreviousMissingStats(queueLive, null,
                                axQueueLog.getTime(),
                                currentHalfHourFloor));
                        }, () -> log.debug("No existing queue log at all for the queue {}", queueLive.getQueueId()));
                });
        return immediatePrevious.get();
    }

    private QueueHalfHourStat processPreviousMissingStats(QueueLive queueLive,
                                                          QueueHalfHourStat immediatePrevious,
                                                          Instant firstHalfHourFloor,
                                                          Instant currentHalfHourFloor) {
        QueueHalfHourStat lastStat = null;
        log.debug("{}/{}/{}", queueLive.getQueueId(), firstHalfHourFloor, currentHalfHourFloor);
        List<Instant> missingHalfHourInstants = InstantUtils.getHalfHourInstants(firstHalfHourFloor, currentHalfHourFloor);
        missingHalfHourInstants.removeIf(instant -> instant.equals(firstHalfHourFloor)); //remove the existing
        missingHalfHourInstants.removeIf(instant -> instant.equals(currentHalfHourFloor)); //remove the current
        log.debug("Missing instants {}", missingHalfHourInstants);
        for (Instant intermediate : missingHalfHourInstants.stream().sorted().collect(Collectors.toList())) {
            lastStat = createQueueHalfHourStat(queueLive, immediatePrevious, intermediate);
            immediatePrevious = lastStat;
        }
        return lastStat;
    }

    private QueueHalfHourStat createQueueHalfHourStat(QueueLive queueLive, QueueHalfHourStat immediatePrevious, Instant floorHalfHour) {
        log.debug("Create half hour {} for {} with previous {}", queueLive.getQueueId(), floorHalfHour, immediatePrevious);
        long continuedAgents = 0;
        long continuedCallers = 0;
        if (immediatePrevious != null) {
            continuedAgents = immediatePrevious.getContinuedAgents();
            continuedCallers = immediatePrevious.getContinuedCallers();
            List<AXQueueLog> axQueueLogs = axQueueLogRepository.findAllByQueueNameAndTimeBetweenOrderByTimeDesc(queueLive.getQueueId(),
                immediatePrevious.getStart(), immediatePrevious.getStart().plus(30, ChronoUnit.MINUTES));
            for (AXQueueLog axQueueLog : axQueueLogs) {
                if (axQueueLog.getEvent().equals(QueueLogEvent.ADDMEMBER)) {
                    continuedAgents++;
                }

                if (axQueueLog.getEvent().equals(QueueLogEvent.REMOVEMEMBER)) {
                    continuedAgents--;
                }
            }

            List<QueueChannelQueueLog> queueChannelQueueLogs = queueChannelQueueLogRepository.findByQueueIdAndQueuedBetweenOrDoneBetween(queueLive.getId(),
                immediatePrevious.getStart(), immediatePrevious.getStart().plus(30, ChronoUnit.MINUTES))
                .stream()
                .filter(queueChannelQueueLog -> !queueChannelQueueLog.getQueueChannelLog().isOutbound())
                .collect(Collectors.toList());

            for (QueueChannelQueueLog queueChannelQueueLog : queueChannelQueueLogs) {
                if (queueChannelQueueLog.getQueued().isAfter(immediatePrevious.getStart())
                    && queueChannelQueueLog.getQueued().isBefore(immediatePrevious.getStart().plus(30, ChronoUnit.MINUTES))) {
                    continuedCallers++;
                }

                if (queueChannelQueueLog.getHandled() != null) {
                    if (queueChannelQueueLog.getHandled().isAfter(immediatePrevious.getStart())
                        && queueChannelQueueLog.getHandled().isBefore(immediatePrevious.getStart().plus(30, ChronoUnit.MINUTES))) {
                        continuedCallers--;
                    }
                } else if (queueChannelQueueLog.getAbandoned() != null) {
                    if (queueChannelQueueLog.getAbandoned().isAfter(immediatePrevious.getStart())
                        && queueChannelQueueLog.getAbandoned().isBefore(immediatePrevious.getStart().plus(30, ChronoUnit.MINUTES))) {
                        continuedCallers--;
                    }
                }
            }

        }

        //NOTE: live map size doesnt matter even for the current floor because this might be created 2 minutes later!!
        int maxAgents;
        int maxCallers;

        log.debug("Half hour stat starting {}", floorHalfHour);
        //find overlapping sessions for this half hour
        List<AXQueueLog> axQueueLogs = axQueueLogRepository.findAllByQueueNameAndTimeBetweenOrderByTimeDesc(queueLive.getQueueId(),
            floorHalfHour, floorHalfHour.plus(30, ChronoUnit.MINUTES));
        maxAgents = (int) InstantUtils.maxConcurrent(
            axQueueLogs.stream()
                .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.ADDMEMBER))
                .map(AXQueueLog::getTime)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()),
            axQueueLogs.stream()
                .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.REMOVEMEMBER))
                .map(AXQueueLog::getTime)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));

        List<QueueChannelQueueLog> queueChannelQueueLogs = queueChannelQueueLogRepository.findByQueueIdAndQueuedBetweenOrDoneBetween(queueLive.getId(),
            floorHalfHour, floorHalfHour.plus(30, ChronoUnit.MINUTES))
            .stream()
            .filter(queueChannelQueueLog -> !queueChannelQueueLog.getQueueChannelLog().isOutbound())
            .collect(Collectors.toList());

        maxCallers = (int) InstantUtils.maxConcurrent(queueChannelQueueLogs.stream()
                .map(QueueChannelQueueLog::getQueued)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()),
            queueChannelQueueLogs.stream()
                .map(queueChannelQueueLog -> {
                    if (queueChannelQueueLog.getHandled() != null) {
                        return queueChannelQueueLog.getHandled();
                    }
                    return queueChannelQueueLog.getAbandoned();
                })
                .filter(Objects::nonNull) //this can be null for calls that are on going
                .collect(Collectors.toList()));

        if (continuedAgents > maxAgents) {
            maxAgents = (int) continuedAgents;
        }
        if (continuedCallers > maxCallers) {
            maxCallers = (int) continuedCallers;
        }

        QueueHalfHourStat save = new QueueHalfHourStat();
        save.setStart(floorHalfHour);
        save.setCompanyId(queueLive.getCompanyId());
        save.setQueueId(queueLive.getId());
        save.setQueueName(queueLive.getQueue().getQueueName());
        save.setContinuedAgents(continuedAgents);
        save.setMaxJoinedAgents(maxAgents);
        save.setContinuedCallers(continuedCallers);
        save.setMaxCallers(maxCallers);
        return queueHalfHourStatRepository.save(save);
    }

    @Override
    public List<QueueHalfHourStat> findAllByQueueId(Long queueId, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return queueHalfHourStatRepository.findByQueueIdAndStartBetween(queueId, fromDate, toDate);
    }
}
