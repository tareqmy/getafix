package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.System;

/**
 * Created by tareqmy on 2019-02-06.
 */
public interface SystemService {

    System getSystem();
}
