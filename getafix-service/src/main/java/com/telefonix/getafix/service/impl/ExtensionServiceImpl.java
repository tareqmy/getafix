package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.repository.ExtensionRepository;
import com.telefonix.getafix.service.ChannelLogService;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.VoiceMailService;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getfix.dto.ExtensionDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-01-14.
 */
@Slf4j
@Service
@Transactional
public class ExtensionServiceImpl implements ExtensionService {

    private ExtensionRepository extensionRepository;

    private ChannelLogService channelLogService;

    private VoiceMailService voiceMailService;

    private GeneralService generalService;

    private AgentLiveRepository agentLiveRepository;

    @Autowired
    public ExtensionServiceImpl(ExtensionRepository extensionRepository,
                                ChannelLogService channelLogService,
                                VoiceMailService voiceMailService,
                                GeneralService generalService,
                                AgentLiveRepository agentLiveRepository) {
        this.extensionRepository = extensionRepository;
        this.channelLogService = channelLogService;
        this.voiceMailService = voiceMailService;
        this.generalService = generalService;
        this.agentLiveRepository = agentLiveRepository;
    }

    private void verifyBaseExtension(String baseExtension, Company company) {
        if (extensionRepository.findByCompany(company)
            .stream()
            .map(Extension::getBaseExtension)
            .anyMatch(s -> s.equals(baseExtension))) {
            throw new BusinessRuleViolationException("This extensions is taken " + baseExtension);
        }
    }

    @Override
    public Extension createExtension(ExtensionDTO extensionDTO) {
        Company company = generalService.getCompany(extensionDTO.getCompanyId());
        verifyBaseExtension(extensionDTO.getBaseExtension(), company);
        Extension extension = new Extension();
        extension.setBaseExtension(extensionDTO.getBaseExtension());
        extension.setRingDuration(extensionDTO.getRingDuration());
        extension.setCompany(company);
        if (extensionDTO.getUserId() != null) {
            User user = generalService.getUser(extensionDTO.getUserId());
            if (!company.equals(user.getCompany())) {
                throw new BusinessRuleViolationException("Assigned User must belong to same company");
            }
            if(extension.getQueue() != null) {
                throw new BusinessRuleViolationException("Queue extension cant be assigned to a user!");
            }
            extension.setUser(user);
        }
        extension.setVoiceMail(new VoiceMail(extension));
        return extensionRepository.save(extension);
    }

    @Override
    public Extension updateExtension(ExtensionDTO extensionDTO) {
        Extension extension = getExtension(extensionDTO.getId());
        extension.setRingDuration(extensionDTO.getRingDuration());

        if (extensionDTO.getUserId() != null) { //when a user is being assigned to this extension
            User user = generalService.getUser(extensionDTO.getUserId());
            if (!extension.getCompany().equals(user.getCompany())) {
                throw new BusinessRuleViolationException("Assigned User must belong to same company");
            }
            if(extension.getQueue() != null) {
                throw new BusinessRuleViolationException("Queue extension cant be assigned to a user!");
            }
            // before reassign extension check being used by agent
            if (extension.getUser() != null && !user.equals(extension.getUser())) {
                validateAgentLive(extension);
            }
            extension.setUser(user);
        } else { //when removing any assigned user to extension
            // before unassign extension check being used by agent
            if (extension.getUser() != null) {
                validateAgentLive(extension);
            }
            extension.setUser(null);
        }
        return extensionRepository.save(extension);
    }

    private void validateAgentLive(Extension extension) {
        if (agentLiveRepository.isUsedByLiveAgent(extension)) {
            throw new BusinessRuleViolationException("This extension is being used by the previous owner in live callcenter!");
        }
    }

    @Override
    public Extension unassignUser(Extension extension) {
        extension.setUser(null);
        return extensionRepository.save(extension);
    }

    @Override
    public List<Extension> findAll() {
        return extensionRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Extension> findOne(Long id) {
        return extensionRepository.findById(id);
    }

    @Override
    public Optional<Extension> findByCompanyIdAndBaseExtension(String companyId, String baseExtension) {
        return extensionRepository.findByCompanyIdAndBaseExtension(companyId, baseExtension);
    }

    @Override
    public List<Extension> getExtensions(User user) {
        return extensionRepository.findByUser(user)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<Extension> getExtensions(Phone phone, boolean unAssigned) {
        return getExtensions(phone.getCompany(), unAssigned);
    }

    @Override
    public List<Extension> getExtensions(Company company, boolean unAssigned) {
        return extensionRepository.findByCompany(company)
            .stream()
            .filter(extension -> {
                if (unAssigned)
                    return extension.getQueue() == null;
                return true;
            })
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Extension getExtension(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid extension id " + id)
        );
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getExtensions().forEach(this::delete);
    }

    private void delete(Extension extension) {
        if (extension.getQueue() != null || extension.getPhoneLine() != null
            || extension.getAutoAttendant() != null || extension.getPhoneNumber() != null) {
            //cascading deletion for these too?? no. they must be deleted individually. because extension does not own them. company does
            throw new GenericGetafixException("The extension has queue/phonelines/autoattendant/phonenumber associated. Delete them first!");
        }
        voiceMailService.delete(extension);
        extensionRepository.delete(extension);
        log.debug("Deleted extension: {}", extension);
    }

    @Override
    public List<ChannelLog> getChannelLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        return channelLogService.getChannelLogsForExtension(id, from, to, zoneId);
    }

    @Override
    public Optional<PhoneLine> getPhoneLine(Long id) {
        Extension extension = getExtension(id);
        return Optional.ofNullable(extension.getPhoneLine());
    }
}
