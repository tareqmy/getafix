package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Conference;
import com.telefonix.getfix.dto.ConferenceDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-14.
 */
public interface ConferenceService {

    Conference createConference(ConferenceDTO conferenceDTO);

    Conference updateConference(ConferenceDTO conferenceDTO);

    List<Conference> findAll();

    List<Conference> findByCompany(String companyId);

    List<Conference> getConference(Company company);

    Optional<Conference> findOne(Long id);

    Conference getConference(Long id);

    void delete(Long id);

    void delete(Company company);

}
