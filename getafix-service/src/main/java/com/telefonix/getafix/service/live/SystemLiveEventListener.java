package com.telefonix.getafix.service.live;

import com.telefonix.getafix.service.live.entity.SystemLive;

/**
 * Created by tareqmy on 2019-07-10.
 */
@FunctionalInterface
public interface SystemLiveEventListener {

    void onEvent(SystemLive systemLive);
}
