package com.telefonix.getafix.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tmyousuf on 17/9/18.
 */
@Component
@ConfigurationProperties("getafix")
public class GetafixProperties {

    private final Async async = new Async();

    private final Scheduler scheduler = new Scheduler();

    private final Asterisk asterisk = new Asterisk();

    public Async getAsync() {
        return async;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public Asterisk getAsterisk() {
        return asterisk;
    }

    public static class Async {

        private final Asterisk asterisk = new Asterisk();
        private String name;
        private int corePoolSize = 2;
        private int maxPoolSize = 20;
        private int queueCapacity = 10000;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCorePoolSize() {
            return corePoolSize;
        }

        public void setCorePoolSize(int corePoolSize) {
            this.corePoolSize = corePoolSize;
        }

        public int getMaxPoolSize() {
            return maxPoolSize;
        }

        public void setMaxPoolSize(int maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
        }

        public int getQueueCapacity() {
            return queueCapacity;
        }

        public void setQueueCapacity(int queueCapacity) {
            this.queueCapacity = queueCapacity;
        }

        public Asterisk getAsterisk() {
            return asterisk;
        }

        public static class Asterisk {

            private String name;

            private int corePoolSize = 2;

            private int maxPoolSize = 50;

            private int queueCapacity = 10000;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getCorePoolSize() {
                return corePoolSize;
            }

            public void setCorePoolSize(int corePoolSize) {
                this.corePoolSize = corePoolSize;
            }

            public int getMaxPoolSize() {
                return maxPoolSize;
            }

            public void setMaxPoolSize(int maxPoolSize) {
                this.maxPoolSize = maxPoolSize;
            }

            public int getQueueCapacity() {
                return queueCapacity;
            }

            public void setQueueCapacity(int queueCapacity) {
                this.queueCapacity = queueCapacity;
            }
        }
    }

    public static class Scheduler {

        private String name;

        private int poolSize = 2;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPoolSize() {
            return poolSize;
        }

        public void setPoolSize(int poolSize) {
            this.poolSize = poolSize;
        }
    }

    public static class Asterisk {

        private final Asterisk.Dialplan dialplan = new Asterisk.Dialplan();

        public Asterisk.Dialplan getDialplan() {
            return dialplan;
        }

        public static class Dialplan {

            private final Map<String, String> general = new HashMap<>();

            private final Map<String, String> globals = new HashMap<>();

            private final List<String> getafix = new ArrayList<>();

            private final List<String> company = new ArrayList<>();

            private final List<String> queueexitcontext = new ArrayList<>();

            public Map<String, String> getGeneral() {
                return general;
            }

            public Map<String, String> getGlobals() {
                return globals;
            }

            public List<String> getGetafix() {
                return getafix;
            }

            public List<String> getCompany() {
                return company;
            }

            public List<String> getQueueexitcontext() {
                return queueexitcontext;
            }
        }
    }
}
