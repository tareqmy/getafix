package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-07-14.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"company"}, callSuper = true)
public class CallCenterLive extends Live {

    private Company company;

    private Map<User, AgentLive> agentLiveMap = new ConcurrentHashMap<>();

    private Map<String, QueueLive> queueLiveMap = new ConcurrentHashMap<>();

    public CallCenterLive(Company company) {
        super(company.getId());
        this.company = company;
    }

    @Override
    public String toString() {
        return "CallCenterLive{" +
            "company=" + company.getCompanyId() +
            ", agentLiveMap=" + agentLiveMap.keySet() +
            ", queueLiveMap=" + queueLiveMap.keySet() +
            '}';
    }
}
