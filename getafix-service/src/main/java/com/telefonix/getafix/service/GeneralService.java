package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-04-28.
 */
public interface GeneralService {

    User getCurrentLoggedInUser();

    Company getCompany(Long id);

    User getUser(String email);

    User getUser(Long id);

    Extension getExtension(Long id);

    Optional<Extension> findByCompanyIdAndBaseExtension(String companyId, String baseExtension);

    List<VoiceMail> findAllVoiceMails();

    List<VoiceMail> findAllEnabledVoiceMails();

    Phone getPhone(Long id);

    PhoneLine getPhoneLine(Long id);

    Optional<PhoneLine> findBySipUserName(String sipUserName);

    Queue getQueue(Long id);

    Optional<Queue> findByCompanyIdAndQueueId(String companyId, String queueId);

    Provider getProvider(Long id);

    PhoneNumber getPhoneNumber(String id);
}
