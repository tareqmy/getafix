package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.ReportService;
import com.telefonix.getfix.dto.ExtensionReportDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-07-01.
 */
@Slf4j
@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    private ExtensionService extensionService;

    @Autowired
    public ReportServiceImpl(ExtensionService extensionService) {
        this.extensionService = extensionService;
    }

    @Override
    public List<ExtensionReportDTO> getExtensionReports(User user, Extension extension) {

        List<ExtensionReportDTO> dtoList = new ArrayList<>();
        LocalDate from = LocalDate.now().minusDays(6);
        LocalDate to = LocalDate.now();

        List<ChannelLog> channelLogs = extensionService.getChannelLogs(extension.getId(), from, to, user.getZoneId());

        for (int i = 0; i < 7; i++) {
            LocalDate buffer = from.plusDays(i);
            ExtensionReportDTO dto = new ExtensionReportDTO();
            dto.setDayString(buffer.format(DateTimeFormatter.ofPattern("d MMM")));
            long outCalls = 0;
            long inCalls = 0;
            long outCallsDuration = 0;
            long inCallsDuration = 0;
            for (ChannelLog channelLog : channelLogs) {
                if (channelLog.getStart().isAfter(buffer.atStartOfDay(user.getZoneId()).toInstant())
                    && channelLog.getStart().isBefore(buffer.plusDays(1).atStartOfDay(user.getZoneId()).toInstant())) {
                    if (channelLog.isCaller()) {
                        outCallsDuration = outCallsDuration + channelLog.getDuration();
                        outCalls = outCalls + 1;
                    } else {
                        inCallsDuration = inCallsDuration + channelLog.getDuration();
                        inCalls = inCalls + 1;
                    }
                }
            }

            dto.setIncomingCallCount(inCalls);
            dto.setOutgoingCallCount(outCalls);
            dto.setCallCount(inCalls + outCalls);
            dto.setIncomingCallDuration(inCallsDuration);
            dto.setOutgoingCallDuration(outCallsDuration);
            dto.setCallDuration(inCallsDuration + outCallsDuration);
            dtoList.add(dto);
        }

        return dtoList;
    }
}
