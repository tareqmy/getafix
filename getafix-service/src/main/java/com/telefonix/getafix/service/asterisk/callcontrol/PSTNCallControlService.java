package com.telefonix.getafix.service.asterisk.callcontrol;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;

/**
 * Created by tareqmy on 2019-05-02.
 */
public interface PSTNCallControlService {

    CallEventListener handleCall(Channel channel, String accountCode, String exten, String context);

    CallEventListener handleOutboundCall(Channel channel, String accountCode, String exten, String context);
}
