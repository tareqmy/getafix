package com.telefonix.getafix.service;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.entity.QueueChannelLog;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getafix.service.live.entity.QueueChannelLive;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * Created by tareqmy on 4/9/19.
 */
public interface QueueChannelLogService {

    QueueChannelLog onQueueChannelStartQueue(QueueChannelLive queueChannelLive);

    void onQueueChannelLeave(QueueChannelLive queueChannelLive);

    void onQueueChannelAbandoned(QueueChannelLive queueChannelLive);

    void onQueueChannelEndQueue(QueueChannelLive queueChannelLive);

    void onQueueChannelEnd(ChannelLive channelLive);

    List<QueueChannelQueueLog> findAllByQueueId(Long queueId, LocalDate from, LocalDate to, ZoneId zoneId);
}
