package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getafix.service.live.entity.QueueChannelLive;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

/**
 * Created by tareqmy on 2019-07-17.
 */
@Getter
@Setter
@ToString
class QueueChannelLiveDTO extends ChannelLiveDTO {

    private boolean queued;

    private int position;

    private long wait;

    private Instant queuedInstant;

    private String queueCallDirection;

    QueueChannelLiveDTO(QueueChannelLive queueChannelLive, ZoneId zoneId) {
        super(queueChannelLive.getChannelLive(), zoneId);
        if (queueChannelLive.isLeft()) {
            setQueued(false);
            setWait(0L);
            setPosition(0);
        } else {
            setQueued(true);
            setQueuedInstant(queueChannelLive.getQueuedInstant());
            Duration wait = MiscUtils.getDuration(queueChannelLive.getQueuedInstant(), Instant.now());
            setWait(wait.toSeconds());
            setPosition(queueChannelLive.getPosition());
        }

        if (queueChannelLive.isOutbound()) {
            setQueueCallDirection(Constants.outgoingCall);
        } else {
            setQueueCallDirection(Constants.incomingCall);
        }
        setCallType(queueChannelLive.getChannelLive().getCallType());
    }
}
