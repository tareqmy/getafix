package com.telefonix.getafix.service;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;

import java.util.List;

/**
 * Created by tareqmy on 2019-07-28.
 */
public interface VoiceMailMessageService {

    List<AXVoiceMessage> getAllVoiceMailMessageInfo(Long extensionId);

    List<AXVoiceMessage> getAllVoiceMailMessageInfo(Long extensionId, String dir);

    AXVoiceMessage getVoiceMailMessageInfo(Long extensionId, String msgId);

    void deleteVoiceMailMessageInfo(Long extensionId, String msgId);

    void callbackVoiceMailMessage(Long extensionId, String msgId);
}
