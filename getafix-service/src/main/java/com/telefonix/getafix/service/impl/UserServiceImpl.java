package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.repository.EmailConfirmTokenRepository;
import com.telefonix.getafix.repository.PasswordResetTokenRepository;
import com.telefonix.getafix.repository.UserRepository;
import com.telefonix.getafix.service.*;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getafix.service.live.UserLiveRepository;
import com.telefonix.getfix.dto.AgentReportDTO;
import com.telefonix.getfix.dto.UserDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.RandomUtils;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private EmailService emailService;

    private UserRepository userRepository;

    private UserRoleService userRoleService;

    private GeneralService generalService;

    private PasswordEncoder passwordEncoder;

    private PasswordResetTokenRepository passwordResetTokenRepository;

    private EmailConfirmTokenRepository emailConfirmTokenRepository;

    private CallCenterLiveRepository callCenterLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    private UserLiveRepository userLiveRepository;

    private AgentLogService agentLogService;

    private EndpointLogService endpointLogService;

    private AgentReportService agentReportService;

    @Autowired
    public UserServiceImpl(EmailService emailService, UserRepository userRepository,
                           UserRoleService userRoleService, GeneralService generalService,
                           PasswordEncoder passwordEncoder, PasswordResetTokenRepository passwordResetTokenRepository,
                           EmailConfirmTokenRepository emailConfirmTokenRepository,
                           CallCenterLiveRepository callCenterLiveRepository,
                           AgentLiveRepository agentLiveRepository,
                           UserLiveRepository userLiveRepository,
                           AgentLogService agentLogService,
                           EndpointLogService endpointLogService,
                           AgentReportService agentReportService) {
        this.emailService = emailService;
        this.userRepository = userRepository;
        this.userRoleService = userRoleService;
        this.generalService = generalService;
        this.passwordEncoder = passwordEncoder;
        this.passwordResetTokenRepository = passwordResetTokenRepository;
        this.emailConfirmTokenRepository = emailConfirmTokenRepository;
        this.callCenterLiveRepository = callCenterLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.userLiveRepository = userLiveRepository;
        this.agentLogService = agentLogService;
        this.endpointLogService = endpointLogService;
        this.agentReportService = agentReportService;
    }

    @Override
    public User createUser(UserDTO userDTO) {
        verifyEmailUniqueness(userDTO.getEmail());

        UserRole userRole = userRoleService.getUserRole(userDTO.getUserRole());
        Company company = generalService.getCompany(userDTO.getCompanyId());
        User user = new User();
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userRole.getRole().equals(Role.ROLE_SYSTEM_ADMIN)) {
            throw new BusinessRuleViolationException("System Admin cant be created");
        }
        user.setUserRole(userRole);
        updateUserPassword(user, userDTO.getPassword(), true);
        user.setEnabled(userDTO.isEnabled());
        user.setLocked(userDTO.isLocked());
        user.setZoneId(userDTO.getZoneId());
        user.setCompany(company);
        log.debug("Created Information for User: {}", user);
        User save = userRepository.save(user);
        userLiveRepository.putUserLive(save);
        return save;
    }

    private void updateUserPassword(User user, String password, boolean checkStrength) {
        if (checkStrength)
            verifyPasswordStrength(password);

        if (passwordEncoder.matches(password, user.getPassword())) {
            throw new BusinessRuleViolationException("Cant save the same password!");
        }

        String encryptedPassword = passwordEncoder.encode(password);
        user.setPassword(encryptedPassword);
        emailService.sendPasswordChangedMail(user.getEmail());
    }

    private void verifyEmailUniqueness(String email) {
        userRepository.findOneByEmail(email).ifPresent(user -> {
            throw new BusinessRuleViolationException("This email address is already taken. Choose another. Email: " + email);
        });
    }

    @Override
    public User updateUserInformation(UserDTO userDTO) {
        User user = findOne(userDTO.getId())
            .orElseThrow(() -> new GenericGetafixException("No user found with id: " + userDTO.getId()));

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEnabled(userDTO.isEnabled());
        user.setLocked(userDTO.isLocked());
        user.setZoneId(userDTO.getZoneId());
        User save = userRepository.save(user);
        userLiveRepository.putUserLive(save);
        return save;
    }

    @Override
    public void changePassword(Long id, String password) {
        findOne(id).ifPresent(user -> {
            changeUserPassword(user.getEmail(), password);
        });
    }

    @Override
    public void verifyOldPassword(String email, String password) {
        userRepository.findOneByEmail(email).ifPresent(user -> {
            if (!passwordEncoder.matches(password, user.getPassword())) {
                throw new BusinessRuleViolationException("Old password is not valid");
            }
        });
    }

    @Override
    public void changeUserPassword(String email, String password) {
        userRepository.findOneByEmail(email).ifPresent(user -> {
            updateUserPassword(user, password, true);
            userRepository.save(user);
            log.debug("Change password for user: {}", user);
        });
    }

    @Override
    public Optional<User> findOne(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User getUser(String email) {
        return userRepository.findOneByEmail(email).orElseThrow(
            () -> new GenericGetafixException("Current Logged user may not be active or found!")
        );
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid user id " + id)
        );
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<User> findByJoinedPhoneLineNotNull() {
        return userRepository.findByJoinedPhoneLineNotNull()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<User> getUsers(Company company) {
        return userRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getUsers().forEach(this::delete);
    }

    private void delete(User user) {
        //1. never delete system admin
        if (user.getUserRole().getRole().equals(Role.ROLE_SYSTEM_ADMIN)) {
            throw new BusinessRuleViolationException("System Admin cant be deleted");
        }

        //2. cant delete self
        SecurityUtils.getCurrentUserLogin().ifPresent(s -> {
            if (user.getEmail().equals(s)) {
                throw new BusinessRuleViolationException("You cant delete yourself!");
            }
        });

        //3. before user delete check being used by agent
        if (agentLiveRepository.isLiveAgent(user)) {
            throw new BusinessRuleViolationException("This user is currently live in callcenter!");
        }

        callCenterLiveRepository.deleteAgentLive(user);
        agentLiveRepository.removeAgentLive(user.getJoinedPhoneLine());
        userLiveRepository.removeUserLive(user);
        userRepository.delete(user);
        log.debug("Deleted user: {}", user);
    }

    private void verifyPasswordStrength(String password) {

        if (StringUtils.isEmpty(password)) {
            throw new BusinessRuleViolationException("Password cannot be empty!");
        }

        // 1. Web portal password must contain at least one numeric and one non-numeric characters, and must be at least 8 characters in length.
        if (password.length() < 8) {
            throw new BusinessRuleViolationException("Password length must be atleast 8.");
        }
    }

    @Override
    public void initiatePasswordReset(String email) {
        userRepository.findOneByEmail(email).ifPresent(user -> {
            if (!user.isEnabled() || user.isLocked()) {
                throw new BusinessRuleViolationException("Cannot reset password. User is disabled/locked!");
            }
            if (!user.isEmailConfirmed()) {
                throw new BusinessRuleViolationException("Cannot reset password. Please confirm email first!");
            }
            passwordResetTokenRepository.deleteAllByUser(user);
            PasswordResetToken passwordResetToken = new PasswordResetToken();
            passwordResetToken.setUser(user);
            passwordResetToken.setExpiryDate(ZonedDateTime.now().plusHours(1));
            passwordResetToken.setToken(RandomUtils.generateResetKey());
            passwordResetTokenRepository.save(passwordResetToken);
            emailService.sendPasswordResetMail(user.getEmail(), passwordResetToken.getToken());
        });
    }

    @Override
    public boolean completePasswordReset(String token, String email, String newPassword) {
        return userRepository.findOneByEmail(email).map(user -> {
            if (!user.isEnabled() || user.isLocked()) {
                throw new BusinessRuleViolationException("Cannot reset password. User is disabled/locked!");
            }
            if (passwordResetTokenRepository.findAllByUserAndToken(user, token)
                .stream()
                .anyMatch(passwordResetToken ->
                    passwordResetToken.getExpiryDate().isAfter(ZonedDateTime.now()))) {
                updateUserPassword(user, newPassword, true);
                userRepository.save(user);
                passwordResetTokenRepository.deleteAllByUser(user);
                return true;
            }
            return false;
        }).orElse(false);
    }

    @Override
    public void initiateEmailConfirm(String email) {
        userRepository.findOneByEmail(email).ifPresent(user -> {
            if (!user.isEnabled() || user.isLocked()) {
                throw new BusinessRuleViolationException("Cannot confirm email. User is disabled/locked!");
            }
            if (user.isEmailConfirmed()) {
                throw new BusinessRuleViolationException("Email is already confirmed!");
            }
            passwordResetTokenRepository.deleteAllByUser(user);
            EmailConfirmToken emailConfirmToken = new EmailConfirmToken();
            emailConfirmToken.setUser(user);
            emailConfirmToken.setExpiryDate(ZonedDateTime.now().plusHours(1));
            emailConfirmToken.setToken(RandomUtils.generateResetKey());
            emailConfirmTokenRepository.save(emailConfirmToken);
            emailService.sendEmailConfirmMail(user.getEmail(), emailConfirmToken.getToken());
        });
    }

    @Override
    public boolean completeEmailConfirm(String token, String email) {
        return userRepository.findOneByEmail(email).map(user -> {
            if (!user.isEnabled() || user.isLocked()) {
                throw new BusinessRuleViolationException("Cannot confirm email. User is disabled/locked!");
            }
            if (emailConfirmTokenRepository.findAllByUserAndToken(user, token)
                .stream()
                .anyMatch(passwordResetToken ->
                    passwordResetToken.getExpiryDate().isAfter(ZonedDateTime.now()))) {
                user.setEmailConfirmed(true);
                userRepository.save(user);
                emailConfirmTokenRepository.deleteAllByUser(user);
                return true;
            }
            return false;
        }).orElse(false);
    }

    @Override
    @Scheduled(cron = "${getafix.scheduled.removeExpiredTokens}")
    public void removeExpiredTokens() {
        ZonedDateTime now = ZonedDateTime.now();
        passwordResetTokenRepository.deleteAllByExpiryDateBefore(now);
        emailConfirmTokenRepository.deleteAllByExpiryDateBefore(now);
    }

    @Override
    public List<AgentLog> getAgentLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        return agentLogService.getAgentLogs(id, from, to, zoneId);
    }

    @Override
    public AgentReportDTO getAgentReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        User user = getUser(id);
        return agentReportService.getAgentReport(user, from, to, zoneId);
    }

    @Override
    public List<EndpointLog> getEndpointLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        User user = getUser(id);
        List<String> sipUserNames = user.getExtensions().stream().map(Extension::getPhoneLine)
            .filter(Objects::nonNull)
            .map(PhoneLine::getSipUserName)
            .collect(Collectors.toList());
        return endpointLogService.getEndpointLogs(sipUserNames, from, to, zoneId);
    }
}
