package com.telefonix.getafix.service.live;

import com.telefonix.getafix.entity.User;

/**
 * Created by tareqmy on 10/11/19.
 */
public interface UserLiveEventListener {

    void onUserLeftContactCenter(User user);
}
