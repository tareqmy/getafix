package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Created by tareqmy on 2019-07-23.
 */
@Getter
@Setter
@ToString
class PSEndpointLiveDTO extends LiveDTO {

    private String technology;

    private String resource;

    private String state;

    private String peerStatus;

    private String callerId;

    private Instant updateInstant;

    private String updateTime;

    PSEndpointLiveDTO(PSEndpointLive psEndpointLive, ZoneId zoneId) {
        super(zoneId);
        setTechnology(psEndpointLive.getTechnology());
        setResource(psEndpointLive.getResource());
        setState(psEndpointLive.getState());
        setPeerStatus(psEndpointLive.getPeerStatus());
        setCallerId(psEndpointLive.getPsEndpoint().getCallerId());
        setUpdateInstant(psEndpointLive.getUpdateInstant());
        setUpdateTime(psEndpointLive.getUpdateInstant()
            .atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("HH:mm:ss")));
    }
}
