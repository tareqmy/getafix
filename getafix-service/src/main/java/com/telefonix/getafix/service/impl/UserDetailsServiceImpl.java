package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Slf4j
@Service(value = "getafixUserDetailsService")
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String email) {
        log.info("User sign in requested by " + email);
        return userRepository.findOneByEmail(email).map(user -> {
            log.info("Found user {}", user);
            return user;
        }).orElseThrow(() -> new UsernameNotFoundException("User " + email + " was not found in the " +
            "database"));
    }
}
