package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;

/**
 * Created by tareqmy on 2019-05-20.
 */
@Slf4j
@Service
@Transactional
public class FileServiceImpl implements FileService {

    private String path;

    private RestTemplate restTemplate;

    private String URI_CREATEBUCKET;

    private String URI_DELETE;

    private String URI_UPLOAD;

    private String URI_DOWNLOAD;

    private String tmp = "/tmp";

    @Autowired
    public FileServiceImpl(@Value(value = "${getafix.obelix.rootURI}") String rootURI,
                           @Value(value = "${getafix.obelix.username}") String username,
                           @Value(value = "${getafix.obelix.password}") String password,
                           @Value(value = "${getafix.obelix.path}") String path) {

        this.path = path;

        RestTemplateBuilder builder = new RestTemplateBuilder();
        this.restTemplate = builder.basicAuthentication(username, password).build();

        this.URI_CREATEBUCKET = rootURI + "/api/files/make/directory" + "?path={path}&fileName={fileName}";

        this.URI_DELETE = rootURI + "/api/files/delete" + "?path={path}&fileName={fileName}";

        this.URI_UPLOAD = rootURI + "/api/files/upload" + "?path={path}&fileName={fileName}";

        this.URI_DOWNLOAD = rootURI + "/api/files/download" + "?path={path}&fileName={fileName}";
    }

    @Override
    public void createBucket(String bucket) {
        restTemplate.put(URI_CREATEBUCKET, String.class, path, bucket);
    }

    @Override
    public void deleteBucket(String bucket) {
        //todo: folder deletion is not supported by obelix yet
        restTemplate.delete(URI_DELETE, path, bucket);
    }

    @Override
    public void upload(String bucket, String name, File file) {
        try {
            createBucket(bucket);
            String pathWithBucket = path + "/" + bucket;

            //upload to obelix
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", new FileSystemResource(file));

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, httpHeaders);

            ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(URI_UPLOAD, requestEntity, String.class, pathWithBucket, name);
            log.trace(stringResponseEntity.toString());

        } catch (Exception e) {
            throw new RuntimeException("Failed to upload media");
        }
    }

    @Override
    public void deleteFile(String bucket, String name) {
        String pathWithBucket = path + "/" + bucket;
        restTemplate.delete(URI_DELETE, pathWithBucket, name);
    }
}
