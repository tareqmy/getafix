package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.repository.CompanyRepository;
import com.telefonix.getafix.service.*;
import com.telefonix.getafix.service.asterisk.GetafixDialplanService;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getfix.dto.CompanyDTO;
import com.telefonix.getfix.dto.CompanyReportDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 3/1/19.
 */
@Slf4j
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    private CompanyRepository companyRepository;

    private UserService userService;

    private ExtensionService extensionService;

    private PhoneService phoneService;

    private PhoneNumberService phoneNumberService;

    private QueueService queueService;

    private AutoAttendantService autoAttendantService;

    private ConferenceService conferenceService;

    private CompanyReportService companyReportService;

    private GetafixDialplanService getafixDialplanService;

    private CallCenterLiveRepository callCenterLiveRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository,
                              UserService userService,
                              ExtensionService extensionService,
                              PhoneService phoneService,
                              PhoneNumberService phoneNumberService,
                              QueueService queueService,
                              AutoAttendantService autoAttendantService,
                              ConferenceService conferenceService,
                              CompanyReportService companyReportService,
                              GetafixDialplanService getafixDialplanService,
                              CallCenterLiveRepository callCenterLiveRepository) {
        this.companyRepository = companyRepository;
        this.userService = userService;
        this.extensionService = extensionService;
        this.phoneService = phoneService;
        this.phoneNumberService = phoneNumberService;
        this.queueService = queueService;
        this.autoAttendantService = autoAttendantService;
        this.conferenceService = conferenceService;
        this.companyReportService = companyReportService;
        this.getafixDialplanService = getafixDialplanService;
        this.callCenterLiveRepository = callCenterLiveRepository;
    }

    private void verifyCustomerId(String companyId) {
        findOne(companyId).ifPresent(customer -> {
            throw new BusinessRuleViolationException("This companyId is taken " + companyId);
        });
    }

    @Override
    public Company createCompany(CompanyDTO companyDTO) {
        verifyCustomerId(companyDTO.getCompanyId());
        Company company = new Company();
        company.setCompanyId(companyDTO.getCompanyId().toLowerCase());
        getafixDialplanService.onCompanyCreated(company);

        Company save = processCompany(companyDTO, company);
        callCenterLiveRepository.initCallCenterLive(save);
        return save;
    }

    private Company processCompany(CompanyDTO companyDTO, Company company) {
        company.setCompanyName(companyDTO.getCompanyName());
        company.setDescription(companyDTO.getDescription());
        company.setEnabled(companyDTO.isEnabled());
        company.setZoneId(companyDTO.getZoneId());
        company.setEmail(companyDTO.getEmail());
        company.setPhone(companyDTO.getPhone());
        company.setNotes(companyDTO.getNotes());
        return companyRepository.save(company);
    }

    @Override
    public Company updateCompany(CompanyDTO companyDTO) {
        Company company = getCompany(companyDTO.getId());
        Company updated = processCompany(companyDTO, company);
        callCenterLiveRepository.updateCallCenterLive(updated);
        return updated;
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Page<Company> findAll(Pageable pageable) {
        return companyRepository.findAll(pageable);
    }

    @Override
    public Optional<Company> findOne(String companyId) {
        return companyRepository.findByCompanyId(companyId);
    }

    @Override
    public Optional<Company> findOne(Long id) {
        return companyRepository.findById(id);
    }

    @Override
    public Company getCompany(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid company id " + id)
        );
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(company -> {
            autoAttendantService.delete(company); //simple
            phoneService.delete(company); //deletes pl
            queueService.delete(company); //complicated
            phoneNumberService.delete(company); //simple
            extensionService.delete(company); //needs aa,q,pl,pn to be deleted first
            userService.delete(company); //really complicated

            callCenterLiveRepository.removeCallCenterLive(company);

            companyRepository.delete(company);

            getafixDialplanService.onCompanyDeleted(company);
            log.debug("Deleted company: {}", company);
        });
    }

    @Override
    public List<User> getUsers(Long id) {
        Company company = getCompany(id);
        return userService.getUsers(company);
    }

    @Override
    public List<Extension> getExtensions(Long id, boolean unAssigned) {
        Company company = getCompany(id);
        return extensionService.getExtensions(company, unAssigned);
    }

    @Override
    public List<Phone> getPhones(Long id) {
        Company company = getCompany(id);
        return phoneService.getPhones(company);
    }

    @Override
    public List<PhoneNumber> getPhoneNumbers(Long id) {
        Company company = getCompany(id);
        return phoneNumberService.getPhoneNumbers(company);
    }

    @Override
    public List<Queue> getQueues(Long id) {
        Company company = getCompany(id);
        return queueService.getQueues(company);
    }

    @Override
    public List<AutoAttendant> getAutoAttendants(Long id) {
        Company company = getCompany(id);
        return autoAttendantService.getAutoAttendants(company);
    }

    @Override
    public List<Conference> getConferences(Long id) {
        Company company = getCompany(id);
        return conferenceService.getConference(company);
    }

    @Override
    public void rebuildAllDialplan() {
        getafixDialplanService.rebuildDialplans();
        companyRepository.findAll().forEach(company -> getafixDialplanService.rebuildDialplanForCompany(company));
    }

    @Override
    public CompanyReportDTO getCompanyReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        Company company = getCompany(id);
        return companyReportService.getCompanyReport(company, from, to, zoneId);
    }
}
