package com.telefonix.getafix.service.live;

import com.telefonix.getafix.asterisk.service.live.ChannelLiveEventListener;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveEventListener;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import com.telefonix.getafix.service.live.entity.SystemLive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by tareqmy on 2019-07-10.
 */
@Slf4j
@Component
public class SystemLiveRepository implements ChannelLiveEventListener, PSEndpointLiveEventListener {

    private SystemLive systemLive;

    private List<SystemLiveEventListener> systemLiveEventListeners;

    private ChannelLiveRepository channelLiveRepository;

    @Autowired
    public SystemLiveRepository(PSEndpointLiveRepository psEndpointLiveRepository,
                                ChannelLiveRepository channelLiveRepository) {
        this.channelLiveRepository = channelLiveRepository;
        this.systemLive = new SystemLive();
        this.systemLiveEventListeners = new ArrayList<>();
        channelLiveRepository.addChannelLiveEventListener(this);
        psEndpointLiveRepository.addPSEndpointLiveEventListener(this);
    }

    public void addSystemLiveEventListener(SystemLiveEventListener systemLiveEventListener) {
        systemLiveEventListeners.add(systemLiveEventListener);
    }

    private void notifyListeners() {
        systemLiveEventListeners.forEach(systemLiveEventListener -> systemLiveEventListener.onEvent(systemLive));
    }

    public SystemLive getSystemLive() {
        return systemLive;
    }

    @Async
    @Override
    public void onChannelStart(ChannelLive channelLive) {
        systemLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
        notifyListeners();
    }

    @Async
    @Override
    public void onChannelAnswered(ChannelLive channelLive) {
        systemLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
        notifyListeners();
    }

    @Async
    @Override
    public void onChannelAppStart(ChannelLive channelLive) {
        systemLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
        notifyListeners();
    }

    @Async
    @Override
    public void onChannelAppStop(ChannelLive channelLive) {
        systemLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
        notifyListeners();
    }

    @Async
    @Override
    public void onChannelEnd(ChannelLive channelLive) {
        systemLive.getChannelLiveMap().remove(channelLive.getUniqueID());
        notifyListeners();
    }

    @Async
    @Override
    public void onPSEndpointStateUpdate(PSEndpointLive psEndpointLive) {
        systemLive.getPsEndpointLiveMap().put(psEndpointLive.getResource(), psEndpointLive);
        notifyListeners();
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void garbageCollection() {
        Iterator<Map.Entry<String, ChannelLive>> iterator = systemLive.getChannelLiveMap().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, ChannelLive> next = iterator.next();
            ChannelLive channelLive = next.getValue();
            ChannelLive live = channelLiveRepository.getChannelLive(channelLive.getUniqueID());
            if (live == null) {
                onChannelEnd(channelLive);
            }
        }
    }
}
