package com.telefonix.getafix.service.live;

import com.telefonix.getafix.asterisk.service.ami.processor.QueueActionResponseProcessor;
import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveRepository;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveRepository;
import com.telefonix.getafix.service.ChannelLogService;
import com.telefonix.getafix.service.CompanyService;
import com.telefonix.getafix.service.QueueService;
import com.telefonix.getafix.service.UserService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.CallCenterLive;
import com.telefonix.getafix.service.live.entity.QueueLive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-07-16.
 * directions of methods
 * 1. initialization(creation and application start), update and deletion
 * 2. user action from resources > goes towards asterisk
 * 3. events from asterisk > goes to any live listeners
 * 4. live entities containing minor live entities and have them all reflect to changes
 */
@Slf4j
@Component
public class LiveObjectInitiator implements ApplicationListener<ContextRefreshedEvent>,
    CallCenterLiveEventListener, QueueLiveEventListener, AgentLiveEventListener {

    private CallCenterLiveRepository callCenterLiveRepository;

    private QueueLiveRepository queueLiveRepository;

    private QueueActionResponseProcessor queueActionResponseProcessor;

    private AgentLiveRepository agentLiveRepository;

    private VoiceMailLiveRepository voiceMailLiveRepository;

    private PSEndpointLiveRepository psEndpointLiveRepository;

    private AXVoiceMailLiveRepository axVoiceMailLiveRepository;

    private ChannelLiveRepository channelLiveRepository;

    private UserLiveRepository userLiveRepository;

    private CompanyService companyService;

    private QueueService queueService;

    private UserService userService;

    private ChannelLogService channelLogService;

    @Autowired
    public LiveObjectInitiator(CallCenterLiveRepository callCenterLiveRepository,
                               QueueLiveRepository queueLiveRepository,
                               QueueActionResponseProcessor queueActionResponseProcessor,
                               AgentLiveRepository agentLiveRepository,
                               PSEndpointLiveRepository psEndpointLiveRepository,
                               VoiceMailLiveRepository voiceMailLiveRepository,
                               AXVoiceMailLiveRepository axVoiceMailLiveRepository,
                               ChannelLiveRepository channelLiveRepository,
                               UserLiveRepository userLiveRepository,
                               CompanyService companyService,
                               QueueService queueService,
                               UserService userService,
                               ChannelLogService channelLogService) {
        callCenterLiveRepository.addCallCenterLiveEventListener(this);
        queueLiveRepository.addQueueLiveEventListener(this);
        agentLiveRepository.addAgentLiveEventListener(this);


        this.callCenterLiveRepository = callCenterLiveRepository;
        this.queueLiveRepository = queueLiveRepository;
        this.queueActionResponseProcessor = queueActionResponseProcessor;
        this.agentLiveRepository = agentLiveRepository;
        this.psEndpointLiveRepository = psEndpointLiveRepository;
        this.voiceMailLiveRepository = voiceMailLiveRepository;
        this.axVoiceMailLiveRepository = axVoiceMailLiveRepository;
        this.channelLiveRepository = channelLiveRepository;
        this.userLiveRepository = userLiveRepository;
        this.companyService = companyService;
        this.queueService = queueService;
        this.userService = userService;
        this.channelLogService = channelLogService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("Enter: onApplicationEvent()");
        StopWatch watch = new StopWatch();
        watch.start();
        axVoiceMailLiveRepository.initialize();
        voiceMailLiveRepository.initialize();
        userLiveRepository.initialize();

        companyService.findAll().forEach(callCenterLiveRepository::initCallCenterLive);
        queueService.findAll().forEach(callCenterLiveRepository::initQueueLive);

        //NOTE: an user must have an extension which has a phoneline to be eligible for being an agent
        //NOTE: an agent must join his callcenter to be an agentlive
        userService.findByJoinedPhoneLineNotNull().forEach(callCenterLiveRepository::initAgentLive);

        //NOTE: this should be done after callcenter, queue and agentlives are created so that everyone gets updated before application is started
        queueActionResponseProcessor.refresh();

        psEndpointLiveRepository.refresh();

        List<String> channelVariables = new ArrayList<>();
        channelVariables.add(CallControlProperties.GX_NUMBER);
        channelVariables.add(CallControlProperties.GX_QUEUEID);
        channelLiveRepository.init(channelVariables);
        channelLogService.refresh();
        watch.stop();
        log.info("Exit: onApplicationEvent() --- done in {} ms", watch.getTotalTimeMillis());
    }

    @Async
    @Override
    public void onEvent(CallCenterLive callCenterLive) {
        //is anybody interested in the live repo??
    }

    @Async
    @Override
    public void onEvent(QueueLive queueLive) {
        callCenterLiveRepository.onQueueLiveEvent(queueLive);
    }

    @Async
    @Override
    public void onEvent(AgentLive agentLive) {
        //is anybody interested in the live repo??
    }
}
