package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Provider;
import com.telefonix.getafix.repository.ProviderRepository;
import com.telefonix.getafix.service.ProviderService;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getfix.dto.ProviderDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Slf4j
@Service
@Transactional
public class ProviderServiceImpl implements ProviderService {

    private static List<String> reservedCategories = Arrays.asList("general", "default", "getafix", "company", "customer", "group",
        "system", "service", "admin", "globals", "global", "locals", "local", "parkedcalls");

    private ProviderRepository providerRepository;

    private EntityService entityService;

    @Autowired
    public ProviderServiceImpl(ProviderRepository providerRepository, EntityService entityService) {
        this.providerRepository = providerRepository;
        this.entityService = entityService;
    }

    @Override
    public Provider createProvider(ProviderDTO providerDTO) {
        verify(providerDTO);
        Provider provider = new Provider();
        provider.setProviderId(providerDTO.getProviderId().toLowerCase());
        provider.setHost(providerDTO.getHost());
        entityService.createSipEntity(provider);
        return providerRepository.save(provider);
    }

    @Override
    public Provider updateProvider(ProviderDTO providerDTO) {
        verify(providerDTO);
        Provider provider = getProvider(providerDTO.getId());
        provider.setHost(providerDTO.getHost());
        entityService.createSipEntity(provider);
        return providerRepository.save(provider);
    }

    private void verify(ProviderDTO dto) {
        if (reservedCategories.contains(dto.getProviderId())) {
            throw new BusinessRuleViolationException("This is a reserved name. Cant be used.");
        }

        providerRepository.findByProviderId(dto.getProviderId()).ifPresent(provider -> {
            if (!provider.getId().equals(dto.getId())) {
                throw new BusinessRuleViolationException("Provider id already in use: " + dto.getProviderId());
            }
        });

        if (!TelephonyUtils.isValidDomainName(dto.getHost())) {
            throw new BusinessRuleViolationException("Host is invalid domain name - " + dto.getHost());
        }
    }

    @Override
    public List<Provider> findAll() {
        return providerRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Page<Provider> findAll(Pageable pageable) {
        return providerRepository.findAll(pageable);
    }

    @Override
    public Optional<Provider> findOne(Long id) {
        return providerRepository.findById(id);
    }

    @Override
    public Optional<Provider> findOne(String providerId) {
        return providerRepository.findByProviderId(providerId);
    }

    @Override
    public Provider getProvider(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid provider id " + id)
        );
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(provider -> {
            entityService.deleteSipEntity(provider);
            providerRepository.delete(provider);
            log.debug("Deleted provider: {}", provider);

        });
    }
}
