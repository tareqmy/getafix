package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.PhoneNumber;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.service.*;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getafix.service.live.QueueLiveRepository;
import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.CallCenterLive;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-07-14.
 */
@Slf4j
@Service
@Transactional
public class CallCenterServiceImpl implements CallCenterService {

    private AccountService accountService;

    private GeneralService generalService;

    private CallCenterLiveRepository callCenterLiveRepository;

    private QueueLiveRepository queueLiveRepository;

    private AgentLiveRepository agentLiveRepository;

    private VoiceMailMessageService voiceMailMessageService;

    private CallControlService callControlService;

    @Autowired
    public CallCenterServiceImpl(AccountService accountService, GeneralService generalService,
                                 CallCenterLiveRepository callCenterLiveRepository,
                                 QueueLiveRepository queueLiveRepository,
                                 AgentLiveRepository agentLiveRepository,
                                 VoiceMailMessageService voiceMailMessageService,
                                 CallControlService callControlService) {
        this.accountService = accountService;
        this.generalService = generalService;
        this.callCenterLiveRepository = callCenterLiveRepository;
        this.queueLiveRepository = queueLiveRepository;
        this.agentLiveRepository = agentLiveRepository;
        this.voiceMailMessageService = voiceMailMessageService;
        this.callControlService = callControlService;
    }

    @Override
    public String getAgentSipUserName() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        return agentLiveRepository.getAgentSipUserName(currentLoggedInUser);
    }

    @Override
    public void join(Long phoneLineId) {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        PhoneLine phoneLine = generalService.getPhoneLine(phoneLineId);
        if (!phoneLine.getExtension().getUser().equals(currentLoggedInUser)) {
            throw new BusinessRuleViolationException("This phone line does not belong to you!");
        }

        callCenterLiveRepository.join(currentLoggedInUser, phoneLine);
    }

    @Override
    public void leave() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        callCenterLiveRepository.leave(currentLoggedInUser);
    }

    @Override
    public void pause() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        AgentLive agentLive = getAgentLive(currentLoggedInUser);
        if (agentLive.getStatusMap().isEmpty()) {
            throw new BusinessRuleViolationException("Please join a queue first!");
        }
        callCenterLiveRepository.pause(currentLoggedInUser);
    }

    @Override
    public void resume() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        callCenterLiveRepository.resume(currentLoggedInUser);
    }

    @Override
    public Map<Long, String> getQueuesMap() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        CallCenterLive callCenterLive = getCallCenterLive(currentLoggedInUser);
        return callCenterLive.getQueueLiveMap().entrySet().stream()
            .collect(Collectors.toMap(entry -> entry.getValue().getQueue().getId(), entry -> entry.getValue().getQueue().getQueueName()));
    }

    @Override
    public Map<Long, String> getAgentsMap() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        CallCenterLive callCenterLive = getCallCenterLive(currentLoggedInUser);
        return callCenterLive.getAgentLiveMap().entrySet().stream()
            .filter(userPhoneLineEntry -> userPhoneLineEntry.getKey().getUserRole().getRole().equals(Role.ROLE_USER))
            .collect(Collectors.toMap(entry -> entry.getKey().getId(), entry -> entry.getKey().getFullName()));
    }

    @Override
    public Map<Long, String> getSupervisorsMap() {
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        CallCenterLive callCenterLive = getCallCenterLive(currentLoggedInUser);
        return callCenterLive.getAgentLiveMap().entrySet().stream()
            .filter(userPhoneLineEntry -> userPhoneLineEntry.getKey().getUserRole().getRole().equals(Role.ROLE_ADMIN))
            .collect(Collectors.toMap(entry -> entry.getKey().getId(), entry -> entry.getKey().getFullName()));
    }

    @Override
    public void joinQueue(Long id) {
        Queue queue = generalService.getQueue(id);
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        if (!queue.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid queue id");
        }
        AgentLive agentLive = getAgentLive(currentLoggedInUser);
        if (agentLive.isPaused()) {
            throw new BusinessRuleViolationException("Please resume agent first");
        }
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        if (agentLive.getStatusMap().containsKey(queueName)) {
            log.info("Request for a already joined queue!");
            throw new BusinessRuleViolationException("You are already signed in!");
        } else {
            queueLiveRepository.joinQueue(queue, agentLive.getPhoneLine());
        }
    }

    @Override
    public void leaveQueue(Long id) {
        Queue queue = generalService.getQueue(id);
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        if (!queue.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid queue id");
        }
        AgentLive agentLive = getAgentLive(currentLoggedInUser);
        if (agentLive.isPaused()) {
            throw new BusinessRuleViolationException("Please resume agent first");
        }
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        if (agentLive.getStatusMap().containsKey(queueName)) {
            queueLiveRepository.leaveQueue(queue, agentLive.getPhoneLine());
        } else {
            log.info("Request for a already left queue!");
            throw new BusinessRuleViolationException("You are not signed in!");
        }
    }

    @Override
    public void callQueue(Long id, String destinationNumber) {
        Queue queue = generalService.getQueue(id);
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        if (!queue.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid queue id");
        }

        AgentLive agentLive = getAgentLive(currentLoggedInUser);
        if (agentLive.isPaused()) {
            throw new BusinessRuleViolationException("Please resume the agent first");
        }
        //should be on behalf of queue
        String queueNumber = queue.getExtension().getBaseExtension();
        PhoneNumber phoneNumber = queue.getExtension().getPhoneNumber();
        if (phoneNumber != null) {
            queueNumber = phoneNumber.getNumber();
        }
        String destinationCallerId = TelephonyUtils.getCallerId(queue.getQueueName(), queueNumber);
        callControlService.dial(Constants.queueCallTypePrefix + queue.getQueueName(), destinationNumber, destinationCallerId, agentLive.getPhoneLine().getExtension().getBaseExtension(), queue.getQueueId());
    }

    @Override
    public void kickOutAgentQueue(Long id, Long phoneLineId) {
        Queue queue = generalService.getQueue(id);
        PhoneLine phoneLine = generalService.getPhoneLine(phoneLineId);
        if (phoneLine.getJoinedUser() == null) {
            throw new BusinessRuleViolationException("This phone line is not used by any agent!");
        }
        AgentLive agentLive = getAgentLive(phoneLine.getJoinedUser());
        if (agentLive.isPaused()) {
            throw new BusinessRuleViolationException("Please resume agent first");
        }
        queueLiveRepository.leaveQueue(queue, phoneLine);
    }

    @Override
    public AXVoiceMessage getVoiceMailMessageInfo(Long id, String msgId) {
        Queue queue = generalService.getQueue(id);
        return voiceMailMessageService.getVoiceMailMessageInfo(queue.getExtension().getId(), msgId);
    }

    @Override
    public void callbackVoiceMailMessage(Long id, String msgId) {
        Queue queue = generalService.getQueue(id);
        User currentLoggedInUser = accountService.findCurrentLoggedInUser();
        if (!queue.getCompany().equals(currentLoggedInUser.getCompany())) {
            throw new BusinessRuleViolationException("Invalid queue id");
        }

        AgentLive agentLive = getAgentLive(currentLoggedInUser);
        AXVoiceMessage axVoiceMessage = voiceMailMessageService.getVoiceMailMessageInfo(queue.getExtension().getId(), msgId);
        String callerName = TelephonyUtils.getCallerName(axVoiceMessage.getCallerId());
        String callerNumber = TelephonyUtils.getCallerNumber(axVoiceMessage.getCallerId());

        //should be on behalf of queue
        String queueNumber = queue.getExtension().getBaseExtension();
        PhoneNumber phoneNumber = queue.getExtension().getPhoneNumber();
        if (phoneNumber != null) {
            queueNumber = phoneNumber.getNumber();
        }
        String destinationCallerId = TelephonyUtils.getCallerId(queue.getQueueName(), queueNumber);
        callControlService.dial(Constants.queueCallTypePrefix + queue.getQueueName(), callerNumber, destinationCallerId, agentLive.getPhoneLine().getExtension().getBaseExtension(), queue.getQueueId());
    }

    @Override
    public void deleteVoiceMailMessage(Long id, String msgId) {
        Queue queue = generalService.getQueue(id);
        voiceMailMessageService.deleteVoiceMailMessageInfo(queue.getExtension().getId(), msgId);
    }

    private CallCenterLive getCallCenterLive(User user) {
        CallCenterLive callCenterLive = callCenterLiveRepository.getCallCenterLive(user.getCompany().getCompanyId());
        AgentLive agentLive = callCenterLive.getAgentLiveMap().get(user);
        if (agentLive == null) {
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }
        return callCenterLive;
    }

    private AgentLive getAgentLive(User user) {
        AgentLive agentLive = agentLiveRepository.getAgentLive(user);
        if (agentLive == null) {
            //because any logged in agent should have a live entry in agentlive repo
            throw new BusinessRuleViolationException("You are not logged in this callcenter!");
        }
        return agentLive;
    }
}
