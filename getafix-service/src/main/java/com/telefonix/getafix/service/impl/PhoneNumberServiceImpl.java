package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.PhoneNumber;
import com.telefonix.getafix.entity.Provider;
import com.telefonix.getafix.repository.PhoneNumberRepository;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.PhoneNumberService;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getfix.dto.PhoneNumberDTO;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Slf4j
@Service
@Transactional
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private PhoneNumberRepository phoneNumberRepository;

    private EntityService entityService;

    private GeneralService generalService;

    @Autowired
    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository,
                                  EntityService entityService,
                                  GeneralService generalService) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.entityService = entityService;
        this.generalService = generalService;
    }

    @Override
    public PhoneNumber createPhoneNumber(PhoneNumberDTO phoneNumberDTO) {
        Company company = generalService.getCompany(phoneNumberDTO.getCompanyId());
        Extension extension = generalService.getExtension(phoneNumberDTO.getExtensionId());
        Provider provider = generalService.getProvider(phoneNumberDTO.getProviderId());
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber(phoneNumberDTO.getNumber());
        phoneNumber.setPassword(phoneNumberDTO.getPassword());
        phoneNumber.setProvider(provider);
        phoneNumber.setCompany(company);
        phoneNumber.setExtension(extension);
        entityService.createSipEntity(phoneNumber);
        return phoneNumberRepository.save(phoneNumber);
    }

    @Override
    public PhoneNumber updatePhoneNumber(PhoneNumberDTO phoneNumberDTO) {
        PhoneNumber phoneNumber = getPhoneNumber(phoneNumberDTO.getNumber());
        Extension extension = generalService.getExtension(phoneNumberDTO.getExtensionId());
        phoneNumber.setExtension(extension);
        phoneNumber.setPassword(phoneNumberDTO.getPassword());
        entityService.createSipEntity(phoneNumber);
        return phoneNumberRepository.save(phoneNumber);
    }

    @Override
    public List<PhoneNumber> findAll() {
        return phoneNumberRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<PhoneNumber> findOne(String id) {
        return phoneNumberRepository.findById(id);
    }

    @Override
    public PhoneNumber getPhoneNumber(String id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phoneNumber id " + id)
        );
    }

    @Override
    public List<PhoneNumber> getPhoneNumbers(Company company) {
        return phoneNumberRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getPhoneNumbers().forEach(this::delete);
    }

    private void delete(PhoneNumber phoneNumber) {
        phoneNumberRepository.delete(phoneNumber);
        log.debug("Deleted phoneNumber: {}", phoneNumber);
    }
}
