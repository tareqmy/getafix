package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.VoiceMail;
import com.telefonix.getafix.repository.VoiceMailRepository;
import com.telefonix.getafix.service.VoiceMailService;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getafix.service.live.VoiceMailLiveRepository;
import com.telefonix.getfix.dto.VoiceMailDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-23.
 */
@Slf4j
@Service
@Transactional
public class VoiceMailServiceImpl implements VoiceMailService {

    private VoiceMailRepository voiceMailRepository;

    private EntityService entityService;

    private VoiceMailLiveRepository voiceMailLiveRepository;

    @Autowired
    public VoiceMailServiceImpl(VoiceMailRepository voiceMailRepository,
                                EntityService entityService,
                                VoiceMailLiveRepository voiceMailLiveRepository) {
        this.voiceMailRepository = voiceMailRepository;
        this.entityService = entityService;
        this.voiceMailLiveRepository = voiceMailLiveRepository;
    }

    @Override
    public VoiceMail updateVoiceMail(VoiceMailDTO voiceMailDTO) {
        VoiceMail voiceMail = getVoiceMail(voiceMailDTO.getId());
        if (voiceMail.getExtension().getQueue() != null) {
            throw new BusinessRuleViolationException("This voicemail belongs to a Queue. Cant be edited");
        }
        voiceMail.setFullName(voiceMailDTO.getFullName());
        voiceMail.setEmail(voiceMailDTO.getEmail());

        if (voiceMailDTO.isEnabled()) {
            //request to enable just enable it
            entityService.createSipEntity(voiceMail);
            voiceMailLiveRepository.initVoiceMailLive(voiceMail);
        } else {
            //if it was enabled before disable it
            if (voiceMail.isEnabled()) {
                voiceMailLiveRepository.removeVoiceMailLive(voiceMail);
                entityService.deleteSipEntity(voiceMail);
            }
        }

        voiceMail.setEnabled(voiceMailDTO.isEnabled());
        return voiceMailRepository.save(voiceMail);
    }

    @Override
    public void changePin(VoiceMail voiceMail, String pin) {
        if (voiceMail.isEnabled()) {
            //cannot be empty
            if (StringUtils.isEmpty(pin)) {
                throw new BusinessRuleViolationException("PIN cannot be empty!");
            }

            //must be digits only, atleast 5 digits
            if (!pin.matches("^\\d{5,32}$")) {
                throw new BusinessRuleViolationException("PIN must be at least 5 digits and only digits!");
            }

            entityService.updateSipEntity(voiceMail, pin);
        }
    }

    @Override
    public List<VoiceMail> findAll() {
        return voiceMailRepository.findAll();
    }

    @Override
    public Optional<VoiceMail> findOne(Long id) {
        return voiceMailRepository.findById(id);
    }

    @Override
    public VoiceMail getVoiceMail(Extension extension) {
        return voiceMailRepository.findByExtension(extension).orElseThrow(
            () -> new GenericGetafixException("Invalid extension " + extension)
        );
    }

    @Override
    public VoiceMail getVoiceMail(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid voicemail id " + id)
        );
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Extension extension) {
        delete(extension.getVoiceMail());
    }

    private void delete(VoiceMail voiceMail) {
        if (voiceMail.isEnabled()) {
            entityService.deleteSipEntity(voiceMail);
        }
        voiceMailRepository.delete(voiceMail);
        voiceMailLiveRepository.removeVoiceMailLive(voiceMail);
        log.debug("Deleted voiceMail: {}", voiceMail);
    }

    @Override
    public VoiceMail enable(Queue queue) {
        return voiceMailRepository.findByExtension(queue.getExtension())
            .map(voiceMail -> {
                voiceMail.setFullName(queue.getQueueName());
                voiceMail.setEmail(queue.getEmail());
                voiceMail.setEnabled(true);
                entityService.createSipEntity(voiceMail);
                voiceMailLiveRepository.initVoiceMailLive(voiceMail);
                return voiceMailRepository.save(voiceMail);
            }).orElseThrow(() -> {
                throw new IllegalStateException("It should be impossible to have an extension without voicemail!");
            });
    }

    @Override
    public VoiceMail update(Queue queue) {
        return voiceMailRepository.findByExtension(queue.getExtension())
            .map(voiceMail -> {
                voiceMail.setFullName(queue.getQueueName());
                voiceMail.setEmail(queue.getEmail());
                voiceMail.setEnabled(true);
                entityService.updateSipEntity(voiceMail);
                return voiceMailRepository.save(voiceMail);
            }).orElseThrow(() -> {
                throw new IllegalStateException("It should be impossible to have an extension without voicemail!");
            });
    }

    @Override
    public VoiceMail disable(Queue queue) {
        return voiceMailRepository.findByExtension(queue.getExtension())
            .map(voiceMail -> {
                voiceMailLiveRepository.removeVoiceMailLive(voiceMail);
                entityService.deleteSipEntity(voiceMail);
                voiceMail.setEnabled(false);
                voiceMail.setFullName(null);
                voiceMail.setEmail(null);
                return voiceMailRepository.save(voiceMail);
            }).orElseThrow(() -> {
                throw new IllegalStateException("It should be impossible to have an extension without voicemail!");
            });
    }
}
