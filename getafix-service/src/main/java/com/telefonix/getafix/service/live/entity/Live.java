package com.telefonix.getafix.service.live.entity;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * Created by tareqmy on 2019-07-15.
 */
@EqualsAndHashCode(of = {"id"})
public abstract class Live implements Serializable {

    /**
     * Id of the Live objects which will extend it. This Id will represent the id of the database
     * entity which this LiveObject represents. For example at any given time {@link QueueLive} will have
     * id 10 if the {@link com.telefonix.getafix.entity.Queue} which the {@link QueueLive} represents has id 10.
     */
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    Live(long id) {
        this.id = id;
    }
}
