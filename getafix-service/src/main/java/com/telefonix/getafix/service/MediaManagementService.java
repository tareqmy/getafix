package com.telefonix.getafix.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by tareqmy on 2019-05-22.
 */
public interface MediaManagementService {

    void upload(String companyId, String name, MultipartFile file);

    void uploadMoh(String companyId, String mohId, String name, MultipartFile file);

    void delete(String companyId, String name);

    void deleteMoh(String companyId, String mohId, String name);
}
