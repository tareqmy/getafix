package com.telefonix.getafix.service.asterisk;

import com.telefonix.getafix.entity.*;

/**
 * Created by tareqmy on 2019-01-24.
 */
public interface EntityService {

    void createSipEntity(PhoneLine phoneLine);

    void createSipEntity(Queue queue);

    void createSipEntity(Provider provider);

    void createSipEntity(PhoneNumber phoneNumber);

    void createSipEntity(VoiceMail voiceMail);

    void updateSipEntity(VoiceMail voiceMail);

    void updateSipEntity(VoiceMail voiceMail, String password);

    void updateSipEntity(Queue queue);

    void deleteSipEntity(PhoneLine phoneLine);

    void deleteSipEntity(Queue queue);

    void deleteSipEntity(Provider provider);

    void deleteSipEntity(PhoneNumber phoneNumber);

    void deleteSipEntity(VoiceMail voiceMail);
}
