package com.telefonix.getafix.service;

import java.io.File;

/**
 * Created by tareqmy on 2019-05-20.
 */
public interface FileService {

    //1. create folder
    void createBucket(String bucket);

    //2. delete folder
    void deleteBucket(String bucket);

    //3. upload file
    void upload(String bucket, String name, File file);

    //4. download file

    //5. delete file
    void deleteFile(String bucket, String name);
}
