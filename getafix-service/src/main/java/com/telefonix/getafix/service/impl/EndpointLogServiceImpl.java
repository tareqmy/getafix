package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.EndpointLog;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.repository.EndpointLogRepository;
import com.telefonix.getafix.service.EndpointLogService;
import com.telefonix.getafix.service.live.entity.DeviceStatus;
import com.telefonix.getafix.service.live.entity.EndpointStatus;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-08-20.
 */
@Slf4j
@Service
@Transactional
public class EndpointLogServiceImpl implements EndpointLogService {

    private EndpointLogRepository endpointLogRepository;

    private Map<String, EndpointLog> latestEndpointLogMap;

    @Autowired
    public EndpointLogServiceImpl(EndpointLogRepository endpointLogRepository) {
        this.endpointLogRepository = endpointLogRepository;
        this.latestEndpointLogMap = new ConcurrentHashMap<>();
    }

    @Async
    @Override
    public synchronized void onEndpointUpdate(String resource, String endpointStatus, String deviceStatus, PhoneLine phoneLine, User user) {
        if (resource == null) {
            log.error("Invalid resource");
            return;
        }
        if (endpointStatus == null) {
            endpointStatus = EndpointStatus.unknown.name();
        }
        if (deviceStatus == null) {
            deviceStatus = DeviceStatus.UNKNOWN.name();
        }

        EndpointLog cached = latestEndpointLogMap.get(resource);
        if (cached == null) {
            Optional<EndpointLog> latest = endpointLogRepository.findTopByResourceOrderByEventInstantDesc(resource);
            if (latest.isPresent()) {
                latestEndpointLogMap.put(resource, latest.get());
                if (endpointStatus.equals(latest.get().getEndpointState())
                    && deviceStatus.equals(latest.get().getDeviceState())) {
                    cached = latest.get();
                }
            }
        }

        if (cached != null) {
            if (endpointStatus.equals(cached.getEndpointState())
                && deviceStatus.equals(cached.getDeviceState())) {
                return;
            }
        }

        EndpointLog endpointLog = new EndpointLog();
        endpointLog.setEventInstant(Instant.now());
        endpointLog.setResource(resource);
        endpointLog.setCompanyId(TelephonyUtils.getCompanyId(resource));
        if (phoneLine != null) {
            endpointLog.setPhoneLineId(phoneLine.getId());
        }
        if (user != null) {
            endpointLog.setUserId(user.getId());
            endpointLog.setUserName(user.getFullName());
        }
        endpointLog.setEndpointState(endpointStatus);
        endpointLog.setDeviceState(deviceStatus);
        latestEndpointLogMap.put(resource, endpointLogRepository.save(endpointLog));
    }

    @Override
    public List<EndpointLog> getEndpointLogs(String sipUserName, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return endpointLogRepository.findByResourceAndStartBetween(sipUserName, fromDate, toDate);
    }

    @Override
    public List<EndpointLog> getEndpointLogs(List<String> sipUserNames, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return endpointLogRepository.findByResourceInAndStartBetween(sipUserNames, fromDate, toDate);
    }

    @Override
    public List<EndpointLog> getEndpointLogs(Long userId, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return endpointLogRepository.findByUserIdAndStartBetween(userId, fromDate, toDate);
    }
}
