package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.repository.PhoneLineRepository;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.PhoneLineService;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getfix.dto.PhoneLineDTO;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Slf4j
@Service
@Transactional
public class PhoneLineServiceImpl implements PhoneLineService {

    private PhoneLineRepository phoneLineRepository;

    private EntityService entityService;

    private GeneralService generalService;

    private AgentLiveRepository agentLiveRepository;

    @Autowired
    public PhoneLineServiceImpl(PhoneLineRepository phoneLineRepository,
                                EntityService entityService,
                                GeneralService generalService,
                                AgentLiveRepository agentLiveRepository) {
        this.phoneLineRepository = phoneLineRepository;
        this.entityService = entityService;
        this.generalService = generalService;
        this.agentLiveRepository = agentLiveRepository;
    }

    private void verifyExtension(Extension extension) {
        if (extension.getPhoneLine() != null) {
            throw new BusinessRuleViolationException("This extension is already assigned to a phone line.");
        }

        if (extension.getQueue() != null) {
            throw new BusinessRuleViolationException("This extension is already assigned to a queue.");
        }

        if (extension.getAutoAttendant() != null) {
            throw new BusinessRuleViolationException("This extension is already assigned to a auto attendant");
        }
    }

    @Override
    public PhoneLine createPhoneLine(PhoneLineDTO phoneLineDTO) {
        Phone phone = generalService.getPhone(phoneLineDTO.getPhoneId());
        Extension extension = generalService.getExtension(phoneLineDTO.getExtensionId());
        PhoneLine phoneLine = new PhoneLine();
        phoneLine.setDescription(phoneLineDTO.getDescription());
        phoneLine.setPhone(phone);
        phoneLine.setCallerName(phoneLineDTO.getCallerName());
        verifyExtension(extension);
        //if this is a new extension renew.
        if (!extension.equals(phoneLine.getExtension())) {
            phoneLine.setExtension(extension);
            phoneLine.setSipUserName(TelephonyUtils.getFullExtension(extension));
        }
        phoneLine.setSipPassword(phoneLineDTO.getSipPassword());
        phoneLine.setCallWaitingEnabled(phoneLineDTO.isCallWaitingEnabled());
        PhoneLine save = phoneLineRepository.save(phoneLine);
        entityService.createSipEntity(save);
        return save;
    }

    @Override
    public PhoneLine updatePhoneLine(PhoneLineDTO phoneLineDTO) {
        PhoneLine phoneLine = getPhoneLine(phoneLineDTO.getId());
        phoneLine.setDescription(phoneLineDTO.getDescription());
        phoneLine.setCallerName(phoneLineDTO.getCallerName());
        phoneLine.setSipPassword(phoneLineDTO.getSipPassword());
        phoneLine.setCallWaitingEnabled(phoneLineDTO.isCallWaitingEnabled());
        PhoneLine updated = phoneLineRepository.save(phoneLine);
        agentLiveRepository.updateAgentLive(updated);
        entityService.createSipEntity(updated);
        return updated;
    }

    @Override
    public List<PhoneLine> findAll() {
        return phoneLineRepository.findAll();
    }

    @Override
    public Optional<PhoneLine> findOne(Long id) {
        return phoneLineRepository.findById(id);
    }

    @Override
    public Optional<PhoneLine> findBySipUserName(String sipUserName) {
        return phoneLineRepository.findBySipUserName(sipUserName);
    }

    @Override
    public PhoneLine getPhoneLine(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phoneLine id " + id)
        );
    }

    @Override
    public List<PhoneLine> getPhoneLines(Extension extension) {
        return phoneLineRepository.findByExtensionOrderBySipUserNameAsc(extension)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<PhoneLine> getPhoneLines(Phone phone) {
        return phoneLineRepository.findByPhone(phone)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<PhoneLine> findByJoinedUserNotNull() {
        return phoneLineRepository.findByJoinedUserNotNull()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Phone phone) {
        phone.getPhoneLines().forEach(this::delete);
    }

    private void delete(PhoneLine phoneLine) {
        //before device line delete check being used by agent
        if (agentLiveRepository.isLiveAgent(phoneLine.getSipUserName())) {
            throw new BusinessRuleViolationException("This phone line is being used by live agent");
        }
        entityService.deleteSipEntity(phoneLine);
        agentLiveRepository.removeAgentLive(phoneLine);
        phoneLineRepository.delete(phoneLine);
        log.debug("Deleted phoneLine: {}", phoneLine);
    }
}
