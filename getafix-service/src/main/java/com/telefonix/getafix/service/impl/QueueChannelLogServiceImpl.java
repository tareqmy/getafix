package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueChannelLog;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getafix.entity.enums.QueueChannelLogStatus;
import com.telefonix.getafix.repository.QueueChannelLogRepository;
import com.telefonix.getafix.repository.QueueChannelQueueLogRepository;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.QueueChannelLogService;
import com.telefonix.getafix.service.live.entity.QueueChannelLive;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 4/9/19.
 */
@Slf4j
@Service
@Transactional
public class QueueChannelLogServiceImpl implements QueueChannelLogService {

    private QueueChannelLogRepository queueChannelLogRepository;

    private QueueChannelQueueLogRepository queueChannelQueueLogRepository;

    private GeneralService generalService;

    private Map<String, QueueChannelLog> queueChannelLogMap;

    private Map<String, QueueChannelQueueLog> lastQueueChannelLogMap;

    private ScheduledExecutorService scheduleExecutor;

    @Autowired
    public QueueChannelLogServiceImpl(QueueChannelLogRepository queueChannelLogRepository,
                                      QueueChannelQueueLogRepository queueChannelQueueLogRepository,
                                      GeneralService generalService,
                                      ScheduledExecutorService scheduleExecutor) {
        this.queueChannelLogRepository = queueChannelLogRepository;
        this.queueChannelQueueLogRepository = queueChannelQueueLogRepository;
        this.generalService = generalService;
        this.scheduleExecutor = scheduleExecutor;
        this.queueChannelLogMap = new ConcurrentHashMap<>();
        this.lastQueueChannelLogMap = new ConcurrentHashMap<>();
    }

    @Async
    @Override
    public synchronized QueueChannelLog onQueueChannelStartQueue(QueueChannelLive queueChannelLive) {
        QueueChannelLog queueChannelLog = queueChannelLogMap.get(queueChannelLive.getChannelLive().getUniqueID());
        ChannelLive channelLive = queueChannelLive.getChannelLive();
        if (queueChannelLog == null) {
            queueChannelLog = new QueueChannelLog();
            queueChannelLog.setUniqueId(channelLive.getUniqueID());
            queueChannelLog.setCompanyId(channelLive.getContext());
            queueChannelLog.setLinkedId(channelLive.getLinkedID());
            queueChannelLog.setStart(channelLive.getStartInstant());
            queueChannelLog.setStatus(QueueChannelLogStatus.IN_PROGRESS);
            queueChannelLogMap.put(queueChannelLog.getUniqueId(), queueChannelLog);
        }

        Optional<Queue> findQueue = generalService.findByCompanyIdAndQueueId(queueChannelLive.getCompanyId(), queueChannelLive.getQueueId());
        if (findQueue.isEmpty()) {
            log.error("Invalid queue found from queue channel live {}", queueChannelLive);
            return null;
        }
        Queue queue = findQueue.get();
        String baseExtension = queue.getExtension().getBaseExtension();
        QueueChannelQueueLog queueChannelQueueLog = getLast(queueChannelLog);
        if (queueChannelQueueLog == null) {
            queueChannelQueueLog = new QueueChannelQueueLog();
            queueChannelQueueLog.setQueueChannelLog(queueChannelLog);
            queueChannelQueueLog.setQueueId(queue.getId());
            queueChannelQueueLog.setBaseExtension(baseExtension);
            queueChannelQueueLog.setQueued(Instant.now());
            if (queueChannelLive.isOutbound()) {
                queueChannelQueueLog.setLeft(queueChannelQueueLog.getQueued());
            }
        } else {
            if (queueChannelQueueLog.getQueued() != null && queueChannelQueueLog.getLeft() != null) {
                queueChannelQueueLog = new QueueChannelQueueLog();
                queueChannelQueueLog.setQueueChannelLog(queueChannelLog);
                queueChannelQueueLog.setQueueId(queue.getId());
                queueChannelQueueLog.setBaseExtension(baseExtension);
                queueChannelQueueLog.setQueued(Instant.now());
                if (queueChannelLive.isOutbound()) {
                    queueChannelQueueLog.setLeft(queueChannelQueueLog.getQueued());
                }
            }
        }

        if (queueChannelLive.isOutbound()) {
            queueChannelLog.setFrom(baseExtension);
            queueChannelLog.setTo(channelLive.getCalledNumber());
            queueChannelLog.setCallerName(queue.getQueueName());
            queueChannelLog.setCallerNumber(baseExtension);
            queueChannelLog.setCalledNumber(channelLive.getCalledNumber());
            queueChannelLog.setOutbound(true);
            if (!TelephonyUtils.isValidExtension(channelLive.getCalledNumber())) {
                queueChannelLog.setExternal(true);
            }
        } else {
            queueChannelLog.setFrom(channelLive.getCallerIdNum());
            queueChannelLog.setTo(baseExtension);
            queueChannelLog.setCallerName(channelLive.getCallerIdName());
            queueChannelLog.setCallerNumber(channelLive.getCallerIdNum());
            queueChannelLog.setCalledNumber(channelLive.getCalledNumber());
            if (!TelephonyUtils.isValidInternalEntity(channelLive.getAccountCode())) {
                queueChannelLog.setExternal(true);
            }
        }

        queueChannelLogRepository.save(queueChannelLog);
        QueueChannelQueueLog save = queueChannelQueueLogRepository.save(queueChannelQueueLog);
        lastQueueChannelLogMap.put(channelLive.getUniqueID(), save);
        return queueChannelLog;
    }

    @Async
    @Override
    public synchronized void onQueueChannelLeave(QueueChannelLive queueChannelLive) {
        QueueChannelLog queueChannelLog = queueChannelLogMap.get(queueChannelLive.getChannelLive().getUniqueID());
        if (queueChannelLog == null) {
            log.error("Could not find queue channel log on leave {}", queueChannelLive);
        } else {
            QueueChannelQueueLog last = getLast(queueChannelLog);
            if (last != null) {
                last.setLeft(getEqualOrAfter(last.getQueued()));
                queueChannelQueueLogRepository.save(last);
            }
        }
    }

    private Instant getEqualOrAfter(Instant before) {
        Instant now = Instant.now();
        if (before == null) {
            log.info("Before is null so return now!");
            return now;
        }
        if (now.isBefore(before)) {
            return before;
        }
        return now;
    }

    @Async
    @Override
    public synchronized void onQueueChannelAbandoned(QueueChannelLive queueChannelLive) {
        QueueChannelLog queueChannelLog = queueChannelLogMap.get(queueChannelLive.getChannelLive().getUniqueID());
        if (queueChannelLog == null) {
            log.error("Could not find queue channel log on abandoned {}", queueChannelLive);
        } else {
            if (queueChannelLog.isOutbound()) {
                //outbound call cant be abandoned
                return;
            }
            QueueChannelQueueLog last = getLast(queueChannelLog);
            if (last != null) {
                last.setAbandoned(Instant.now());
                queueChannelQueueLogRepository.save(last);
            }
        }
    }

    @Async
    @Override
    public synchronized void onQueueChannelEndQueue(QueueChannelLive queueChannelLive) {
        if (queueChannelLive == null) {
            log.error("Invalid argument for onQueueChannelEndQueue");
            return;
        }
        QueueChannelLog queueChannelLog = queueChannelLogMap.get(queueChannelLive.getChannelLive().getUniqueID());
        if (queueChannelLog == null) {
            //happens in case of voicemail because of joinempty
            queueChannelLog = onQueueChannelStartQueue(queueChannelLive);
            if (queueChannelLog != null) {
                //NOTE: should never happen... because that means the queue is not valid
                QueueChannelQueueLog last = getLast(queueChannelLog);
                last.setLeft(last.getQueued());
                last.setAbandoned(last.getQueued());
                queueChannelQueueLogRepository.save(last);
            }
            return;
        } else {
            if (queueChannelLog.isOutbound()) {
                //outbound call cant be endqueued
                return;
            }
            QueueChannelQueueLog last = getLast(queueChannelLog);
            if (last != null) {
                if (last.getAbandoned() == null) {
                    //ensure handled is not before left. incase call was answered and handled pretty quick
                    last.setHandled(getEqualOrAfter(last.getLeft()));
                    if (queueChannelLog.getEnd() != null) {
                        last.setHandled(queueChannelLog.getEnd());
                    }
                    queueChannelQueueLogRepository.save(last);
                }
            }
        }
    }

    @Async
    @Override
    public synchronized void onQueueChannelEnd(ChannelLive channelLive) {
        QueueChannelLog queueChannelLog = queueChannelLogMap.get(channelLive.getUniqueID());
        if (queueChannelLog != null) {
            queueChannelLog.setEnd(Instant.now());
            QueueChannelQueueLog last = getLast(queueChannelLog);
            if (queueChannelLog.isOutbound()) {
                if (channelLive.getAnswerInstant() == null) {
                    queueChannelLog.setStatus(QueueChannelLogStatus.CALL_NOT_ANSWERED);
                    if (last != null) {
                        last.setLeft(channelLive.getEndInstant()); //for unanswered left is not updated through answer
                        last.setAbandoned(channelLive.getEndInstant());
                    }
                } else {
                    if (last != null) {
                        last.setHandled(channelLive.getEndInstant());
                    }
                    queueChannelLog.setStatus(QueueChannelLogStatus.CALL_ANSWERED);
                }
            } else {
                if (last != null && last.getAbandoned() != null) {
                    queueChannelLog.setStatus(QueueChannelLogStatus.CALL_ABANDONED);
                } else {
                    queueChannelLog.setStatus(QueueChannelLogStatus.CALL_ANSWERED);
                }
            }
            queueChannelLogRepository.save(queueChannelLog);
            if (last != null) {
                queueChannelQueueLogRepository.save(last);
            }
        }

        scheduleExecutor.schedule(() -> {
            queueChannelLogMap.remove(channelLive.getUniqueID());
            lastQueueChannelLogMap.remove(channelLive.getUniqueID());
        }, 3L, TimeUnit.SECONDS);
    }

    @Override
    public List<QueueChannelQueueLog> findAllByQueueId(Long queueId, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return queueChannelQueueLogRepository.findByQueueIdAndQueuedBetween(queueId, fromDate, toDate);
    }

    private QueueChannelQueueLog getLast(QueueChannelLog queueChannelLog) {
        return lastQueueChannelLogMap.get(queueChannelLog.getUniqueId());
    }
}
