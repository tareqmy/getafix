package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.asterisk.callcontrol.VoiceMailCallControlService;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-04-30.
 */
@Slf4j
@Service
@Transactional
public class VoiceMailCallControlServiceImpl extends AbstractCallControlServiceImpl implements VoiceMailCallControlService {

    private ExtensionService extensionService;

    @Autowired
    public VoiceMailCallControlServiceImpl(AsteriskResourceService asteriskResourceService, ExtensionService extensionService) {
        super(asteriskResourceService);
        this.extensionService = extensionService;
    }

    @Override
    public CallEventListener handleVoiceMailDrop(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        String baseExtension = exten.substring(1);
        String destination = extensionService.findByCompanyIdAndBaseExtension(context, baseExtension).map(extension -> {
            if (extension.getVoiceMail().isEnabled()) {
                vMap.put(CallControlProperties.GX_MAILBOX, baseExtension);
                vMap.put(CallControlProperties.GX_CONTEXT, context);
                vMap.put(CallControlProperties.GX_MAILBOX_OPTIONS, "u");
                return CallControlProperties.drop_voicemail_exten;
            }
            return CallControlProperties.invalid_exten; //todo: playMessageAndHangup
        }).orElse(CallControlProperties.invalid_exten);
        return new TerminalCall(accountCode, destination, getafix_context, null, vMap, asteriskResourceService);
    }

    @Override
    public CallEventListener handleVoiceMailCheck(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        String destination = CallControlProperties.invalid_exten;
        if (TelephonyUtils.isValidInternalEntity(accountCode)) {
            String baseExtension = TelephonyUtils.getBaseExtension(accountCode);
            String companyId = TelephonyUtils.getCompanyId(accountCode);
            destination = extensionService.findByCompanyIdAndBaseExtension(companyId, baseExtension).map(extension -> {
                vMap.put(CallControlProperties.GX_MAILBOX, baseExtension);
                vMap.put(CallControlProperties.GX_CONTEXT, context);
                vMap.put(CallControlProperties.GX_MAILBOX_OPTIONS, "s");
                return CallControlProperties.check_voicemail_exten;
            }).orElse(CallControlProperties.invalid_exten);
        }
        return new TerminalCall(accountCode, destination, getafix_context, null, vMap, asteriskResourceService);
    }
}
