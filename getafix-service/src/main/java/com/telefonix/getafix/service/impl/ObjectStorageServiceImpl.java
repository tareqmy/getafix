package com.telefonix.getafix.service.impl;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.telefonix.getafix.service.ObjectStorageService;
import com.telefonix.getfix.dto.SoundFile;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-12.
 */
@Slf4j
@Service
@Transactional
public class ObjectStorageServiceImpl implements ObjectStorageService {

    private final AmazonS3 amazonS3;

    @Autowired
    public ObjectStorageServiceImpl(@Value(value = "${getafix.objectStore.endpoint}") String endpoint,
                                    @Value(value = "${getafix.objectStore.region}") String region,
                                    @Value(value = "${getafix.objectStore.accessKey}") String accessKey,
                                    @Value(value = "${getafix.objectStore.secretKey}") String secretKey) {
        this.amazonS3 = AmazonS3ClientBuilder.standard()
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region))
            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
            .build();
    }

    //{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetBucketLocation","s3:ListBucket"],"Resource":["arn:aws:s3:::getafix"]},{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetObject"],"Resource":["arn:aws:s3:::getafix/*"]}]}
    //{"Version":"2012-10-17","Statement":[{"Sid":"1","Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetObject"],"Resource":["arn:aws:s3:::vantage/*"]}]}
    @Override
    public void createBucket(String bucket) {
        if (!this.amazonS3.doesBucketExistV2(bucket)) {
            this.amazonS3.createBucket(bucket);
            Policy policy = new Policy().withStatements(
                new Statement(Statement.Effect.Allow)
                    .withPrincipals(Principal.AllUsers)
                    .withActions(S3Actions.GetObject)
                    .withResources(new Resource(
                        "arn:aws:s3:::" + bucket + "/*")));
            this.amazonS3.setBucketPolicy(bucket, policy.toJson());
        }
    }

    @Override
    public void deleteBucket(String bucket) {
        if (this.amazonS3.doesBucketExistV2(bucket)) {
            this.amazonS3.deleteBucket(bucket);
        }
    }

    @Override
    public void upload(String bucket, String directory, String key) {
        if (StringUtils.isEmpty(bucket) || StringUtils.isEmpty(directory) || StringUtils.isEmpty(key)) {
            return;
        }
        createBucket(bucket);
        log.info("Uploading {}", key);
        Path path = Paths.get(directory, key);
        try {
            amazonS3.putObject(bucket, key, path.toFile());
        } catch (Exception e) {
            log.warn("Failed to upload {}", key, e);
        }
    }

    @Override
    public void download(String bucket, String directory, String key) {
        if (StringUtils.isEmpty(bucket) || StringUtils.isEmpty(directory) || StringUtils.isEmpty(key)) {
            return;
        }
        Path path = Paths.get(directory, key);
        S3Object object = amazonS3.getObject(bucket, key);
        try {
            Files.copy(object.getObjectContent(), path);
        } catch (IOException e) {
            log.warn("Failed to download {}", key, e);
        }
    }

    @Override
    public SoundFile getObject(String bucket, String key) {
        try (S3Object object = amazonS3.getObject(new GetObjectRequest(bucket, key))) {
            byte[] bytes = IOUtils.toByteArray(object.getObjectContent());
            SoundFile soundFile = new SoundFile();
            soundFile.setFileName(key);
            soundFile.setBytes(bytes);
            return soundFile;
        } catch (Exception e) {
            log.info("File not found: {}", key);
            throw new GenericGetafixException("File not found: " + key);
        }
    }

    @Override
    public void delete(String bucket, String key) {
        amazonS3.deleteObject(bucket, key);
    }

    @Override
    public Set<String> list(String bucket) {
        Set<String> keys = new HashSet<>();
        try {
            log.info("Listing all keys in bucket {}", bucket);
            ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket);
            ListObjectsV2Result result;
            do {
                // Fetch one page of results (default limit is 1,000 per page).
                result = amazonS3.listObjectsV2(req);

                keys.addAll(result
                    .getObjectSummaries()
                    .stream()
                    .map(S3ObjectSummary::getKey)
                    .collect(Collectors.toSet()));

                log.info("Found {} keys so far...", keys.size());

                // Advance to the next page.
                req.setContinuationToken(result.getNextContinuationToken());
            } while (result.isTruncated());

            log.info("Found total of {} keys.", keys.size());
        } catch (Exception e) {
            log.warn("Failed to list all keys", e);
        }
        return keys;
    }

    @Override
    public boolean exist(String bucket, String key) {
        return amazonS3.doesObjectExist(bucket, key);
    }
}
