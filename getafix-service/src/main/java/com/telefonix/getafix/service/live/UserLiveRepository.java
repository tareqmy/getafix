package com.telefonix.getafix.service.live;

import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 26/9/19.
 */
@Slf4j
@Component
public class UserLiveRepository {

    private Map<String, User> userLiveMap;

    private UserRepository userRepository;

    private List<UserLiveEventListener> userLiveEventListeners;

    public UserLiveRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.userLiveMap = new ConcurrentHashMap<>();
        this.userLiveEventListeners = new ArrayList<>();
    }

    public void addUserLiveEventListener(UserLiveEventListener userLiveEventListener) {
        userLiveEventListeners.add(userLiveEventListener);
    }

    void initialize() {
        userRepository.findAll().forEach(this::putUserLive);
    }

    public synchronized User getUserLive(String email) {
        return userLiveMap.get(email);
    }

    public synchronized void putUserLive(User user) {
        User oldUser = userLiveMap.get(user.getEmail());
        if (oldUser != null && oldUser.getJoinedPhoneLine() != null && user.getJoinedPhoneLine() == null) {
            userLiveEventListeners.forEach(userLiveEventListener -> {
                userLiveEventListener.onUserLeftContactCenter(oldUser);
            });
        }
        userLiveMap.put(user.getEmail(), user);
    }

    private void removeUserLive(String email) {
        userLiveMap.remove(email);
    }

    public synchronized void removeUserLive(User user) {
        removeUserLive(user.getEmail());
    }
}
