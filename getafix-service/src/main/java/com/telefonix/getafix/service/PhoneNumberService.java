package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.PhoneNumber;
import com.telefonix.getfix.dto.PhoneNumberDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-01.
 */
public interface PhoneNumberService {

    PhoneNumber createPhoneNumber(PhoneNumberDTO phoneNumberDTO);

    PhoneNumber updatePhoneNumber(PhoneNumberDTO phoneNumberDTO);

    List<PhoneNumber> findAll();

    Optional<PhoneNumber> findOne(String id);

    PhoneNumber getPhoneNumber(String id);

    List<PhoneNumber> getPhoneNumbers(Company company);

    void delete(String id);

    void delete(Company company);
}
