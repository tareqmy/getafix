package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.entity.enums.QueueLogEvent;
import com.telefonix.getafix.asterisk.repository.AXQueueLogRepository;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueChannelLog;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getafix.entity.QueueHalfHourStat;
import com.telefonix.getafix.entity.enums.QueueChannelLogStatus;
import com.telefonix.getafix.service.QueueChannelLogService;
import com.telefonix.getafix.service.QueueHalfHourStatService;
import com.telefonix.getafix.service.QueueReportService;
import com.telefonix.getafix.service.live.Constants;
import com.telefonix.getfix.dto.QueueReportDTO;
import com.telefonix.getfix.utils.MiscUtils;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.text.DecimalFormat;
import java.time.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 4/9/19.
 */
@Slf4j
@Service
public class QueueReportServiceImpl implements QueueReportService {

    private AXQueueLogRepository axQueueLogRepository;

    private QueueChannelLogService queueChannelLogService;

    private QueueHalfHourStatService queueHalfHourStatService;

    private DecimalFormat decimalFormat = new DecimalFormat();

    @Autowired
    public QueueReportServiceImpl(AXQueueLogRepository axQueueLogRepository,
                                  QueueChannelLogService queueChannelLogService,
                                  QueueHalfHourStatService queueHalfHourStatService) {
        this.axQueueLogRepository = axQueueLogRepository;
        this.queueChannelLogService = queueChannelLogService;
        this.queueHalfHourStatService = queueHalfHourStatService;
        decimalFormat.setMaximumFractionDigits(2);
    }

    @Override
    public QueueReportDTO getQueueReport(Queue queue, LocalDate from, LocalDate to, ZoneId zoneId) {
        StopWatch watch = new StopWatch();
        watch.start();

        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        List<AXQueueLog> queueLogs = axQueueLogRepository.findAllByQueueNameAndTimeBetweenOrderByTimeDesc(queueName, fromDate, toDate);
        List<QueueChannelQueueLog> queueChannelQueueLogs = queueChannelLogService.findAllByQueueId(queue.getId(), from, to, zoneId);
        List<QueueHalfHourStat> queueHalfHourStats = queueHalfHourStatService.findAllByQueueId(queue.getId(), from, to, zoneId);

        QueueReportDTO queueReport = initialize(queue, fromDate, toDate, zoneId);
        processQueueHalfHourStats(queueReport, queueHalfHourStats);
        processQueueLogs(queueReport, queueLogs, queue.getServiceLevel());
        processQueueChannelQueueLogs(queueReport, queueChannelQueueLogs);
        applyStats(queueReport);

        watch.stop();
        log.debug("calculated report in {} ms", watch.getTotalTimeMillis());
        return queueReport;
    }

    private QueueReportDTO initialize(Queue queue, Instant startInstant, Instant endInstant, ZoneId zoneId) {
        QueueReportDTO report = new QueueReportDTO();
        report.setQueueId(queue.getId());
        report.setUniqueQueueId(TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId()));
        report.setQueueName(queue.getQueueName());

        report.setStart(startInstant);
        report.setEnd(endInstant);
        report.setZoneId(zoneId);
        report.setFrom(LocalDateTime.ofInstant(startInstant, zoneId));
        report.setTo(LocalDateTime.ofInstant(endInstant, zoneId));

        report.setPeriod(Period.between(LocalDate.ofInstant(startInstant, zoneId), LocalDate.ofInstant(endInstant, zoneId)));
        report.setDuration(MiscUtils.getDuration(startInstant, endInstant));
        return report;
    }

    private void processQueueHalfHourStats(QueueReportDTO report, List<QueueHalfHourStat> queueHalfHourStats) {
        long maxAgents = queueHalfHourStats.stream().mapToLong(QueueHalfHourStat::getMaxJoinedAgents).max().orElse(0);
        long maxCallers = queueHalfHourStats.stream().mapToLong(QueueHalfHourStat::getMaxCallers).max().orElse(0);
        QueueHalfHourStat firstStat = queueHalfHourStats.get(0);
        if (firstStat != null) {
            if (firstStat.getContinuedAgents() > maxAgents) {
                maxAgents = firstStat.getContinuedAgents();
            }
            if (firstStat.getContinuedCallers() > maxCallers) {
                maxCallers = firstStat.getContinuedCallers();
            }
        }
        report.setMaxJoinedAgents(maxAgents);
        report.setMaxCallers(maxCallers);
    }

    private void processQueueLogs(QueueReportDTO report, List<AXQueueLog> queueLogs, int serviceLevel) {

        report.setOverflowed(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.JOINEMPTY)
                || axQueueLog.getEvent().equals(QueueLogEvent.FULL)).count());

        report.setReceived(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.ENTERQUEUE)).count());

        report.setAnswered(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.COMPLETEAGENT)
                || axQueueLog.getEvent().equals(QueueLogEvent.COMPLETECALLER)).count());

        report.setTransferred(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.BLINDTRANSFER)
                || axQueueLog.getEvent().equals(QueueLogEvent.ATTENDEDTRANSFER)
                || axQueueLog.getEvent().equals(QueueLogEvent.TRANSFER)).count());

        report.setUnanswered(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.EXITEMPTY)
                || axQueueLog.getEvent().equals(QueueLogEvent.EXITWITHTIMEOUT)).count());

        //abandoned is inclusive of ringcanceled
        report.setAbandoned(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.ABANDON)
                || axQueueLog.getEvent().equals(QueueLogEvent.EXITWITHKEY)).count());

        report.setTotal(report.getReceived() + report.getOverflowed());

        report.setLogins(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.ADDMEMBER)).count());

        report.setPauses(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.PAUSE)).count());

        report.setResumes(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.UNPAUSE)).count());

        report.setLogoffs(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.REMOVEMEMBER)).count());

        report.setCallsWithinSLA(queueLogs.stream()
            .filter(axQueueLog -> axQueueLog.getEvent().equals(QueueLogEvent.COMPLETEAGENT)
                || axQueueLog.getEvent().equals(QueueLogEvent.COMPLETECALLER))
            .map(axQueueLog -> Integer.parseInt(axQueueLog.getData1()) + Integer.parseInt(axQueueLog.getData2()))
            .filter(integer -> integer <= serviceLevel)
            .count());
    }

    private void processQueueChannelQueueLogs(QueueReportDTO report, List<QueueChannelQueueLog> queueChannelQueueLogs) {

        List<QueueChannelQueueLog> inboundQueueLogs = queueChannelQueueLogs.stream()
            .filter(queueChannelQueueLog -> !queueChannelQueueLog.getQueueChannelLog().isOutbound())
            .collect(Collectors.toList());

        report.setTotalWaitDuration(inboundQueueLogs.stream()
            .mapToLong(queueLog -> {
                Instant left = Instant.now();
                if (queueLog.getLeft() != null) {
                    left = queueLog.getLeft();
                }
                return MiscUtils.getDuration(queueLog.getQueued(), left).getSeconds();
            }).sum());

        report.setTotalTalkDuration(inboundQueueLogs.stream()
            .filter(queueLog -> queueLog.getHandled() != null)
            .mapToLong(queueLog -> MiscUtils.getDuration(queueLog.getLeft(), queueLog.getHandled()).getSeconds()).sum());

        if (report.getReceived() > 0) {
            report.setAverageWaitDuration((float) report.getTotalWaitDuration() / report.getReceived());
        }

        if (report.getAnswered() > 0) {
            report.setAverageTalkDuration((float) report.getTotalTalkDuration() / report.getAnswered());
        }

        List<QueueChannelQueueLog> outboundQueueLogs = queueChannelQueueLogs.stream()
            .filter(queueChannelQueueLog -> queueChannelQueueLog.getQueueChannelLog().isOutbound())
            .collect(Collectors.toList());

        report.setTotalOutbound(outboundQueueLogs.size());
        report.setAnsweredOutbound(outboundQueueLogs.stream()
            .filter(queueChannelQueueLog -> queueChannelQueueLog.getQueueChannelLog().getStatus().equals(QueueChannelLogStatus.CALL_ANSWERED))
            .count());
        report.setUnansweredOutbound(outboundQueueLogs.stream()
            .filter(queueChannelQueueLog -> queueChannelQueueLog.getQueueChannelLog().getStatus().equals(QueueChannelLogStatus.CALL_NOT_ANSWERED))
            .count());

        report.setTotalRingOutboundDuration(outboundQueueLogs.stream()
            .mapToLong(log -> MiscUtils.getDuration(log.getQueued(), log.getLeft()).getSeconds()).sum());

        report.setTotalTalkOutboundDuration(outboundQueueLogs.stream()
            .filter(queueChannelQueueLog -> queueChannelQueueLog.getQueueChannelLog().getStatus().equals(QueueChannelLogStatus.CALL_ANSWERED))
            .mapToLong(log -> MiscUtils.getDuration(log.getLeft(), log.getHandled()).getSeconds()).sum());

        if (report.getTotalOutbound() > 0) {
            report.setAverageRingOutboundDuration((float) report.getTotalRingOutboundDuration() / report.getTotalOutbound());
        }

        if (report.getAnsweredOutbound() > 0) {
            report.setAverageTalkOutboundDuration((float) report.getTotalTalkOutboundDuration() / report.getAnsweredOutbound());
        }
    }

    private void applyStats(QueueReportDTO report) {
        if (report.getTotal() > 0) {
            report.setPercentAnswered(formatDecimal(((float) report.getAnswered() / report.getTotal()) * 100));
            report.setPercentTransferred(formatDecimal(((float) report.getTransferred() / report.getTotal()) * 100));
            report.setPercentUnanswered(formatDecimal(((float) report.getUnanswered() / report.getTotal()) * 100));
            report.setPercentAbandoned(formatDecimal(((float) report.getAbandoned() / report.getTotal()) * 100));
        }

        if (report.getTotalOutbound() > 0) {
            report.setPercentAnsweredOutbound(formatDecimal(((float) report.getAnsweredOutbound() / report.getTotalOutbound()) * 100));
            report.setPercentUnansweredOutbound(formatDecimal(((float) report.getUnansweredOutbound() / report.getTotalOutbound()) * 100));
        }

        report.setTotalWaitTime(formatDuration(report.getTotalWaitDuration()));
        report.setTotalTalkTime(formatDuration(report.getTotalTalkDuration()));
        report.setAverageWaitTime(formatDuration((long) report.getAverageWaitDuration()));
        report.setAverageTalkTime(formatDuration((long) report.getAverageTalkDuration()));

        report.setTotalRingOutboundTime(formatDuration(report.getTotalRingOutboundDuration()));
        report.setTotalTalkOutboundTime(formatDuration(report.getTotalTalkOutboundDuration()));
        report.setAverageRingOutboundTime(formatDuration((long) report.getAverageRingOutboundDuration()));
        report.setAverageTalkOutboundTime(formatDuration((long) report.getAverageTalkOutboundDuration()));

        if (report.getAnswered() > 0) {
            report.setPercentSLA(((float) report.getCallsWithinSLA() / report.getAnswered()) * 100);
        }
    }

    private String formatDuration(long durationSecs) {
        if (durationSecs < 0) {
            log.error("Format requested for negative {}", durationSecs);
            return Constants.zeroDurationFormat;
        }
        return DurationFormatUtils.formatDuration(durationSecs * 1000, Constants.durationFormat);
    }

    private String formatDecimal(float decimal) {
        if (decimal <= 0) {
            return "0";
        }
        return decimalFormat.format(decimal);
    }
}
