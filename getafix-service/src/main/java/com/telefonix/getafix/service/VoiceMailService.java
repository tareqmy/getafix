package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.VoiceMail;
import com.telefonix.getfix.dto.VoiceMailDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-23.
 */
public interface VoiceMailService {

    VoiceMail updateVoiceMail(VoiceMailDTO voiceMailDTO);

    void changePin(VoiceMail voiceMail, String pin);

    List<VoiceMail> findAll();

    Optional<VoiceMail> findOne(Long id);

    VoiceMail getVoiceMail(Extension extension);

    VoiceMail getVoiceMail(Long id);

    void delete(Long id);

    void delete(Extension extension);

    VoiceMail enable(Queue queue);

    VoiceMail update(Queue queue);

    VoiceMail disable(Queue queue);
}
