package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.entity.AutoAttendant;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.asterisk.callcontrol.ExtensionCallControlService;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-04-30.
 */
@Slf4j
@Service
@Transactional
public class ExtensionCallControlServiceImpl extends AbstractCallControlServiceImpl implements ExtensionCallControlService {

    @Value(value = "${getafix.obelix.path}")
    private String path;

    private ExtensionService extensionService;

    @Autowired
    public ExtensionCallControlServiceImpl(AsteriskResourceService asteriskResourceService, ExtensionService extensionService) {
        super(asteriskResourceService);
        this.extensionService = extensionService;
    }

    @Override
    public CallEventListener handleExtensionCall(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put(CallControlProperties.GX_EXTEN, exten);
        vMap.put(CallControlProperties.GX_CONTEXT, context);
        String destination = extensionService.findByCompanyIdAndBaseExtension(context, exten).map(extension -> {
            if (extension.getPhoneLine() != null) {
                String serverGenerated = getChannelVariable(channel, CallControlProperties.SERVER_GENERATED);
                if (!StringUtils.isEmpty(serverGenerated) && serverGenerated.equals(CallControlProperties.yes)) {
                    log.info("this is server generated");
                    vMap.put(CallControlProperties.GX_NO_VOICEMAIL, CallControlProperties.yes);
                }
                String dial = String.format("%s/%s", TECHNOLOGY, extension.getPhoneLine().getSipUserName());
                vMap.put(CallControlProperties.GX_DIAL, dial);
                vMap.put(CallControlProperties.GX_RING_DURATION, extension.getRingDuration().toString());
                vMap.put(CallControlProperties.GX_DIAL_OPTIONS, "T");
                return CallControlProperties.dial_exten;
            } else if (extension.getQueue() != null) {
                Queue queue = extension.getQueue();
                String queueId = queue.getQueueId();
                String companyId = queue.getCompany().getCompanyId();
                vMap.put(CallControlProperties.GX_QUEUE, TelephonyUtils.getUniqueQueueId(companyId, queueId));
                vMap.put(CallControlProperties.GX_QUEUE_OPTIONS, "iRktx");
                vMap.put(CallControlProperties.GX_MAILBOX, exten);
                vMap.put(CallControlProperties.GX_MAILBOX_OPTIONS, "u");

                String callerName = getChannelVariable(channel, CallControlProperties.GX_CALLERNAME);
                if (StringUtils.isEmpty(callerName)) {
                    callerName = getChannelVariable(channel, CallControlProperties.CALLERID_NAME);
                    vMap.put(CallControlProperties.GX_CALLERNAME, callerName);
                }
                vMap.put(CallControlProperties.CALLERID_NAME, "[" + queueId + "] " + callerName);

                return CallControlProperties.queue_exten;
            } else if (extension.getAutoAttendant() != null) {
                AutoAttendant autoAttendant = extension.getAutoAttendant();
                autoAttendant.getOptionsMap().forEach((s, s2) -> {
                    vMap.put(CallControlProperties.OPTION_ + s, context + "," + s2 + ",1");
                });
                vMap.put(CallControlProperties.GX_PROMPTS, path + "/" + autoAttendant.getCompany().getCompanyId() + "/" + autoAttendant.getAutoAttendantId() + "_prompt");
                vMap.put(CallControlProperties.GX_REPEAT, "2");
                return CallControlProperties.aa_exten;
            }
            return CallControlProperties.invalid_exten;
        }).orElse(CallControlProperties.invalid_exten);

        return new TerminalCall(accountCode, destination, getafix_context, null, vMap, asteriskResourceService);
    }
}
