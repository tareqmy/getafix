package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.repository.PhoneRepository;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.PhoneLineService;
import com.telefonix.getafix.service.PhoneService;
import com.telefonix.getfix.dto.PhoneDTO;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Slf4j
@Service
@Transactional
public class PhoneServiceImpl implements PhoneService {

    private PhoneRepository phoneRepository;

    private PhoneLineService phoneLineService;

    private ExtensionService extensionService;

    private GeneralService generalService;

    @Autowired
    public PhoneServiceImpl(PhoneRepository phoneRepository,
                            PhoneLineService phoneLineService,
                            ExtensionService extensionService,
                            GeneralService generalService) {
        this.phoneRepository = phoneRepository;
        this.phoneLineService = phoneLineService;
        this.extensionService = extensionService;
        this.generalService = generalService;
    }

    @Override
    public Phone createPhone(PhoneDTO phoneDTO) {
        Company company = generalService.getCompany(phoneDTO.getCompanyId());
        Phone phone = new Phone();
        phone.setCompany(company);
        phone.setPhoneName(phoneDTO.getPhoneName());
        return phoneRepository.save(phone);
    }

    @Override
    public Phone updatePhone(PhoneDTO phoneDTO) {
        Phone phone = getPhone(phoneDTO.getId());
        phone.setPhoneName(phoneDTO.getPhoneName());
        return phoneRepository.save(phone);
    }

    @Override
    public List<Phone> findAll() {
        return phoneRepository.findAll();
    }

    @Override
    public Optional<Phone> findOne(Long id) {
        return phoneRepository.findById(id);
    }

    @Override
    public Phone getPhone(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phone id " + id)
        );
    }

    @Override
    public List<PhoneLine> getPhoneLines(Long id) {
        Phone phone = getPhone(id);
        return phoneLineService.getPhoneLines(phone);
    }

    @Override
    public List<Extension> getExtensions(Long id) {
        Phone phone = getPhone(id);
        return extensionService.getExtensions(phone, false);
    }

    @Override
    public List<Phone> getPhones(Company company) {
        return phoneRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getPhones().forEach(this::delete);
    }

    @Override
    public List<Phone> getPhones(User user) {
        return extensionService.getExtensions(user)
            .stream()
            .filter(extension -> extension.getPhoneLine() != null)
            .map(Extension::getPhoneLine)
            .map(PhoneLine::getPhone)
            .sorted()
            .collect(Collectors.toList());
    }

    private void delete(Phone phone) {
        phoneLineService.delete(phone);
        phoneRepository.delete(phone);
        log.debug("Deleted phone: {}", phone);
    }
}
