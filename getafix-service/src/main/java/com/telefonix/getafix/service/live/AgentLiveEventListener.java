package com.telefonix.getafix.service.live;

import com.telefonix.getafix.service.live.entity.AgentLive;

/**
 * Created by tareqmy on 2019-07-03.
 */
@FunctionalInterface
public interface AgentLiveEventListener {

    void onEvent(AgentLive agentLive);
}
