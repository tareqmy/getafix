package com.telefonix.getafix.service;

/**
 * Created by tmyousuf on 16/9/18.
 */
public interface EmailService {

    void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml);

    void prepareAndSend(String recipient, String message);

    void sendPasswordResetMail(String email, String resetToken);

    void sendPasswordChangedMail(String email);

    void sendEmailConfirmMail(String email, String confirmToken);
}
