package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueHalfHourStat;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * Created by tareqmy on 1/10/19.
 */
public interface QueueHalfHourStatService {

    void onQueueCreated(Queue queue);

    List<QueueHalfHourStat> findAllByQueueId(Long queueId, LocalDate from, LocalDate to, ZoneId zoneId);
}
