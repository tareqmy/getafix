package com.telefonix.getafix.service.live.entity;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-06-24.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"user"}, callSuper = true)
public class AgentLive extends Live {

    private User user;

    private PhoneLine phoneLine;

    private DeviceStatus deviceStatus = DeviceStatus.UNKNOWN;

    private boolean paused;

    private Map<String, AgentStatus> statusMap = new ConcurrentHashMap<>();

    private PSEndpointLive psEndpointLive;

    private Map<String, ChannelLive> channelLiveMap = new ConcurrentHashMap<>();

    public AgentLive(User user, PhoneLine phoneLine, PSEndpointLive psEndpointLive) {
        super(phoneLine.getId());
        this.user = user;
        this.phoneLine = phoneLine;
        this.psEndpointLive = psEndpointLive;
    }

    @Override
    public String toString() {
        return "AgentLive{" +
            "phoneLine=" + phoneLine.getSipUserName() +
            ", user=" + user.getEmail() +
            ", paused=" + paused +
            ", deviceStatus=" + deviceStatus +
            ", statusMap=" + statusMap +
            ", psEndpointLive=" + psEndpointLive +
            ", channelLiveMap=" + channelLiveMap +
            '}';
    }
}
