package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.User;
import com.telefonix.getfix.dto.ChangePassword;
import com.telefonix.getfix.dto.ExtensionReportDTO;
import com.telefonix.getfix.dto.UserDTO;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.List;

/**
 * Created by tmyousuf on 6/9/18.
 */
public interface AccountService {

    Authentication authenticate(UsernamePasswordAuthenticationToken authenticationToken);

    User findCurrentLoggedInUser();

    User updateAccount(UserDTO userDTO);

    void changePassword(ChangePassword changePassword);

    void initiateEmailConfirm();

    boolean completeEmailConfirm(String token);

    List<Extension> getExtensionsForAccount();

    List<ExtensionReportDTO> getExtensionReports(Long id);

    List<Phone> getPhonesForAccount();
}
