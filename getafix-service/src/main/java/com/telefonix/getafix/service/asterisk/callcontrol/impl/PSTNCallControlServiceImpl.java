package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.entity.PhoneNumber;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.service.PhoneNumberService;
import com.telefonix.getafix.service.ProviderService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.asterisk.callcontrol.ExtensionCallControlService;
import com.telefonix.getafix.service.asterisk.callcontrol.PSTNCallControlService;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-02.
 */
@Slf4j
@Service
@Transactional
public class PSTNCallControlServiceImpl extends AbstractCallControlServiceImpl implements PSTNCallControlService {

    private ProviderService providerService;

    private PhoneNumberService phoneNumberService;

    private ExtensionCallControlService extensionCallControlService;

    private ExtensionService extensionService;

    @Autowired
    public PSTNCallControlServiceImpl(AsteriskResourceService asteriskResourceService,
                                      ProviderService providerService,
                                      PhoneNumberService phoneNumberService,
                                      ExtensionCallControlService extensionCallControlService,
                                      ExtensionService extensionService) {
        super(asteriskResourceService);
        this.providerService = providerService;
        this.phoneNumberService = phoneNumberService;
        this.extensionCallControlService = extensionCallControlService;
        this.extensionService = extensionService;
    }

    @Override
    public CallEventListener handleCall(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        return providerService.findOne(accountCode)
            .map(provider -> phoneNumberService.findOne(exten)
                .map(phoneNumber -> extensionCallControlService.handleExtensionCall(channel, accountCode,
                    phoneNumber.getExtension().getBaseExtension(), phoneNumber.getCompany().getCompanyId()))
                .orElse(new TerminalCall(accountCode, CallControlProperties.invalid_exten, getafix_context, null, vMap, asteriskResourceService)))
            .orElse(new TerminalCall(accountCode, CallControlProperties.invalid_exten, getafix_context, null, vMap, asteriskResourceService));
    }

    @Override
    public CallEventListener handleOutboundCall(Channel channel, String accountCode, String exten, String context) {
        Map<String, String> vMap = new HashMap<>();
        String destination = CallControlProperties.invalid_exten;
        if (TelephonyUtils.isValidInternalEntity(accountCode)) {
            String baseExtension = TelephonyUtils.getBaseExtension(accountCode);
            String companyId = TelephonyUtils.getCompanyId(accountCode);
            destination = extensionService.findByCompanyIdAndBaseExtension(companyId, baseExtension).map(extension -> {
                PhoneNumber phoneNumber = extension.getPhoneNumber();
                if (phoneNumber == null) {
                    Optional<PhoneNumber> first = extension.getCompany().getPhoneNumbers().stream().findFirst();
                    if (first.isPresent()) {
                        phoneNumber = first.get();
                    }
                }
                if (phoneNumber != null) {
                    vMap.put(CallControlProperties.CALLERID_NAME, extension.getCompany().getCompanyName());
                    vMap.put(CallControlProperties.CALLERID_NUMBER, phoneNumber.getNumber());

                    String pstnDestination = String.format("%s/%s@%s", TECHNOLOGY, exten, phoneNumber.getNumber());
                    vMap.put(CallControlProperties.GX_DIAL, pstnDestination);
                    vMap.put(CallControlProperties.GX_RING_DURATION, extension.getRingDuration().toString());
                    vMap.put(CallControlProperties.GX_DIAL_OPTIONS, "T");
                    return CallControlProperties.pstn_exten;
                }
                return CallControlProperties.invalid_exten;
            }).orElse(CallControlProperties.invalid_exten);
        }

        return new TerminalCall(accountCode, destination, getafix_context, null, vMap, asteriskResourceService);
    }
}
