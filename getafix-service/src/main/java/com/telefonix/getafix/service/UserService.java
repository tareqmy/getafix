package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.EndpointLog;
import com.telefonix.getafix.entity.User;
import com.telefonix.getfix.dto.AgentReportDTO;
import com.telefonix.getfix.dto.UserDTO;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Created by tmyousuf on 6/8/18.
 */
public interface UserService {

    User createUser(UserDTO userDTO);

    User updateUserInformation(UserDTO userDTO);

    void changePassword(Long id, String password);

    void verifyOldPassword(String email, String password);

    void changeUserPassword(String email, String password);

    Optional<User> findOne(Long id);

    User getUser(String email);

    User getUser(Long id);

    List<User> findAll();

    List<User> findByJoinedPhoneLineNotNull();

    List<User> getUsers(Company company);

    void delete(Long id);

    void delete(Company company);

    void initiatePasswordReset(String email);

    boolean completePasswordReset(String token, String email, String newPassword);

    void initiateEmailConfirm(String email);

    boolean completeEmailConfirm(String token, String email);

    void removeExpiredTokens();

    List<AgentLog> getAgentLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    AgentReportDTO getAgentReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    List<EndpointLog> getEndpointLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);
}
