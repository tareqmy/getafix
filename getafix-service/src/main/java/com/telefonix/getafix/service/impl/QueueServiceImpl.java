package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.service.MusicOnHoldService;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getafix.entity.enums.Strategy;
import com.telefonix.getafix.repository.QueueRepository;
import com.telefonix.getafix.service.*;
import com.telefonix.getafix.service.asterisk.EntityService;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getfix.dto.QueueDTO;
import com.telefonix.getfix.dto.QueueReportDTO;
import com.telefonix.getfix.dto.SoundFile;
import com.telefonix.getfix.exceptions.BusinessRuleViolationException;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-01-28.
 */
@Slf4j
@Service
@Transactional
public class QueueServiceImpl implements QueueService {

    @Value(value = "${getafix.asterisk.sip.technology}")
    private String technology;

    @Value(value = "${getafix.obelix.path}")
    private String path;

    private QueueRepository queueRepository;

    private VoiceMailService voiceMailService;

    private QueueLogService queueLogService;

    private QueueChannelLogService queueChannelLogService;

    private MediaManagementService mediaManagementService;

    private ObjectStorageService objectStorageService;

    private EntityService entityService;

    private MusicOnHoldService musicOnHoldService;

    private GeneralService generalService;

    private ExtensionService extensionService;

    private QueueReportService queueReportService;

    private CallCenterLiveRepository callCenterLiveRepository;

    private QueueHalfHourStatService queueHalfHourStatService;

    @Autowired
    public QueueServiceImpl(QueueRepository queueRepository,
                            VoiceMailService voiceMailService,
                            QueueLogService queueLogService,
                            QueueChannelLogService queueChannelLogService,
                            MediaManagementService mediaManagementService,
                            ObjectStorageService objectStorageService,
                            EntityService entityService,
                            MusicOnHoldService musicOnHoldService,
                            GeneralService generalService,
                            QueueReportService queueReportService,
                            ExtensionService extensionService,
                            CallCenterLiveRepository callCenterLiveRepository,
                            QueueHalfHourStatService queueHalfHourStatService) {
        this.queueRepository = queueRepository;
        this.voiceMailService = voiceMailService;
        this.queueLogService = queueLogService;
        this.queueChannelLogService = queueChannelLogService;
        this.mediaManagementService = mediaManagementService;
        this.objectStorageService = objectStorageService;
        this.entityService = entityService;
        this.musicOnHoldService = musicOnHoldService;
        this.generalService = generalService;
        this.queueReportService = queueReportService;
        this.extensionService = extensionService;
        this.callCenterLiveRepository = callCenterLiveRepository;
        this.queueHalfHourStatService = queueHalfHourStatService;
    }

    private void verifyQueueId(String queueId, Company company) {
        if (queueRepository.findByCompany(company)
            .stream()
            .map(Queue::getQueueId)
            .anyMatch(s -> s.equals(queueId))) {
            throw new BusinessRuleViolationException("This queueId is taken " + queueId);
        }
    }

    private void verifyCompany(Extension extension, Company company) {
        if (!extension.getCompany().equals(company)) {
            throw new BusinessRuleViolationException("Assigned extension must belong to same company");
        }
    }

    private void verifyExtension(Queue queue, Extension extension) {
        if (extension.getQueue() != null && !extension.getQueue().equals(queue)) {
            throw new BusinessRuleViolationException("This extension is already assigned to another queue");
        }

        if (extension.getAutoAttendant() != null) {
            throw new BusinessRuleViolationException("This extension is already assigned to a auto attendant");
        }

        if (extension.getPhoneLine() != null) {
            throw new BusinessRuleViolationException("This extension is already assigned to a phone line");
        }
    }

    @Override
    public Queue createQueue(QueueDTO queueDTO) {
        Company company = generalService.getCompany(queueDTO.getCompanyId());
        verifyQueueId(queueDTO.getQueueId(), company);
        Extension extension = generalService.getExtension(queueDTO.getExtensionId());
        Queue queue = new Queue();
        verifyCompany(extension, company);
        verifyExtension(queue, extension);
        queue.setQueueId(queueDTO.getQueueId().toLowerCase());
        queue.setQueueName(queueDTO.getQueueName());
        queue.setEmail(queueDTO.getEmail());
        queue.setCompany(company);
        queue.setStrategy(Strategy.valueOf(queueDTO.getStrategy()));
        queue.setWeight(queueDTO.getWeight());
        queue.setServiceLevel(queueDTO.getServiceLevel());
        queue.setMaxLength(queueDTO.getMaxLength());
        queue.setPlayInitialMessage(false);
        queue.setPlayMusicOnHold(false);
        queue.setPlayComfortMessage(false);
        if (queueDTO.getComfortMessageInterval() == null) {
            queue.setComfortMessageInterval(60);
        } else {
            queue.setComfortMessageInterval(queueDTO.getComfortMessageInterval());
        }
        queue.setZeroEscapeEnabled(queueDTO.isZeroEscapeEnabled());

        queue.setExtension(extensionService.unassignUser(extension)); //remove from any user. queue extension is not personal
        Queue saved = queueRepository.save(queue);
        initQueueLive(saved);
        return saved;
    }

    private void initQueueLive(Queue queue) {
        voiceMailService.enable(queue);
        entityService.createSipEntity(queue);
        callCenterLiveRepository.initQueueLive(queue);
        queueHalfHourStatService.onQueueCreated(queue);
    }

    @Override
    public Queue updateQueue(QueueDTO queueDTO) {
        Queue queue = getQueue(queueDTO.getId());
        Extension extension = generalService.getExtension(queueDTO.getExtensionId());
        verifyCompany(extension, queue.getCompany());
        verifyExtension(queue, extension);
        queue.setQueueName(queueDTO.getQueueName());
        queue.setEmail(queueDTO.getEmail());
        queue.setStrategy(Strategy.valueOf(queueDTO.getStrategy()));
        queue.setWeight(queueDTO.getWeight());
        queue.setServiceLevel(queueDTO.getServiceLevel());
        queue.setMaxLength(queueDTO.getMaxLength());

        if (queueDTO.isPlayInitialMessage() && StringUtils.isEmpty(queue.getInitialMessageFileName())) {
            throw new BusinessRuleViolationException("Please upload the initial message first");
        }

        if (queueDTO.isPlayMusicOnHold() && StringUtils.isEmpty(queue.getMohFileName())) {
            throw new BusinessRuleViolationException("Please upload the moh first");
        }

        if (queueDTO.isPlayComfortMessage() && StringUtils.isEmpty(queue.getComfortMessageFileName())) {
            throw new BusinessRuleViolationException("Please upload the comfort message first");
        }

        queue.setPlayInitialMessage(queueDTO.isPlayInitialMessage());
        queue.setPlayMusicOnHold(queueDTO.isPlayMusicOnHold());
        queue.setPlayComfortMessage(queueDTO.isPlayComfortMessage());
        if (queueDTO.getComfortMessageInterval() == null) {
            queue.setComfortMessageInterval(60);
        } else {
            queue.setComfortMessageInterval(queueDTO.getComfortMessageInterval());
        }
        queue.setZeroEscapeEnabled(queueDTO.isZeroEscapeEnabled());

        queue.setExtension(extensionService.unassignUser(extension)); //remove from any user. queue extension is not personal
        entityService.updateSipEntity(queue);
        voiceMailService.update(queue);

        Queue updated = queueRepository.save(queue);
        callCenterLiveRepository.updateQueueLive(updated);
        return updated;
    }

    @Override
    public List<Queue> findAll() {
        return queueRepository.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Queue> findOne(Long id) {
        return queueRepository.findById(id);
    }

    @Override
    public Queue getQueue(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid queue id " + id)
        );
    }

    @Override
    public List<Queue> getQueues(Company company) {
        return queueRepository.findByCompany(company)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        findOne(id).ifPresent(this::delete);
    }

    @Override
    public void delete(Company company) {
        company.getQueues().forEach(this::delete);
    }

    private void delete(Queue queue) {
        callCenterLiveRepository.deleteQueueLive(queue);
        voiceMailService.disable(queue);

        entityService.deleteSipEntity(queue);

        if (!StringUtils.isEmpty(queue.getInitialMessageFileName())) {
            mediaManagementService.delete(queue.getCompany().getCompanyId(), queue.getInitialMessageFileName());
        }

        if (!StringUtils.isEmpty(queue.getMohFileName())) {
            mediaManagementService.deleteMoh(queue.getCompany().getCompanyId(), queue.getQueueId(), queue.getMohFileName());
        }

        if (!StringUtils.isEmpty(queue.getComfortMessageFileName())) {
            mediaManagementService.delete(queue.getCompany().getCompanyId(), queue.getComfortMessageFileName());
        }

        musicOnHoldService.delete(TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId()));
        queueRepository.delete(queue);
        log.debug("Deleted queue: {}", queue);
    }

    @Override
    public List<AXQueueLog> getQueueLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        Queue queue = getQueue(id);
        String queueName = TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId());
        return queueLogService.findAllByQueueName(queueName, from, to, zoneId);
    }

    @Override
    public List<QueueChannelQueueLog> getQueueChannelLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        Queue queue = getQueue(id);
        return queueChannelLogService.findAllByQueueId(queue.getId(), from, to, zoneId);
    }

    @Override
    public QueueReportDTO getQueueReport(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        Queue queue = getQueue(id);
        return queueReportService.getQueueReport(queue, from, to, zoneId);
    }

    @Override
    public Queue uploadInitialMessage(Long id, MultipartFile file) {
        Queue queue = getQueue(id);
        String companyId = queue.getCompany().getCompanyId();
        String fileName = String.format("queue-initial_msg-%s_%s.%s",
            companyId, queue.getQueueId(), "wav");
        mediaManagementService.upload(companyId, fileName, file);
        queue.setInitialMessageFileName(fileName);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public Queue deleteInitialMessage(Long id) {
        Queue queue = getQueue(id);
        if (!StringUtils.isEmpty(queue.getInitialMessageFileName())) {
            mediaManagementService.delete(queue.getCompany().getCompanyId(), queue.getInitialMessageFileName());
        }
        queue.setInitialMessageFileName(null);
        queue.setPlayInitialMessage(false);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public Queue uploadMusicOnHold(Long id, MultipartFile file) {
        Queue queue = getQueue(id);
        String companyId = queue.getCompany().getCompanyId();
        String queueId = queue.getQueueId();
        String uniqueQueueId = TelephonyUtils.getUniqueQueueId(companyId, queueId);

        //unique needed for object store
        String fileName = String.format("queue-moh-%s.%s", uniqueQueueId, "wav");
        mediaManagementService.uploadMoh(companyId, queueId, fileName, file);

        //unique path because there can be other moh for the company
        String directory = path + "/" + companyId + "/moh/" + queueId;
        musicOnHoldService.create(uniqueQueueId, directory);

        queue.setMohFileName(fileName);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public Queue deleteMusicOnHold(Long id) {
        Queue queue = getQueue(id);
        if (!StringUtils.isEmpty(queue.getMohFileName())) {
            mediaManagementService.deleteMoh(queue.getCompany().getCompanyId(), queue.getQueueId(), queue.getMohFileName());
            musicOnHoldService.delete(TelephonyUtils.getUniqueQueueId(queue.getCompany().getCompanyId(), queue.getQueueId()));
        }
        queue.setMohFileName(null);
        queue.setPlayMusicOnHold(false);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public Queue uploadComfortMessage(Long id, MultipartFile file) {
        Queue queue = getQueue(id);
        String companyId = queue.getCompany().getCompanyId();
        String fileName = String.format("queue-comfort_msg-%s_%s.%s",
            companyId, queue.getQueueId(), "wav");
        mediaManagementService.upload(companyId, fileName, file);
        queue.setComfortMessageFileName(fileName);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public Queue deleteComfortMessage(Long id) {
        Queue queue = getQueue(id);
        if (!StringUtils.isEmpty(queue.getComfortMessageFileName())) {
            mediaManagementService.delete(queue.getCompany().getCompanyId(), queue.getComfortMessageFileName());
        }
        queue.setComfortMessageFileName(null);
        queue.setPlayComfortMessage(false);
        entityService.updateSipEntity(queue);
        return queueRepository.save(queue);
    }

    @Override
    public SoundFile downloadInitialMessage(Long id) {
        Queue queue = getQueue(id);
        if (StringUtils.isEmpty(queue.getInitialMessageFileName())) {
            throw new BusinessRuleViolationException("Please upload a file first");
        }
        String companyId = queue.getCompany().getCompanyId();
        return objectStorageService.getObject(companyId, queue.getInitialMessageFileName());
    }

    @Override
    public SoundFile downloadMusicOnHold(Long id) {
        Queue queue = getQueue(id);
        if (StringUtils.isEmpty(queue.getMohFileName())) {
            throw new BusinessRuleViolationException("Please upload a file first");
        }
        String companyId = queue.getCompany().getCompanyId();
        return objectStorageService.getObject(companyId, queue.getMohFileName());
    }

    @Override
    public SoundFile downloadComfortMessage(Long id) {
        Queue queue = getQueue(id);
        if (StringUtils.isEmpty(queue.getComfortMessageFileName())) {
            throw new BusinessRuleViolationException("Please upload a file first");
        }
        String companyId = queue.getCompany().getCompanyId();
        return objectStorageService.getObject(companyId, queue.getComfortMessageFileName());
    }
}
