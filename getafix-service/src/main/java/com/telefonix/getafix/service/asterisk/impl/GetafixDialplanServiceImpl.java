package com.telefonix.getafix.service.asterisk.impl;

import com.telefonix.getafix.asterisk.service.AstConfigService;
import com.telefonix.getafix.asterisk.service.AsteriskControlService;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.service.GetafixProperties;
import com.telefonix.getafix.service.asterisk.GetafixDialplanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by tareqmy on 2019-02-04.
 */
@Slf4j
@Service
@Transactional
public class GetafixDialplanServiceImpl implements GetafixDialplanService {

    private AstConfigService astConfigService;

    private AsteriskControlService asteriskControlService;

    private static final String EXTENSION_VARNAME = "exten";

    private static List<String> reservedCategories = Arrays.asList("general", "default", "getafix", "company", "customer", "group",
        "system", "service", "admin", "globals", "global", "locals", "local", "parkedcalls", "queueexitcontext");

    private Map<String, String> generalMap;

    private Map<String, String> globalsMap;

    private List<String> getafixList;

    private List<String> companyExtenList;

    private List<String> queueExitContextList;

    @Autowired
    public GetafixDialplanServiceImpl(AstConfigService astConfigService,
                                      AsteriskControlService asteriskControlService,
                                      GetafixProperties getafixProperties) {
        this.astConfigService = astConfigService;
        this.asteriskControlService = asteriskControlService;
        this.generalMap = getafixProperties.getAsterisk().getDialplan().getGeneral();
        this.globalsMap = getafixProperties.getAsterisk().getDialplan().getGlobals();
        this.getafixList = getafixProperties.getAsterisk().getDialplan().getGetafix();
        this.companyExtenList = getafixProperties.getAsterisk().getDialplan().getCompany();
        this.queueExitContextList = getafixProperties.getAsterisk().getDialplan().getQueueexitcontext();
    }

    @Override
    public boolean isReservedCategory(String category) {
        return reservedCategories.contains(category);
    }

    @Override
    public void onCompanyCreated(Company company) {
        String category = company.getCompanyId();
        if (isReservedCategory(category)) {
            throw new RuntimeException("This is a reserved category. Cannot be created dialplan for this " + category);
        }
        rebuildDialplanForCompany(company);
    }

    @Override
    public void onCompanyDeleted(Company company) {
        //verify and delete all that belongs to this customer
        deleteCategory(company.getCompanyId());
        asteriskControlService.setReloadDialplan(true);
    }

    @Override
    public void rebuildDialplans() {
        String category = "general";
        int catMetric = getCatMetric(category);
        deleteCategory(category);

        int i = 1;
        for (Map.Entry<String, String> next : generalMap.entrySet()) {
            astConfigService.save(catMetric, i, category, next.getKey(), next.getValue());
            i++;
        }

        category = "globals";
        catMetric = getCatMetric(category);
        deleteCategory(category);

        i = 1;
        for (Map.Entry<String, String> next : globalsMap.entrySet()) {
            astConfigService.save(catMetric, i, category, next.getKey(), next.getValue());
            i++;
        }

        category = "getafix";
        catMetric = getCatMetric(category);
        rebuildDialplanForCategory(category, catMetric, getafixList);

        category = "queueexitcontext";
        catMetric = getCatMetric(category);
        rebuildDialplanForCategory(category, catMetric, queueExitContextList);
    }

    @Override
    public void rebuildDialplanForCompany(Company company) {
        String category = company.getCompanyId();
        int catMetric = getCatMetric(category);
        rebuildDialplanForCategory(category, catMetric, companyExtenList);
    }

    private int rebuildDialplanForCategory(String category, int catMetric, List<String> list) {
        deleteCategory(category);

        for (int i = 0; i < list.size(); i++) {
            astConfigService.save(catMetric, i + 1, category, EXTENSION_VARNAME, list.get(i));
        }

        asteriskControlService.setReloadDialplan(true);
        return list.size();
    }

    private int getCatMetric(String category) {
        int catMetric = astConfigService.getCatMetric(category);
        if (catMetric > 0) {
            return catMetric;
        }
        return astConfigService.getNextCatMetric();
    }

    private void deleteCategory(String category) {
        astConfigService.deleteByCategory(category);
    }
}
