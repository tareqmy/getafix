package com.telefonix.getafix.service.live.entity;

public enum DeviceStatus {
    UNKNOWN(0),
    NOT_INUSE(1),
    INUSE(2),
    BUSY(3),
    INVALID(4),
    UNAVAILABLE(5),
    RINGING(6),
    RINGINUSE(7),
    ONHOLD(8);

    private int status;

    DeviceStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return name();
    }
}
