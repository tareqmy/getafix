package com.telefonix.getafix.service.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ResourceService;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveEventListener;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.enums.ChannelLogStatus;
import com.telefonix.getafix.repository.ChannelLogRepository;
import com.telefonix.getafix.service.ChannelLogService;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getfix.utils.MiscUtils;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-06-16.
 */
@Slf4j
@Service
@Transactional
public class ChannelLogServiceImpl implements ChannelLiveEventListener, ChannelLogService {

    private String missing = "Missing";

    private Map<String, ChannelLog> channelLogMap;

    private GeneralService generalService;

    private ChannelLogRepository channelLogRepository;

    private ResourceService resourceService;

    @Autowired
    public ChannelLogServiceImpl(ChannelLiveRepository channelLiveRepository,
                                 ChannelLogRepository channelLogRepository,
                                 GeneralService generalService,
                                 ResourceService resourceService) {
        channelLiveRepository.addChannelLiveEventListener(this);
        this.channelLogRepository = channelLogRepository;
        this.generalService = generalService;
        this.resourceService = resourceService;
        this.channelLogMap = new ConcurrentHashMap<>();
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    @Override
    public void refresh() {
        channelLogRepository.findByEndIsNull().forEach(channelLog -> {
            Channel channel = resourceService.getChannel(channelLog.getUniqueId());
            if (channel == null) {
                //ending channellogs that were not ended properly
                endChannelLog(channelLog, Instant.now());
                channelLogRepository.save(channelLog);
            }
        });
    }

    @Async
    @Override
    public synchronized void onChannelStart(ChannelLive channelLive) {
        String accountCode = channelLive.getAccountCode();
        if (!TelephonyUtils.isValidInternalEntity(accountCode)) {
            return;
        }
        String companyId = TelephonyUtils.getCompanyId(accountCode);
        String baseExtension = TelephonyUtils.getBaseExtension(accountCode);
        Optional<Extension> extension = generalService.findByCompanyIdAndBaseExtension(companyId, baseExtension);
        if (extension.isEmpty()) {
            log.error("Invalid extension {}", channelLive);
            return;
        }
        Optional<PhoneLine> phoneLine = generalService.findBySipUserName(accountCode);
        if (phoneLine.isEmpty()) {
            log.error("Invalid phone line {}", channelLive);
            return;
        }
        Extension ext = extension.get();
        Long userId = null;
        String userName = null;
        if (ext.getUser() != null) {
            userId = ext.getUser().getId();
            userName = ext.getUser().getFullName();
        }
        ChannelLog channelLog = new ChannelLog();
        channelLog.setUniqueId(channelLive.getUniqueID());
        channelLog.setAccountCode(accountCode);
        channelLog.setCompanyId(companyId);
        channelLog.setUserId(userId);
        channelLog.setUserName(userName);
        channelLog.setExtensionId(ext.getId());
        channelLog.setBaseExtension(baseExtension);
        channelLog.setPhoneName(phoneLine.get().getPhone().getPhoneName());
        channelLog.setLinkedId(channelLive.getLinkedID());
        channelLog.setCaller(channelLive.isCaller());
        channelLog.setExternal(false);
        if (channelLive.isCaller()) {
            channelLog.setFrom(baseExtension);
            channelLog.setTo(channelLive.getExten());
            channelLog.setCallerName(channelLive.getCallerIdName());
            channelLog.setCallerNumber(channelLive.getCallerIdNum());
            channelLog.setCalledNumber(channelLive.getCalledNumber());
            if (!TelephonyUtils.isValidExtension(channelLive.getExten())) {
                channelLog.setExternal(true);
            }
        } else {
            channelLog.setTo(baseExtension);
            ChannelLive linkedChannel = channelLive.getLinkedChannel();
            if (linkedChannel != null) {
                //1. other end is dialed by valid account
                if (TelephonyUtils.isValidInternalEntity(linkedChannel.getAccountCode())) {
                    channelLog.setFrom(linkedChannel.getCallerIdNum());
                    channelLog.setTo(baseExtension);
                    channelLog.setCallerName(linkedChannel.getCallerIdName());
                    channelLog.setCallerNumber(linkedChannel.getCallerIdNum());
                    channelLog.setCalledNumber(linkedChannel.getCalledNumber());
                } else {
                    //2. other end is dialed by external account
                    if (!TelephonyUtils.isServerGenerated(linkedChannel.getChannel())) {
                        channelLog.setExternal(true);
                        channelLog.setFrom(linkedChannel.getCallerIdNum());
                        channelLog.setTo(baseExtension);
                        channelLog.setCallerName(linkedChannel.getCallerIdName());
                        channelLog.setCallerNumber(linkedChannel.getCallerIdNum());
                        channelLog.setCalledNumber(linkedChannel.getCalledNumber());
                    } else {
                        //3. other end is triggered by server
                        channelLog.setFrom(linkedChannel.getCalledNumber());
                        channelLog.setTo(baseExtension);
                        channelLog.setCallerName(linkedChannel.getCallerIdName());
                        channelLog.setCallerNumber(linkedChannel.getCallerIdNum());
                        channelLog.setCalledNumber(linkedChannel.getCalledNumber());
                        String gxNumber = linkedChannel.getVariableMap().get(CallControlProperties.GX_NUMBER);
                        if (!StringUtils.isEmpty(gxNumber)) {
                            channelLog.setCalledNumber(gxNumber);
                        }
                        channelLog.setCallInitiator(channelLive.isCallInitiator());
                    }
                }
            } else {
                log.error("We have weird case where the channel is not caller yet has not linked channel! {}", channelLive);
                channelLog.setFrom(missing);
                channelLog.setCallerName(missing);
                channelLog.setCallerNumber(missing);
                channelLog.setCalledNumber(missing);
            }
        }

        channelLog.setStart(channelLive.getStartInstant());
        channelLog.setCallType(channelLive.getCallType());
        channelLog.setDuration(0L);
        channelLog.setStatus(ChannelLogStatus.IN_PROGRESS);
        ChannelLog save = channelLogRepository.save(channelLog);
        channelLogMap.put(save.getUniqueId(), save);
    }

    @Async
    @Override
    public synchronized void onChannelAnswered(ChannelLive channelLive) {
        if (!TelephonyUtils.isValidInternalEntity(channelLive.getAccountCode())) {
            return;
        }
        ChannelLog channelLog = channelLogMap.get(channelLive.getUniqueID());
        if (channelLog == null) {
            //it can happen when dialed wrong extension and sound being played... but channel start log is being created at the same time
            log.warn("Could not find channel log on answered {}", channelLive);
        } else {
            channelLog.setAnswer(channelLive.getAnswerInstant());
            setMissingData(channelLive, channelLog);
            channelLogRepository.save(channelLog);
        }
    }

    @Async
    @Override
    public synchronized void onChannelAppStart(ChannelLive channelLive) {
        if (!TelephonyUtils.isValidInternalEntity(channelLive.getAccountCode())) {
            return;
        }
        log.trace("onChannelAppStart {}", channelLive);
    }

    @Async
    @Override
    public synchronized void onChannelAppStop(ChannelLive channelLive) {
        if (!TelephonyUtils.isValidInternalEntity(channelLive.getAccountCode())) {
            return;
        }
        log.trace("onChannelAppStop {}", channelLive);
    }

    private void setMissingData(ChannelLive channelLive, ChannelLog channelLog) {
        if (!channelLive.isCaller()) {
            if (channelLive.getLinkedChannel() != null) {
                String linkedAccountCode = channelLive.getLinkedChannel().getAccountCode();
                if (StringUtils.isEmpty(channelLog.getFrom())) {
                    if (TelephonyUtils.isValidInternalEntity(linkedAccountCode)) {
                        channelLog.setFrom(TelephonyUtils.getBaseExtension(linkedAccountCode));
                    } else {
                        channelLog.setFrom(channelLive.getLinkedChannel().getCallerIdNum());
                    }
                }
                if (StringUtils.isEmpty(channelLog.getCallerName())) {
                    channelLog.setCallerName(channelLive.getLinkedChannel().getCallerIdName());
                }
                if (StringUtils.isEmpty(channelLog.getCallerNumber())) {
                    channelLog.setCallerNumber(channelLive.getLinkedChannel().getCallerIdNum());
                }
            }
        }
    }

    @Async
    @Override
    public synchronized void onChannelEnd(ChannelLive channelLive) {
        if (!TelephonyUtils.isValidInternalEntity(channelLive.getAccountCode())) {
            return;
        }
        ChannelLog channelLog = channelLogMap.remove(channelLive.getUniqueID());
        if (channelLog == null) {
            log.debug("Could not find channel log on end {}", channelLive);
        } else {
            endChannelLog(channelLog, channelLive.getEndInstant());

            setMissingData(channelLive, channelLog);
            //last resort
            if (StringUtils.isEmpty(channelLog.getFrom())) {
                channelLog.setFrom(missing);
            }
            if (StringUtils.isEmpty(channelLog.getCallerName())) {
                channelLog.setCallerName(missing);
            }
            if (StringUtils.isEmpty(channelLog.getCallerNumber())) {
                channelLog.setCallerNumber(missing);
            }
            channelLog.setCallType(channelLive.getCallType());
            channelLogRepository.save(channelLog);
        }
    }

    private void endChannelLog(ChannelLog channelLog, Instant end) {
        channelLog.setEnd(end);
        channelLog.setDuration(MiscUtils.getDuration(channelLog.getStart(), channelLog.getEnd()).getSeconds());
        if (channelLog.isCaller() || channelLog.isCallInitiator()) {
            if (channelLog.getAnswer() != null) {
                channelLog.setStatus(ChannelLogStatus.CALL_MADE);
            } else {
                channelLog.setStatus(ChannelLogStatus.CALL_MISSED);
            }
        } else {
            if (channelLog.getAnswer() != null) {
                channelLog.setStatus(ChannelLogStatus.CALL_RECEIVED);
            } else {
                channelLog.setStatus(ChannelLogStatus.CALL_MISSED);
            }
        }
    }

    @Override
    public List<ChannelLog> getChannelLogsForUser(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return channelLogRepository.findByUserIdAndStartBetween(id, fromDate, toDate);
    }

    @Override
    public List<ChannelLog> getChannelLogsForExtension(Long id, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return channelLogRepository.findByExtensionIdAndStartBetween(id, fromDate, toDate);
    }
}
