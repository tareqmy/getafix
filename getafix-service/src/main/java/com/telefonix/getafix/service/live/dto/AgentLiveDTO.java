package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.AgentStatus;
import com.telefonix.getafix.service.live.entity.DeviceStatus;
import com.telefonix.getafix.service.live.entity.EndpointStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-06-27.
 */
@Getter
@Setter
@ToString
public class AgentLiveDTO extends LiveDTO {

    private Long userId;

    private String email;

    private String fullName;

    private Long phoneLineId;

    private String baseExtension;

    private String agentName;

    private String sipUserName;

    private boolean paused;

    private Map<String, AgentStatus> statusMap = new HashMap<>();

    private DeviceStatus deviceStatus;

    private EndpointStatus endpointStatus;

    private List<ChannelLiveDTO> channels;

    public AgentLiveDTO(AgentLive agentLive, ZoneId zoneId) {
        super(zoneId);
        setUserId(agentLive.getUser().getId());
        setEmail(agentLive.getUser().getEmail());
        setFullName(agentLive.getUser().getFullName());
        setPhoneLineId(agentLive.getPhoneLine().getId());
        setBaseExtension(agentLive.getPhoneLine().getExtension().getBaseExtension());
        setAgentName(agentLive.getPhoneLine().getCallerName());
        setSipUserName(agentLive.getPhoneLine().getSipUserName());
        setPaused(agentLive.isPaused());
        setStatusMap(agentLive.getStatusMap());
        setDeviceStatus(agentLive.getDeviceStatus());
        if (agentLive.getPsEndpointLive() != null) {
            setEndpointStatus(EndpointStatus.valueOf(agentLive.getPsEndpointLive().getState()));
        }
        setChannels(agentLive.getChannelLiveMap().values().
            stream()
            .map(channelLive -> new ChannelLiveDTO(channelLive, zoneId))
            .collect(Collectors.toList()));
    }
}
