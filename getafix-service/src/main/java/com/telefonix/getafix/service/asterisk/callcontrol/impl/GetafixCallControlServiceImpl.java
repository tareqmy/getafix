package com.telefonix.getafix.service.asterisk.callcontrol.impl;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.AsteriskCallControllerService;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.TerminalCall;
import com.telefonix.getafix.service.asterisk.callcontrol.*;
import com.telefonix.getafix.service.live.QueueLiveRepository;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
@Service
@Transactional
public class GetafixCallControlServiceImpl extends AbstractCallControlServiceImpl implements AsteriskCallControllerService {

    private ExtensionCallControlService extensionCallControlService;

    private VoiceMailCallControlService voiceMailCallControlService;

    private PSTNCallControlService pstnCallControlService;

    private ConferenceCallControlService conferenceCallControlService;

    private QueueLiveRepository queueLiveRepository;

    @Autowired
    public GetafixCallControlServiceImpl(AsteriskResourceService asteriskResourceService,
                                         ExtensionCallControlService extensionCallControlService,
                                         VoiceMailCallControlService voiceMailCallControlService,
                                         PSTNCallControlService pstnCallControlService,
                                         ConferenceCallControlService conferenceCallControlService,
                                         QueueLiveRepository queueLiveRepository) {
        super(asteriskResourceService);
        this.extensionCallControlService = extensionCallControlService;
        this.voiceMailCallControlService = voiceMailCallControlService;
        this.pstnCallControlService = pstnCallControlService;
        this.conferenceCallControlService = conferenceCallControlService;
        this.queueLiveRepository = queueLiveRepository;
    }

    @Override
    public CallEventListener getNewCall(Channel channel, List<String> args) {
        String exten = channel.getDialplan().getExten();
        String context = channel.getDialplan().getContext();
        String accountCode = channel.getAccountcode();
        String channelName = channel.getName();
        String callerNumber = channel.getCaller().getNumber();
        log.debug("{}/{}/{}/{}/{}", exten, context, accountCode, callerNumber, channelName);

        if (args.contains(CallControlProperties.incoming_arg)) {
            //handle extension call
            return handleNewCall(channel, accountCode, exten, context);
        } else if (args.contains(CallControlProperties.readpin_arg)) {
            return conferenceCallControlService.onReadPin(channel, accountCode, exten, context);

        } else if (args.contains(CallControlProperties.click2dial_arg)) {
            String gxNumber = getChannelVariable(channel, CallControlProperties.GX_NUMBER);
            if (!StringUtils.isEmpty(gxNumber)) {
                String callerId = getChannelVariable(channel, CallControlProperties.GX_CALLERID);
                if (!StringUtils.isEmpty(callerId)) {
                    setChannelVariable(channel, CallControlProperties.CALLERID, callerId);
                }
                String queueId = getChannelVariable(channel, CallControlProperties.GX_QUEUEID);
                if (!StringUtils.isEmpty(queueId)) {
                    //this means it is a queue outbound call??
                    queueLiveRepository.anticipateOutboundQueueCall(context, queueId, channel);
                }
                return handleNewCall(channel, accountCode, gxNumber, context);
            }
        } else if (args.contains(CallControlProperties.zeroprov_arg)) {
            String channelVariable = getChannelVariable(channel, CallControlProperties.GX_QUEUE);
            if (!StringUtils.isEmpty(channelVariable)) {
                log.info("caller pressed {} to exit from queue {}", exten, channelVariable);
                return new TerminalCall(accountCode, CallControlProperties.queue_exten, getafix_context, "voicemail", null, asteriskResourceService);
            }
        }

        //treat unsupported call.
        return hangupChannel(channel, accountCode, context);
    }

    private CallEventListener handleNewCall(Channel channel, String accountCode, String exten, String context) {

        if (exten.matches(CallControlProperties.EXTEN_REGEX)) {
            return extensionCallControlService.handleExtensionCall(channel, accountCode, exten, context);

        } else if (exten.equalsIgnoreCase(CallControlProperties.EXTEN_VM_CHECK)) {
            return voiceMailCallControlService.handleVoiceMailCheck(channel, accountCode, exten, context);

        } else if (exten.matches(CallControlProperties.EXTEN_VM_REGEX)) {
            return voiceMailCallControlService.handleVoiceMailDrop(channel, accountCode, exten, context);

        } else if (exten.matches(CallControlProperties.EXTEN_PSTN_REGEX)) {
            if (!TelephonyUtils.isValidInternalEntity(accountCode)) {
                return pstnCallControlService.handleCall(channel, accountCode, exten, context);
            }
            return pstnCallControlService.handleOutboundCall(channel, accountCode, exten, context);

        } else if (exten.equalsIgnoreCase(CallControlProperties.EXTEN_CONFERENCE)) {
            return conferenceCallControlService.handleConferenceCall(channel, accountCode, exten, context);

        }

        //treat unsupported call.
        return hangupChannel(channel, accountCode, context);
    }
}
