package com.telefonix.getafix.service.live;

import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveListener;
import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXVoiceMailLive;
import com.telefonix.getafix.entity.VoiceMail;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getafix.service.live.entity.VoiceMailLive;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-08-01.
 */
@Slf4j
@Component
public class VoiceMailLiveRepository implements AXVoiceMailLiveListener {

    private Map<String, VoiceMailLive> voiceMailLiveMap;

    private AXVoiceMailLiveRepository axVoiceMailLiveRepository;

    private GeneralService generalService;

    private List<VoiceMailLiveEventListener> voiceMailLiveEventListeners;

    @Autowired
    public VoiceMailLiveRepository(AXVoiceMailLiveRepository axVoiceMailLiveRepository,
                                   GeneralService generalService) {
        axVoiceMailLiveRepository.addAXVoiceMailLiveListener(this);

        this.axVoiceMailLiveRepository = axVoiceMailLiveRepository;
        this.generalService = generalService;

        this.voiceMailLiveMap = new ConcurrentHashMap<>();
        this.voiceMailLiveEventListeners = new ArrayList<>();
    }

    void initialize() {
        generalService.findAllEnabledVoiceMails().forEach(this::initVoiceMailLive);
    }

    // ----------

    public void addVoiceMailLiveEventListener(VoiceMailLiveEventListener voiceMailLiveEventListener) {
        voiceMailLiveEventListeners.add(voiceMailLiveEventListener);
    }

    private void notifyListeners(VoiceMailLive voiceMailLive) {
        if (voiceMailLive == null) {
            log.info("Request for null live notify!");
            return;
        }
        voiceMailLiveEventListeners.forEach(voiceMailLiveEventListener -> {
            voiceMailLiveEventListener.onVoiceMailLiveEvent(voiceMailLive);
        });
    }

    public Collection<VoiceMailLive> getVoiceMailLiveMapValues() {
        return voiceMailLiveMap.values();
    }

    // ------ live map manipulators ----

    public synchronized void initVoiceMailLive(VoiceMail voiceMail) {
        String mailBox = TelephonyUtils.getMailBox(voiceMail.getExtension().getBaseExtension(),
            voiceMail.getExtension().getCompany().getCompanyId());
        VoiceMailLive voiceMailLive = voiceMailLiveMap.get(mailBox);
        if (voiceMailLive == null) { //even though init method im not sure whether this was created before or not!!
            AXVoiceMailLive axVoiceMailLive = axVoiceMailLiveRepository.getAXVoiceMailLive(mailBox);
            //NOTE: not all voicemail belongs to a queue
            voiceMailLive = new VoiceMailLive(voiceMail, axVoiceMailLive);
            voiceMailLiveMap.put(mailBox, voiceMailLive);
            log.trace("Creating new {}", voiceMailLive); //done on queue creation
        } else {
            log.trace("Existing {}", voiceMailLive); //on app start... because it was created on axvmrefresh
        }
    }

    public synchronized void removeVoiceMailLive(VoiceMail voiceMail) {
        String mailBox = TelephonyUtils.getMailBox(voiceMail.getExtension().getBaseExtension(),
            voiceMail.getExtension().getCompany().getCompanyId());
        voiceMailLiveMap.remove(mailBox);
    }

    synchronized VoiceMailLive getVoiceMailLive(String mailbox) {
        VoiceMailLive voiceMailLive = voiceMailLiveMap.get(mailbox);
        if (voiceMailLive == null) {
            if (!TelephonyUtils.isValidMailbox(mailbox)) {
                log.error("Not a valid mailbox {}", mailbox);
                return voiceMailLive;
            }

            String mailExtension = TelephonyUtils.getMailExtension(mailbox);
            String mailContext = TelephonyUtils.getMailContext(mailbox);
            voiceMailLive = generalService.findByCompanyIdAndBaseExtension(mailContext, mailExtension)
                .map(extension -> {
                    AXVoiceMailLive axVoiceMailLive = axVoiceMailLiveRepository.getAXVoiceMailLive(mailbox);
                    log.debug("Creating voicemail live on message update for {} > {}", mailbox, axVoiceMailLive);
                    return new VoiceMailLive(extension.getVoiceMail(), axVoiceMailLive);
                })
                .orElse(null);

            if (voiceMailLive == null) {
                log.error("{} for an invalid extension!", mailbox);
                return voiceMailLive;
            }

            if (!voiceMailLive.getVoiceMail().isEnabled()) {
                log.warn("A Mailbox live {} with a disabled voice-mail!", mailbox);
            }

            voiceMailLiveMap.put(mailbox, voiceMailLive);
        }
        return voiceMailLive;
    }

    // ----------

    @Async
    @Override
    public void onVoiceMailLiveUpdate(AXVoiceMailLive axVoiceMailLive) {
        VoiceMailLive voiceMailLive = getVoiceMailLive(axVoiceMailLive.getMailbox());
        if (voiceMailLive != null) {
            notifyListeners(voiceMailLive);
            log.trace("{}", voiceMailLive);
        }
    }
}
