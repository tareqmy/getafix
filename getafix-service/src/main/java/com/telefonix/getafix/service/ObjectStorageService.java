package com.telefonix.getafix.service;

import com.telefonix.getfix.dto.SoundFile;

import java.util.Set;

/**
 * Created by tareqmy on 2019-05-12.
 */
public interface ObjectStorageService {

    void createBucket(String bucket);

    void deleteBucket(String bucket);

    void upload(String bucket, String directory, String key);

    void download(String bucket, String directory, String key);

    SoundFile getObject(String bucket, String key);

    void delete(String bucket, String key);

    Set<String> list(String bucket);

    boolean exist(String bucket, String key);
}
