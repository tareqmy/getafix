package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.service.AXQueueLogService;
import com.telefonix.getafix.service.QueueLogService;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-06-23.
 */
@Slf4j
@Service
@Transactional
public class QueueLogServiceImpl implements QueueLogService {

    private AXQueueLogService axQueueLogService;

    @Autowired
    public QueueLogServiceImpl(AXQueueLogService axQueueLogService) {
        this.axQueueLogService = axQueueLogService;
    }

    @Override
    public List<AXQueueLog> findAll() {
        return axQueueLogService.findAll()
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<AXQueueLog> findOne(Long id) {
        return axQueueLogService.findOne(id);
    }

    @Override
    public AXQueueLog getAXQueueLog(Long id) {
        return findOne(id).orElseThrow(
            () -> new GenericGetafixException("Invalid queue log id " + id)
        );
    }

    @Override
    public List<AXQueueLog> findAllByQueueName(String queueName, LocalDate from, LocalDate to, ZoneId zoneId) {
        if (from.isAfter(to)) {
            log.info("changing {} to {}", from, to);
            from = to;
        }
        Instant fromDate = from.atStartOfDay(zoneId).toInstant();
        Instant toDate = to.plusDays(1).atStartOfDay(zoneId).toInstant();
        return axQueueLogService.findByQueueNameAndTimeBetween(queueName, fromDate, toDate)
            .stream()
            .sorted()
            .collect(Collectors.toList());
    }
}
