package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.User;
import com.telefonix.getfix.dto.ExtensionReportDTO;

import java.util.List;

/**
 * Created by tareqmy on 2019-07-01.
 */
public interface ReportService {

    List<ExtensionReportDTO> getExtensionReports(User user, Extension extension);
}
