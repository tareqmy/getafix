package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.UserRole;
import com.telefonix.getafix.entity.enums.Role;

/**
 * Created by tmyousuf on 2/15/16.
 */
public interface UserRoleService {

    UserRole getUserRole(Role role);

    UserRole getUserRole(String role);
}
