package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.service.live.entity.SystemLive;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-07-09.
 */
@Getter
@Setter
@ToString
public class SystemLiveDTO extends LiveDTO {

    private ZoneId zoneId;

    private List<ChannelLiveDTO> channels = new ArrayList<>();

    private List<PSEndpointLiveDTO> endpoints = new ArrayList<>();

    public SystemLiveDTO(SystemLive systemLive, ZoneId zoneId) {
        super(zoneId);
        setZoneId(zoneId);
        setChannels(systemLive.getChannelLiveMap().values().stream()
            .map(channelLive -> new ChannelLiveDTO(channelLive, zoneId))
            .collect(Collectors.toList()));
        setEndpoints(systemLive.getPsEndpointLiveMap().values().stream()
            .map(psEndpointLive -> new PSEndpointLiveDTO(psEndpointLive, zoneId))
            .collect(Collectors.toList()));
    }
}
