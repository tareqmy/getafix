package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.asterisk.service.live.entity.QueueParams;
import com.telefonix.getafix.service.live.entity.AgentStatus;
import com.telefonix.getafix.service.live.entity.QueueLive;
import com.telefonix.getfix.dto.VoiceMailMessageDTO;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-06-25.
 */
@Getter
@Setter
@ToString
public class QueueLiveDTO extends LiveDTO {

    private Long id;

    private String queueId;

    private String queueName;

    private String baseExtension;

    private String companyId;

    private int max;

    private int weight;

    private int serviceLevel;

    private String strategy;

    private int holdTime;

    private int talkTime;

    private int completed;

    private int abandoned;

    private long totalWait;

    private long avgWait;

    private long maxWait;

    private Map<String, AgentStatus> agentStatusMap = new HashMap<>();

    private int agentsCount;

    private List<AgentLiveDTO> agents;

    private int queuedChannelsCount;

    private List<QueueChannelLiveDTO> queuedChannels;

    private int channelsCount;

    private List<ChannelLiveDTO> channels;

    private int messageCount;

    private List<VoiceMailMessageDTO> messages;

    public QueueLiveDTO(QueueLive queueLive, ZoneId zoneId, boolean supervise) {
        super(zoneId);
        setId(queueLive.getQueue().getId());
        setQueueId(queueLive.getQueueId());
        setQueueName(queueLive.getQueue().getQueueName());
        setBaseExtension(queueLive.getQueue().getExtension().getBaseExtension());
        setCompanyId(queueLive.getCompanyId());
        setMax(queueLive.getQueue().getMaxLength());
        setWeight(queueLive.getQueue().getWeight());
        setServiceLevel(queueLive.getQueue().getServiceLevel());
        setStrategy(queueLive.getQueue().getStrategy().name());

        QueueParams queueParams = queueLive.getQueueParams();
        if (queueParams != null) {
            setHoldTime(queueParams.getHoldTime());
            setTalkTime(queueParams.getTalkTime());
            setCompleted(queueParams.getCompleted());
            setAbandoned(queueParams.getAbandoned());
        }

        //how to ensure the agents in livemap has also the entry for this queue in its statusmap.
        Map<String, AgentStatus> agentStatusMap = queueLive.getAgentLiveMap().entrySet().stream()
            .filter(stringAgentLiveEntry -> stringAgentLiveEntry.getValue().getStatusMap().containsKey(queueId)) //precautionary statep
            .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getStatusMap().get(queueId)));
        setAgentStatusMap(agentStatusMap);

        if (supervise) {
            setAgents(queueLive.getAgentLiveMap().values().stream()
                .map(agentLive -> new AgentLiveDTO(agentLive, zoneId))
                .collect(Collectors.toList()));
        }
        setAgentsCount(queueLive.getAgentLiveMap().size());

        List<QueueChannelLiveDTO> queuedChannels = queueLive.getChannelLiveMap().values().stream()
            .filter(queueChannelLive -> !queueChannelLive.isLeft())
            .map(queueChannelLive -> new QueueChannelLiveDTO(queueChannelLive, zoneId))
            .collect(Collectors.toList());
        if (supervise) {
            setQueuedChannels(queuedChannels);
        }
        setQueuedChannelsCount(queuedChannels.size());

        Instant now = Instant.now();
        Long totalWait = queuedChannels
            .stream()
            .map(channelLiveDTO -> MiscUtils.getDuration(channelLiveDTO.getQueuedInstant(), now).toSeconds())
            .reduce(0L, Long::sum);
        setTotalWait(totalWait);
        if (queuedChannelsCount > 0) {
            setAvgWait(totalWait / queuedChannelsCount);
        }
        Long maxWait = queuedChannels
            .stream()
            .map(channelLiveDTO -> MiscUtils.getDuration(channelLiveDTO.getQueuedInstant(), now).toSeconds())
            .max(Long::compareTo)
            .orElse(0L);
        setMaxWait(maxWait);

        List<ChannelLiveDTO> channels = queueLive.getChannelLiveMap().values().stream()
            .map(queueChannelLive -> new QueueChannelLiveDTO(queueChannelLive, zoneId))
            .collect(Collectors.toList());
        if (supervise) {
            setChannels(channels);
        }
        setChannelsCount(channels.size());

        List<VoiceMailMessageDTO> messages = queueLive.getVoiceMailLive().getAxVoiceMailLive().getAxVoiceMessageMap().values().stream()
            .map(axVoiceMessage -> new VoiceMailMessageDTO(axVoiceMessage, zoneId)).collect(Collectors.toList());
        if (supervise) {
            setMessages(messages);
        }
        setMessageCount(messages.size());
    }
}
