package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.ChannelLog;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-20.
 */
public interface ChannelLogService {

    void refresh();

    List<ChannelLog> getChannelLogsForUser(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    List<ChannelLog> getChannelLogsForExtension(Long id, LocalDate from, LocalDate to, ZoneId zoneId);
}
