package com.telefonix.getafix.service.live;

import com.telefonix.getafix.asterisk.service.live.ChannelLiveEventListener;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveEventListener;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.repository.PhoneLineRepository;
import com.telefonix.getafix.service.AgentLogService;
import com.telefonix.getafix.service.EndpointLogService;
import com.telefonix.getafix.service.asterisk.callcontrol.CallControlProperties;
import com.telefonix.getafix.service.live.entity.AgentLive;
import com.telefonix.getafix.service.live.entity.AgentStatus;
import com.telefonix.getafix.service.live.entity.DeviceStatus;
import com.telefonix.getafix.service.live.entity.QueueLive;
import com.telefonix.getfix.utils.TelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-07-03.
 */
@Slf4j
@Component
public class AgentLiveRepository implements PSEndpointLiveEventListener, ChannelLiveEventListener {

    private Map<String, AgentLive> agentLiveMap;

    private PhoneLineRepository phoneLineRepository;

    private PSEndpointLiveRepository psEndpointLiveRepository;

    private AgentLogService agentLogService;

    private EndpointLogService endpointLogService;

    private ScheduledExecutorService scheduleExecutor;

    private List<AgentLiveEventListener> agentLiveEventListeners;

    public AgentLiveRepository(ChannelLiveRepository channelLiveRepository,
                               PhoneLineRepository phoneLineRepository,
                               PSEndpointLiveRepository psEndpointLiveRepository,
                               AgentLogService agentLogService,
                               EndpointLogService endpointLogService,
                               ScheduledExecutorService scheduleExecutor) {
        channelLiveRepository.addChannelLiveEventListener(this);
        psEndpointLiveRepository.addPSEndpointLiveEventListener(this);

        this.phoneLineRepository = phoneLineRepository;
        this.psEndpointLiveRepository = psEndpointLiveRepository;
        this.agentLogService = agentLogService;
        this.endpointLogService = endpointLogService;
        this.scheduleExecutor = scheduleExecutor;

        this.agentLiveMap = new ConcurrentHashMap<>();
        this.agentLiveEventListeners = new ArrayList<>();
    }

    public void addAgentLiveEventListener(AgentLiveEventListener agentLiveEventListener) {
        agentLiveEventListeners.add(agentLiveEventListener);
    }

    private void notifyListeners(AgentLive agentLive) {
        if (agentLive == null) {
            log.info("Request for null live notify!");
            return;
        }

        agentLiveEventListeners.forEach(agentLiveEventListener -> {
            agentLiveEventListener.onEvent(agentLive);
        });
    }

    public Collection<AgentLive> getAgentLiveMapValues() {
        return agentLiveMap.values();
    }

    // ----- live map manipulators -----

    //caveat: this is removed only when phone line is deleted!! but not added until agent joins callcenter
    synchronized AgentLive initAgentLive(User user, PhoneLine phoneLine) {
        PSEndpointLive psEndpointLive = psEndpointLiveRepository.getPSEndpointLive(phoneLine.getSipUserName());
        AgentLive agentLive = new AgentLive(user, phoneLine, psEndpointLive);
        agentLiveMap.put(phoneLine.getSipUserName(), agentLive);
        return agentLive;
    }

    public synchronized void updateAgentLive(PhoneLine phoneLine) {
        AgentLive agentLive = agentLiveMap.get(phoneLine.getSipUserName());
        if (agentLive != null) {
            agentLive.setPhoneLine(phoneLine);
        }
    }

    public synchronized void removeAgentLive(PhoneLine phoneLine) {
        agentLiveMap.remove(phoneLine.getSipUserName());
    }

    private synchronized AgentLive getAgentLive(String sipUserName) {
        if (!TelephonyUtils.isValidInternalEntity(sipUserName)) {
            return null;
        }
        AgentLive agentLive = agentLiveMap.get(sipUserName);
        if (agentLive == null) {
            Optional<PhoneLine> phoneLine = phoneLineRepository.findBySipUserName(sipUserName);
            if (phoneLine.isPresent() && phoneLine.get().getJoinedUser() != null) {
                PSEndpointLive psEndpointLive = psEndpointLiveRepository.getPSEndpointLive(sipUserName);
                agentLive = new AgentLive(phoneLine.get().getJoinedUser(), phoneLine.get(), psEndpointLive);
                agentLiveMap.put(sipUserName, agentLive);
            }
        }
        return agentLive;
    }

    // -----

    public synchronized AgentLive getAgentLive(User user) {
        if (user.getJoinedPhoneLine() == null) {
            return null;
        }
        return agentLiveMap.get(user.getJoinedPhoneLine().getSipUserName());
    }

    public ChannelLive getAgentChannel(User user, String channelId) {
        return findAgentLive(user)
            .map(agentLive -> agentLive.getChannelLiveMap().get(channelId))
            .orElse(null);
    }

    // ----- public method to avoid exposing map/live to other packages

    private Optional<AgentLive> findAgentLive(User user) {
        if (user == null || user.getJoinedPhoneLine() == null) {
            return Optional.empty();
        }
        return findAgentLive(user.getJoinedPhoneLine().getSipUserName());
    }

    private Optional<AgentLive> findAgentLive(String sipUserName) {
        if (!TelephonyUtils.isValidInternalEntity(sipUserName)) {
            return Optional.empty();
        }
        AgentLive agentLive = agentLiveMap.get(sipUserName);
        if (agentLive == null) {
            return Optional.empty();
        }
        return Optional.of(agentLive);
    }

    public boolean isLiveAgent(String sipUserName) {
        return findAgentLive(sipUserName).isPresent();
    }

    public boolean isLiveAgent(User user) {
        return findAgentLive(user).isPresent();
    }

    public boolean isUsedByLiveAgent(Extension extension) {
        if (extension == null) {
            return false;
        }
        //a user can have multiple extension. user being present does not mean this is the extension being used
        return findAgentLive(extension.getUser())
            .map(agentLive -> agentLive.getPhoneLine().getExtension().equals(extension))
            .orElse(false);
    }

    public String getAgentSipUserName(User user) {
        return findAgentLive(user)
            .map(live -> live.getPhoneLine().getSipUserName())
            .orElse(null);
    }

    public boolean isAgentChannel(User user, String channelId) {
        return findAgentLive(user)
            .map(agentLive -> agentLive.getChannelLiveMap().containsKey(channelId))
            .orElse(false);
    }

    // ----- pause actions -----

    void pauseAgent(AgentLive agentLive, User user) {
        agentLive.setPaused(true);
        agentLogService.onPauseAgent(user, agentLive.getPhoneLine());
        notifyListeners(agentLive);
    }

    void resumeAgent(AgentLive agentLive, User user) {
        agentLive.setPaused(false);
        agentLogService.onResumeAgent(user, agentLive.getPhoneLine());
        notifyListeners(agentLive);
    }

    // ---- on member events ----- called through queue live repo. because queue live needs the changes to agent belonging to it

    AgentLive onQueueMemberAdded(String sipUserName, QueueLive queueLive, boolean paused, Integer deviceStatus) {
        AgentLive agentLive = getAgentLive(sipUserName);
        if (agentLive != null) {
            boolean existing = agentLive.getStatusMap().get(queueLive.getQueueId()) != null;
            agentLive.getStatusMap().put(queueLive.getQueueId(), AgentStatus.JOINED);
            if (paused) {
                agentLive.getStatusMap().put(queueLive.getQueueId(), AgentStatus.PAUSED);
            }
            agentLive.setDeviceStatus(DeviceStatus.values()[deviceStatus]);
            endpointLogService.onEndpointUpdate(agentLive.getPsEndpointLive().getResource(), agentLive.getPsEndpointLive().getState(), agentLive.getDeviceStatus().name(), agentLive.getPhoneLine(), agentLive.getUser());
            log.trace("onQueueMemberAdded {}", agentLive);
            if (!existing) {
                agentLogService.onJoinQueue(agentLive.getUser(), agentLive.getPhoneLine(), queueLive.getQueue());
            }
            notifyListeners(agentLive);
        }
        return agentLive;
    }

    AgentLive onQueueMemberRemoved(String sipUserName, QueueLive queueLive, Integer deviceStatus) {
        AgentLive agentLive = getAgentLive(sipUserName);
        if (agentLive != null) {
            agentLive.getStatusMap().remove(queueLive.getQueueId());
            agentLive.setDeviceStatus(DeviceStatus.values()[deviceStatus]);
            endpointLogService.onEndpointUpdate(agentLive.getPsEndpointLive().getResource(), agentLive.getPsEndpointLive().getState(), agentLive.getDeviceStatus().name(), agentLive.getPhoneLine(), agentLive.getUser());
            log.trace("queueAgentRemoved {}", agentLive);
            agentLogService.onLeaveQueue(agentLive.getUser(), agentLive.getPhoneLine(), queueLive.getQueue());
            notifyListeners(agentLive);
        }
        return agentLive;
    }

    AgentLive onQueueMemberRemoved(String sipUserName, QueueLive queueLive) {
        AgentLive agentLive = getAgentLive(sipUserName);
        if (agentLive != null) {
            agentLive.getStatusMap().remove(queueLive.getQueueId());
            endpointLogService.onEndpointUpdate(agentLive.getPsEndpointLive().getResource(), agentLive.getPsEndpointLive().getState(), agentLive.getDeviceStatus().name(), agentLive.getPhoneLine(), agentLive.getUser());
            log.trace("queueAgentRemoved {}", agentLive);
            agentLogService.onLeaveQueue(agentLive.getUser(), agentLive.getPhoneLine(), queueLive.getQueue());
            notifyListeners(agentLive);
        }
        return agentLive;
    }

    AgentLive onQueueMemberPause(String sipUserName, String queueName, boolean paused, Integer deviceStatus) {
        AgentLive agentLive = getAgentLive(sipUserName);
        if (agentLive != null) {
            if (paused) {
                agentLive.getStatusMap().put(queueName, AgentStatus.PAUSED);
            } else {
                agentLive.getStatusMap().put(queueName, AgentStatus.JOINED);
            }
            agentLive.setDeviceStatus(DeviceStatus.values()[deviceStatus]);
            endpointLogService.onEndpointUpdate(agentLive.getPsEndpointLive().getResource(), agentLive.getPsEndpointLive().getState(), agentLive.getDeviceStatus().name(), agentLive.getPhoneLine(), agentLive.getUser());
            log.trace("queueAgentPause {}", agentLive);
            notifyListeners(agentLive);
        }
        return agentLive;
    }

    AgentLive onQueueMemberStatus(String sipUserName, Integer deviceStatus) {
        AgentLive agentLive = getAgentLive(sipUserName);
        if (agentLive != null) {
            agentLive.setDeviceStatus(DeviceStatus.values()[deviceStatus]);
            endpointLogService.onEndpointUpdate(agentLive.getPsEndpointLive().getResource(), agentLive.getPsEndpointLive().getState(), agentLive.getDeviceStatus().name(), agentLive.getPhoneLine(), agentLive.getUser());
            log.trace("queueAgentStatus {}", agentLive);
            notifyListeners(agentLive);
        }
        return agentLive;
    }

    // ----- channel events -----

    @Async
    @Override
    public void onChannelStart(ChannelLive channelLive) {
        log.trace("onChannelStart {}", channelLive);
        AgentLive agentLive = getAgentLive(channelLive.getAccountCode());
        if (agentLive != null) {
            if (channelLive.getLinkedChannel() != null) {
                //NOTE: this is to find out an outbound queue call of agents end
                String queueId = channelLive.getLinkedChannel().getVariableMap().get(CallControlProperties.GX_QUEUEID); //this is set through server generated call
                if (!StringUtils.isEmpty(queueId)) {
                    channelLive.setCallType(Constants.queueCallTypePrefix + queueId);
                    //this end of the call should be treated as caller
                    channelLive.setCallInitiator(true);
                }

                if (channelLive.getLinkedChannel().getApp().equals(ChannelLiveRepository.queue)) {
                    //for incoming acd queue call to agent
                    String queueName = channelLive.getLinkedChannel().getAppData().split(",")[0];
                    channelLive.setCallType(Constants.queueCallTypePrefix + TelephonyUtils.getQueueIdFromQueueName(queueName));
                }
            }
            agentLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
            notifyListeners(agentLive);
        }
    }

    @Async
    @Override
    public void onChannelAnswered(ChannelLive channelLive) {
        log.trace("onChannelAnswered {}", channelLive);
        AgentLive agentLive = getAgentLive(channelLive.getAccountCode());
        if (agentLive != null) {
            agentLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
            notifyListeners(agentLive);
        }
    }

    @Async
    @Override
    public void onChannelAppStart(ChannelLive channelLive) {
        log.trace("onChannelAppStart {}", channelLive);
        AgentLive agentLive = getAgentLive(channelLive.getAccountCode());
        if (agentLive != null) {
            //this should be already set from channel start
            if (channelLive.getApp().equals(ChannelLiveRepository.appQueue)) {
                String queueName = channelLive.getLinkedChannel().getAppData().split(",")[0];
                channelLive.setCallType(Constants.queueCallTypePrefix + TelephonyUtils.getQueueIdFromQueueName(queueName));
            }
            agentLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
            notifyListeners(agentLive);
        }
    }

    @Async
    @Override
    public void onChannelAppStop(ChannelLive channelLive) {
        log.trace("onChannelAppStop {}", channelLive);
        AgentLive agentLive = getAgentLive(channelLive.getAccountCode());
        if (agentLive != null) {
            if (channelLive.getApp().equals(ChannelLiveRepository.appQueue)) {
                //NOTE: it means the agent end of the queue call is
                //Does it matter to us?
            }
            agentLive.getChannelLiveMap().put(channelLive.getUniqueID(), channelLive);
            notifyListeners(agentLive);
        }
    }

    @Async
    @Override
    public void onChannelEnd(ChannelLive channelLive) {
        log.trace("onChannelEnd {}", channelLive);
        AgentLive agentLive = getAgentLive(channelLive.getAccountCode());
        if (agentLive != null) {
            scheduleExecutor.schedule(() -> {
                agentLive.getChannelLiveMap().remove(channelLive.getUniqueID());
                notifyListeners(agentLive);
            }, 1L, TimeUnit.SECONDS);
        }
    }

    // ------ endpoint events -----

    @Async
    @Override
    public void onPSEndpointStateUpdate(PSEndpointLive psEndpointLive) {
        log.trace("onPSEndpointStateUpdate {}", psEndpointLive);
        if (psEndpointLive == null || psEndpointLive.getResource() == null) {
            log.error("{}", psEndpointLive);
            return;
        }
        AgentLive agentLive = getAgentLive(psEndpointLive.getResource());
        String deviceStatus = DeviceStatus.UNKNOWN.name();
        PhoneLine phoneLine = null;
        User user = null;
        if (agentLive != null) {
            deviceStatus = agentLive.getDeviceStatus().name();
            phoneLine = agentLive.getPhoneLine();
            user = agentLive.getUser();
            notifyListeners(agentLive);
        }
        endpointLogService.onEndpointUpdate(psEndpointLive.getResource(), psEndpointLive.getState(), deviceStatus, phoneLine, user);
    }
}
