package com.telefonix.getafix.service.impl;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.repository.*;
import com.telefonix.getafix.service.GeneralService;
import com.telefonix.getfix.exceptions.GenericGetafixException;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-04-28.
 */
@Slf4j
@Service
public class GeneralServiceImpl implements GeneralService {

    private CompanyRepository companyRepository;

    private UserRepository userRepository;

    private ExtensionRepository extensionRepository;

    private VoiceMailRepository voiceMailRepository;

    private PhoneRepository phoneRepository;

    private PhoneLineRepository phoneLineRepository;

    private QueueRepository queueRepository;

    private ProviderRepository providerRepository;

    private PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public GeneralServiceImpl(CompanyRepository companyRepository, UserRepository userRepository,
                              ExtensionRepository extensionRepository, VoiceMailRepository voiceMailRepository,
                              PhoneRepository phoneRepository,
                              PhoneLineRepository phoneLineRepository, QueueRepository queueRepository,
                              ProviderRepository providerRepository, PhoneNumberRepository phoneNumberRepository) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.extensionRepository = extensionRepository;
        this.voiceMailRepository = voiceMailRepository;
        this.phoneRepository = phoneRepository;
        this.phoneLineRepository = phoneLineRepository;
        this.queueRepository = queueRepository;
        this.providerRepository = providerRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public User getCurrentLoggedInUser() {
        return getUser(SecurityUtils.getCurrentUserName());
    }

    @Override
    public Company getCompany(Long id) {
        return companyRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid company id " + id)
        );
    }

    @Override
    public User getUser(String email) {
        return userRepository.findOneByEmail(email).orElseThrow(
            () -> new GenericGetafixException("Current Logged user may not be active or found!")
        );
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid user id " + id)
        );
    }

    @Override
    public Extension getExtension(Long id) {
        return extensionRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid extension id " + id)
        );
    }

    @Override
    public Optional<Extension> findByCompanyIdAndBaseExtension(String companyId, String baseExtension) {
        return extensionRepository.findByCompanyIdAndBaseExtension(companyId, baseExtension);
    }

    @Override
    public List<VoiceMail> findAllVoiceMails() {
        return voiceMailRepository.findAll();
    }

    @Override
    public List<VoiceMail> findAllEnabledVoiceMails() {
        return voiceMailRepository.findAllByEnabledTrue();
    }

    @Override
    public Phone getPhone(Long id) {
        return phoneRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phone id " + id)
        );
    }

    @Override
    public PhoneLine getPhoneLine(Long id) {
        return phoneLineRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phoneLine id " + id)
        );
    }

    @Override
    public Optional<PhoneLine> findBySipUserName(String sipUserName) {
        return phoneLineRepository.findBySipUserName(sipUserName);
    }

    @Override
    public Queue getQueue(Long id) {
        return queueRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid queue id " + id)
        );
    }

    @Override
    public Optional<Queue> findByCompanyIdAndQueueId(String companyId, String queueId) {
        return queueRepository.findByCompanyIdAndQueueId(companyId, queueId);
    }

    @Override
    public Provider getProvider(Long id) {
        return providerRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid provider id " + id)
        );
    }

    @Override
    public PhoneNumber getPhoneNumber(String id) {
        return phoneNumberRepository.findById(id).orElseThrow(
            () -> new GenericGetafixException("Invalid phoneNumber id " + id)
        );
    }
}
