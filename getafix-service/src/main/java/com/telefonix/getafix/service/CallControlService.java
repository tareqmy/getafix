package com.telefonix.getafix.service;

/**
 * Created by tareqmy on 2019-07-11.
 */
public interface CallControlService {

    void dial(String name, String number, String fromExtension);

    void dial(String name, String number, String destinationCallerId, String fromExtension, String queueId);

    void transfer(String channelId, Long userId);

    void escalate(String channelId, Long userId);

    void moveToQueue(String channelId, Long queueId);

    void hangup(String channelId);
}
