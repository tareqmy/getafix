package com.telefonix.getafix.service.asterisk.callcontrol;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;

/**
 * Created by tareqmy on 2019-05-14.
 */
public interface ConferenceCallControlService {

    CallEventListener handleConferenceCall(Channel channel, String accountCode, String exten, String context);

    CallEventListener onReadPin(Channel channel, String accountCode, String exten, String context);
}
