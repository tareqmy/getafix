package com.telefonix.getafix.service.asterisk.callcontrol;

import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Component
public class CallControlProperties {

    public static final String drop_voicemail_exten = "drop_voicemail";

    public static final String check_voicemail_exten = "check_voicemail";

    public static final String dial_exten = "dial";

    public static final String aa_exten = "aa";

    public static final String queue_exten = "queue";

    public static final String conference_exten = "conference";

    public static final String readpin_exten = "readpin";

    public static final String invalidpin_exten = "invalidpin";

    public static final String pstn_exten = "pstn";

    public static final String end_with_message_exten = "end_with_message";

    public static final String busy_treat_exten = "busy_treat";

    public static final String invalid_exten = "i";

    //asterisk functions
    public static final String CALLERID = "CALLERID(all)";

    public static final String CALLERID_NAME = "CALLERID(name)";

    public static final String CALLERID_NUMBER = "CALLERID(num)";

    //getafix variables
    public static final String yes = "yes";

    public static final String no = "no";

    public static final String SERVER_GENERATED = "SERVER_GENERATED";

    public static final String GX_DIAL = "GX_DIAL";

    public static final String GX_RING_DURATION = "GX_RING_DURATION";

    public static final String GX_DIAL_OPTIONS = "GX_DIAL_OPTIONS";

    public static final String GX_NO_VOICEMAIL = "GX_NO_VOICEMAIL";

    public static final String GX_EXTEN = "GX_EXTEN";

    public static final String GX_CONTEXT = "GX_CONTEXT";

    public static final String GX_MAILBOX = "GX_MAILBOX";

    public static final String GX_MAILBOX_OPTIONS = "GX_MAILBOX_OPTIONS";

    public static final String GX_QUEUE = "GX_QUEUE";

    public static final String GX_QUEUE_OPTIONS = "GX_QUEUE_OPTIONS";

    public static final String GX_CALLERNAME = "GX_CALLERNAME";

    public static final String OPTION_ = "OPTION_";

    public static final String GX_REPEAT = "GX_REPEAT";

    public static final String GX_PROMPTS = "GX_PROMPTS";

    public static final String GX_END_MESSAGE = "GX_END_MESSAGE";

    public static final String PIN = "PIN";

    public static final String GX_CONF_ID = "GX_CONF_ID";

    public static final String GX_MOH_CLASS = "GX_MOH_CLASS";

    public static final String GX_ANNOUNCE = "GX_ANNOUNCE";

    public static final String GX_ANNOUNCE_ONLY = "GX_ANNOUNCE_ONLY";

    public static final String GX_ADMIN = "GX_ADMIN";

    public static final String GX_MARKED = "GX_MARKED";

    public static final String GX_WAIT_MARKED = "GX_WAIT_MARKED";

    public static final String GX_END_MARKED = "GX_END_MARKED";

    public static final String GX_START_MUTED = "GX_START_MUTED";

    public static final String GX_NUMBER = "GX_NUMBER";

    public static final String GX_CALLERID = "GX_CALLERID";

    public static final String GX_QUEUEID = "GX_QUEUEID";

    public static final String incoming_arg = "incoming";

    public static final String readpin_arg = "readpin";

    public static final String click2dial_arg = "click2dial";

    public static final String zeroprov_arg = "zeroprov";

    //regex ------

    public static final String EXTEN_REGEX = "^[0-9]{2}$";

    public static final String EXTEN_VM_REGEX = "^\\*[0-9]{2}$";

    public static final String EXTEN_VM_CHECK = "*29";

    public static final String EXTEN_CONFERENCE = "*33";

    public static final String EXTEN_PSTN_REGEX = "^\\d{11,14}$";

    //sounds
    public final String pbx_invalid_sound = "pbx-invalid";

    public final String feature_not_available_sound = "feature-not-avail-line";
}
