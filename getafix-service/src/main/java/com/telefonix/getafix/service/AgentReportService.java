package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.User;
import com.telefonix.getfix.dto.AgentReportDTO;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Created by tareqmy on 29/8/19.
 */
public interface AgentReportService {

    AgentReportDTO getAgentReport(User user, LocalDate from, LocalDate to, ZoneId zoneId);
}
