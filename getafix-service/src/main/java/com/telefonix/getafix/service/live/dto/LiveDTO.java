package com.telefonix.getafix.service.live.dto;

import com.telefonix.getfix.utils.MiscUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;

/**
 * Created by tareqmy on 2019-07-03.
 */
@Getter
@Setter
@ToString
public abstract class LiveDTO implements Serializable {

    private Long epoch;

    private String time;

    LiveDTO(ZoneId zoneId) {
        Instant now = Instant.now();
        setEpoch(now.getEpochSecond());
        setTime(MiscUtils.formatTime(now, zoneId));
    }
}
