package com.telefonix.getafix.service.asterisk;

import com.telefonix.getafix.entity.Company;

/**
 * Created by tareqmy on 2019-02-04.
 */
public interface GetafixDialplanService {

    boolean isReservedCategory(String category);

    void onCompanyCreated(Company company);

    void onCompanyDeleted(Company company);

    void rebuildDialplans();

    void rebuildDialplanForCompany(Company company);
}
