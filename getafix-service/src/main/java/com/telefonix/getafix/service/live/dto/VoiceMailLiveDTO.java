package com.telefonix.getafix.service.live.dto;

import com.telefonix.getafix.service.live.entity.VoiceMailLive;
import com.telefonix.getfix.dto.VoiceMailMessageDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 18/9/19.
 */
@Getter
@Setter
@ToString
public class VoiceMailLiveDTO extends LiveDTO {

    private Long id;

    private Long extensionId;

    private String baseExtension;

    private int messageCount;

    private List<VoiceMailMessageDTO> messages;

    public VoiceMailLiveDTO(VoiceMailLive voiceMailLive, ZoneId zoneId) {
        super(zoneId);
        setId(voiceMailLive.getId());
        setExtensionId(voiceMailLive.getExtension().getId());
        setBaseExtension(voiceMailLive.getExtension().getBaseExtension());

        List<VoiceMailMessageDTO> messages = voiceMailLive.getAxVoiceMailLive().getAxVoiceMessageMap().values().stream()
            .map(axVoiceMessage -> new VoiceMailMessageDTO(axVoiceMessage, zoneId)).collect(Collectors.toList());
        setMessages(messages);
        setMessageCount(messages.size());
    }
}
