package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.*;
import com.telefonix.getfix.dto.PhoneDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-03-25.
 */
public interface PhoneService {

    Phone createPhone(PhoneDTO phoneDTO);

    Phone updatePhone(PhoneDTO phoneDTO);

    List<Phone> findAll();

    Optional<Phone> findOne(Long id);

    Phone getPhone(Long id);

    List<PhoneLine> getPhoneLines(Long id);

    List<Extension> getExtensions(Long id);

    List<Phone> getPhones(Company company);

    void delete(Long id);

    void delete(Company company);

    List<Phone> getPhones(User user);
}
