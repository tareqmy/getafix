package com.telefonix.getafix.service;

import com.telefonix.getafix.entity.*;
import com.telefonix.getfix.dto.ExtensionDTO;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-14.
 */
public interface ExtensionService {

    Extension createExtension(ExtensionDTO extensionDTO);

    Extension updateExtension(ExtensionDTO extensionDTO);

    Extension unassignUser(Extension extension);

    List<Extension> findAll();

    Optional<Extension> findOne(Long id);

    Optional<Extension> findByCompanyIdAndBaseExtension(String companyId, String baseExtension);

    List<Extension> getExtensions(User user);

    List<Extension> getExtensions(Phone phone, boolean unAssigned);

    List<Extension> getExtensions(Company company, boolean unAssigned);

    Extension getExtension(Long id);

    void delete(Long id);

    void delete(Company company);

    List<ChannelLog> getChannelLogs(Long id, LocalDate from, LocalDate to, ZoneId zoneId);

    Optional<PhoneLine> getPhoneLine(Long id);
}
