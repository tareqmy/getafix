package com.telefonix.getafix.service.live;

import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.live.dto.LiveDTO;
import lombok.Getter;
import lombok.ToString;
import reactor.core.publisher.FluxSink;

import java.time.ZoneId;
import java.util.function.Consumer;

/**
 * Created by tareqmy on 17/9/19.
 */
@Getter
@ToString
public class LiveConsumer<T extends LiveDTO> {

    private User user;

    //NOTE: if zoneid changed during live stream it will not have any affect on this
    private ZoneId zoneId;

    private Consumer<T> consumer;

    private FluxSink<T> emitter;

    public LiveConsumer(User user, Consumer<T> consumer, FluxSink<T> emitter) {
        this.user = user;
        this.zoneId = user.getZoneId();
        this.consumer = consumer;
        this.emitter = emitter;
    }
}
