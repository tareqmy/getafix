package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueue;
import com.telefonix.getafix.asterisk.entity.enums.*;
import com.telefonix.getafix.asterisk.repository.AXQueueRepository;
import com.telefonix.getafix.asterisk.service.AXQueueService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXQueueServiceImpl implements AXQueueService {

    private AXQueueRepository axQueueRepository;

    @Autowired
    public AXQueueServiceImpl(AXQueueRepository axQueueRepository) {
        this.axQueueRepository = axQueueRepository;
    }

// * queue sample
// * name;weight;musiconhold;strategy;ringinuse;autopause;maxlen;setinterfacevar;setqueueentryvar;setqueuevar;
// timeout;retry;timeoutpriority;announce_frequency;min_announce_frequency;periodic_announce_frequency;
// random_periodic_announce;relative_periodic_announce;announce_holdtime;announce_position;
// announce_position_limit;announce_round_seconds;joinempty;leavewhenempty
// * support;10;default;rrmemory;no;no;20;yes;yes;yes;
// 10;5;app;30;30;45;
// no;yes;once;limit;
// 10;30;unavailable,invalid,unknown;unavailable,invalid,unknown

    // * sales;5;default;rrmemory;no;no;20;yes;yes;yes;10;5;app;30;30;45;no;yes;once;limit;10;30;unavailable,invalid,unknown;unavailable,invalid,unknown
    @Override
    public AXQueue create(String queueId, Strategy strategy, int weight, int serviceLevel, int maxLen, String context) {
        AXQueue axQueue = new AXQueue();
        axQueue.setName(queueId);
        axQueue.setMusicclass("default");
        axQueue.setStrategy(strategy);
        axQueue.setWeight(weight);
        axQueue.setServicelevel(serviceLevel);
        axQueue.setMaxlen(maxLen);
        axQueue.setContext(context);
        axQueue.setRinginuse(YesNo.no);
        axQueue.setAutopause(YesNo.no);
        axQueue.setSetinterfacevar(YesNo.yes);
        axQueue.setSetqueueentryvar(YesNo.yes);
        axQueue.setSetqueuevar(YesNo.yes);
        axQueue.setTimeout(10);
        axQueue.setWrapuptime(15);
        axQueue.setRetry(5);
        axQueue.setTimeoutpriority(TimeoutPriority.app);
        axQueue.setAnnounceFrequency(30);
        axQueue.setMinAnnounceFrequency(30);
        axQueue.setPeriodicAnnounceFrequency(45);
        axQueue.setRandomPeriodicAnnounce(YesNo.no);
        axQueue.setRelativePeriodicAnnounce(YesNo.yes);
        axQueue.setAnnounceHoldtime(YesNoOnce.once);
        axQueue.setAnnouncePosition(YesNoLimitMore.limit);
        axQueue.setAnnouncePositionLimit(10);
        axQueue.setAnnounceRoundSeconds(30);
        axQueue.setJoinempty("unavailable,invalid,unknown");
        axQueue.setLeavewhenempty("unavailable,invalid,unknown");
        return axQueueRepository.save(axQueue);
    }

    @Override
    public AXQueue update(String queueId, Strategy strategy, int weight, int serviceLevel, int maxLen, String context,
                          String musicclass, String periodicAnnounce, int periodicAnnounceFrequency) {
        AXQueue axQueue = getQueue(queueId);
        axQueue.setStrategy(strategy);
        axQueue.setWeight(weight);
        axQueue.setServicelevel(serviceLevel);
        axQueue.setMaxlen(maxLen);
        axQueue.setContext(context);

        if (!StringUtils.isEmpty(musicclass)) {
            axQueue.setMusicclass(musicclass);
        } else {
            axQueue.setMusicclass("default");
        }
        axQueue.setPeriodicAnnounce(periodicAnnounce);
        axQueue.setPeriodicAnnounceFrequency(periodicAnnounceFrequency);
        return axQueueRepository.save(axQueue);
    }

    @Override
    public Optional<AXQueue> findOne(String id) {
        return axQueueRepository.findById(id);
    }

    @Override
    public AXQueue getQueue(String id) {
        return findOne(id).orElseThrow(
            () -> new RuntimeException("Invalid ax queue id " + id)
        );
    }

    @Override
    public List<AXQueue> findAll() {
        return axQueueRepository.findAll();
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(axQueue -> {
            axQueueRepository.delete(axQueue);
            log.debug("Deleted axQueue: {}", axQueue);
        });
    }
}
