package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AXQueue;
import com.telefonix.getafix.asterisk.entity.enums.Strategy;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXQueueService {

    AXQueue create(String queueId, Strategy strategy, int weight, int serviceLevel, int maxLen, String context);

    AXQueue update(String queueId, Strategy strategy, int weight, int serviceLevel, int maxLen, String context,
                   String musicclass, String periodicAnnounce, int periodicAnnounceFrequency);

    Optional<AXQueue> findOne(String id);

    AXQueue getQueue(String id);

    List<AXQueue> findAll();

    void delete(String id);
}
