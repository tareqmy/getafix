package com.telefonix.getafix.asterisk.service.live;

import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Variable;
import com.telefonix.getafix.asterisk.service.ResourceService;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.CelEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-05-30.
 */
@Slf4j
@Component
public class ChannelLiveRepository {

    public static final String queue = "Queue";

    public static final String appQueue = "AppQueue";

    private Map<String, ChannelLive> channelLiveMap;

    private List<ChannelLiveEventListener> channelLiveEventListeners;

    private ResourceService resourceService;

    private ScheduledExecutorService scheduleExecutor;

    private List<String> variables;

    @Autowired
    public ChannelLiveRepository(ResourceService resourceService,
                                 ScheduledExecutorService scheduleExecutor) {
        this.channelLiveMap = new ConcurrentHashMap<>();
        this.channelLiveEventListeners = new ArrayList<>();
        this.resourceService = resourceService;
        this.scheduleExecutor = scheduleExecutor;
    }

    public void init(List<String> variables) {
        this.variables = variables;
        resourceService.getAllChannels().forEach(channel -> {
            //todo: load the existing channels
            //channel does not have linkedid like celevent has!!!
        });
    }

    public void addChannelLiveEventListener(ChannelLiveEventListener channelLiveEventListener) {
        channelLiveEventListeners.add(channelLiveEventListener);
    }

    public void onChannelStart(ChannelLive channelLive) {
        if (channelLive.isCaller()) {
            channelLive.setCalledNumber(channelLive.getExten());
        } else {
            if (channelLive.getLinkedChannel() != null) {
                channelLive.setCalledNumber(channelLive.getLinkedChannel().getExten());
            } else {
                channelLive.setCalledNumber("Missing");
            }
        }

        variables.forEach(s -> {
            Variable variable = resourceService.getChannelVar(channelLive.getUniqueID(), s);
            if (variable != null) {
                channelLive.getVariableMap().put(s, variable.getValue());
            }
        });

        channelLiveEventListeners.forEach(channelLiveEventListener -> channelLiveEventListener.onChannelStart(channelLive));
    }

    public Collection<ChannelLive> getChannelLiveMapValues() {
        return channelLiveMap.values();
    }

    public void onChannelAppStart(ChannelLive channelLive) {
        channelLiveEventListeners.forEach(channelLiveEventListener -> channelLiveEventListener.onChannelAppStart(channelLive));
    }

    public void onChannelAppStop(ChannelLive channelLive) {
        channelLiveEventListeners.forEach(channelLiveEventListener -> channelLiveEventListener.onChannelAppStop(channelLive));
    }

    public void onChannelAnswered(ChannelLive channelLive) {
        channelLiveEventListeners.forEach(channelLiveEventListener -> channelLiveEventListener.onChannelAnswered(channelLive));
    }

    public void onChannelEnd(ChannelLive channelLive) {
        channelLiveEventListeners.forEach(channelLiveEventListener -> channelLiveEventListener.onChannelEnd(channelLive));
        scheduleExecutor.schedule(() -> removeChannelLive(channelLive.getUniqueID()), 3000L, TimeUnit.MILLISECONDS);
    }

    public synchronized ChannelLive getChannelLive(CelEvent event) {
        ChannelLive channelLive = channelLiveMap.get(event.getUniqueID());
        if (channelLive == null) {
            channelLive = new ChannelLive(event);
            channelLive.setCaller(event.getUniqueID().equalsIgnoreCase(event.getLinkedID()));
            if (!channelLive.isCaller()) {
                ChannelLive linkedChannel = channelLiveMap.get(channelLive.getLinkedID());
                channelLive.setLinkedChannel(linkedChannel);
            }
            channelLiveMap.put(event.getUniqueID(), channelLive);
        }
        return channelLive;
    }

    public synchronized ChannelLive getChannelLive(String uniqueId) {
        return channelLiveMap.get(uniqueId);
    }

    //why should anybody outside gets to remove channel live
    private synchronized ChannelLive removeChannelLive(String uniqueId) {
        return channelLiveMap.remove(uniqueId);
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void garbageCollection() {
        Iterator<Map.Entry<String, ChannelLive>> iterator = channelLiveMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, ChannelLive> next = iterator.next();
            ChannelLive channelLive = next.getValue();
            Channel channel = resourceService.getChannel(channelLive.getUniqueID());
            if (channel == null) {
                iterator.remove();
                onChannelEnd(channelLive);
            }
        }
    }
}
