package com.telefonix.getafix.asterisk.service.ari.processor;

import ch.loway.oss.ari4java.generated.StasisEnd;
import ch.loway.oss.ari4java.generated.ari_4_0_0.models.StasisEnd_impl_ari_4_0_0;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.CallManager;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by tareqmy on 2019-02-10.
 */
@Slf4j
public class StasisEndMessageProcessor extends AbstractMessageProcessor<StasisEnd> {

    public StasisEndMessageProcessor(CallManager callManager) {
        super(callManager);
    }

    @Override
    public Class getMessageClass() {
        return StasisEnd_impl_ari_4_0_0.class;
    }

    @Override
    public void processMessage(StasisEnd message) {
        log.trace("StasisEnd received {} from {} {} to {}", message.getChannel().getId(), message.getChannel().getAccountcode(),
            message.getChannel().getName(), message.getChannel().getDialplan().getExten());
        CallEventListener callEventListener = callManager.getCallEventListener(message.getChannel().getId());
        if (callEventListener != null)
            callEventListener.onStasisEnd(message.getChannel());
    }
}
