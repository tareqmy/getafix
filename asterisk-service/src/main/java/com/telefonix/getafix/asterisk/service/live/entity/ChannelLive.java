package com.telefonix.getafix.asterisk.service.live.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.asteriskjava.manager.event.CelEvent;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-05-30.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"uniqueID"})
@ToString
public class ChannelLive {

    private String uniqueID;

    private String channel;

    private String accountCode;

    private String callerIdNum;

    private String callerIdName;

    private String calledNumber;

    private String context;

    private String exten;

    private Long priority;

    private String linkedID;

    private ChannelLive linkedChannel;

    private boolean caller;

    private Instant startInstant;

    private Instant answerInstant;

    private Instant endInstant;

    private String bridgeId;

    private String app;

    private String appData;

    private String extra;

    private String callType = "Personal";

    private boolean callInitiator;

    private Map<String, String> variableMap = new HashMap<>();

    public ChannelLive(CelEvent event) {
        setUniqueID(event.getUniqueID());
        setChannel(event.getChannel());
        setAccountCode(event.getAccountCode());
        setCallerIdName(event.getCallerIdName());
        setCallerIdNum(event.getCallerIdNum());

        setContext(event.getContext());
        setExten(event.getExten());
        if (event.getPriority() != null) {
            setPriority(Long.valueOf(event.getPriority()));
        }

        setLinkedID(event.getLinkedID());
        setExtra(event.getExtra());
    }
}
