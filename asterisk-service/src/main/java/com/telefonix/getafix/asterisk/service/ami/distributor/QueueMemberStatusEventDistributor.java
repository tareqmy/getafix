package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberStatusEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Component
public class QueueMemberStatusEventDistributor implements EventDistributor<QueueMemberStatusEvent> {

    private List<EventProcessor<QueueMemberStatusEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return QueueMemberStatusEvent.class;
    }

    @Override
    public void register(EventProcessor<QueueMemberStatusEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(QueueMemberStatusEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
