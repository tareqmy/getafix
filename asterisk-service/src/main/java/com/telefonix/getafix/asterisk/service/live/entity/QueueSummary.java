package com.telefonix.getafix.asterisk.service.live.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.asteriskjava.manager.event.QueueSummaryEvent;

/**
 * Created by tareqmy on 2019-06-23.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"queue"})
@ToString
public class QueueSummary {

    private String queue;

    private Integer loggedIn;

    private Integer available;

    private Integer callers;

    private Integer holdTime;

    private Integer talkTime;

    private Integer longestHoldTime;

    public QueueSummary(QueueSummaryEvent queueSummaryEvent) {
        setQueue(queueSummaryEvent.getQueue());
        setLoggedIn(queueSummaryEvent.getLoggedIn());
        setAvailable(queueSummaryEvent.getAvailable());
        setCallers(queueSummaryEvent.getCallers());
        setHoldTime(queueSummaryEvent.getHoldTime());
        setTalkTime(queueSummaryEvent.getTalkTime());
        setLongestHoldTime(queueSummaryEvent.getLongestHoldTime());
    }
}
