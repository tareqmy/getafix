package com.telefonix.getafix.asterisk.service.ami;

import org.asteriskjava.manager.event.ManagerEvent;

/**
 * Created by tareqmy on 2019-05-29.
 */
public interface EventProcessor<T extends ManagerEvent> {

    void onEvent(T event);
}
