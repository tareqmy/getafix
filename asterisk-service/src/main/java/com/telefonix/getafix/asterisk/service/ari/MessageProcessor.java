package com.telefonix.getafix.asterisk.service.ari;

import ch.loway.oss.ari4java.generated.Message;

/**
 * Created by tareqmy on 2019-02-10.
 */
public interface MessageProcessor<T extends Message> {

    Class getMessageClass();

    void processMessage(T message);
}
