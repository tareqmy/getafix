package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueCallerJoinEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueCallerLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueCallerJoinEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueCallerJoinEventProcessor implements EventProcessor<QueueCallerJoinEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueCallerJoinEventProcessor(QueueCallerJoinEventDistributor queueCallerJoinEventDistributor,
                                         AXQueueLiveRepository axQueueLiveRepository) {
        queueCallerJoinEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueCallerJoinEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        AXQueueCallerLive axQueueCallerLive = new AXQueueCallerLive(event);
        axQueueLiveRepository.onQueueCallerJoin(axQueueLive, axQueueCallerLive);
    }
}
