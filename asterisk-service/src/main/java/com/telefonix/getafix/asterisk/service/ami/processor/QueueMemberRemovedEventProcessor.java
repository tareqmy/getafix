package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueMemberRemovedEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberRemovedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-18.
 * when member unjoins queue
 */
@Slf4j
@Component
public class QueueMemberRemovedEventProcessor implements EventProcessor<QueueMemberRemovedEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueMemberRemovedEventProcessor(QueueMemberRemovedEventDistributor queueMemberRemovedEventDistributor,
                                            AXQueueLiveRepository axQueueLiveRepository) {
        queueMemberRemovedEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueMemberRemovedEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        axQueueLiveRepository.onQueueMemberRemoved(axQueueLive, event.getInterface());
    }
}
