package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.AsteriskControlService;
import com.telefonix.getafix.asterisk.service.ManagerService;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ResponseEvents;
import org.asteriskjava.manager.event.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueActionResponseProcessor {

    private AXQueueLiveRepository axQueueLiveRepository;

    private ManagerService managerService;

    private AsteriskControlService asteriskControlService;

    @Autowired
    public QueueActionResponseProcessor(AXQueueLiveRepository axQueueLiveRepository,
                                        ManagerService managerService,
                                        AsteriskControlService asteriskControlService) {
        this.axQueueLiveRepository = axQueueLiveRepository;
        this.managerService = managerService;
        this.asteriskControlService = asteriskControlService;
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void refresh() {
        //NOTE: every queue will atleast get the paramsevent
        ResponseEvents responseEvents = managerService.queueStatus();
        if (responseEvents == null) {
            log.info("Queue status response is not working!~");
            return;
        }
        List<AXQueueMemberLive> memberLives = responseEvents.getEvents().stream()
            .filter(responseEvent -> responseEvent instanceof QueueMemberEvent)
            .map(responseEvent -> (QueueMemberEvent) responseEvent)
            .map(AXQueueMemberLive::new)
            .collect(Collectors.toList());
        for (ResponseEvent responseEvent : responseEvents.getEvents()) {
            if (responseEvent instanceof QueueParamsEvent) {
                handleQueueParamsEvent(responseEvent, memberLives);
            } else if (responseEvent instanceof QueueEntryEvent) {
                handleQueueEntryEvent(responseEvent);
            }
        }
    }


    private void handleQueueParamsEvent(ResponseEvent responseEvent, List<AXQueueMemberLive> memberLives) {
        QueueParamsEvent queueParamsEvent = (QueueParamsEvent) responseEvent;
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(queueParamsEvent.getQueue());
        if (axQueueLive == null) {
            log.warn("Invalid queuelive params for {}", queueParamsEvent);
            asteriskControlService.executeQueueReload();
            return;
        }
        QueueParams queueParams = new QueueParams(queueParamsEvent);

        axQueueLiveRepository.onQueueParams(axQueueLive, queueParams);
        log.trace("{}", queueParams);

        ResponseEvents events = managerService.queueSummary(queueParams.getQueue());
        events.getEvents().forEach(event -> {
            if (event instanceof QueueSummaryEvent) {
                QueueSummaryEvent queueSummaryEvent = (QueueSummaryEvent) event;
                QueueSummary queueSummary = new QueueSummary(queueSummaryEvent);
                axQueueLiveRepository.onQueueSummary(axQueueLive, queueSummary);
                log.trace("{}", queueSummary);
            }
        });

        axQueueLiveRepository.onQueueMembers(axQueueLive, memberLives.stream()
            .filter(axQueueMemberLive -> axQueueMemberLive.getQueue().equals(axQueueLive.getAxQueue().getName()))
            .collect(Collectors.toList()));
    }

    private void handleQueueEntryEvent(ResponseEvent responseEvent) {
        QueueEntryEvent queueEntryEvent = (QueueEntryEvent) responseEvent;
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(queueEntryEvent.getQueue());
        if (axQueueLive == null) {
            log.warn("Invalid queuelive entry for {}", queueEntryEvent);
            asteriskControlService.executeQueueReload();
            return;
        }
        AXQueueCallerLive axQueueCallerLive = new AXQueueCallerLive(queueEntryEvent);

        axQueueLiveRepository.onQueueCaller(axQueueLive, axQueueCallerLive);
        log.trace("{}", axQueueCallerLive);
    }
}
