package com.telefonix.getafix.asterisk.service;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;

import java.util.List;

/**
 * Created by tareqmy on 2019-02-11.
 */
public interface AsteriskCallControllerService {

    CallEventListener getNewCall(Channel channel, List<String> args);
}
