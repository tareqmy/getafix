package com.telefonix.getafix.asterisk.service.ari.application;

import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.service.AsteriskCallControllerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-02-12.
 */
@Slf4j
@Component
public class CallManager {

    private Map<String, CallEventListener> callMap = new ConcurrentHashMap<>();

    private AsteriskCallControllerService asteriskCallControllerService;

    @Autowired
    public CallManager(AsteriskCallControllerService asteriskCallControllerService) {
        this.asteriskCallControllerService = asteriskCallControllerService;
    }

    public CallEventListener getCallEventListener(String key) {
        return callMap.get(key);
    }

    public CallEventListener onStasisStart(Channel channel, List<String> args) {
        return asteriskCallControllerService.getNewCall(channel, args);
    }
}
