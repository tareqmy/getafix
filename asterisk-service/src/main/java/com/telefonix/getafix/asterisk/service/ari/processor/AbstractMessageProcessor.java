package com.telefonix.getafix.asterisk.service.ari.processor;

import ch.loway.oss.ari4java.generated.Event;
import com.telefonix.getafix.asterisk.service.ari.MessageProcessor;
import com.telefonix.getafix.asterisk.service.ari.application.CallManager;

/**
 * Created by tmyousuf on 3/21/16.
 */
abstract class AbstractMessageProcessor<T extends Event> implements MessageProcessor<T> {

    CallManager callManager;

    AbstractMessageProcessor(CallManager callManager) {
        this.callManager = callManager;
    }
}
