package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSContact;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSContactService {

    List<PSContact> findAll();
}
