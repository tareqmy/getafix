package com.telefonix.getafix.asterisk.service.live.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.asteriskjava.manager.event.QueueCallerJoinEvent;
import org.asteriskjava.manager.event.QueueEntryEvent;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"uniqueId"})
@ToString
public class AXQueueCallerLive {

    private String queue;

    private Integer position;

    private String uniqueId;

    private String channel;

    private String callerIdName;

    private String callerIdNum;

    private String callerId;

    private Long wait;

    private boolean abandoned;

    public AXQueueCallerLive(QueueEntryEvent queueEntryEvent) {
        setQueue(queueEntryEvent.getQueue());
        setPosition(queueEntryEvent.getPosition());
        setUniqueId(queueEntryEvent.getUniqueId());
        setChannel(queueEntryEvent.getChannel());
        setCallerId(queueEntryEvent.getCallerId());
        setCallerIdName(queueEntryEvent.getCallerIdName());
        setCallerIdNum(queueEntryEvent.getCallerIdNum());
        setWait(queueEntryEvent.getWait());
    }

    public AXQueueCallerLive(QueueCallerJoinEvent queueCallerJoinEvent) {
        setQueue(queueCallerJoinEvent.getQueue());
        setPosition(queueCallerJoinEvent.getPosition());
        setUniqueId(queueCallerJoinEvent.getUniqueId());
        setChannel(queueCallerJoinEvent.getChannel());
        setCallerIdName(queueCallerJoinEvent.getCallerIdName());
        setCallerIdNum(queueCallerJoinEvent.getCallerIdNum());
        setWait(0L);
    }
}
