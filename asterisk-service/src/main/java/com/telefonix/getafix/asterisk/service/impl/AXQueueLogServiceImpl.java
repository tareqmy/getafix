package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import com.telefonix.getafix.asterisk.repository.AXQueueLogRepository;
import com.telefonix.getafix.asterisk.service.AXQueueLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXQueueLogServiceImpl implements AXQueueLogService {

    private AXQueueLogRepository axQueueLogRepository;

    @Autowired
    public AXQueueLogServiceImpl(AXQueueLogRepository axQueueLogRepository) {
        this.axQueueLogRepository = axQueueLogRepository;
    }

    @Override
    public List<AXQueueLog> findAll() {
        return axQueueLogRepository.findAll();
    }

    @Override
    public Optional<AXQueueLog> findOne(Long id) {
        return axQueueLogRepository.findById(id);
    }

    @Override
    public List<AXQueueLog> findByQueueNameAndTimeBetween(String queuename, Instant fromDate, Instant toDate) {
        return axQueueLogRepository.findAllByQueueNameAndTimeBetweenOrderByTimeDesc(queuename, fromDate, toDate);
    }
}
