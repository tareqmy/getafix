package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueMemberAddedEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueMemberLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberAddedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-18.
 * When member joins queue
 */
@Slf4j
@Component
public class QueueMemberAddedEventProcessor implements EventProcessor<QueueMemberAddedEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueMemberAddedEventProcessor(QueueMemberAddedEventDistributor queueMemberAddedEventDistributor,
                                          AXQueueLiveRepository axQueueLiveRepository) {
        queueMemberAddedEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueMemberAddedEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        AXQueueMemberLive axQueueMemberLive = new AXQueueMemberLive(event);
        axQueueLiveRepository.onQueueMemberAdded(axQueueLive, axQueueMemberLive);
    }
}
