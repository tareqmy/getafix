package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.MusicOnHold;
import com.telefonix.getafix.asterisk.entity.enums.MusicOnHoldMode;
import com.telefonix.getafix.asterisk.repository.MusicOnHoldRepository;
import com.telefonix.getafix.asterisk.service.MusicOnHoldService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 15/9/19.
 */
@Slf4j
@Service
@Transactional
public class MusicOnHoldServiceImpl implements MusicOnHoldService {

    private MusicOnHoldRepository musicOnHoldRepository;

    @Autowired
    public MusicOnHoldServiceImpl(MusicOnHoldRepository musicOnHoldRepository) {
        this.musicOnHoldRepository = musicOnHoldRepository;
    }

    @Override
    public MusicOnHold create(String name, String directory) {
        MusicOnHold musicOnHold = new MusicOnHold();
        musicOnHold.setName(name);
        musicOnHold.setMode(MusicOnHoldMode.files);
        musicOnHold.setDirectory(directory);
        return musicOnHoldRepository.save(musicOnHold);
    }

    @Override
    public MusicOnHold update(String name, String directory) {
        MusicOnHold musicOnHold = getMusicOnHold(name);
        musicOnHold.setDirectory(directory);
        return musicOnHoldRepository.save(musicOnHold);
    }

    @Override
    public List<MusicOnHold> findAll() {
        return musicOnHoldRepository.findAll();
    }

    @Override
    public Optional<MusicOnHold> findOne(String name) {
        return musicOnHoldRepository.findById(name);
    }

    @Override
    public MusicOnHold getMusicOnHold(String name) {
        return findOne(name).orElseThrow(
            () -> new RuntimeException("Invalid moh id " + name)
        );
    }

    @Override
    public void delete(String name) {
        findOne(name).ifPresent(musicOnHold -> {
            musicOnHoldRepository.delete(musicOnHold);
        });
    }
}
