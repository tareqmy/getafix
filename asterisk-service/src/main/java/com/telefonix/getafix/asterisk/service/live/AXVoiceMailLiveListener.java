package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.service.live.entity.AXVoiceMailLive;

/**
 * Created by tareqmy on 2019-07-31.
 */
public interface AXVoiceMailLiveListener {

    void onVoiceMailLiveUpdate(AXVoiceMailLive axVoiceMailLive);
}
