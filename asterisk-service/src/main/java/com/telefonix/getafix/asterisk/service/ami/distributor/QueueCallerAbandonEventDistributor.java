package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueCallerAbandonEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueCallerAbandonEventDistributor implements EventDistributor<QueueCallerAbandonEvent> {

    private List<EventProcessor<QueueCallerAbandonEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return QueueCallerAbandonEvent.class;
    }

    @Override
    public void register(EventProcessor<QueueCallerAbandonEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(QueueCallerAbandonEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
