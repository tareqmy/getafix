package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueCallerLeaveEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueCallerLeaveEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueCallerLeaveEventProcessor implements EventProcessor<QueueCallerLeaveEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueCallerLeaveEventProcessor(QueueCallerLeaveEventDistributor queueCallerLeaveEventDistributor,
                                          AXQueueLiveRepository axQueueLiveRepository) {
        queueCallerLeaveEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueCallerLeaveEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        axQueueLiveRepository.onQueueCallerLeave(axQueueLive, event.getUniqueId());
    }
}
