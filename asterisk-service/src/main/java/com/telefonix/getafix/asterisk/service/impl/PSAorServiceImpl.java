package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSAor;
import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import com.telefonix.getafix.asterisk.repository.PSAorRepository;
import com.telefonix.getafix.asterisk.service.PSAorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSAorServiceImpl implements PSAorService {

    private PSAorRepository psAorRepository;

    @Autowired
    public PSAorServiceImpl(PSAorRepository psAorRepository) {
        this.psAorRepository = psAorRepository;
    }

    @Override
    public PSAor create(String sipEntityId, String mailBox, int maxContacts) {
//id;mailboxes;max_contacts;remove_existing;default_expiration;maximum_expiration
//1001;1001@example;1;yes;100;100
        PSAor psAor = new PSAor();
        psAor.setId(sipEntityId);
        psAor.setMaxContacts(maxContacts);
        psAor.setMailBoxes(mailBox);
        psAor.setRemoveExisting(YesNo.yes);
        psAor.setDefaultExpiration(3600);
        psAor.setMinimumExpiration(60);
        psAor.setMaximumExpiration(100);
        psAor.setQualifyTimeout(3);
        return psAorRepository.save(psAor);
    }

    @Override
    public PSAor create(String sipEntityId, String contact) {
//        id;contact
//        support;sip:sip.nextfone.com.bd:5060
        PSAor psAor = new PSAor();
        psAor.setId(sipEntityId);
        psAor.setContact(contact);
//        psAor.setDefaultExpiration(null);
//        psAor.setMinimumExpiration(null);
//        psAor.setQualifyFrequency(null);
//        psAor.setMaximumExpiration(null);
//        psAor.setQualifyTimeout(null);

        return psAorRepository.save(psAor);
    }

    @Override
    public Optional<PSAor> findOne(String id) {
        return psAorRepository.findById(id);
    }

    @Override
    public List<PSAor> findAll() {
        return psAorRepository.findAll();
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(psAor -> {
            psAorRepository.delete(psAor);
            log.debug("Deleted psAor: {}", psAor);
        });
    }
}
