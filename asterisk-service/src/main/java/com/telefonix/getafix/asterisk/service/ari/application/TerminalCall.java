package com.telefonix.getafix.asterisk.service.ari.application;

import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.tools.RestException;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * Created by tareqmy on 2019-02-11.
 * A call that will be delivered to the dialplan.
 */
@Slf4j
public class TerminalCall extends AbstractCall {

    private Map<String, String> variableMap;

    public TerminalCall(String accountCode, String exten, String context,
                        String label,
                        Map<String, String> variableMap,
                        AsteriskResourceService asteriskResourceService) {
        super(accountCode, exten, context, label, asteriskResourceService);
        this.variableMap = variableMap;
        this.asteriskResourceService = asteriskResourceService;
    }

    @Override
    public void onStasisStart(Channel channel) {
        try {
            if (variableMap != null) {
                variableMap.forEach((varName, varValue) -> {
                    log.debug("Set variable {}={}", varName, varValue);
                    try {
                        asteriskResourceService.setChannelVar(channel.getId(), varName, varValue);
                    } catch (RestException e) {
                        log.debug("Failed to set variable {}", varName);
                    }
                });
            }
            asteriskResourceService.continueChannelInDialplan(channel.getId(), context, exten, 1, label);
        } catch (RestException e) {
            log.debug("Failed to continue the channel in dialplan {} {}", channel.getId(), e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "TerminalCall{" +
            "variableMap=" + variableMap +
            '}' + super.toString();
    }
}
