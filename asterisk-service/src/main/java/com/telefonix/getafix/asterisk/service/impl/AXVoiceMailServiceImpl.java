package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.dto.AXVoiceMailDTO;
import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import com.telefonix.getafix.asterisk.repository.AXVoiceMailRepository;
import com.telefonix.getafix.asterisk.service.AXVoiceMailService;
import com.telefonix.getafix.asterisk.service.AXVoiceMessageService;
import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXVoiceMailServiceImpl implements AXVoiceMailService {

    private AXVoiceMailRepository axVoiceMailRepository;

    private AXVoiceMailLiveRepository axVoiceMailLiveRepository;

    private AXVoiceMessageService axVoiceMessageService;

    @Autowired
    public AXVoiceMailServiceImpl(AXVoiceMailRepository axVoiceMailRepository,
                                  AXVoiceMailLiveRepository axVoiceMailLiveRepository,
                                  AXVoiceMessageService axVoiceMessageService) {
        this.axVoiceMailRepository = axVoiceMailRepository;
        this.axVoiceMailLiveRepository = axVoiceMailLiveRepository;
        this.axVoiceMessageService = axVoiceMessageService;
    }

    @Override
    public AXVoiceMail create(AXVoiceMailDTO axVoiceMailDTO) {
        AXVoiceMail axVoiceMail = new AXVoiceMail();
        axVoiceMail.setUniqueid(axVoiceMailDTO.getUniqueid());
        axVoiceMail.setContext(axVoiceMailDTO.getContext());
        axVoiceMail.setMailbox(axVoiceMailDTO.getMailbox());
        axVoiceMail.setPassword(axVoiceMailDTO.getPassword());
        axVoiceMail.setFullName(axVoiceMailDTO.getFullName());
        axVoiceMail.setEmail(axVoiceMailDTO.getEmail());
        AXVoiceMail saved = axVoiceMailRepository.save(axVoiceMail);
        axVoiceMailLiveRepository.onAXVoiceMailCreated(axVoiceMail);
        return saved;
    }

    @Override
    public void update(String context, String mailbox, String fullName, String email) {
        findOne(context, mailbox)
            .ifPresent(axVoiceMail -> {
                axVoiceMail.setFullName(fullName);
                axVoiceMail.setEmail(email);
                axVoiceMailRepository.save(axVoiceMail);
            });
    }

    @Override
    public void changePassword(String context, String mailbox, String password) {
        findOne(context, mailbox)
            .ifPresent(axVoiceMail -> {
                axVoiceMail.setPassword(password);
                axVoiceMailRepository.save(axVoiceMail);
            });
    }

    @Override
    public Optional<AXVoiceMail> findOne(Long uniqueid) {
        return axVoiceMailRepository.findById(uniqueid);
    }

    @Override
    public Optional<AXVoiceMail> findOne(String context, String mailbox) {
        return axVoiceMailRepository.findByContextAndMailbox(context, mailbox);
    }

    @Override
    public List<AXVoiceMail> findAll() {
        return axVoiceMailRepository.findAll();
    }

    @Override
    public AXVoiceMail getAXVoiceMail(Long uniqueid) {
        return findOne(uniqueid).orElseThrow(
            () -> new RuntimeException("Invalid voicemail uniqueid " + uniqueid)
        );
    }

    @Override
    public AXVoiceMail getAXVoiceMail(String context, String mailbox) {
        return findOne(context, mailbox).orElseThrow(
            () -> new RuntimeException("Invalid voicemail " + mailbox + "@" + context)
        );
    }

    @Override
    public void delete(Long uniqueid) {
        findOne(uniqueid).ifPresent(this::deleteAXVoiceMail);
    }

    private void deleteAXVoiceMail(AXVoiceMail axVoiceMail) {
        axVoiceMessageService.deleteVoiceMailMessages(axVoiceMail.getMailbox(), axVoiceMail.getContext());
        axVoiceMailRepository.delete(axVoiceMail);
        axVoiceMailLiveRepository.onAXVoiceMailDeleted(axVoiceMail.getMailbox() + "@" + axVoiceMail.getContext());
        log.debug("Deleted axVoiceMail: {}", axVoiceMail);
    }

    @Override
    public void delete(String context, String mailbox) {
        findOne(context, mailbox).ifPresent(this::deleteAXVoiceMail);
    }
}
