package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.PeerStatusEventDistributor;
import com.telefonix.getafix.asterisk.service.live.PSEndpointLiveRepository;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.PeerStatusEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-19.
 */
@Slf4j
@Component
public class PeerStatusEventProcessor implements EventProcessor<PeerStatusEvent> {

    private PSEndpointLiveRepository psEndpointLiveRepository;

    @Autowired
    public PeerStatusEventProcessor(PeerStatusEventDistributor peerStatusEventDistributor,
                                    PSEndpointLiveRepository psEndpointLiveRepository) {
        peerStatusEventDistributor.register(this);
        this.psEndpointLiveRepository = psEndpointLiveRepository;
    }

    @Override
    public void onEvent(PeerStatusEvent event) {
        log.debug("{}: {}", event.getPeer(), event.getPeerStatus());
        event.getPeerStatus(); //Unknown, Registered, Unregistered, Rejected, Lagged
        this.psEndpointLiveRepository.onStatus(event.getPeer(), event.getPeerStatus());
    }
}
