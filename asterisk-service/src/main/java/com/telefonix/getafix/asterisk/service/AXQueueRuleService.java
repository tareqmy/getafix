package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AXQueueRule;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXQueueRuleService {

    List<AXQueueRule> findAll();
}
