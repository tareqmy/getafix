package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSAuth;
import com.telefonix.getafix.asterisk.entity.enums.AuthType;
import com.telefonix.getafix.asterisk.repository.PSAuthRepository;
import com.telefonix.getafix.asterisk.service.PSAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSAuthServiceImpl implements PSAuthService {

    private PSAuthRepository psAuthRepository;

    @Autowired
    public PSAuthServiceImpl(PSAuthRepository psAuthRepository) {
        this.psAuthRepository = psAuthRepository;
    }

    @Override
    public PSAuth create(String sipEntityId, String password) {
//id;auth_type;username;password
//1001;userpass;1001;1001
        PSAuth psAuth = new PSAuth();
        psAuth.setId(sipEntityId);
        psAuth.setAuthType(AuthType.userpass);
        psAuth.setNonceLifetime(32);
        psAuth.setPassword(password);
        psAuth.setUsername(sipEntityId); //NOTE: this is actually used as userid in authentication
        return psAuthRepository.save(psAuth);
    }

    @Override
    public List<PSAuth> findAll() {
        return psAuthRepository.findAll();
    }

    @Override
    public Optional<PSAuth> findOne(String id) {
        return psAuthRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(psAuth -> {
            psAuthRepository.delete(psAuth);
            log.debug("Deleted psAuth: {}", psAuth);
        });
    }
}
