package com.telefonix.getafix.asterisk.service.ami;

import com.telefonix.getafix.asterisk.ami.ManagerEventListener;
import com.telefonix.getafix.asterisk.ami.ManagerEventNotificationService;
import com.telefonix.getafix.asterisk.service.ami.distributor.*;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.ManagerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
@Component
public class ManagerEventListenerImpl implements ManagerEventListener {

    private HashMap<Class, EventDistributor<?>> eventDistributors = new HashMap<>();

    @Autowired
    public ManagerEventListenerImpl(CELEventDistributor celEventDistributor,
                                    PeerStatusEventDistributor peerStatusEventDistributor,
                                    MessageWaitingEventDistributor messageWaitingEventDistributor,
                                    QueueCallerAbandonEventDistributor queueCallerAbandonEventDistributor,
                                    QueueCallerJoinEventDistributor queueCallerJoinEventDistributor,
                                    QueueCallerLeaveEventDistributor queueCallerLeaveEventDistributor,
                                    QueueMemberAddedEventDistributor queueMemberAddedEventDistributor,
                                    QueueMemberRemovedEventDistributor queueMemberRemovedEventDistributor,
                                    QueueMemberPauseEventDistributor queueMemberPauseEventDistributor,
                                    QueueMemberStatusEventDistributor queueMemberStatusEventDistributor,
                                    ManagerEventNotificationService managerEventNotificationService) {
        register(celEventDistributor);
        register(peerStatusEventDistributor);
        register(messageWaitingEventDistributor);
        register(queueCallerAbandonEventDistributor);
        register(queueCallerJoinEventDistributor);
        register(queueCallerLeaveEventDistributor);
        register(queueMemberAddedEventDistributor);
        register(queueMemberRemovedEventDistributor);
        register(queueMemberPauseEventDistributor);
        register(queueMemberStatusEventDistributor);

        managerEventNotificationService.registerManagerEventListener(this);
    }

    private void register(EventDistributor<?> eventDistributor) {
        eventDistributors.put(eventDistributor.getEventClass(), eventDistributor);
    }

    @SuppressWarnings("unchecked")
    @Async("asteriskTaskExecutor")
    @Override
    public void onManagerEvent(ManagerEvent event) {
        if (event != null) {
            EventDistributor eventDistributor = eventDistributors.get(event.getClass());
            if (eventDistributor != null) {
                log.trace("ManagerEvent {}", event);
                eventDistributor.distributeEvent(event);
            } else {
                log.trace("Unhandled event: {}", event.getClass());
            }
        }
    }
}
