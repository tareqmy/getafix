package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.asterisk.repository.AXVoiceMessageRepository;
import com.telefonix.getafix.asterisk.service.AXVoiceMessageService;
import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXVoiceMessageServiceImpl implements AXVoiceMessageService {

    private final String voiceMailDirPrefix = "/var/spool/asterisk/voicemail/%s/%s/%s";

    private AXVoiceMessageRepository axVoiceMessageRepository;

    private AXVoiceMailLiveRepository axVoiceMailLiveRepository;

    @Autowired
    public AXVoiceMessageServiceImpl(AXVoiceMessageRepository axVoiceMessageRepository, AXVoiceMailLiveRepository axVoiceMailLiveRepository) {
        this.axVoiceMessageRepository = axVoiceMessageRepository;
        this.axVoiceMailLiveRepository = axVoiceMailLiveRepository;
    }

    @Override
    public List<AXVoiceMessage> findAllVoiceMailMessageInfo(String mailboxUser, String mailboxContext) {
        return axVoiceMessageRepository.findByMailboxUserAndMailboxContext(mailboxUser, mailboxContext);
    }

    @Override
    public List<AXVoiceMessage> getAllVoiceMailMessageInfo(String mailboxUser, String mailboxContext, String dir) {
        String directory = generateFolderName(mailboxContext, mailboxUser, dir);
        return axVoiceMessageRepository.findByDir(directory);
    }

    @Override
    public Optional<AXVoiceMessage> getVoiceMailMessageInfo(String mailboxUser, String mailboxContext, String msgId) {
        return axVoiceMessageRepository.findByMailboxUserAndMailboxContextAndMsgId(mailboxUser, mailboxContext, msgId);
    }

    @Override
    public void deleteVoiceMailMessage(String mailboxUser, String mailboxContext, String msgId) {
        axVoiceMessageRepository.findByMailboxUserAndMailboxContextAndMsgId(mailboxUser, mailboxContext, msgId)
            .ifPresent(axVoiceMessage -> {
                axVoiceMessageRepository.delete(axVoiceMessage);
                axVoiceMailLiveRepository.onAXVoiceMessageDelete(mailboxUser + "@" + mailboxContext);
            });
    }

    @Override
    public void deleteVoiceMailMessages(String mailboxUser, String mailboxContext) {
        findAllVoiceMailMessageInfo(mailboxUser, mailboxContext)
            .forEach(axVoiceMessage -> axVoiceMessageRepository.delete(axVoiceMessage));
    }

    private String generateFolderName(String context, String mailBox, String folder) {
        return String.format(voiceMailDirPrefix, context, mailBox, folder);
    }
}
