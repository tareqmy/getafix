package com.telefonix.getafix.asterisk.service.ari.application;

import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
public abstract class AbstractCall implements CallEventListener {

    protected String callId;

    protected Channel caller;

    protected String callerNamePrepend = null;

    protected String callerName = null;

    protected String callerNumber = null;

    protected String accountCode;

    protected String exten;

    protected String context;

    protected String label;

    protected AsteriskResourceService asteriskResourceService;

    AbstractCall(String accountCode, String exten, String context, String label, AsteriskResourceService asteriskResourceService) {
//        this.callId = AsteriskUtil.generateCallId();
        this.accountCode = accountCode;
        this.exten = exten;
        this.context = context;
        this.label = label;
        this.asteriskResourceService = asteriskResourceService;
    }

    public String getCallId() {
        return callId;
    }

    public Channel getCaller() {
        return caller;
    }

    public void setCaller(Channel caller) {
        this.caller = caller;
    }

    public String getCallerNamePrepend() {
        return callerNamePrepend;
    }

    public void setCallerNamePrepend(String callerNamePrepend) {
        this.callerNamePrepend = callerNamePrepend;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public String getCallerNumber() {
        return callerNumber;
    }

    public void setCallerNumber(String callerNumber) {
        this.callerNumber = callerNumber;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public String getExten() {
        return exten;
    }

    public String getContext() {
        return context;
    }

    public AsteriskResourceService getAsteriskResourceService() {
        return asteriskResourceService;
    }

    private void methodNotImplemented(String name) {
        log.warn("This method {} is not implemented by the current callState", name);
    }

    @Override
    public void onStasisStart(Channel channel) {
        methodNotImplemented("onStasisStart");
    }

    @Override
    public void onChannelStateChange(Channel channel) {
        methodNotImplemented("onChannelStateChange");
    }

    @Override
    public void onChannelHangupRequest(Channel channel) {
        methodNotImplemented("onChannelHangupRequest");
    }

    @Override
    public void onStasisEnd(Channel channel) {
        methodNotImplemented("onStasisEnd");
    }

    @Override
    public void onChannelEnteredBridge(Channel channel, Bridge bridge) {
        methodNotImplemented("onChannelEnteredBridge");
    }

    @Override
    public void onChannelLeftBridge(Channel channel, Bridge bridge) {
        methodNotImplemented("onChannelLeftBridge");
    }

    @Override
    public void onChannelVarset(Channel channel, String variable, String value) {
        methodNotImplemented("onChannelVarset");
    }

    @Override
    public String toString() {
        return "AbstractCall{" +
            "callId='" + callId + '\'' +
            ", caller=" + caller +
            ", callerNamePrepend='" + callerNamePrepend + '\'' +
            ", callerName='" + callerName + '\'' +
            ", callerNumber='" + callerNumber + '\'' +
            ", accountCode='" + accountCode + '\'' +
            ", exten='" + exten + '\'' +
            ", context='" + context + '\'' +
            '}';
    }
}
