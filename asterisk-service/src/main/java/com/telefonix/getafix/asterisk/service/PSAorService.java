package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSAor;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSAorService {

    PSAor create(String sipEntityId, String mailBox, int maxContacts);

    PSAor create(String sipEntityId, String contact);

    Optional<PSAor> findOne(String id);

    List<PSAor> findAll();

    void delete(String id);
}
