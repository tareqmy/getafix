package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSEndpointIdentify;
import com.telefonix.getafix.asterisk.repository.PSEndpointIdentifyRepository;
import com.telefonix.getafix.asterisk.service.PSEndpointIdentifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSEndpointIdentifyServiceImpl implements PSEndpointIdentifyService {

    private PSEndpointIdentifyRepository psEndpointIdentifyRepository;

    @Autowired
    public PSEndpointIdentifyServiceImpl(PSEndpointIdentifyRepository psEndpointIdentifyRepository) {
        this.psEndpointIdentifyRepository = psEndpointIdentifyRepository;
    }

    @Override
    public PSEndpointIdentify create(String sipEntityId, String host) {
//        id;endpoint;match
//        support;support;sip.nextfone.com.bd
        //because line support is not provided by nextfone, we need this
        PSEndpointIdentify psEndpointIdentify = new PSEndpointIdentify();
        psEndpointIdentify.setId(sipEntityId);//support
        //have a unique endpoint for provider
        psEndpointIdentify.setEndpoint(sipEntityId); //support
        psEndpointIdentify.setMatch(host); //sip.nextfone.com.bd
        return psEndpointIdentifyRepository.save(psEndpointIdentify);
    }

    @Override
    public Optional<PSEndpointIdentify> findOne(String id) {
        return psEndpointIdentifyRepository.findById(id);
    }

    @Override
    public List<PSEndpointIdentify> findByEndpoint(String endpoint) {
        return psEndpointIdentifyRepository.findByEndpoint(endpoint);
    }

    @Override
    public void delete(String sipEntityId) {
        findOne(sipEntityId).ifPresent(psEndpointIdentify -> {
            psEndpointIdentifyRepository.delete(psEndpointIdentify);
            log.debug("Deleted psEndpointIdentify: {}", psEndpointIdentify);
        });
    }
}
