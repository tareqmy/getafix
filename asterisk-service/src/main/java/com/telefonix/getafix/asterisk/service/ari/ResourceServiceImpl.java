package com.telefonix.getafix.asterisk.service.ari;

import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Endpoint;
import ch.loway.oss.ari4java.generated.Variable;
import ch.loway.oss.ari4java.tools.RestException;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-03-17.
 */
@Slf4j
@Service
@Transactional
class ResourceServiceImpl implements ResourceService {

    private AsteriskResourceService asteriskResourceService;

    @Value(value = "${getafix.asterisk.sip.technology}")
    private String endpoint_technology;

    @Autowired
    public ResourceServiceImpl(AsteriskResourceService asteriskResourceService) {
        this.asteriskResourceService = asteriskResourceService;
    }

    @Override
    public Endpoint getEndpoint(String resource) {
        try {
            return asteriskResourceService.getEndpoint(endpoint_technology, resource);
        } catch (RestException e) {
            log.warn("failed {}/{}", resource, e.getMessage());
        }
        return null;
    }

    @Override
    public List<Endpoint> getAllEndpoints() {
        try {
            return asteriskResourceService.getAllEndpoints();
        } catch (RestException e) {
            log.warn("failed {}", e.getMessage());
        }
        return null;
    }

    @Override
    public List<Endpoint> getEndpoints(List<String> resources) {
        if (CollectionUtils.isEmpty(resources))
            return null;
        return resources.stream()
            .map(resource -> {
                try {
                    return asteriskResourceService.getEndpoint(endpoint_technology, resource);
                } catch (RestException e) {
                    log.warn("failed {}/{}", resource, e.getMessage());
                }
                return null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    @Override
    public Channel getChannel(String channelId) {
        Channel channel = null;
        try {
            channel = asteriskResourceService.getChannel(channelId);
            if (channel != null) {
                log.debug("Channel found {}", channel.getName());
            }
        } catch (RestException e) {
            //generally it means Channel not found. is there other reason for this exception? i dont know
            log.warn("{} {}", channelId, e.getMessage());
        }
        return channel;
    }

    @Override
    public List<Channel> getAllChannels() {
        try {
            //stop using this directly faced issue for large data of endpoints
            //netty toolongframeexception HTTP content length exceeded 262144 bytes
            return asteriskResourceService.getAllChannels();
        } catch (RestException e) {
            log.warn("failed {}", e.getMessage());
        }
        return null;
    }

    @Override
    public Variable getChannelVar(String channelId, String varName) {
        try {
            return asteriskResourceService.getChannelVar(channelId, varName);
        } catch (RestException e) {
            log.debug("{} {}", channelId, e.getMessage());
        }
        return null;
    }

    @Override
    public void holdChannel(String channelId) {
        try {
            asteriskResourceService.holdChannel(channelId);
        } catch (RestException e) {
            log.warn("{} {}", channelId, e.getMessage());
        }
    }

    @Override
    public void continueChannelInDialplan(String channelId, String context, String extension, int priority, String label) {
        try {
            asteriskResourceService.continueChannelInDialplan(channelId, context, extension, priority, label);
        } catch (RestException e) {
            log.warn("{} {}", channelId, e.getMessage());
        }
    }

    @Override
    public void hangupChannel(String channelId, String hangupReason) {
        try {
            asteriskResourceService.hangupChannel(channelId, hangupReason);
        } catch (RestException e) {
            log.warn("{} {}", channelId, e.getMessage());
        }
    }

    @Override
    public Bridge getBridge(String bridgeId) {
        try {
            return asteriskResourceService.getBridge(bridgeId);
        } catch (RestException e) {
            log.warn("failed {}", e.getMessage());
        }
        return null;
    }

    @Override
    public List<Bridge> getAllBridges() {
        try {
            return asteriskResourceService.getAllBridges();
        } catch (RestException e) {
            log.warn("failed {}", e.getMessage());
        }
        return null;
    }

    @Override
    public void addChannel(String bridgeId, String channelId) {
        try {
            asteriskResourceService.addChannel(bridgeId, channelId);
        } catch (RestException e) {
            log.warn("failed {}", e.getMessage());
        }
    }
}
