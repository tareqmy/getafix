package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.service.live.entity.AXQueueCallerLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import com.telefonix.getafix.asterisk.service.live.entity.QueueParams;
import com.telefonix.getafix.asterisk.service.live.entity.QueueSummary;

/**
 * Created by tareqmy on 2019-06-23.
 */
public interface AXQueueLiveEventListener {

    void onQueueSummary(AXQueueLive axQueueLive, QueueSummary queueSummary);

    void onQueueParams(AXQueueLive axQueueLive, QueueParams queueParams);

    void onQueueCallerJoin(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive);

    void onQueueCallerLeave(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive);

    void onQueueCallerAbandoned(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive);
}
