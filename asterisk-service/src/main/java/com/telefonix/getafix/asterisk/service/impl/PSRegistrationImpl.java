package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSRegistration;
import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import com.telefonix.getafix.asterisk.repository.PSRegistrationRepository;
import com.telefonix.getafix.asterisk.service.PSRegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSRegistrationImpl implements PSRegistrationService {

    @Value(value = "${getafix.asterisk.ari.pjsip.transport}")
    private String TRANSPORT;

    private PSRegistrationRepository psRegistrationRepository;

    @Autowired
    public PSRegistrationImpl(PSRegistrationRepository psRegistrationRepository) {
        this.psRegistrationRepository = psRegistrationRepository;
    }

    @Override
    public PSRegistration create(String sipEntityId, String host) {
//        id;transport;outbound_auth;server_uri;client_uri;contact_user;retry_interval;forbidden_retry_interval;fatal_retry_interval;expiration;line;endpoint
//        support;transport-udp;support_auth;sip:sip.nextfone.com.bd;sip:09614500737@sip.nextfone.com.bd;09614500737;60;600;300;3600;yes;support
        PSRegistration psRegistration = new PSRegistration();
        psRegistration.setId(sipEntityId);//support
        psRegistration.setClientUri("sip:" + sipEntityId + "@" + host);//"sip:09614500737@sip.nextfone.com.bd"
        psRegistration.setContactUser(sipEntityId);//09614500737
        psRegistration.setExpiration(3600);
        psRegistration.setOutboundAuth(sipEntityId);//support_auth
        psRegistration.setRetryInterval(60);
        psRegistration.setForbiddenRetryInterval(600);
        psRegistration.setServerUri("sip:" + host);//sip:sip.nextfone.com.bd
        psRegistration.setTransport(TRANSPORT);
        psRegistration.setFatalRetryInterval(300);
        psRegistration.setLine(YesNo.no);
        return psRegistrationRepository.save(psRegistration);
    }

    @Override
    public List<PSRegistration> findAll() {
        return psRegistrationRepository.findAll();
    }

    @Override
    public Optional<PSRegistration> findOne(String id) {
        return psRegistrationRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(psRegistration -> {
            psRegistrationRepository.delete(psRegistration);
            log.debug("Deleted psRegistration: {}", psRegistration);
        });
    }
}
