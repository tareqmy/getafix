package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueRule;
import com.telefonix.getafix.asterisk.repository.AXQueueRuleRepository;
import com.telefonix.getafix.asterisk.service.AXQueueRuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXQueueRuleServiceImpl implements AXQueueRuleService {

    private AXQueueRuleRepository axQueueRuleRepository;

    @Autowired
    public AXQueueRuleServiceImpl(AXQueueRuleRepository axQueueRuleRepository) {
        this.axQueueRuleRepository = axQueueRuleRepository;
    }

    @Override
    public List<AXQueueRule> findAll() {
        return axQueueRuleRepository.findAll();
    }
}
