package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.entity.AXQueue;
import com.telefonix.getafix.asterisk.service.AXQueueService;
import com.telefonix.getafix.asterisk.service.live.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Component
public class AXQueueLiveRepository {

    private Map<String, AXQueueLive> queueLiveMap;

    private List<AXQueueLiveEventListener> axQueueLiveEventListeners;

    private List<AXQueueMemberLiveEventListener> axQueueMemberLiveEventListeners;

    private AXQueueService axQueueService;

    private ScheduledExecutorService scheduleExecutor;

    @Autowired
    public AXQueueLiveRepository(AXQueueService axQueueService,
                                 ScheduledExecutorService scheduleExecutor) {
        this.queueLiveMap = new ConcurrentHashMap<>();
        this.axQueueLiveEventListeners = new ArrayList<>();
        this.axQueueMemberLiveEventListeners = new ArrayList<>();
        this.axQueueService = axQueueService;
        this.scheduleExecutor = scheduleExecutor;
    }

    public void addAXQueueLiveEventListener(AXQueueLiveEventListener axQueueLiveEventListener) {
        axQueueLiveEventListeners.add(axQueueLiveEventListener);
    }

    public void addAXQueueMemberLiveEventListener(AXQueueMemberLiveEventListener axQueueMemberLiveEventListener) {
        axQueueMemberLiveEventListeners.add(axQueueMemberLiveEventListener);
    }

    public synchronized AXQueueLive getAXQueueLive(String queueName) {
        AXQueueLive axQueueLive = queueLiveMap.get(queueName);
        if (axQueueLive == null) {
            Optional<AXQueue> axQueue = axQueueService.findOne(queueName);
            if (axQueue.isPresent()) {
                axQueueLive = new AXQueueLive(axQueue.get());
                queueLiveMap.put(queueName, axQueueLive);
            }
        }
        return axQueueLive;
    }

    public synchronized AXQueueLive removeAXQueueLive(String queueName) {
        return queueLiveMap.remove(queueName);
    }

    public void onQueueSummary(AXQueueLive axQueueLive, QueueSummary queueSummary) {
        axQueueLive.setQueueSummary(queueSummary);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            axQueueLiveEventListener.onQueueSummary(axQueueLive, queueSummary);
        });
    }

    public void onQueueParams(AXQueueLive axQueueLive, QueueParams queueParams) {
        axQueueLive.setQueueParams(queueParams);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            axQueueLiveEventListener.onQueueParams(axQueueLive, queueParams);
        });
    }

    public void onQueueMembers(AXQueueLive axQueueLive, List<AXQueueMemberLive> axQueueMemberLives) {
        log.trace("{}/{}", axQueueLive.getQueueMemberLiveMap().keySet(), axQueueMemberLives);
        Iterator<String> iterator = axQueueLive.getQueueMemberLiveMap().keySet().iterator();
        //if empty just remove all and return
        if (axQueueMemberLives.isEmpty()) {
            while (iterator.hasNext()) {
                String interFace = iterator.next();
                onQueueMemberRemoved(axQueueLive, interFace);
            }
            axQueueLive.getQueueMemberLiveMap().clear();
            return;
        }

        //remove those that are not removed
        while (iterator.hasNext()) {
            String interFace = iterator.next();
            //check in lives. if not present remove?
            if (axQueueMemberLives.stream().noneMatch(ax -> interFace.equals(ax.getInterFace()))) {
                onQueueMemberRemoved(axQueueLive, interFace);
                log.debug("removing {}", interFace);
            }
        }

        axQueueMemberLives.forEach(axQueueMemberLive -> {
            //todo: trying to not send add when it is already added caused issues on upper services
//            if (axQueueLive.getQueueMemberLiveMap().containsKey(axQueueMemberLive.getInterFace())) {
//                onQueueMemberStatus(axQueueLive, axQueueMemberLive);
//                log.trace("updating {}", axQueueMemberLive.getInterFace());
//            } else {
//                onQueueMemberAdded(axQueueLive, axQueueMemberLive);
//                log.debug("adding {}", axQueueMemberLive.getInterFace());
//            }
            onQueueMemberAdded(axQueueLive, axQueueMemberLive);
        });
    }

    public void onQueueCaller(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        axQueueLive.getQueueCallerLiveMap().put(axQueueCallerLive.getUniqueId(), axQueueCallerLive);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            //todo: needs reevaluation
            axQueueLiveEventListener.onQueueCallerJoin(axQueueLive, axQueueCallerLive);
        });
    }

    public void onQueueMemberAdded(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        axQueueLive.getQueueMemberLiveMap().put(axQueueMemberLive.getInterFace(), axQueueMemberLive);
        axQueueMemberLiveEventListeners.forEach(axQueueMemberLiveEventListener -> {
            axQueueMemberLiveEventListener.onQueueMemberAdded(axQueueLive, axQueueMemberLive);
        });
    }

    public void onQueueMemberRemoved(AXQueueLive axQueueLive, String interFace) {
        AXQueueMemberLive axQueueMemberLive = axQueueLive.getQueueMemberLiveMap().get(interFace);
        if (axQueueMemberLive != null) {
            axQueueMemberLiveEventListeners.forEach(axQueueMemberLiveEventListener -> {
                axQueueMemberLiveEventListener.onQueueMemberRemoved(axQueueLive, axQueueMemberLive);
            });
        } else {
            //NOTE: in some weird case if not found use the other method to notify listeners
            axQueueMemberLiveEventListeners.forEach(axQueueMemberLiveEventListener -> {
                axQueueMemberLiveEventListener.onQueueMemberRemoved(axQueueLive, interFace);
            });
        }
        axQueueLive.getQueueMemberLiveMap().remove(interFace);
    }

    public void onQueueMemberPause(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        axQueueMemberLiveEventListeners.forEach(axQueueMemberLiveEventListener -> {
            axQueueMemberLiveEventListener.onQueueMemberPause(axQueueLive, axQueueMemberLive);
        });
    }

    public void onQueueMemberStatus(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive) {
        axQueueLive.getQueueMemberLiveMap().put(axQueueMemberLive.getInterFace(), axQueueMemberLive);
        axQueueMemberLiveEventListeners.forEach(axQueueMemberLiveEventListener -> {
            axQueueMemberLiveEventListener.onQueueMemberStatus(axQueueLive, axQueueMemberLive);
        });
    }

    public void onQueueCallerAbandoned(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        axQueueCallerLive.setAbandoned(true);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            axQueueLiveEventListener.onQueueCallerAbandoned(axQueueLive, axQueueCallerLive);
        });
    }

    public void onQueueCallerJoin(AXQueueLive axQueueLive, AXQueueCallerLive axQueueCallerLive) {
        axQueueLive.getQueueCallerLiveMap().put(axQueueCallerLive.getUniqueId(), axQueueCallerLive);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            axQueueLiveEventListener.onQueueCallerJoin(axQueueLive, axQueueCallerLive);
        });
    }

    public void onQueueCallerLeave(AXQueueLive axQueueLive, String uniqueId) {
        AXQueueCallerLive axQueueCallerLive = axQueueLive.getQueueCallerLiveMap().get(uniqueId);
        axQueueLiveEventListeners.forEach(axQueueLiveEventListener -> {
            axQueueLiveEventListener.onQueueCallerLeave(axQueueLive, axQueueCallerLive);
        });
        scheduleExecutor.schedule(() -> axQueueLive.getQueueCallerLiveMap().remove(uniqueId), 3000L, TimeUnit.MILLISECONDS);
    }
}
