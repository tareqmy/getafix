package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXVoiceMessageService {

    List<AXVoiceMessage> findAllVoiceMailMessageInfo(String mailboxUser, String mailboxContext);

    List<AXVoiceMessage> getAllVoiceMailMessageInfo(String mailboxUser, String mailboxContext, String dir);

    Optional<AXVoiceMessage> getVoiceMailMessageInfo(String mailboxUser, String mailboxContext, String msgId);

    void deleteVoiceMailMessage(String mailboxUser, String mailboxContext, String msgId);

    void deleteVoiceMailMessages(String mailboxUser, String mailboxContext);
}
