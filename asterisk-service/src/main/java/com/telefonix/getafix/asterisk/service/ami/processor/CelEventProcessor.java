package com.telefonix.getafix.asterisk.service.ami.processor;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.CELEventDistributor;
import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.asteriskjava.manager.event.CelEvent;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Component
public class CelEventProcessor implements EventProcessor<CelEvent> {

    private ChannelLiveRepository channelLiveRepository;

    public CelEventProcessor(CELEventDistributor celEventDistributor,
                             ChannelLiveRepository channelLiveRepository) {
        celEventDistributor.register(this);
        this.channelLiveRepository = channelLiveRepository;
    }

    @Override
    public void onEvent(CelEvent event) {
        String eventName = event.getEventName();
        ChannelLive channelLive = channelLiveRepository.getChannelLive(event);

        //because for Local channel these are not available on chan start
        if (StringUtils.isEmpty(channelLive.getCallerIdName())) {
            channelLive.setCallerIdName(event.getCallerIdName());
        }
        if (StringUtils.isEmpty(channelLive.getCallerIdNum())) {
            channelLive.setCallerIdNum(event.getCallerIdNum());
        }
        switch (eventName) {
            case CelEvent.CEL_EVENT_CHAN_START:
                channelLive.setStartInstant(Instant.now());
                channelLive.setApp(event.getApplication());
                channelLive.setAppData(event.getAppData());
                channelLiveRepository.onChannelStart(channelLive);
                break;
            case CelEvent.CEL_EVENT_ANSWER:
                channelLive.setAnswerInstant(Instant.now());
                channelLiveRepository.onChannelAnswered(channelLive);
                break;
            case CelEvent.CEL_EVENT_BRIDGE_ENTER:
                channelLive.setBridgeId(getBridgeId(event.getExtra()));
                break;
            case CelEvent.CEL_EVENT_BRIDGE_EXIT:
                channelLive.setBridgeId(null);
                break;
            case CelEvent.CEL_EVENT_CHAN_END:
            case CelEvent.CEL_EVENT_HANGUP:
                channelLive.setEndInstant(Instant.now());
                channelLiveRepository.onChannelEnd(channelLive);
                break;
            case CelEvent.CEL_EVENT_APP_START:
                channelLive.setApp(event.getApplication());
                channelLive.setAppData(event.getAppData());
                channelLiveRepository.onChannelAppStart(channelLive);
                break;
            case CelEvent.CEL_EVENT_APP_END:
                channelLive.setApp(event.getApplication());
                channelLive.setAppData(event.getAppData());
                channelLiveRepository.onChannelAppStop(channelLive);
                break;
            case CelEvent.CEL_EVENT_BLINDTRANSFER:
            case CelEvent.CEL_EVENT_ATTENDEDTRANSFER:
            case CelEvent.CEL_EVENT_PARK_START:
            case CelEvent.CEL_EVENT_PARK_END:
            case CelEvent.CEL_EVENT_PICKUP:
            case CelEvent.CEL_EVENT_FORWARD:
            default:
                log.info("{} is not handled", eventName);
                break;
        }
    }

    private String getBridgeId(String extra) {
        //extra='{"bridge_id":"4a3bf85a-98ae-4140-b896-a4f95c756ed1","bridge_technology":"simple_bridge"}'
        JsonObject jsonObject = new JsonParser().parse(extra).getAsJsonObject();
        return jsonObject.get("bridge_id").getAsString();
    }
}
