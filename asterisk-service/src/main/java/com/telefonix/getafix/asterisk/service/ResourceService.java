package com.telefonix.getafix.asterisk.service;

import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Endpoint;
import ch.loway.oss.ari4java.generated.Variable;

import java.util.List;

/**
 * Created by tareqmy on 2019-03-17.
 */
public interface ResourceService {

    Endpoint getEndpoint(String resource);

    List<Endpoint> getAllEndpoints();

    List<Endpoint> getEndpoints(List<String> resources);

    Channel getChannel(String channelId);

    List<Channel> getAllChannels();

    Variable getChannelVar(String channelId, String varName);

    void holdChannel(String channelId);

    void continueChannelInDialplan(String channelId, String context, String extension, int priority, String label);

    void hangupChannel(String channelId, String hangupReason);

    Bridge getBridge(String bridgeId);

    List<Bridge> getAllBridges();

    void addChannel(String bridgeId, String channelId);
}
