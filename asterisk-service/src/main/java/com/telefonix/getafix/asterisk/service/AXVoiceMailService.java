package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.dto.AXVoiceMailDTO;
import com.telefonix.getafix.asterisk.entity.AXVoiceMail;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXVoiceMailService {

    AXVoiceMail create(AXVoiceMailDTO axVoiceMailDTO);

    void update(String context, String mailbox, String fullName, String email);

    void changePassword(String context, String mailbox, String password);

    Optional<AXVoiceMail> findOne(Long uniqueid);

    Optional<AXVoiceMail> findOne(String context, String mailbox);

    List<AXVoiceMail> findAll();

    AXVoiceMail getAXVoiceMail(Long uniqueid);

    AXVoiceMail getAXVoiceMail(String context, String mailbox);

    void delete(Long uniqueid);

    void delete(String context, String mailbox);
}
