package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXQueueLogService {

    List<AXQueueLog> findAll();

    Optional<AXQueueLog> findOne(Long id);

    List<AXQueueLog> findByQueueNameAndTimeBetween(String queuename, Instant fromDate, Instant toDate);
}
