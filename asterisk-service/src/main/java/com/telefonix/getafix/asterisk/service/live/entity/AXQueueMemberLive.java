package com.telefonix.getafix.asterisk.service.live.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.asteriskjava.manager.event.QueueMemberAddedEvent;
import org.asteriskjava.manager.event.QueueMemberEvent;
import org.asteriskjava.manager.event.QueueMemberStatusEvent;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"queue", "membership"})
@ToString
public class AXQueueMemberLive {

    private String queue;

    private String interFace; //not in added event

    private String membership;

    private Integer penalty;

    private Integer callsTaken;

    private Long lastCall;

    private Integer status;

    private Boolean paused;

    private Integer incall;

    private String pausedreason;

    private String wrapuptime;

    private String lastpause;

    private Boolean ringinuse; //only in added event

    public AXQueueMemberLive(QueueMemberEvent queueMemberEvent) {
        setQueue(queueMemberEvent.getQueue());
        setInterFace(queueMemberEvent.getStateinterface());
        setMembership(queueMemberEvent.getMembership());
        setPenalty(queueMemberEvent.getPenalty());
        setCallsTaken(queueMemberEvent.getCallsTaken());
        setLastCall(queueMemberEvent.getLastCall());
        setStatus(queueMemberEvent.getStatus());
        setPaused(queueMemberEvent.getPaused());
        setIncall(queueMemberEvent.getIncall());
        setPausedreason(queueMemberEvent.getPausedreason());
        setWrapuptime(queueMemberEvent.getWrapuptime());
        setLastpause(queueMemberEvent.getLastpause());
    }

    public AXQueueMemberLive(QueueMemberStatusEvent queueMemberStatusEvent) {
        setQueue(queueMemberStatusEvent.getQueue());
        setInterFace(queueMemberStatusEvent.getInterface());
        setMembership(queueMemberStatusEvent.getMembership());
        setPenalty(queueMemberStatusEvent.getPenalty());
        setCallsTaken(queueMemberStatusEvent.getCallsTaken());
        setLastCall(queueMemberStatusEvent.getLastCall());
        setStatus(queueMemberStatusEvent.getStatus());
        setPaused(queueMemberStatusEvent.getPaused());
        setIncall(queueMemberStatusEvent.getIncall());
        setPausedreason(queueMemberStatusEvent.getPausedreason());
        setWrapuptime(queueMemberStatusEvent.getWrapuptime());
        setLastpause(queueMemberStatusEvent.getLastpause());
    }

    public AXQueueMemberLive(QueueMemberAddedEvent queueMemberAddedEvent) {
        setQueue(queueMemberAddedEvent.getQueue());
        setInterFace(queueMemberAddedEvent.getInterface());
        setMembership(queueMemberAddedEvent.getMembership());
        setPenalty(queueMemberAddedEvent.getPenalty());
        setCallsTaken(queueMemberAddedEvent.getCallsTaken());
        setLastCall(queueMemberAddedEvent.getLastCall());
        setStatus(queueMemberAddedEvent.getStatus());
        setPaused(queueMemberAddedEvent.getPaused());
        setIncall(queueMemberAddedEvent.getIncall());
        setPausedreason(queueMemberAddedEvent.getPausedreason());
        setWrapuptime(queueMemberAddedEvent.getWrapuptime());
        setLastpause(queueMemberAddedEvent.getLastpause());

        setRinginuse(queueMemberAddedEvent.getRinginuse());
    }
}
