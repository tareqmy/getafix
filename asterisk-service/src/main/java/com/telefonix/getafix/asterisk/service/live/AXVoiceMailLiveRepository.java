package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import com.telefonix.getafix.asterisk.repository.AXVoiceMailRepository;
import com.telefonix.getafix.asterisk.repository.AXVoiceMessageRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXVoiceMailLive;
import com.telefonix.getafix.asterisk.utils.AsteriskTelephonyUtils;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.MessageWaitingEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-07-30.
 */
@Slf4j
@Component
public class AXVoiceMailLiveRepository {

    private Map<String, AXVoiceMailLive> axVoiceMailLiveMap;

    private List<AXVoiceMailLiveListener> axVoiceMailLiveListeners;

    private AXVoiceMailRepository axVoiceMailRepository;

    private AXVoiceMessageRepository axVoiceMessageRepository;

    public AXVoiceMailLiveRepository(AXVoiceMailRepository axVoiceMailRepository,
                                     AXVoiceMessageRepository axVoiceMessageRepository) {

        this.axVoiceMailRepository = axVoiceMailRepository;
        this.axVoiceMessageRepository = axVoiceMessageRepository;

        this.axVoiceMailLiveMap = new ConcurrentHashMap<>();
        this.axVoiceMailLiveListeners = new ArrayList<>();
    }

    //only called on application start... should not notify anybody
    public void initialize() {
        axVoiceMailRepository.findAll().forEach(this::initAXVoiceMailLive);
    }

    // -----

    public void addAXVoiceMailLiveListener(AXVoiceMailLiveListener axVoiceMailLiveListener) {
        axVoiceMailLiveListeners.add(axVoiceMailLiveListener);
    }

    private void notifyListeners(AXVoiceMailLive axVoiceMailLive) {
        if (axVoiceMailLive == null) {
            log.warn("Request for null live notify!");
            return;
        }
        axVoiceMailLiveListeners.forEach(axVoiceMailLiveListener -> {
            axVoiceMailLiveListener.onVoiceMailLiveUpdate(axVoiceMailLive);
        });
    }

    // ----- live map manipulators -----

    private AXVoiceMailLive initAXVoiceMailLive(AXVoiceMail axVoiceMail) {
        AXVoiceMailLive axVoiceMailLive = new AXVoiceMailLive(axVoiceMail);
        axVoiceMailLiveMap.put(axVoiceMailLive.getMailbox(), axVoiceMailLive);
        rePopulateVoiceMessages(axVoiceMailLive);
        return axVoiceMailLive;
    }

    public synchronized AXVoiceMailLive getAXVoiceMailLive(String mailbox) {
        AXVoiceMailLive axVoiceMailLive = axVoiceMailLiveMap.get(mailbox);
        if (axVoiceMailLive == null) {
            String mailExtension = AsteriskTelephonyUtils.getMailExtension(mailbox);
            String mailContext = AsteriskTelephonyUtils.getMailContext(mailbox);
            axVoiceMailLive = axVoiceMailRepository.findByContextAndMailbox(mailContext, mailExtension)
                .map(this::initAXVoiceMailLive).orElse(null);
        }
        return axVoiceMailLive;
    }

    public synchronized void onAXVoiceMailDeleted(String mailbox) {
        axVoiceMailLiveMap.remove(mailbox);
    }

    // ------------

    public synchronized void onAXVoiceMailCreated(AXVoiceMail axVoiceMail) {
        initAXVoiceMailLive(axVoiceMail);
    }

    public synchronized void onAXVoiceMessageDelete(String mailbox) {
        AXVoiceMailLive axVoiceMailLive = getAXVoiceMailLive(mailbox);
        if (axVoiceMailLive != null) {
            rePopulateVoiceMessages(axVoiceMailLive);
            notifyListeners(axVoiceMailLive);
            log.trace("{}", axVoiceMailLive);
        }
    }

    private void rePopulateVoiceMessages(AXVoiceMailLive axVoiceMailLive) {
        axVoiceMailLive.getAxVoiceMessageMap().clear();
        axVoiceMessageRepository.findByMailboxUserAndMailboxContext(axVoiceMailLive.getAxVoiceMail().getMailbox(), axVoiceMailLive.getAxVoiceMail().getContext())
            .forEach(axVoiceMessage -> {
                axVoiceMailLive.getAxVoiceMessageMap().put(axVoiceMessage.getMsgId(), axVoiceMessage);
            });
    }

    // ------------

    public void onMessage(MessageWaitingEvent event) {
        AXVoiceMailLive axVoiceMailLive = getAXVoiceMailLive(event.getMailbox());
        if (axVoiceMailLive == null) {
            log.warn("Found a axVoiceMail without a live! {}", event.getMailbox());
        } else {
            rePopulateVoiceMessages(axVoiceMailLive);
            notifyListeners(axVoiceMailLive);
        }
        log.trace("{}", axVoiceMailLive);
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void refresh() { //scheduled refresh of axvoicemail lives and repopulate it with messages and notify
        axVoiceMailRepository.findAll().forEach(axVoiceMail -> {
            AXVoiceMailLive axVoiceMailLive = getAXVoiceMailLive(AsteriskTelephonyUtils.getMailBox(axVoiceMail.getMailbox(), axVoiceMail.getContext()));
            if (axVoiceMailLive == null) {
                log.warn("Found a axVoiceMail without a live! {}", axVoiceMail);
            } else {
                rePopulateVoiceMessages(axVoiceMailLive);
                notifyListeners(axVoiceMailLive);
            }
            log.trace("{}", axVoiceMailLive);
        });
    }
}
