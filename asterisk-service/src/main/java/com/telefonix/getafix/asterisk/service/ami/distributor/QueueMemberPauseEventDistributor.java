package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberPauseEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Component
public class QueueMemberPauseEventDistributor implements EventDistributor<QueueMemberPauseEvent> {

    private List<EventProcessor<QueueMemberPauseEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return QueueMemberPauseEvent.class;
    }

    @Override
    public void register(EventProcessor<QueueMemberPauseEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(QueueMemberPauseEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
