package com.telefonix.getafix.asterisk.service.ami;

import com.telefonix.getafix.asterisk.ami.AsteriskManagerCommandService;
import com.telefonix.getafix.asterisk.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ResponseEvents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Service
@Transactional
public class ManagerServiceImpl implements ManagerService {

    private AsteriskManagerCommandService asteriskManagerCommandService;

    @Autowired
    public ManagerServiceImpl(AsteriskManagerCommandService asteriskManagerCommandService) {
        this.asteriskManagerCommandService = asteriskManagerCommandService;
    }

    @Override
    public String queueAdd(String queueName, String iface, String memberName) {
        return asteriskManagerCommandService.queueAdd(queueName, iface, memberName);
    }

    @Override
    public String queueRemove(String queueName, String iface) {
        return asteriskManagerCommandService.queueRemove(queueName, iface);
    }

    @Override
    public String queuePause(String queueName, String iface) {
        return asteriskManagerCommandService.queuePause(queueName, iface);
    }

    @Override
    public String queueUnpause(String queueName, String iface) {
        return asteriskManagerCommandService.queueUnpause(queueName, iface);
    }

    @Override
    public ResponseEvents queueStatus() {
        return asteriskManagerCommandService.queueStatus();
    }

    @Override
    public ResponseEvents queueStatus(String queueName) {
        return asteriskManagerCommandService.queueStatus(queueName);
    }

    @Override
    public ResponseEvents queueSummary(String queueName) {
        return asteriskManagerCommandService.queueSummary(queueName);
    }

    @Override
    public String originate(String channel, String callerId, String application, String data, Map<String, String> variables) {
        return asteriskManagerCommandService.originate(channel, callerId, application, data, variables);
    }

    @Override
    public String blindTransfer(String channel, String context, String exten) {
        return asteriskManagerCommandService.blindTransfer(channel, context, exten);
    }
}
