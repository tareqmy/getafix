package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberRemovedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Slf4j
@Component
public class QueueMemberRemovedEventDistributor implements EventDistributor<QueueMemberRemovedEvent> {

    private List<EventProcessor<QueueMemberRemovedEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return QueueMemberRemovedEvent.class;
    }

    @Override
    public void register(EventProcessor<QueueMemberRemovedEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(QueueMemberRemovedEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
