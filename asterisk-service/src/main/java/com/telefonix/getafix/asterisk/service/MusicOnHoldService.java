package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.MusicOnHold;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 15/9/19.
 */
public interface MusicOnHoldService {

    MusicOnHold create(String name, String directory);

    MusicOnHold update(String name, String directory);

    List<MusicOnHold> findAll();

    Optional<MusicOnHold> findOne(String name);

    MusicOnHold getMusicOnHold(String name);

    void delete(String name);
}
