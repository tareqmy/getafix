package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AXQueueMember;
import com.telefonix.getafix.asterisk.entity.AXQueueMemberId;
import com.telefonix.getafix.asterisk.repository.AXQueueMemberRepository;
import com.telefonix.getafix.asterisk.service.AXQueueMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class AXQueueMemberServiceImpl implements AXQueueMemberService {

    private AXQueueMemberRepository axQueueMemberRepository;

    @Autowired
    public AXQueueMemberServiceImpl(AXQueueMemberRepository axQueueMemberRepository) {
        this.axQueueMemberRepository = axQueueMemberRepository;
    }

    @Override
    public Optional<AXQueueMember> findOne(String queueName, String interFace) {
        AXQueueMemberId axQueueMemberId = new AXQueueMemberId();
        axQueueMemberId.setQueueName(queueName);
        axQueueMemberId.setInterFace(interFace);
        return axQueueMemberRepository.findById(axQueueMemberId);
    }

    @Override
    public AXQueueMember getQueue(String queueName, String interFace) {
        return findOne(queueName, interFace).orElseThrow(
            () -> new RuntimeException("Invalid ax queue member id " + queueName + " interFace " + interFace)
        );
    }

    @Override
    public List<AXQueueMember> findAll() {
        return axQueueMemberRepository.findAll();
    }
}
