package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueMemberLive;

/**
 * Created by tareqmy on 2019-07-04.
 */
public interface AXQueueMemberLiveEventListener {

    void onQueueMemberAdded(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive);

    void onQueueMemberRemoved(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive);

    void onQueueMemberRemoved(AXQueueLive axQueueLive, String interFace);

    void onQueueMemberPause(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive);

    void onQueueMemberStatus(AXQueueLive axQueueLive, AXQueueMemberLive axQueueMemberLive);
}
