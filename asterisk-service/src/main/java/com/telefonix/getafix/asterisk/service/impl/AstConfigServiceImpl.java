package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.AstConfig;
import com.telefonix.getafix.asterisk.repository.AstConfigRepository;
import com.telefonix.getafix.asterisk.service.AstConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-29.
 */
@Slf4j
@Service
@Transactional
public class AstConfigServiceImpl implements AstConfigService {

    private static final String EXTENSION_FILENAME = "extensions.conf";

    private AstConfigRepository astConfigRepository;

    @Autowired
    public AstConfigServiceImpl(AstConfigRepository astConfigRepository) {
        this.astConfigRepository = astConfigRepository;
    }

    @Override
    public AstConfig save(int catMetric, int varMetric, String category, String varName, String varValue) {
        AstConfig astConfig = new AstConfig();
        astConfig.setCatMetric(catMetric);
        astConfig.setVarMetric(varMetric);
        astConfig.setFilename(EXTENSION_FILENAME);
        astConfig.setCategory(category);
        astConfig.setVarName(varName);
        astConfig.setVarValue(varValue);
        astConfig.setCommented(0);
        return astConfigRepository.save(astConfig);
    }

    @Override
    public List<String> findAllCategory() {
        return astConfigRepository.findAllCategory();
    }

    @Override
    public List<AstConfig> findAll() {
        return astConfigRepository.findAll();
    }

    @Override
    public AstConfig findOneByCategory(String category) {
        return astConfigRepository.findFirstByCategoryOrderByVarMetricAsc(category);
    }

    @Override
    public List<AstConfig> findByCategory(String category) {
        return astConfigRepository.findByCategory(category);
    }

    @Override
    public int getNextCatMetric() {
        AstConfig topByOrderByCatMetric = astConfigRepository.findTopByOrderByCatMetricDesc();
        log.debug("Found top by cat metric {}", topByOrderByCatMetric);
        if (topByOrderByCatMetric != null) {
            return topByOrderByCatMetric.getCatMetric() + 1;
        }
        return 1;
    }

    @Override
    public int getNextVarMetric(String category) {
        AstConfig lastByCategoryOrderByVarMetricAsc = astConfigRepository.findFirstByCategoryOrderByVarMetricDesc(category);
        if (lastByCategoryOrderByVarMetricAsc != null) {
            return lastByCategoryOrderByVarMetricAsc.getVarMetric() + 1;
        }
        return 1;
    }

    @Override
    public int getCatMetric(String category) {
        AstConfig oneByCategory = findOneByCategory(category);
        if (oneByCategory != null) {
            return oneByCategory.getCatMetric();
        }
        return 0;
    }

    @Override
    public void delete(Long id) {
        astConfigRepository.findById(id).ifPresent(astConfig -> {
            log.debug("Deleting astConfig:{}", astConfig);
            astConfigRepository.deleteById(id);
        });
    }

    @Override
    public void deleteByCategory(String category) {
        astConfigRepository.deleteByCategory(category);
    }
}
