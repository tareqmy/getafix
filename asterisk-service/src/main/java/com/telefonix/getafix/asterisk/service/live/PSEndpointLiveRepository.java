package com.telefonix.getafix.asterisk.service.live;

import ch.loway.oss.ari4java.generated.Endpoint;
import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import com.telefonix.getafix.asterisk.service.PSEndpointService;
import com.telefonix.getafix.asterisk.service.ResourceService;
import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class PSEndpointLiveRepository {

    private Map<String, PSEndpointLive> peerLiveMap;

    private List<PSEndpointLiveEventListener> psEndpointLiveEventListeners;

    private PSEndpointService psEndpointService;

    private ResourceService resourceService;

    private final static Pattern peerPattern = Pattern.compile("^PJSIP/(.*?)$");

    @Autowired
    public PSEndpointLiveRepository(PSEndpointService psEndpointService,
                                    ResourceService resourceService) {
        this.peerLiveMap = new ConcurrentHashMap<>();
        this.psEndpointLiveEventListeners = new ArrayList<>();
        this.psEndpointService = psEndpointService;
        this.resourceService = resourceService;
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void refresh() {
        resourceService.getAllEndpoints().forEach(endpoint -> {
            PSEndpointLive psEndpointLive = getPSEndpointLive(endpoint.getResource());
            updateEndpoint(psEndpointLive, endpoint, "Unknown");
        });
        log.trace("{}", peerLiveMap);
    }

    public void addPSEndpointLiveEventListener(PSEndpointLiveEventListener psEndpointLiveEventListener) {
        psEndpointLiveEventListeners.add(psEndpointLiveEventListener);
    }

    private void notifyListeners(PSEndpointLive psEndpointLive) {
        if (psEndpointLive == null) {
            log.info("Request for null live notify!");
            return;
        }
        psEndpointLiveEventListeners.forEach(psEndpointLiveEventListener -> {
            psEndpointLiveEventListener.onPSEndpointStateUpdate(psEndpointLive);
        });
    }

    public void onStatus(String peer, String peerStatus) {
        Matcher matcher = peerPattern.matcher(peer);
        if (matcher.find()) {
            peer = matcher.group(1);
        }
        PSEndpointLive psEndpointLive = getPSEndpointLive(peer);
        Endpoint endpoint = resourceService.getEndpoint(peer);
        updateEndpoint(psEndpointLive, endpoint, peerStatus);
    }

    private void updateEndpoint(PSEndpointLive psEndpointLive, Endpoint endpoint, String peerStatus) {
        psEndpointLive.setTechnology(endpoint.getTechnology());
        psEndpointLive.setResource(endpoint.getResource());
        psEndpointLive.setState(endpoint.getState());
        psEndpointLive.setPeerStatus(peerStatus);
        psEndpointLive.setUpdateInstant(Instant.now());
        //NOTICE: incase of registration 3 quick consecutive updates come
        notifyListeners(psEndpointLive);
        log.trace("{}", psEndpointLive);
    }

    public synchronized PSEndpointLive getPSEndpointLive(String peerName) {
        PSEndpointLive psEndpointLive = peerLiveMap.get(peerName);
        if (psEndpointLive == null) {
            Optional<PSEndpoint> psEndpoint = psEndpointService.findOne(peerName);
            if (psEndpoint.isPresent()) {
                psEndpointLive = new PSEndpointLive(psEndpoint.get());
                peerLiveMap.put(peerName, psEndpointLive);
            }
        }
        return psEndpointLive;
    }

    public synchronized PSEndpointLive removePSEndpointLive(String peerName) {
        return peerLiveMap.remove(peerName);
    }
}
