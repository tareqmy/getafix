package com.telefonix.getafix.asterisk.service.ari;

import ch.loway.oss.ari4java.generated.Message;
import com.telefonix.getafix.asterisk.ari.ResourceMessageListener;
import com.telefonix.getafix.asterisk.ari.ResourceMessageNotificationService;
import com.telefonix.getafix.asterisk.service.ari.application.CallManager;
import com.telefonix.getafix.asterisk.service.ari.processor.StasisEndMessageProcessor;
import com.telefonix.getafix.asterisk.service.ari.processor.StasisStartMessageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by tareqmy on 2019-02-10.
 */
@Slf4j
@Component
public class MessageDistributor implements ResourceMessageListener {

    private HashMap<Class, MessageProcessor> messageProcessors = new HashMap<>();

    @Autowired
    public MessageDistributor(ResourceMessageNotificationService resourceMessageNotificationService,
                              CallManager callManager) {
        register(new StasisStartMessageProcessor(callManager));
        register(new StasisEndMessageProcessor(callManager));

        resourceMessageNotificationService.registerResourceMessageListener(this);
    }

    private void register(MessageProcessor messageProcessor) {
        messageProcessors.put(messageProcessor.getMessageClass(), messageProcessor);
    }

    @Async("asteriskTaskExecutor")
    @Override
    public void onResourceMessage(Message message) {
        if (message != null) {
            MessageProcessor messageProcessor = messageProcessors.get(message.getClass());
            if (messageProcessor != null) {
                messageProcessor.processMessage(message);
            }
        }
    }
}
