package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.MessageWaitingEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-07-30.
 */
@Slf4j
@Component
public class MessageWaitingEventDistributor implements EventDistributor<MessageWaitingEvent> {

    private List<EventProcessor<MessageWaitingEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return MessageWaitingEvent.class;
    }

    @Override
    public void register(EventProcessor<MessageWaitingEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(MessageWaitingEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
