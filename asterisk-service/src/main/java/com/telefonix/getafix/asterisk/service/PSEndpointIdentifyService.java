package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSEndpointIdentify;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSEndpointIdentifyService {

    PSEndpointIdentify create(String sipEntityId, String host);

    Optional<PSEndpointIdentify> findOne(String id);

    List<PSEndpointIdentify> findByEndpoint(String endpoint);

    void delete(String sipEntityId);
}
