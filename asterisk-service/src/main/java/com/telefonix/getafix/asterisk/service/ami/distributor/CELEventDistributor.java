package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.CelEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
@Component
public class CELEventDistributor implements EventDistributor<CelEvent> {

    private List<EventProcessor<CelEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return CelEvent.class;
    }

    @Override
    public void register(EventProcessor<CelEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(CelEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
