package com.telefonix.getafix.asterisk.service;

/**
 * Created by tareqmy on 2019-02-07.
 */
public interface AsteriskControlService {

    void executeDialplanReload();

    void setReloadDialplan(boolean reloadDialplan);

    void executeQueueReload();
}
