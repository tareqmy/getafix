package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AXQueueMember;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface AXQueueMemberService {

    Optional<AXQueueMember> findOne(String queueName, String interFace);

    AXQueueMember getQueue(String queueName, String interFace);

    List<AXQueueMember> findAll();
}
