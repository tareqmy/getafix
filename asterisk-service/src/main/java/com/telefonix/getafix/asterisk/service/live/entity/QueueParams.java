package com.telefonix.getafix.asterisk.service.live.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.asteriskjava.manager.event.QueueParamsEvent;

/**
 * Created by tareqmy on 2019-06-23.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"queue"})
@ToString
public class QueueParams {

    private String queue;

    private Integer max;

    private String strategy;

    private Integer calls;

    private Integer holdTime;

    private Integer talkTime;

    private Integer completed;

    private Integer abandoned;

    private Integer serviceLevel;

    private Double serviceLevelPerf;

    private Double serviceLevelPerf2;

    public QueueParams(QueueParamsEvent queueParamsEvent) {
        setQueue(queueParamsEvent.getQueue());
        setMax(queueParamsEvent.getMax());
        setStrategy(queueParamsEvent.getStrategy());
        setCalls(queueParamsEvent.getCalls());
        setHoldTime(queueParamsEvent.getHoldTime());
        setTalkTime(queueParamsEvent.getTalkTime());
        setCompleted(queueParamsEvent.getCompleted());
        setAbandoned(queueParamsEvent.getAbandoned());
        setServiceLevel(queueParamsEvent.getServiceLevel());
        setServiceLevelPerf(queueParamsEvent.getServiceLevelPerf());
        setServiceLevelPerf2(queueParamsEvent.getServiceLevelPerf2());
    }
}
