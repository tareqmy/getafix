package com.telefonix.getafix.asterisk.service.ari.processor;

import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.StasisStart;
import ch.loway.oss.ari4java.generated.ari_4_0_0.models.StasisStart_impl_ari_4_0_0;
import com.telefonix.getafix.asterisk.service.ari.application.CallEventListener;
import com.telefonix.getafix.asterisk.service.ari.application.CallManager;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by tareqmy on 2019-02-10.
 */
@Slf4j
public class StasisStartMessageProcessor extends AbstractMessageProcessor<StasisStart> {

    public StasisStartMessageProcessor(CallManager callManager) {
        super(callManager);
    }

    @Override
    public Class getMessageClass() {
        return StasisStart_impl_ari_4_0_0.class;
    }

    @Override
    public void processMessage(StasisStart message) {
        log.trace("StasisStart received {} from {} {} to {}", message.getChannel().getId(), message.getChannel().getAccountcode(),
            message.getChannel().getName(), message.getChannel().getDialplan().getExten());
        Channel channel = message.getChannel();

        CallEventListener callEventListener = callManager.getCallEventListener(channel.getId());
        if (callEventListener == null)
            callEventListener = callManager.onStasisStart(channel, message.getArgs());
        //in case this is not a new call
        if (callEventListener != null)
            callEventListener.onStasisStart(channel);
    }
}
