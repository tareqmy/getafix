package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.MessageWaitingEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXVoiceMailLiveRepository;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.MessageWaitingEvent;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-07-30.
 */
@Slf4j
@Component
public class MessageWaitingEventProcessor implements EventProcessor<MessageWaitingEvent> {

    private AXVoiceMailLiveRepository AXVoiceMailLiveRepository;

    public MessageWaitingEventProcessor(MessageWaitingEventDistributor messageWaitingEventDistributor,
                                        AXVoiceMailLiveRepository AXVoiceMailLiveRepository) {
        messageWaitingEventDistributor.register(this);
        this.AXVoiceMailLiveRepository = AXVoiceMailLiveRepository;
    }

    @Override
    public void onEvent(MessageWaitingEvent event) {
        this.AXVoiceMailLiveRepository.onMessage(event);
    }
}
