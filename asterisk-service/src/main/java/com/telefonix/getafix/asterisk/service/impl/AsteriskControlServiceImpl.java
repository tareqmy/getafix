package com.telefonix.getafix.asterisk.service.impl;

import ch.loway.oss.ari4java.tools.RestException;
import com.telefonix.getafix.asterisk.ari.AsteriskResourceService;
import com.telefonix.getafix.asterisk.service.AsteriskControlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tareqmy on 2019-02-07.
 */
@Slf4j
@Service
@Transactional
public class AsteriskControlServiceImpl implements AsteriskControlService {

    @Autowired
    private AsteriskResourceService asteriskResourceService;

    private boolean reloadDialplan = false;

    @Scheduled(cron = "${getafix.asterisk.ari.scheduled.executeCommand}")
    public void executeDialplanReload() {
        if (isReloadDialplan()) {
            log.debug("Executing dialplan reload");
            try {
                asteriskResourceService.reloadModule("pbx_config.so");
                setReloadDialplan(false);
            } catch (RestException e) {
                log.error("Failed to reload asterisk dialplan!! {}", e);
            }
        }
    }

    public boolean isReloadDialplan() {
        return reloadDialplan;
    }

    /**
     * to enable asterisk dialplan reload.
     *
     * @param reloadDialplan
     */
    public void setReloadDialplan(boolean reloadDialplan) {
        this.reloadDialplan = reloadDialplan;
    }

    @Override
    public void executeQueueReload() {
        log.debug("Executing queue reload");
        try {
            asteriskResourceService.reloadModule("app_queue.so");
        } catch (RestException ignored) {

        }
    }
}
