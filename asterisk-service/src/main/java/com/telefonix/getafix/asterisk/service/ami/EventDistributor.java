package com.telefonix.getafix.asterisk.service.ami;

import org.asteriskjava.manager.event.ManagerEvent;

/**
 * Created by tareqmy on 2019-02-11.
 */
public interface EventDistributor<T extends ManagerEvent> {

    Class getEventClass();

    void register(EventProcessor<T> listener);

    void distributeEvent(T event);
}
