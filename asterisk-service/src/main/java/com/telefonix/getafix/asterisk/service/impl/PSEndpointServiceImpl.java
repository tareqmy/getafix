package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import com.telefonix.getafix.asterisk.repository.PSEndpointRepository;
import com.telefonix.getafix.asterisk.service.PSEndpointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSEndpointServiceImpl implements PSEndpointService {

    @Value(value = "${getafix.asterisk.ari.pjsip.transport}")
    private String TRANSPORT;

    @Value(value = "${getafix.asterisk.ari.pjsip.codec.disallow}")
    private String DISALLOW;

    @Value(value = "${getafix.asterisk.ari.pjsip.codec.allow}")
    private String ALLOW;

    @Value(value = "${getafix.asterisk.dialplan.context.main}")
    public String GETAFIX_CONTEXT;

    private PSEndpointRepository psEndpointRepository;

    @Autowired
    public PSEndpointServiceImpl(PSEndpointRepository psEndpointRepository) {
        this.psEndpointRepository = psEndpointRepository;
    }

    @Override
    public PSEndpoint create(String sipEntityId, String context, String callerId, boolean authEnabled) {
//id;transport;context;disallow;allow;auth;aors;callerid;accountcode;set_var;direct_media;rtp_symmetric;force_rport;device_state_busy_at
//1001;transport-udp;local;all;ulaw;1001;1001;Jon Doe <1001>;1001@example.com;Queues=support^sales;no;yes;yes;1
        PSEndpoint psEndpoint = new PSEndpoint();
        psEndpoint.setId(sipEntityId);
        psEndpoint.setTransport(TRANSPORT);
        psEndpoint.setContext(context);
        psEndpoint.setDisallow(DISALLOW);
        psEndpoint.setAllow(ALLOW);
        if (authEnabled) {
            psEndpoint.setAuth(sipEntityId);
        }
        psEndpoint.setAors(sipEntityId);
        psEndpoint.setCallerId(callerId);
        psEndpoint.setAccountCode(sipEntityId);
        psEndpoint.setSetVar(null);
        psEndpoint.setDirectMedia(YesNo.no);
        psEndpoint.setRtpSymmetric(YesNo.yes);
        psEndpoint.setForceRport(YesNo.yes);
        psEndpoint.setDeviceStateBusyAt(1);
        return psEndpointRepository.save(psEndpoint);
    }

    @Override
    public PSEndpoint createProviderEndpoint(String sipEntityId) {
//        id;transport;context;disallow;allow;outbound_auth;aors;force_rport;direct_media;ice_support
//        support;transport-udp;trunk-incoming;all;ulaw;support_auth;support;yes;no;yes
        PSEndpoint psEndpoint = new PSEndpoint();
        psEndpoint.setId(sipEntityId);
        psEndpoint.setTransport(TRANSPORT);
        psEndpoint.setAors(sipEntityId);
        psEndpoint.setContext(GETAFIX_CONTEXT);
        psEndpoint.setDisallow(DISALLOW);
        psEndpoint.setAllow(ALLOW);
        psEndpoint.setIceSupport(YesNo.yes);
        psEndpoint.setForceRport(YesNo.yes);
//        psEndpoint.setDeviceStateBusyAt(null);
        psEndpoint.setAccountCode(sipEntityId);
        return psEndpointRepository.save(psEndpoint);
    }

    @Override
    public PSEndpoint createPhoneNumberEndpoint(String sipEntityId, String aors, String callerId) {
        PSEndpoint psEndpoint = new PSEndpoint();
        psEndpoint.setId(sipEntityId);
        psEndpoint.setTransport(TRANSPORT);
        psEndpoint.setAors(aors);
        psEndpoint.setContext(GETAFIX_CONTEXT);
        psEndpoint.setDisallow(DISALLOW);
        psEndpoint.setAllow(ALLOW);
        psEndpoint.setIceSupport(YesNo.yes);
        psEndpoint.setOutboundAuth(sipEntityId);
        psEndpoint.setCallerId(callerId);//will be used only when line support is enabled
        psEndpoint.setAccountCode(sipEntityId);
        return psEndpointRepository.save(psEndpoint);
    }

    @Override
    public List<PSEndpoint> findAll() {
        return psEndpointRepository.findAll();
    }

    @Override
    public List<PSEndpoint> findAllByContext(String context) {
        return psEndpointRepository.findByContext(context);
    }

    @Override
    public Optional<PSEndpoint> findOne(String id) {
        return psEndpointRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        findOne(id).ifPresent(psEndpoint -> {
            psEndpointRepository.delete(psEndpoint);
            log.debug("Deleted psEndpoint: {}", psEndpoint);
        });
    }
}
