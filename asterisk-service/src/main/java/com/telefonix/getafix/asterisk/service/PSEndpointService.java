package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSEndpoint;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSEndpointService {

    PSEndpoint create(String sipEntityId, String context, String callerId, boolean authEnabled);

    PSEndpoint createProviderEndpoint(String sipEntityId);

    PSEndpoint createPhoneNumberEndpoint(String sipEntityId, String aors, String callerId);

    List<PSEndpoint> findAll();

    List<PSEndpoint> findAllByContext(String context);

    Optional<PSEndpoint> findOne(String id);

    void delete(String id);
}
