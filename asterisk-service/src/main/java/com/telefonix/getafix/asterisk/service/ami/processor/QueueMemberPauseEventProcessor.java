package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueMemberPauseEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueMemberLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberPauseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-18.
 * when member pause/unpause queue
 */
@Slf4j
@Component
public class QueueMemberPauseEventProcessor implements EventProcessor<QueueMemberPauseEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueMemberPauseEventProcessor(QueueMemberPauseEventDistributor queueMemberPauseEventDistributor,
                                          AXQueueLiveRepository axQueueLiveRepository) {
        queueMemberPauseEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueMemberPauseEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        AXQueueMemberLive axQueueMemberLive = axQueueLive.getQueueMemberLiveMap().get(event.getInterface());
        axQueueMemberLive.setPaused(event.getPaused());
        axQueueMemberLive.setPausedreason(event.getPausedreason());
        axQueueLiveRepository.onQueueMemberPause(axQueueLive, axQueueMemberLive);
    }
}
