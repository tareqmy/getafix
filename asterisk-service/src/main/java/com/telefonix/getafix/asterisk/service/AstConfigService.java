package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.AstConfig;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-29.
 */
public interface AstConfigService {

    AstConfig save(int catMetric, int varMetric, String category, String varName, String varValue);

    List<String> findAllCategory();

    List<AstConfig> findAll();

    AstConfig findOneByCategory(String category);

    List<AstConfig> findByCategory(String category);

    int getNextCatMetric();

    int getNextVarMetric(String category);

    int getCatMetric(String category);

    void delete(Long id);

    void deleteByCategory(String category);
}
