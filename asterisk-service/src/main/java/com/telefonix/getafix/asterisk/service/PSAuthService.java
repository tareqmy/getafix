package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSAuth;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSAuthService {

    PSAuth create(String sipEntityId, String password);

    List<PSAuth> findAll();

    Optional<PSAuth> findOne(String id);

    void delete(String id);
}
