package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.service.live.entity.PSEndpointLive;

/**
 * Created by tareqmy on 2019-06-23.
 */
public interface PSEndpointLiveEventListener {

    void onPSEndpointStateUpdate(PSEndpointLive psEndpointLive);
}
