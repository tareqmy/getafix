package com.telefonix.getafix.asterisk.service.impl;

import com.telefonix.getafix.asterisk.entity.PSContact;
import com.telefonix.getafix.asterisk.repository.PSContactRepository;
import com.telefonix.getafix.asterisk.service.PSContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Slf4j
@Service
@Transactional
public class PSContactServiceImpl implements PSContactService {

    private PSContactRepository psContactRepository;

    @Autowired
    public PSContactServiceImpl(PSContactRepository psContactRepository) {
        this.psContactRepository = psContactRepository;
    }

    @Override
    public List<PSContact> findAll() {
        return psContactRepository.findAll();
    }
}
