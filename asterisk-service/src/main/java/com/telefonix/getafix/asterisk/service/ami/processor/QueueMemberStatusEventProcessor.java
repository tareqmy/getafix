package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueMemberStatusEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueMemberLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueMemberStatusEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-18.
 * when joined members status changes
 */
@Slf4j
@Component
public class QueueMemberStatusEventProcessor implements EventProcessor<QueueMemberStatusEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueMemberStatusEventProcessor(QueueMemberStatusEventDistributor queueMemberStatusEventDistributor,
                                           AXQueueLiveRepository axQueueLiveRepository) {
        queueMemberStatusEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueMemberStatusEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        AXQueueMemberLive axQueueMemberLive = new AXQueueMemberLive(event);
        axQueueLiveRepository.onQueueMemberStatus(axQueueLive, axQueueMemberLive);
    }
}
