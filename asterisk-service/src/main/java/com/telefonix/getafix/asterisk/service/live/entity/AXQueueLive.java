package com.telefonix.getafix.asterisk.service.live.entity;

import com.telefonix.getafix.asterisk.entity.AXQueue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-06-18.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"axQueue"})
@ToString
public class AXQueueLive {

    private AXQueue axQueue;

    private QueueSummary queueSummary;

    private QueueParams queueParams;

    private Map<String, AXQueueMemberLive> queueMemberLiveMap = new ConcurrentHashMap<>();

    private Map<String, AXQueueCallerLive> queueCallerLiveMap = new ConcurrentHashMap<>();

    public AXQueueLive(AXQueue axQueue) {
        this.axQueue = axQueue;
    }
}
