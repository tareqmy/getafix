package com.telefonix.getafix.asterisk.service.live;

import com.telefonix.getafix.asterisk.service.live.entity.ChannelLive;

/**
 * Created by tareqmy on 2019-06-16.
 */
public interface ChannelLiveEventListener {

    void onChannelStart(ChannelLive channelLive);

    void onChannelAnswered(ChannelLive channelLive);

    void onChannelAppStart(ChannelLive channelLive);

    void onChannelAppStop(ChannelLive channelLive);

    void onChannelEnd(ChannelLive channelLive);
}
