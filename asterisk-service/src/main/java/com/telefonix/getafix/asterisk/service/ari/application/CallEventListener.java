package com.telefonix.getafix.asterisk.service.ari.application;

import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;

/**
 * Created by tareqmy on 2019-02-11.
 */
public interface CallEventListener {

    void onStasisStart(Channel channel);

    void onChannelStateChange(Channel channel);

    void onChannelHangupRequest(Channel channel);

    void onStasisEnd(Channel channel);

    //-----bridge events

    void onChannelEnteredBridge(Channel channel, Bridge bridge);

    void onChannelLeftBridge(Channel channel, Bridge bridge);

    void onChannelVarset(Channel channel, String variable, String value);
}
