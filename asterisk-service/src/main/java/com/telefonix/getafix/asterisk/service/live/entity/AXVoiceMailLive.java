package com.telefonix.getafix.asterisk.service.live.entity;

import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tareqmy on 2019-07-30.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"mailbox"})
public class AXVoiceMailLive {

    private String mailbox;

    private AXVoiceMail axVoiceMail;

    private Map<String, AXVoiceMessage> axVoiceMessageMap = new ConcurrentHashMap<>();

    public AXVoiceMailLive(AXVoiceMail axVoiceMail) {
        setAxVoiceMail(axVoiceMail);
        setMailbox(axVoiceMail.getMailbox() + "@" + axVoiceMail.getContext());
    }

    @Override
    public String toString() {
        return "AXVoiceMailLive{" +
            "mailbox='" + mailbox + '\'' +
            ", axVoiceMail=" + axVoiceMail.getUniqueid() +
            ", axVoiceMessageMap=" + axVoiceMessageMap.values() +
            '}';
    }
}
