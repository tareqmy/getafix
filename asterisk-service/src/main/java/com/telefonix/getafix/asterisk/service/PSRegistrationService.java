package com.telefonix.getafix.asterisk.service;

import com.telefonix.getafix.asterisk.entity.PSRegistration;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
public interface PSRegistrationService {

    PSRegistration create(String sipEntityId, String host);

    List<PSRegistration> findAll();

    Optional<PSRegistration> findOne(String id);

    void delete(String id);
}
