package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueCallerLeaveEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueCallerLeaveEventDistributor implements EventDistributor<QueueCallerLeaveEvent> {

    private List<EventProcessor<QueueCallerLeaveEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return QueueCallerLeaveEvent.class;
    }

    @Override
    public void register(EventProcessor<QueueCallerLeaveEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(QueueCallerLeaveEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
