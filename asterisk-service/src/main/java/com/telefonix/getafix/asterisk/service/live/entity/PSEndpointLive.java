package com.telefonix.getafix.asterisk.service.live.entity;

import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"resource"})
public class PSEndpointLive {

    private String technology;

    private String resource;

    private String state; //supplied by ari

    private String peerStatus; //supplied by ami

    private PSEndpoint psEndpoint;

    private Instant updateInstant;

    public PSEndpointLive(PSEndpoint psEndpoint) {
        this.resource = psEndpoint.getAccountCode();
        this.psEndpoint = psEndpoint;
    }

    @Override
    public String toString() {
        return "PSEndpointLive{" +
            "technology='" + technology + '\'' +
            ", resource='" + resource + '\'' +
            ", state='" + state + '\'' +
            ", peerStatus='" + peerStatus + '\'' +
            ", psEndpoint=" + psEndpoint.getCallerId() +
            ", updateInstant=" + updateInstant +
            '}';
    }
}
