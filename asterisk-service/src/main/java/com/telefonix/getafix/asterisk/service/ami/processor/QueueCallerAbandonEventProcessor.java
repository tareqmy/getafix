package com.telefonix.getafix.asterisk.service.ami.processor;

import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import com.telefonix.getafix.asterisk.service.ami.distributor.QueueCallerAbandonEventDistributor;
import com.telefonix.getafix.asterisk.service.live.AXQueueLiveRepository;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueCallerLive;
import com.telefonix.getafix.asterisk.service.live.entity.AXQueueLive;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.QueueCallerAbandonEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tareqmy on 2019-06-20.
 */
@Slf4j
@Component
public class QueueCallerAbandonEventProcessor implements EventProcessor<QueueCallerAbandonEvent> {

    private AXQueueLiveRepository axQueueLiveRepository;

    @Autowired
    public QueueCallerAbandonEventProcessor(QueueCallerAbandonEventDistributor queueCallerAbandonEventDistributor,
                                            AXQueueLiveRepository axQueueLiveRepository) {
        queueCallerAbandonEventDistributor.register(this);
        this.axQueueLiveRepository = axQueueLiveRepository;
    }

    @Override
    public void onEvent(QueueCallerAbandonEvent event) {
        AXQueueLive axQueueLive = axQueueLiveRepository.getAXQueueLive(event.getQueue());
        AXQueueCallerLive axQueueCallerLive = axQueueLive.getQueueCallerLiveMap().get(event.getUniqueId());
        axQueueLiveRepository.onQueueCallerAbandoned(axQueueLive, axQueueCallerLive);
    }
}
