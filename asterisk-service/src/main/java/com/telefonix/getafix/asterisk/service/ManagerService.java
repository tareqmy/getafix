package com.telefonix.getafix.asterisk.service;

import org.asteriskjava.manager.ResponseEvents;

import java.util.Map;

/**
 * Created by tareqmy on 2019-06-18.
 */
public interface ManagerService {

    String queueAdd(String iface, String queueName, String memberName);

    String queueRemove(String iface, String queueName);

    String queuePause(String iface, String queueName);

    String queueUnpause(String iface, String queueName);

    ResponseEvents queueStatus();

    ResponseEvents queueStatus(String queueName);

    ResponseEvents queueSummary(String queueName);

    String originate(String channel, String callerId, String application, String data, Map<String, String> variables);

    String blindTransfer(String channel, String context, String exten);
}
