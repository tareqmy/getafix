package com.telefonix.getafix.asterisk.service.ami.distributor;

import com.telefonix.getafix.asterisk.service.ami.EventDistributor;
import com.telefonix.getafix.asterisk.service.ami.EventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.PeerStatusEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
@Component
public class PeerStatusEventDistributor implements EventDistributor<PeerStatusEvent> {

    private List<EventProcessor<PeerStatusEvent>> listeners = new ArrayList<>();

    @Override
    public Class getEventClass() {
        return PeerStatusEvent.class;
    }

    @Override
    public void register(EventProcessor<PeerStatusEvent> listener) {
        listeners.add(listener);
    }

    @Override
    public void distributeEvent(PeerStatusEvent event) {
        listeners.forEach(listener -> {
            listener.onEvent(event);
        });
    }
}
