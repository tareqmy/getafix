#!/bin/sh

echo "The application will start in ${GETAFIX_SLEEP}s..." && sleep ${GETAFIX_SLEEP}
exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "${HOME}/app.war" "$@"
