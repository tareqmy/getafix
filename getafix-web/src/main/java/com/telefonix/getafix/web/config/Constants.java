package com.telefonix.getafix.web.config;

/**
 * Created by tmyousuf on 13/9/18.
 */
public class Constants {

    public static final String SPRING_PROFILE_DEVELOPMENT = "development";
    public static final String SPRING_PROFILE_STAGING = "staging";
    public static final String SPRING_PROFILE_FAST = "fast";
    public static final String SPRING_PROFILE_PRODUCTION = "production";

    public static final String SYSTEM_ACCOUNT = "system";

    private Constants() {
    }

}
