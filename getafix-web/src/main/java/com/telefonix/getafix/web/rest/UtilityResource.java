package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.service.UtilityService;
import com.telefonix.getfix.dto.ApplicationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by tareqmy on 20/12/18.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class UtilityResource {

    @Value(value = "${getafix.version}")
    private String applicationVersion;

    @Value(value = "${getafix.developer.email}")
    private String developerEmail;

    @Value(value = "${getafix.developer.name}")
    private String developerName;

    @Value(value = "${getafix.developer.web}")
    private String developerWeb;

    @Value(value = "${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private UtilityService utilityService;

    @RequestMapping(value = "/utils/zoneids",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAllZoneIds() {
        return utilityService.getAllZoneIds();
    }

    @RequestMapping(value = "/utils/zonesmap",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getAllZonesMap() {
        return utilityService.getAllZonesMap();
    }

    @RequestMapping(value = "/utils/rolesmap",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getAllRolesMap() {
        return utilityService.getAllRolesMap();
    }

    /**
     * GET  /application -> get application.
     */
    @RequestMapping(value = "/application/info",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationDTO> getApplication() {
        log.debug("REST request to get application");
        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setActiveProfile(activeProfile);
        applicationDTO.setApplicationVersion(applicationVersion);
        applicationDTO.setDeveloperEmail(developerEmail);
        applicationDTO.setDeveloperName(developerName);
        applicationDTO.setDeveloperWeb(developerWeb);
        return new ResponseEntity<>(applicationDTO, HttpStatus.OK);
    }
}
