package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.AutoAttendant;
import com.telefonix.getafix.service.AutoAttendantService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.AutoAttendantDTO;
import com.telefonix.getfix.dto.SoundFile;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-05.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class AutoAttendantResource {

    @Autowired
    private AutoAttendantService autoAttendantService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /autoattendants -> Create a new autoattendant.
     */
    @RequestMapping(value = "/autoattendants",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<AutoAttendantDTO> createAutoAttendant(@Valid @RequestBody AutoAttendantDTO autoAttendantDTO) {
        log.debug("REST request to save AutoAttendant : {}", autoAttendantDTO);
        if (autoAttendantDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        AutoAttendant result = autoAttendantService.createAutoAttendant(autoAttendantDTO);
        return AutoAttendantDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /autoattendants -> Updates an existing autoattendant.
     */
    @RequestMapping(value = "/autoattendants",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<AutoAttendantDTO> updateAutoAttendant(@Valid @RequestBody AutoAttendantDTO autoAttendantDTO) {
        log.debug("REST request to update AutoAttendant : {}", autoAttendantDTO);
        if (autoAttendantDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(autoAttendantDTO.getCompanyId());
        AutoAttendant result = autoAttendantService.updateAutoAttendant(autoAttendantDTO);
        return AutoAttendantDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /autoattendants -> get all the autoattendants.
     */
    @RequestMapping(value = "/autoattendants",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<AutoAttendantDTO> getAllAutoAttendants() {
        log.debug("REST request to get all AutoAttendants");
        return autoAttendantService.findAll()
            .stream()
            .map(AutoAttendantDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /autoattendants/:id -> get the "id" autoattendant.
     */
    @RequestMapping(value = "/autoattendants/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<AutoAttendantDTO> getAutoAttendant(@PathVariable Long id) {
        log.debug("REST request to get AutoAttendant : {}", id);
        return autoAttendantService.findOne(id).map(autoAttendant -> {
            accessControl.hasCompanyPermission(autoAttendant);
            return new ResponseEntity<>(new AutoAttendantDTO(autoAttendant), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /autoattendants/:id -> delete the "id" autoattendant.
     */
    @RequestMapping(value = "/autoattendants/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteAutoAttendant(@PathVariable Long id) {
        log.debug("REST request to delete AutoAttendant: {}", id);
        autoAttendantService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * POST  /autoattendants/:id/prompts -> upload the prompt for "id" autoattendant.
     */
    @RequestMapping(value = "/autoattendants/{id}/prompts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<AutoAttendantDTO> uploadPrompt(@PathVariable Long id,
                                                         @RequestParam @Valid @NotNull MultipartFile file) {
        log.debug("REST request to create AutoAttendant prompt : {}", id);
        AutoAttendant autoAttendant = autoAttendantService.createPrompt(id, file);
        return new ResponseEntity<>(new AutoAttendantDTO(autoAttendant), HttpStatus.OK);
    }

    /**
     * GET  /autoattendants/:id/prompts/download -> download the prompt for "id" autoattendant.
     */
    @RequestMapping(value = "/autoattendants/{id}/prompts/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<byte[]> downloadPrompt(@PathVariable Long id) {
        log.debug("REST request to download AutoAttendant prompt : {}", id);
        SoundFile soundFile = autoAttendantService.downloadPrompt(id);
        return MiscUtils.getDownload(soundFile.getBytes(), soundFile.getFileName(), MediaType.parseMediaType("audio/wave"));
    }
}
