package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.AccountService;
import com.telefonix.getafix.web.security.JWTFilter;
import com.telefonix.getafix.web.security.JWTProvider;
import com.telefonix.getfix.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tmyousuf on 6/9/18.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class AccountResource {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JWTProvider jwtProvider;

    @RequestMapping(value = "/account/authenticate",
        method = RequestMethod.POST)
    public ResponseEntity authenticate(@Valid @RequestBody LoginDTO loginDTO, HttpServletResponse response) {
        log.info("{}", loginDTO);
        return Optional.ofNullable(accountService.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getPassword())))
            .map(authentication -> {
                //build jwt token and send it in response
                String jwt = jwtProvider.createToken(authentication, loginDTO.getRememberMe());
                response.addHeader(JWTFilter.AUTHORIZATION_HEADER, JWTFilter.BEARER + jwt);
                return ResponseEntity.ok(new JWTToken(jwt));
            })
            .orElse(new ResponseEntity<>(HttpStatus.UNAUTHORIZED));
    }

    @RequestMapping(value = "/account/authenticate",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    @RequestMapping(value = "/account",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getAccount() {
        return Optional.ofNullable(accountService.findCurrentLoggedInUser())
            .map(user -> new ResponseEntity<>(new UserDTO(user), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @RequestMapping(value = "/account",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> updateAccount(@RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);
        if (userDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User result = accountService.updateAccount(userDTO);
        return UserDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/account/change_password",
        method = RequestMethod.PUT,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> changePassword(@Valid @RequestBody ChangePassword changePassword) {
        accountService.changePassword(changePassword);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/account/email/confirm/init",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> requestPasswordReset() {
        accountService.initiateEmailConfirm();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/account/email/confirm/finish", params = {"token"},
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> finishPasswordReset(@RequestParam String token) {
        boolean completeEmailConfirm = accountService.completeEmailConfirm(token);
        if (completeEmailConfirm) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * GET  /account/extensions -> get extensions of own
     */
    @RequestMapping(value = "/account/extensions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ExtensionDTO> getUserExtensions() {
        log.debug("REST request to get Account extensions");
        return accountService.getExtensionsForAccount()
            .stream()
            .map(ExtensionDTO::new)
            .collect(Collectors.toList());
    }

    @RequestMapping(value = "/accounts/reports/extensions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ExtensionReportDTO> getExtensionReports(@PathVariable Long id) {
        return accountService.getExtensionReports(id);
    }

    /**
     * GET  /account/phones -> get phones of own
     */
    @RequestMapping(value = "/account/phones",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PhoneDTO> getUserPhones() {
        log.debug("REST request to get Account phones");
        return accountService.getPhonesForAccount()
            .stream()
            .map(PhoneDTO::new)
            .collect(Collectors.toList());
    }
}
