package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.service.PhoneLineService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.PhoneLineDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by tareqmy on 2019-03-27.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class PhoneLineResource {

    @Autowired
    private PhoneLineService phoneLineService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /phonelines -> Create a new phone.
     */
    @RequestMapping(value = "/phonelines",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<PhoneLineDTO> createPhoneLine(@Valid @RequestBody PhoneLineDTO phoneLineDTO) {
        log.debug("REST request to save PhoneLine : {}", phoneLineDTO);
        if (phoneLineDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        PhoneLine result = phoneLineService.createPhoneLine(phoneLineDTO);
        return PhoneLineDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /phonelines -> Updates an existing phone.
     */
    @RequestMapping(value = "/phonelines",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<PhoneLineDTO> updatePhoneLine(@Valid @RequestBody PhoneLineDTO phoneLineDTO) {
        log.debug("REST request to update PhoneLine : {}", phoneLineDTO);
        if (phoneLineDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasPhoneLinePermission(phoneLineDTO.getId());
        PhoneLine result = phoneLineService.updatePhoneLine(phoneLineDTO);
        return PhoneLineDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /phonelines/:id -> get the "id" phoneline.
     */
    @RequestMapping(value = "/phonelines/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<PhoneLineDTO> getPhoneLine(@PathVariable Long id) {
        log.debug("REST request to get PhoneLine : {}", id);
        accessControl.hasPhoneLinePermission(id);
        return phoneLineService.findOne(id)
            .map(phoneLine -> new ResponseEntity<>(new PhoneLineDTO(phoneLine), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /phonelines/:id -> delete the "id" phoneline.
     */
    @RequestMapping(value = "/phonelines/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deletePhoneLine(@PathVariable Long id) {
        log.debug("REST request to delete PhoneLine: {}", id);
        phoneLineService.delete(id);
        return ResponseEntity.ok().build();
    }
}
