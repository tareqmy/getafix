package com.telefonix.getafix.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

/**
 * Created by tmyousuf on 7/8/18.
 * https://www.baeldung.com/spring-data-jpa-multiple-databases
 * https://medium.com/@joeclever/using-multiple-datasources-with-spring-boot-and-spring-data-6430b00c02e7
 */

@Slf4j
@Configuration
@EnableJpaRepositories(
    basePackages = "com.telefonix.getafix.asterisk.repository",
    entityManagerFactoryRef = "asteriskEntityManager",
    transactionManagerRef = "asteriskTransactionManager"
)
public class AsteriskDatabaseConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSource asteriskDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(env.getProperty("getafix.asterisk.datasource.url"));
        dataSource.setUsername(env.getProperty("getafix.asterisk.datasource.username"));
        dataSource.setPassword(env.getProperty("getafix.asterisk.datasource.password"));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean asteriskEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(asteriskDataSource());
        em.setPackagesToScan("com.telefonix.getafix.asterisk.entity");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    public PlatformTransactionManager asteriskTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(asteriskEntityManager().getObject());
        return transactionManager;
    }
}
