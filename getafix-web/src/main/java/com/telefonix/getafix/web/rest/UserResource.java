package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.UserService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.*;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    private UserService userService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST  /users -> Creates a new user.
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        log.debug("REST request to create User : {}", userDTO);
        if (userDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User result = userService.createUser(userDTO);
        return UserDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));

    }

    /**
     * PUT  /users -> Updates an existing User.
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);
        if (userDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userService.findOne(userDTO.getId())
            .ifPresent(user -> accessControl.hasCompanyPermission(user));
        User result = userService.updateUserInformation(userDTO);
        return UserDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET  /users -> get all users.
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<UserDTO> getAllUsers() {
        return userService.findAll()
            .stream()
            .map(UserDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /users/:id -> get the "id" user.
     */
    @RequestMapping(value = "/users/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);
        return userService.findOne(id).map(user -> {
            accessControl.hasCompanyPermission(user.getCompany().getId());
            return new ResponseEntity<>(new UserDTO(user), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /users/:id -> delete the "id" User.
     */
    @RequestMapping(value = "/users/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        log.debug("REST request to delete User: {}", id);
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * POST  /users/:id/change_password -> changes the user's password
     */
    @RequestMapping(value = "/users/{id}/change_password",
        method = RequestMethod.PUT,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Void> changeUserPassword(@PathVariable Long id, @Valid @RequestBody ChangePassword changePassword) {
        userService.findOne(id).ifPresent(user -> {
            accessControl.hasCompanyPermission(user.getCompany().getId());
            userService.changeUserPassword(user.getEmail(), changePassword.getNewPassword());
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/users/password/reset/init", params = {"email"},
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> requestPasswordReset(@RequestParam String email) {
        userService.initiatePasswordReset(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/users/password/reset/finish",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> finishPasswordReset(@RequestBody PasswordResetDTO dto) {
        boolean completePasswordReset = userService.completePasswordReset(dto.getToken(), dto.getEmail(), dto.getNewPassword());
        if (completePasswordReset) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * GET  /users/:id/agentlogs -> get agentlogs for the user
     */
    @RequestMapping(value = "/users/{id}/agentlogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public List<AgentLogDTO> getAgentLogs(@PathVariable Long id,
                                          @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                          @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get agentlogs for user : {}", id);
        accessControl.hasUserPermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return userService.getAgentLogs(id, from, to, user.getZoneId())
            .stream()
            .map(agentLog -> new AgentLogDTO(agentLog, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /users/:id/agentreports -> get agentreports for the user
     */
    @RequestMapping(value = "/users/{id}/agentreports",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public AgentReportDTO getAgentReport(@PathVariable Long id,
                                         @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                         @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get agentreports for user : {}", id);
        accessControl.hasUserPermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return userService.getAgentReport(id, from, to, user.getZoneId());
    }

    /**
     * GET  /users/:id/endpointlogs -> get endpointlogs for the user
     */
    @RequestMapping(value = "/users/{id}/endpointlogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public List<EndpointLogDTO> getEndpointLogs(@PathVariable Long id,
                                                @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                                @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get endpointlogs for user : {}", id);
        accessControl.hasUserPermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return userService.getEndpointLogs(id, from, to, user.getZoneId())
            .stream()
            .map(endpointLog -> new EndpointLogDTO(endpointLog, user.getZoneId()))
            .collect(Collectors.toList());
    }
}
