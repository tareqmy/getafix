package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.VoiceMailMessageService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.VoiceMailMessageDTO;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-07-28.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class VoiceMailMessageResource {

    @Autowired
    private VoiceMailMessageService voiceMailMessageService;

    @Autowired
    private AccessControl accessControl;

    /**
     * GET  /extensions/:id/voicemails/messages -> get the messages for "id".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<VoiceMailMessageDTO> getAllVoiceMailMessageInfo(@PathVariable Long id) {
        log.debug("REST request to get all message: {}", id);
        User user = accessControl.findCurrentLoggedInUser();
        accessControl.hasVoiceMailPermission(id);
        return voiceMailMessageService.getAllVoiceMailMessageInfo(id)
            .stream()
            .map(axVoiceMessage -> new VoiceMailMessageDTO(axVoiceMessage, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /extensions/:id/voicemails/messages/:dir -> get the messages for "dir".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages/{dir}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<VoiceMailMessageDTO> getAllVoiceMailMessageInfo(@PathVariable Long id, @PathVariable String dir) {
        log.debug("REST request to get all message: {}, {}", id, dir);
        User user = accessControl.findCurrentLoggedInUser();
        accessControl.hasVoiceMailPermission(id);
        return voiceMailMessageService.getAllVoiceMailMessageInfo(id, dir)
            .stream()
            .map(axVoiceMessage -> new VoiceMailMessageDTO(axVoiceMessage, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /extensions/:id/voicemails/messages/:msgId -> get the messages for "msgId".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages/{msgId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoiceMailMessageDTO> getVoiceMailMessageInfo(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to get message: {}, {}", id, msgId);
        User user = accessControl.findCurrentLoggedInUser();
        accessControl.hasVoiceMailPermission(id);
        AXVoiceMessage result = voiceMailMessageService.getVoiceMailMessageInfo(id, msgId);
        return VoiceMailMessageDTO.getDTO(result, user.getZoneId())
            .map(voiceMailMessageDTO -> new ResponseEntity<>(voiceMailMessageDTO, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /extensions/:id/voicemails/messages/:msgId/download -> download the messages for "msgId".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages/{msgId}/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> downloadVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to get message: {}, {}", id, msgId);
        accessControl.hasVoiceMailPermission(id);
        AXVoiceMessage result = voiceMailMessageService.getVoiceMailMessageInfo(id, msgId);
        String filename = String.format("message_%s.%s", msgId, "wav");
        return MiscUtils.getDownload(result.getRecording(), filename, MediaType.parseMediaType("audio/wave"));
    }

    /**
     * DELETE  /extensions/:id/voicemails/messages/:msgId -> delete the messages for "msgId".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages/{msgId}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to delete message: {}, {}", id, msgId);
        accessControl.hasVoiceMailPermission(id);
        voiceMailMessageService.deleteVoiceMailMessageInfo(id, msgId);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /extensions/:id/voicemails/messages/:msgId/callback -> callback the caller of the messages for "msgId".
     */
    @RequestMapping(value = "/extensions/{id}/voicemails/messages/{msgId}/callback",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Void> callbackVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to get message: {}, {}", id, msgId);
        accessControl.hasVoiceMailPermission(id);
        voiceMailMessageService.callbackVoiceMailMessage(id, msgId);
        return ResponseEntity.ok().build();
    }
}
