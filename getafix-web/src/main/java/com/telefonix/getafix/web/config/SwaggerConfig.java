package com.telefonix.getafix.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.StopWatch;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.Date;

/**
 * Created by tmyousuf on 7/8/18.
 */
@Slf4j
@Configuration
@EnableSwagger2
@Profile("!" + Constants.SPRING_PROFILE_PRODUCTION)
public class SwaggerConfig {

    @Value(value = "${getafix.swagger.title}")
    private String title;

    @Value(value = "${getafix.swagger.description}")
    private String description;

    @Value(value = "${getafix.swagger.version}")
    private String version;

    @Value(value = "${getafix.developer.email}")
    private String email;

    @Value(value = "${getafix.developer.fullName}")
    private String fullName;

    @Value(value = "${getafix.developer.web}")
    private String web;

    @Bean
    public Docket api() {
        StopWatch watch = new StopWatch();
        watch.start();
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .directModelSubstitute(java.time.LocalDate.class, String.class)
            .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
            .directModelSubstitute(java.time.LocalDateTime.class, Date.class)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.telefonix.getafix.web.rest"))
            .paths(PathSelectors.ant("/api/*"))
            .build();
        watch.stop();
        log.debug("Started Swagger in {} ms", watch.getTotalTimeMillis());
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(title, description, version, null,
            new Contact(fullName, web, email),
            null, null, Collections.emptyList());
    }
}
