package com.telefonix.getafix.web.security;

import com.telefonix.getafix.entity.*;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.repository.UserRepository;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

/**
 * Created by tareqmy on 2019-01-17.
 * TODO: needs major re-evaluation
 */
@Slf4j
@Component
public class AccessControl {

    @Autowired
    private UserRepository userRepository;

    /**
     * actions that are allowed by admin to his company only
     *
     * @param companyId
     */
    public void hasCompanyPermission(Long companyId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(companyId)) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }

        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            throw new RuntimeException("You are not authorized for this action for this entity");
        }
    }

    public void hasPhonePermission(Long phoneId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (currentLoggedInUser.getCompany().getPhones().stream().noneMatch(phone -> phone.getId().equals(phoneId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }

        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            if (currentLoggedInUser.getExtensions().stream()
                .filter(extension -> extension.getPhoneLine() != null)
                .noneMatch(extension -> extension.getPhoneLine().getPhone().getId().equals(phoneId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasPhoneLinePermission(Long phoneLineId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (currentLoggedInUser.getCompany().getPhones()
                .stream()
                .noneMatch(phone -> phone.getPhoneLines()
                    .stream()
                    .map(PhoneLine::getId)
                    .anyMatch(aLong -> aLong.equals(phoneLineId)))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }

        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            if (currentLoggedInUser.getExtensions()
                .stream()
                .map(Extension::getPhoneLine)
                .map(PhoneLine::getId)
                .noneMatch(aLong -> aLong.equals(phoneLineId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasUserPermission(Long userId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (currentLoggedInUser.getCompany().getUsers().stream().noneMatch(user -> user.getId().equals(userId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }

        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            if (!currentLoggedInUser.getId().equals(userId)) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasExtensionPermission(Long extensionId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (currentLoggedInUser.getCompany().getExtensions().stream().noneMatch(extension -> extension.getId().equals(extensionId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }

        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            if (currentLoggedInUser.getExtensions().stream().noneMatch(extension -> extension.getId().equals(extensionId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasVoiceMailPermission(Long extensionId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getExtensions().stream().noneMatch(extension -> extension.getId().equals(extensionId))) {
            throw new RuntimeException("You are not authorized for this action for this entity");
        }
    }

    public void hasCompanyPermission(Company company) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(company.getId())) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        } else if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            throw new RuntimeException("You are not authorized for this action for this entity");
        }
    }

    public void hasCompanyPermission(Extension extension) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(extension.getCompany().getId())) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        } else if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            User user = extension.getUser();
            if (user != null && currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)
                && !user.equals(currentLoggedInUser)) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasCompanyPermission(Phone phone) {
        hasCompanyPermission(phone.getCompany());
    }

    public void hasCompanyPermission(PhoneLine phoneLine) {
        hasCompanyPermission(phoneLine.getPhone());
    }

    public void hasCompanyPermission(Queue queue) {
        hasCompanyPermission(queue.getCompany());
    }

    public void hasCompanyPermission(AutoAttendant autoAttendant) {
        hasCompanyPermission(autoAttendant.getCompany());
    }

    public User findCurrentLoggedInUser() {
        return userRepository.findOneByEmail(SecurityUtils.getCurrentUserName())
            .orElseThrow(userNotFound());
    }

    private Supplier<RuntimeException> userNotFound() {
        return () -> new RuntimeException("Current Logged user may not be active or found!");
    }

    public void hasCompanyPermission(User user) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(user.getCompany().getId())) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }

            if (user.getUserRole().getRole().equals(Role.ROLE_SYSTEM_ADMIN)
                || user.getUserRole().getRole().equals(Role.ROLE_GETAFIX_ADMIN)) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasCompanyPermission(PhoneNumber phoneNumber) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(phoneNumber.getCompany().getId())) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasCompanyPermission(Conference conference) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)) {
            if (!currentLoggedInUser.getCompany().getId().equals(conference.getCompany().getId())) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }

    public void hasQueuePermission(Long queueId) {
        User currentLoggedInUser = findCurrentLoggedInUser();
        if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_ADMIN)
            || currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
            if (currentLoggedInUser.getCompany().getQueues().stream().noneMatch(queue -> queue.getId().equals(queueId))) {
                throw new RuntimeException("You are not authorized for this action for this entity");
            }
        }
    }
}
