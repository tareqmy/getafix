package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.ExtensionService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.ChannelLogDTO;
import com.telefonix.getfix.dto.ChannelLogsSummaryDTO;
import com.telefonix.getfix.dto.ExtensionDTO;
import com.telefonix.getfix.dto.PhoneLineDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-01-15.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ExtensionResource {

    @Autowired
    private ExtensionService extensionService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /extensions -> Create a new extension.
     */
    @RequestMapping(value = "/extensions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<ExtensionDTO> createExtension(@Valid @RequestBody ExtensionDTO extensionDTO) {
        log.debug("REST request to save Extension : {}", extensionDTO);
        if (extensionDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Extension result = extensionService.createExtension(extensionDTO);
        return ExtensionDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /extensions -> Updates an existing extension.
     */
    @RequestMapping(value = "/extensions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<ExtensionDTO> updateExtension(@Valid @RequestBody ExtensionDTO extensionDTO) {
        log.debug("REST request to update Extension : {}", extensionDTO);
        if (extensionDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(extensionDTO.getCompanyId());
        Extension result = extensionService.updateExtension(extensionDTO);
        return ExtensionDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /extensions -> get all the extensions.
     */
    @RequestMapping(value = "/extensions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<ExtensionDTO> getAllExtensions() {
        log.debug("REST request to get all Extensions");
        return extensionService.findAll()
            .stream()
            .map(ExtensionDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /extensions/:id -> get the "id" extension.
     */
    @RequestMapping(value = "/extensions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExtensionDTO> getExtension(@PathVariable Long id) {
        log.debug("REST request to get Extension : {}", id);
        return extensionService.findOne(id).map(extension -> {
            accessControl.hasCompanyPermission(extension);
            return new ResponseEntity<>(new ExtensionDTO(extension), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /extensions/:id -> delete the "id" extension.
     */
    @RequestMapping(value = "/extensions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteExtension(@PathVariable Long id) {
        log.debug("REST request to delete Extension: {}", id);
        extensionService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /extensions/:id/channellogs -> get channellogs for the extension
     */
    @RequestMapping(value = "/extensions/{id}/channellogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ChannelLogDTO> getChannelLogs(@PathVariable Long id,
                                              @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                              @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get channellogs for extension : {}", id);
        User user = accessControl.findCurrentLoggedInUser();
        return extensionService.getChannelLogs(id, from, to, user.getZoneId())
            .stream()
            .map(channelLog -> new ChannelLogDTO(channelLog, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /extensions/:id/channellogs/summary -> get channellogs summary for the extension
     */
    @RequestMapping(value = "/extensions/{id}/channellogs/summary",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChannelLogsSummaryDTO> getChannelLogsSummary(@PathVariable Long id,
                                                                       @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                                                       @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get channellogs summary for extension : {}", id);
        Extension extension = extensionService.getExtension(id);
        User user = accessControl.findCurrentLoggedInUser();
        List<ChannelLog> channelLogs = extensionService.getChannelLogs(id, from, to, user.getZoneId());
        return new ResponseEntity<>(new ChannelLogsSummaryDTO(extension, channelLogs, from, to), HttpStatus.OK);
    }

    /**
     * GET  /extensions/:id/phoneline -> get phonelines for the extension
     */
    @RequestMapping(value = "/extensions/{id}/phoneline",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<PhoneLineDTO> getPhoneLine(@PathVariable Long id) {
        log.debug("REST request to get Extension phonelines : {}", id);
        accessControl.hasExtensionPermission(id);
        return extensionService.getPhoneLine(id)
            .map(phoneLine -> new ResponseEntity<>(new PhoneLineDTO(phoneLine), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
