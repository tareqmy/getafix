package com.telefonix.getafix.web.config;

import com.telefonix.getafix.service.GetafixProperties;
import com.telefonix.getafix.web.async.ExceptionHandlingAsyncTaskExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StopWatch;

import java.util.concurrent.Executor;

/**
 * Created by tmyousuf on 16/9/18.
 */
@Slf4j
@Configuration
@EnableAsync(proxyTargetClass = true)
public class AsyncConfig implements AsyncConfigurer {

    @Autowired
    private GetafixProperties getafixProperties;

    @Bean
    public ThreadPoolTaskExecutor applicationTaskExecutor(TaskExecutorBuilder builder) {
        return builder.build();
    }

    @Override
    @Bean(name = "asyncExecutor")
    public Executor getAsyncExecutor() { //used by async only?
        log.debug("Creating Async Task Executor");
        StopWatch watch = new StopWatch();
        watch.start();

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(getafixProperties.getAsync().getCorePoolSize());
        executor.setMaxPoolSize(getafixProperties.getAsync().getMaxPoolSize());
        executor.setQueueCapacity(getafixProperties.getAsync().getQueueCapacity());
        executor.setThreadNamePrefix(getafixProperties.getAsync().getName());

        watch.stop();
        log.debug("Started async in {} ms", watch.getTotalTimeMillis());
        return new ExceptionHandlingAsyncTaskExecutor(executor);
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }

    @Bean(name = "asteriskTaskExecutor")
    public Executor getAsteriskTaskExecutor() { //used by asterisk
        log.debug("Creating Async asterisk Task Executor");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(getafixProperties.getAsync().getAsterisk().getCorePoolSize());
        executor.setMaxPoolSize(getafixProperties.getAsync().getAsterisk().getMaxPoolSize());
        executor.setQueueCapacity(getafixProperties.getAsync().getAsterisk().getQueueCapacity());
        executor.setThreadNamePrefix(getafixProperties.getAsync().getAsterisk().getName());
        return new ExceptionHandlingAsyncTaskExecutor(executor);
    }
}
