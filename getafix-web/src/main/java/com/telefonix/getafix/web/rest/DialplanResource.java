package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.service.asterisk.GetafixDialplanService;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tareqmy on 2019-02-11.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class DialplanResource {

    @Autowired
    private GetafixDialplanService getafixDialplanService;

    /**
     * PUT  /dialplan/rebuild -> rebuild all dialplans
     */
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN})
    @RequestMapping(value = "/dialplan/rebuild",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> rebuildAllDialplan() {
        log.debug("REST request to get Rebuild all company dialplans");
        getafixDialplanService.rebuildDialplans();
        return ResponseEntity.ok().build();
    }
}
