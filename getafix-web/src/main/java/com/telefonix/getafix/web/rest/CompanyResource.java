package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.service.CompanyService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.*;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import com.telefonix.getfix.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 3/1/19.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class CompanyResource {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /companies -> Create a new company.
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<CompanyDTO> createCompany(@Valid @RequestBody CompanyDTO companyDTO) {
        log.debug("REST request to save Company : {}", companyDTO);
        if (companyDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Company result = companyService.createCompany(companyDTO);
        return CompanyDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /companies -> Updates an existing company.
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<CompanyDTO> updateCompany(@Valid @RequestBody CompanyDTO companyDTO) {
        log.debug("REST request to update Company : {}", companyDTO);
        if (companyDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Company result = companyService.updateCompany(companyDTO);
        return CompanyDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /companies -> get all the companies.
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<CompanyDTO> getAllCompanies() {
        log.debug("REST request to get all Companies");
        return companyService.findAll()
            .stream()
            .map(CompanyDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /companies/pageable -> get all the companies.
     */
    @RequestMapping(value = "/companies/pageable",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public Page<CompanyDTO> getAllCompanies(Pageable pageable) {
        log.debug("REST request to get all companies");
        return companyService.findAll(pageable)
            .map(CompanyDTO::new);
    }

    /**
     * GET /companies/:id -> get the "id" company.
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<CompanyDTO> getCompany(@PathVariable Long id) {
        log.debug("REST request to get Company : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.findOne(id)
            .map(company -> new ResponseEntity<>(new CompanyDTO(company), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /companies/:id -> delete the "id" company.
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteCompany(@PathVariable Long id) {
        log.debug("REST request to delete Company: {}", id);
        companyService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /companies/:id/users -> get users for the company
     */
    @RequestMapping(value = "/companies/{id}/users",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public List<UserDTO> getUsers(@PathVariable Long id) {
        log.debug("REST request to get Company Users : {}", id);
        boolean admin = SecurityUtils.isAdmin();
        User currentLoggedInUser = accessControl.findCurrentLoggedInUser();
        return companyService.getUsers(id)
            .stream()
            .filter(user -> !admin || (!user.getUserRole().getRole().equals(Role.ROLE_SYSTEM_ADMIN)
                && !user.getUserRole().getRole().equals(Role.ROLE_GETAFIX_ADMIN)))
            .filter(user -> {
                if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
                    return currentLoggedInUser.equals(user);
                }
                return true;
            })
            .map(UserDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/extensions -> get extensions for the company
     */
    @RequestMapping(value = "/companies/{id}/extensions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<ExtensionDTO> getExtensions(@PathVariable Long id) {
        log.debug("REST request to get Company extensions : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getExtensions(id, false)
            .stream()
            .map(ExtensionDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/extensions/unassigned -> get unassignedextensions for the company
     */
    @RequestMapping(value = "/companies/{id}/extensions/unassigned",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<ExtensionDTO> getUnassignedExtensions(@PathVariable Long id) {
        log.debug("REST request to get Company unassigned extensions : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getExtensions(id, true)
            .stream()
            .map(ExtensionDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/phones -> get phones for the company
     */
    @RequestMapping(value = "/companies/{id}/phones",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<PhoneDTO> getPhones(@PathVariable Long id) {
        log.debug("REST request to get Company phones : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getPhones(id)
            .stream()
            .map(PhoneDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/phonenumbers -> get phonenumbers for the company
     */
    @RequestMapping(value = "/companies/{id}/phonenumbers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<PhoneNumberDTO> getPhoneNumbers(@PathVariable Long id) {
        log.debug("REST request to get Company phonenumbers : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getPhoneNumbers(id)
            .stream()
            .map(PhoneNumberDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/queues -> get queues for the company
     */
    @RequestMapping(value = "/companies/{id}/queues",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<QueueDTO> getQueues(@PathVariable Long id) {
        log.debug("REST request to get Company queues : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getQueues(id)
            .stream()
            .map(QueueDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/autoattendants -> get autoattendants for the company
     */
    @RequestMapping(value = "/companies/{id}/autoattendants",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<AutoAttendantDTO> getAutoAttendants(@PathVariable Long id) {
        log.debug("REST request to get Company autoattendants : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getAutoAttendants(id)
            .stream()
            .map(AutoAttendantDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/conferences -> get conferences for the company
     */
    @RequestMapping(value = "/companies/{id}/conferences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<ConferenceDTO> getConferences(@PathVariable Long id) {
        log.debug("REST request to get Company conferences : {}", id);
        accessControl.hasCompanyPermission(id);
        return companyService.getConferences(id)
            .stream()
            .map(ConferenceDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /companies/:id/companyreports -> get companyreports for the company
     */
    @RequestMapping(value = "/companies/{id}/companyreports",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public CompanyReportDTO getCompanyReport(@PathVariable Long id,
                                             @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                             @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get companyreports for company : {}", id);
        accessControl.hasCompanyPermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return companyService.getCompanyReport(id, from, to, user.getZoneId());
    }

    /**
     * GET  /companies/companyreports -> get companyreports for the company
     */
    @RequestMapping(value = "/companies/companyreports",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public CompanyReportDTO getCompanyReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                             @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get companyreports for company");
        User user = accessControl.findCurrentLoggedInUser();
        return companyService.getCompanyReport(user.getCompany().getId(), from, to, user.getZoneId());
    }
}
