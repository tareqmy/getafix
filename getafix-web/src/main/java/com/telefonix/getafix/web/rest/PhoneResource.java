package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.entity.enums.Role;
import com.telefonix.getafix.service.PhoneService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.ExtensionDTO;
import com.telefonix.getfix.dto.PhoneDTO;
import com.telefonix.getfix.dto.PhoneLineDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-03-27.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class PhoneResource {

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /phones -> Create a new phone.
     */
    @RequestMapping(value = "/phones",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<PhoneDTO> createPhone(@Valid @RequestBody PhoneDTO phoneDTO) {
        log.debug("REST request to save Phone : {}", phoneDTO);
        if (phoneDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Phone result = phoneService.createPhone(phoneDTO);
        return PhoneDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /phones -> Updates an existing phone.
     */
    @RequestMapping(value = "/phones",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<PhoneDTO> updatePhone(@Valid @RequestBody PhoneDTO phoneDTO) {
        log.debug("REST request to update Phone : {}", phoneDTO);
        if (phoneDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(phoneDTO.getCompanyId());
        Phone result = phoneService.updatePhone(phoneDTO);
        return PhoneDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /phones -> get all the phones.
     */
    @RequestMapping(value = "/phones",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<PhoneDTO> getAllPhones() {
        log.debug("REST request to get all Phones");
        return phoneService.findAll()
            .stream()
            .map(PhoneDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /phones/:id -> get the "id" phone.
     */
    @RequestMapping(value = "/phones/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<PhoneDTO> getPhone(@PathVariable Long id) {
        log.debug("REST request to get Phone : {}", id);
        accessControl.hasPhonePermission(id);
        return phoneService.findOne(id).map(phone -> {
            return new ResponseEntity<>(new PhoneDTO(phone), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /phones/:id -> delete the "id" phone.
     */
    @RequestMapping(value = "/phones/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deletePhone(@PathVariable Long id) {
        log.debug("REST request to delete Phone: {}", id);
        phoneService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /phones/:id/phonelines -> get phonelines for the phone
     */
    @RequestMapping(value = "/phones/{id}/phonelines",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public List<PhoneLineDTO> getPhoneLines(@PathVariable Long id) {
        log.debug("REST request to get Phone phonelines : {}", id);
        accessControl.hasPhonePermission(id);
        User currentLoggedInUser = accessControl.findCurrentLoggedInUser();
        return phoneService.getPhoneLines(id)
            .stream()
            .filter(phoneLine -> {
                if (currentLoggedInUser.getUserRole().getRole().equals(Role.ROLE_USER)) {
                    return currentLoggedInUser.equals(phoneLine.getExtension().getUser());
                }
                return true;
            })
            .map(PhoneLineDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET  /phones/:id/extensions -> get assignable extensions for the phone
     */
    @RequestMapping(value = "/phones/{id}/extensions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN,
        AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public List<ExtensionDTO> getExtensions(@PathVariable Long id) {
        log.debug("REST request to get Company extensions for phone : {}", id);
        accessControl.hasPhonePermission(id);
        return phoneService.getExtensions(id)
            .stream()
            .map(ExtensionDTO::new)
            .collect(Collectors.toList());
    }
}
