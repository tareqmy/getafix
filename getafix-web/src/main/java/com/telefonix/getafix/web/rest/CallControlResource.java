package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.service.CallControlService;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tareqmy on 2019-07-11.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class CallControlResource {

    @Autowired
    private CallControlService callControlService;

    /**
     * PUT  /calls/transfer/:channelId/:userId -> transfer "channel" to another agent
     */
    @RequestMapping(value = "/calls/transfer/{channelId}/{userId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> transfer(@PathVariable String channelId, @PathVariable Long userId) {
        log.debug("REST request to transfer the call :{} {}", channelId, userId);
        callControlService.transfer(channelId, userId);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /calls/escalate/:channelId/:userId -> escalate "channel"
     */
    @RequestMapping(value = "/calls/escalate/{channelId}/{userId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> escalate(@PathVariable String channelId, @PathVariable Long userId) {
        log.debug("REST request to escalate the call :{} {}", channelId, userId);
        callControlService.escalate(channelId, userId);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /calls/m2q/:channelId/:queueId -> m2q "channel"
     */
    @RequestMapping(value = "/calls/m2q/{channelId}/{queueId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> m2q(@PathVariable String channelId, @PathVariable Long queueId) {
        log.debug("REST request to m2q the call :{} {}", channelId, queueId);
        callControlService.moveToQueue(channelId, queueId);
        return ResponseEntity.ok().build();
    }

    /**
     * DELETE  /calls/hangup/:channelId -> hangup "channel" call
     */
    @RequestMapping(value = "/calls/hangup/{channelId}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> hangupCall(@PathVariable String channelId) {
        log.debug("REST request to hangup channel :{}", channelId);
        callControlService.hangup(channelId);
        return ResponseEntity.ok().build();
    }
}
