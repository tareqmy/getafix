package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.VoiceMail;
import com.telefonix.getafix.service.VoiceMailService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.VoiceMailDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by tareqmy on 2019-01-23.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class VoiceMailResource {

    @Autowired
    private VoiceMailService voiceMailService;

    @Autowired
    private AccessControl accessControl;

    /**
     * PUT  /voicemails -> Updates an existing voicemail.
     */
    @RequestMapping(value = "/voicemails",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoiceMailDTO> updateVoiceMail(@Valid @RequestBody VoiceMailDTO voiceMailDTO) {
        log.debug("REST request to update voicemail : {}", voiceMailDTO);
        VoiceMail voiceMail = voiceMailService.getVoiceMail(voiceMailDTO.getId());
        accessControl.hasCompanyPermission(voiceMail.getExtension());
        voiceMail = voiceMailService.updateVoiceMail(voiceMailDTO);
        return VoiceMailDTO.getDTO(voiceMail)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET  /voicemails/:id -> get the "id" voicemail.
     */
    @RequestMapping(value = "/voicemails/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoiceMailDTO> getVoiceMail(@PathVariable Long id) {
        log.debug("REST request to get voicemail : {}", id);
        return voiceMailService.findOne(id)
            .map(voiceMail -> {
                accessControl.hasCompanyPermission(voiceMail.getExtension());
                return new ResponseEntity<>(new VoiceMailDTO(voiceMail), HttpStatus.OK);
            })
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * PUT  /voicemails/:id/change/:pin -> change the voicemail pin
     */
    @RequestMapping(value = "/voicemails/{id}/change/{pin}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> changePin(@PathVariable Long id, @PathVariable String pin) {
        log.debug("REST request to change voicemail pin : {}", id);
        VoiceMail voiceMail = voiceMailService.getVoiceMail(id);
        accessControl.hasCompanyPermission(voiceMail.getExtension());
        voiceMailService.changePin(voiceMail, pin);
        return ResponseEntity.ok().build();
    }
}
