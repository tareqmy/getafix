package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.asterisk.service.live.ChannelLiveRepository;
import com.telefonix.getafix.service.live.AgentLiveRepository;
import com.telefonix.getafix.service.live.CallCenterLiveRepository;
import com.telefonix.getafix.service.live.QueueLiveRepository;
import com.telefonix.getafix.service.live.VoiceMailLiveRepository;
import com.telefonix.getafix.service.live.dto.*;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 22/10/19.
 */
@RestController
@RequestMapping("/api")
public class LiveResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CallCenterLiveRepository callCenterLiveRepository;

    @Autowired
    private QueueLiveRepository queueLiveRepository;

    @Autowired
    private AgentLiveRepository agentLiveRepository;

    @Autowired
    private ChannelLiveRepository channelLiveRepository;

    @Autowired
    private VoiceMailLiveRepository voiceMailLiveRepository;

    /**
     * GET  /live/callcenters -> get all live call center
     */
    @RequestMapping(value = "/live/callcenters",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<CallCenterLiveDTO> getLiveCallCenters() {
        log.debug("REST request to get all live callcenters");
        return callCenterLiveRepository.getCallCenterLiveMapValues()
            .stream()
            .map(contactCenterLive -> new CallCenterLiveDTO(contactCenterLive, ZoneId.systemDefault()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /live/queues -> get all live queues
     */
    @RequestMapping(value = "/live/queues",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<QueueLiveDTO> getLiveQueues() {
        log.debug("REST request to get all live queues");
        return queueLiveRepository.getQueueLiveMapValues()
            .stream()
            .map(queueLive -> new QueueLiveDTO(queueLive, ZoneId.systemDefault(), true))
            .collect(Collectors.toList());
    }

    /**
     * GET  /live/agents -> get all live agents
     */
    @RequestMapping(value = "/live/agents",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<AgentLiveDTO> getLiveAgents() {
        log.debug("REST request to get all live agents");
        return agentLiveRepository.getAgentLiveMapValues()
            .stream()
            .map(agentLive -> new AgentLiveDTO(agentLive, ZoneId.systemDefault()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /live/voicemails -> get all live voicemails
     */
    @RequestMapping(value = "/live/voicemails",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<VoiceMailLiveDTO> getVoiceMails() {
        log.debug("REST request to get all live voicemails");
        return voiceMailLiveRepository.getVoiceMailLiveMapValues()
            .stream()
            .map(voiceMailLive -> new VoiceMailLiveDTO(voiceMailLive, ZoneId.systemDefault()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /live/channels -> get all live channels
     */
    @RequestMapping(value = "/live/channels",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<ChannelLiveDTO> getLiveChannels() {
        log.debug("REST request to get all live channels");
        return channelLiveRepository.getChannelLiveMapValues()
            .stream()
            .map(agentLive -> new ChannelLiveDTO(agentLive, ZoneId.systemDefault()))
            .collect(Collectors.toList());
    }
}
