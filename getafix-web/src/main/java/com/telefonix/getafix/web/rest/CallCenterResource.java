package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.service.CallCenterService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by tareqmy on 2019-07-14.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class CallCenterResource {

    @Autowired
    private CallCenterService callCenterService;

    @Autowired
    private AccessControl accessControl;

    /**
     * PUT  /callcenters/agents/sipusername -> get agent sipusername if joined else null
     */
    @RequestMapping(value = "/callcenters/agents/sipusername",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<String> getAgentStatus() {
        log.debug("REST request to get sipusername for current user");
        return ResponseEntity.ok().body(callCenterService.getAgentSipUserName());
    }

    /**
     * PUT  /callcenters/join/:phoneLineId -> join callcenter as agent with "phoneLineId".
     */
    @RequestMapping(value = "/callcenters/join/{phoneLineId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> join(@PathVariable Long phoneLineId) {
        log.debug("REST request to join callcenter with: {}", phoneLineId);
        callCenterService.join(phoneLineId);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/leave -> leave from callcenter.
     */
    @RequestMapping(value = "/callcenters/leave",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> leave() {
        log.debug("REST request to leave from callcenter");
        callCenterService.leave();
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/pause -> pause from callcenter.
     */
    @RequestMapping(value = "/callcenters/pause",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> pause() {
        log.debug("REST request to pause from callcenter");
        callCenterService.pause();
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/resume -> resume from callcenter.
     */
    @RequestMapping(value = "/callcenters/resume",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> resume() {
        log.debug("REST request to resume from callcenter");
        callCenterService.resume();
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /callcenters/agents -> get agents map for callcenter.
     */
    @RequestMapping(value = "/callcenters/agents",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public Map<Long, String> getAgentsMap() {
        return callCenterService.getAgentsMap();
    }

    /**
     * GET  /callcenters/supervisors -> get supervisors map for callcenter.
     */
    @RequestMapping(value = "/callcenters/supervisors",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public Map<Long, String> getSupervisorsMap() {
        return callCenterService.getSupervisorsMap();
    }

    /**
     * GET  /callcenters/queues-> get queue map for callcenter.
     */
    @RequestMapping(value = "/callcenters/queues",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public Map<Long, String> getQueuesMap() {
        return callCenterService.getQueuesMap();
    }

    /**
     * PUT  /callcenters/queues/:id/join -> join the "id" queue.
     */
    @RequestMapping(value = "/callcenters/queues/{id}/join",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> joinQueue(@PathVariable Long id) {
        log.debug("REST request to join Queue: {}", id);
        accessControl.hasQueuePermission(id);
        callCenterService.joinQueue(id);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/queues/:id/leave -> leave the "id" queue.
     */
    @RequestMapping(value = "/callcenters/queues/{id}/leave",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> leaveQueue(@PathVariable Long id) {
        log.debug("REST request to leave Queue: {}", id);
        accessControl.hasQueuePermission(id);
        callCenterService.leaveQueue(id);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/queues/:id/call/:number -> call "number" from the "id" queue.
     */
    @RequestMapping(value = "/callcenters/queues/{id}/call/{number}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Void> call(@PathVariable Long id, @PathVariable String number) {
        log.debug("REST request to call {} from Queue: {}", number, id);
        accessControl.hasQueuePermission(id);
        callCenterService.callQueue(id, number);
        return ResponseEntity.ok().build();
    }

    /**
     * PUT  /callcenters/queues/:id/kickout/:phoneLineId -> kickout phoneLineId from the "id" queue.
     */
    @RequestMapping(value = "/callcenters/queues/{id}/kickout/{phoneLineId}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Void> kickOutAgentQueue(@PathVariable Long id, @PathVariable Long phoneLineId) {
        log.debug("REST request to kickout Queue: {} {}", id, phoneLineId);
        accessControl.hasQueuePermission(id);
        callCenterService.kickOutAgentQueue(id, phoneLineId);
        return ResponseEntity.ok().build();
    }

    /**
     * GET  /callcenters/queues/:id/voicemails/messages/:msgId/download -> download the messages for "msgId".
     */
    @RequestMapping(value = "/callcenters/queues/{id}/voicemails/messages/{msgId}/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<byte[]> downloadVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to get message: {}, {}", id, msgId);
        accessControl.hasQueuePermission(id);
        AXVoiceMessage result = callCenterService.getVoiceMailMessageInfo(id, msgId);
        String filename = String.format("message_%s.%s", msgId, "wav");
        return MiscUtils.getDownload(result.getRecording(), filename, MediaType.parseMediaType("audio/wave"));
    }

    /**
     * PUT  /callcenters/queues/:id/voicemails/messages/:msgId/callback -> callback the caller of the messages for "msgId".
     */
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    @RequestMapping(value = "/callcenters/queues/{id}/voicemails/messages/{msgId}/callback",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Void> callbackVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to get message: {}, {}", id, msgId);
        accessControl.hasQueuePermission(id);
        callCenterService.callbackVoiceMailMessage(id, msgId);
        return ResponseEntity.ok().build();
    }

    /**
     * DELETE  /callcenters/queues/:id/voicemails/messages/:msgId -> callback the caller of the messages for "msgId".
     */
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    @RequestMapping(value = "/callcenters/queues/{id}/voicemails/messages/{msgId}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Void> deleteVoiceMailMessage(@PathVariable Long id, @PathVariable String msgId) {
        log.debug("REST request to delete message: {}, {}", id, msgId);
        accessControl.hasQueuePermission(id);
        callCenterService.deleteVoiceMailMessage(id, msgId);
        return ResponseEntity.ok().build();
    }
}
