package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.PhoneNumber;
import com.telefonix.getafix.service.PhoneNumberService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.PhoneNumberDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class PhoneNumberResource {

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /phonenumbers -> Create a new phoneNumber.
     */
    @RequestMapping(value = "/phonenumbers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<PhoneNumberDTO> createPhoneNumber(@Valid @RequestBody PhoneNumberDTO phoneNumberDTO) {
        log.debug("REST request to save PhoneNumber : {}", phoneNumberDTO);
        if (phoneNumberDTO.getNumber() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        PhoneNumber result = phoneNumberService.createPhoneNumber(phoneNumberDTO);
        return PhoneNumberDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /phonenumbers -> Updates an existing phoneNumber.
     */
    @RequestMapping(value = "/phonenumbers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<PhoneNumberDTO> updatePhoneNumber(@Valid @RequestBody PhoneNumberDTO phoneNumberDTO) {
        log.debug("REST request to update PhoneNumber : {}", phoneNumberDTO);
        if (phoneNumberDTO.getNumber() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(phoneNumberDTO.getCompanyId());
        PhoneNumber result = phoneNumberService.updatePhoneNumber(phoneNumberDTO);
        return PhoneNumberDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /phonenumbers -> get all the phonenumbers.
     */
    @RequestMapping(value = "/phonenumbers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<PhoneNumberDTO> getAllPhoneNumbers() {
        log.debug("REST request to get all PhoneNumbers");
        return phoneNumberService.findAll()
            .stream()
            .map(PhoneNumberDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /phonenumbers/:id -> get the "id" phoneNumber.
     */
    @RequestMapping(value = "/phonenumbers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PhoneNumberDTO> getPhoneNumber(@PathVariable String id) {
        log.debug("REST request to get PhoneNumber : {}", id);
        return phoneNumberService.findOne(id).map(phoneNumber -> {
            accessControl.hasCompanyPermission(phoneNumber);
            return new ResponseEntity<>(new PhoneNumberDTO(phoneNumber), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /phonenumbers/:id -> delete the "id" phoneNumber.
     */
    @RequestMapping(value = "/phonenumbers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deletePhoneNumber(@PathVariable String id) {
        log.debug("REST request to delete PhoneNumber: {}", id);
        phoneNumberService.delete(id);
        return ResponseEntity.ok().build();
    }
}
