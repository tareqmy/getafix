package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.Conference;
import com.telefonix.getafix.service.ConferenceService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.ConferenceDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ConferenceResource {

    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /conferences -> Create a new conference.
     */
    @RequestMapping(value = "/conferences",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<ConferenceDTO> createConference(@Valid @RequestBody ConferenceDTO conferenceDTO) {
        log.debug("REST request to save Conference : {}", conferenceDTO);
        if (conferenceDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Conference result = conferenceService.createConference(conferenceDTO);
        return ConferenceDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /conferences -> Updates an existing conference.
     */
    @RequestMapping(value = "/conferences",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<ConferenceDTO> updateConference(@Valid @RequestBody ConferenceDTO conferenceDTO) {
        log.debug("REST request to update Conference : {}", conferenceDTO);
        if (conferenceDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(conferenceDTO.getCompanyId());
        Conference result = conferenceService.updateConference(conferenceDTO);
        return ConferenceDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /conferences -> get all the conferences.
     */
    @RequestMapping(value = "/conferences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<ConferenceDTO> getAllConferences() {
        log.debug("REST request to get all Conferences");
        return conferenceService.findAll()
            .stream()
            .map(ConferenceDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /conferences/:id -> get the "id" conference.
     */
    @RequestMapping(value = "/conferences/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<ConferenceDTO> getConference(@PathVariable Long id) {
        log.debug("REST request to get Conference : {}", id);
        return conferenceService.findOne(id).map(conference -> {
            accessControl.hasCompanyPermission(conference);
            return new ResponseEntity<>(new ConferenceDTO(conference), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /conferences/:id -> delete the "id" conference.
     */
    @RequestMapping(value = "/conferences/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteConference(@PathVariable Long id) {
        log.debug("REST request to delete Conference: {}", id);
        conferenceService.delete(id);
        return ResponseEntity.ok().build();
    }
}

