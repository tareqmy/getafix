package com.telefonix.getafix.web.config;

import com.telefonix.getafix.web.security.JWTFilter;
import com.telefonix.getafix.web.security.JWTProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig {

    @Value("#{'${getafix.endpoints.cors.allowed-origins}'.split(',')}")
    public List<String> allowedOrigins;

    private static final String[] AUTH_WHITELIST = {
        // -- swagger ui
        "/v2/api-docs",
        "/swagger-resources",
        "/swagger-resources/**",
        // other public endpoints of your API may be appended to this array
    };

    @Autowired
    private UserDetailsService getafixUserDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JWTProvider jwtProvider;

    @Configuration
    @Order(1)
    public class BasicAuthWithJWTConfig extends WebSecurityConfigurerAdapter {

        //NOTE: for details on headers see https://docs.spring.io/spring-security/site/docs/current/reference/html/headers.html
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            //support cors with jwt/httpbasic
            http.cors();

            //if not disabled it sends set-cookie -> JSESSIONID=... in authenticate POST
            http.csrf().disable();

            http.exceptionHandling().authenticationEntryPoint((request, response, authException) ->
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not authorized!"));

            http.authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers(HttpMethod.POST, "/api/account/authenticate").permitAll()
                .antMatchers("/api/users/password/reset/init").permitAll()
                .antMatchers("/api/users/password/reset/finish").permitAll()
                .antMatchers("/api/utils/zoneids").permitAll()
                .antMatchers("/api/utils/zonesmap").permitAll()
                .antMatchers("/api/utils/rolesmap").permitAll()
                .antMatchers("/api/application/info").permitAll()
                .antMatchers("/api/management/**").hasRole("SYSTEM_ADMIN")
                .anyRequest().authenticated();

            //if not STATELESS it sends set-cookie -> JSESSIONID=...
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

            //todo: allow httpbasic parallel to jwt. beyond my comprehension at the moment. allowing httpbasic without csrf because jwt dont work with it
            //because according to https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html#csrf-and-stateless-browser-applications
            //Users using basic authentication are also vulnerable to CSRF attacks since the browser will automatically
            //include the username password in any requests in the same manner that the JSESSIONID cookie was sent in our previous example.
            http.antMatcher("/api/**").httpBasic();
            //http.httpBasic().disable();

            http.addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class);
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(getafixUserDetailsService).passwordEncoder(passwordEncoder);
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }
    }

    private JWTFilter jwtFilter() {
        return new JWTFilter(jwtProvider);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        if(CollectionUtils.isNotEmpty(allowedOrigins)) {
            allowedOrigins.forEach(config::addAllowedOrigin);
        }
        config.addAllowedHeader("*");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
