package com.telefonix.getafix.web.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.telefonix.getafix.service.GetafixProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * Created by tmyousuf on 16/9/18.
 */
@Slf4j
@Configuration
@EnableScheduling
public class SchedulerConfig {

    @Autowired
    private GetafixProperties getafixProperties;

    @Bean
    public ScheduledExecutorService scheduleExecutor() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat(getafixProperties.getScheduler().getName()).build();
        return Executors.newScheduledThreadPool(getafixProperties.getScheduler().getPoolSize(), threadFactory);
    }
}
