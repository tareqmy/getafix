package com.telefonix.getafix.web;

import com.telefonix.getafix.web.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * Created by tmyousuf on 18/9/18.
 * This is a helper Java class that provides an alternative to creating a web.xml
 */
public class GetafixWebXML extends SpringBootServletInitializer {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.profiles(addDefaultProfile())
            .sources(Application.class);
    }

    /**
     * Set a default profile if it has not been set
     * <p>
     * Please use -Dspring.profile.active=development
     * </p>
     *
     * @return
     */
    private String addDefaultProfile() {
        String profile = System.getProperty("spring.profiles.active");
        if (profile != null) {
            log.info("Running with Spring profile(s) : {}", profile);
            return profile;
        }

        log.warn("No Spring profile configured, running with default configuration");
        return Constants.SPRING_PROFILE_DEVELOPMENT;
    }
}