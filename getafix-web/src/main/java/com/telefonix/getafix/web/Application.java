package com.telefonix.getafix.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = "com.telefonix.getafix")
@ComponentScan(basePackages = {"com.telefonix.getafix"})
public class Application {
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
    }
}
