package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.entity.Provider;
import com.telefonix.getafix.service.ProviderService;
import com.telefonix.getfix.dto.ProviderDTO;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ProviderResource {

    @Autowired
    private ProviderService providerService;

    /**
     * POST /providers -> Create a new provider.
     */
    @RequestMapping(value = "/providers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<ProviderDTO> createProvider(@Valid @RequestBody ProviderDTO providerDTO) {
        log.debug("REST request to save Provider : {}", providerDTO);
        if (providerDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Provider result = providerService.createProvider(providerDTO);
        return ProviderDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /providers -> Updates an existing provider.
     */
    @RequestMapping(value = "/providers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<ProviderDTO> updateProvider(@Valid @RequestBody ProviderDTO providerDTO) {
        log.debug("REST request to update Provider : {}", providerDTO);
        if (providerDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Provider result = providerService.updateProvider(providerDTO);
        return ProviderDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /providers -> get all the providers.
     */
    @RequestMapping(value = "/providers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<ProviderDTO> getAllProviders() {
        log.debug("REST request to get all providers");
        return providerService.findAll()
            .stream()
            .map(ProviderDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /providers/pageable -> get all the providers.
     */
    @RequestMapping(value = "/providers/pageable",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public Page<ProviderDTO> getAllProviders(Pageable pageable) {
        log.debug("REST request to get all providers");
        return providerService.findAll(pageable)
            .map(ProviderDTO::new);
    }

    /**
     * GET /providers/:id -> get the "id" provider.
     */
    @RequestMapping(value = "/providers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<ProviderDTO> getProvider(@PathVariable Long id) {
        log.debug("REST request to get Provider : {}", id);
        return providerService.findOne(id)
            .map(provider -> new ResponseEntity<>(new ProviderDTO(provider), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /providers/:id -> delete the "id" provider.
     */
    @RequestMapping(value = "/providers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteProvider(@PathVariable Long id) {
        log.debug("REST request to delete Provider: {}", id);
        providerService.delete(id);
        return ResponseEntity.ok().build();
    }
}
