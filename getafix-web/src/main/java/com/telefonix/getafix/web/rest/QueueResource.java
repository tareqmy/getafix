package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.asterisk.dto.AXQueueLogDTO;
import com.telefonix.getafix.entity.Queue;
import com.telefonix.getafix.entity.User;
import com.telefonix.getafix.service.QueueService;
import com.telefonix.getafix.web.security.AccessControl;
import com.telefonix.getfix.dto.QueueChannelLogDTO;
import com.telefonix.getfix.dto.QueueDTO;
import com.telefonix.getfix.dto.QueueReportDTO;
import com.telefonix.getfix.dto.SoundFile;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 2019-01-28.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class QueueResource {

    @Autowired
    private QueueService queueService;

    @Autowired
    private AccessControl accessControl;

    /**
     * POST /queues -> Create a new queue.
     */
    @RequestMapping(value = "/queues",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<QueueDTO> createQueue(@Valid @RequestBody QueueDTO queueDTO) {
        log.debug("REST request to save Queue : {}", queueDTO);
        if (queueDTO.getId() != null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Queue result = queueService.createQueue(queueDTO);
        return QueueDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * PUT /queues -> Updates an existing queue.
     */
    @RequestMapping(value = "/queues",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> updateQueue(@Valid @RequestBody QueueDTO queueDTO) {
        log.debug("REST request to update Queue : {}", queueDTO);
        if (queueDTO.getId() == null) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        accessControl.hasCompanyPermission(queueDTO.getCompanyId());
        Queue result = queueService.updateQueue(queueDTO);
        return QueueDTO.getDTO(result)
            .map(resultDTO -> ResponseEntity.ok().body(resultDTO))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET /queues -> get all the queues.
     */
    @RequestMapping(value = "/queues",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public List<QueueDTO> getAllQueues() {
        log.debug("REST request to get all Queues");
        return queueService.findAll()
            .stream()
            .map(QueueDTO::new)
            .collect(Collectors.toList());
    }

    /**
     * GET /queues/:id -> get the "id" queue.
     */
    @RequestMapping(value = "/queues/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> getQueue(@PathVariable Long id) {
        log.debug("REST request to get Queue : {}", id);
        return queueService.findOne(id).map(queue -> {
            accessControl.hasCompanyPermission(queue);
            return new ResponseEntity<>(new QueueDTO(queue), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /queues/:id -> delete the "id" queue.
     */
    @RequestMapping(value = "/queues/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    public ResponseEntity<Void> deleteQueue(@PathVariable Long id) {
        log.debug("REST request to delete Queue: {}", id);
        queueService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * GET /queues/:id/queuelogs -> get log of the "id" queue.
     */
    @RequestMapping(value = "/queues/{id}/queuelogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<AXQueueLogDTO> getQueueLogs(@PathVariable Long id,
                                            @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get Queue log : {}", id);
        accessControl.hasQueuePermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return queueService.getQueueLogs(id, from, to, user.getZoneId())
            .stream()
            .map(axQueueLog -> new AXQueueLogDTO(axQueueLog, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET /queues/:id/queuechannellogs -> get log of the "id" queue.
     */
    @RequestMapping(value = "/queues/{id}/queuechannellogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public List<QueueChannelLogDTO> getQueueChannelLogs(@PathVariable Long id,
                                                        @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                                        @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get Queue channel log : {}", id);
        accessControl.hasQueuePermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return queueService.getQueueChannelLogs(id, from, to, user.getZoneId())
            .stream()
            .map(queueChannelQueueLog -> new QueueChannelLogDTO(queueChannelQueueLog, user.getZoneId()))
            .collect(Collectors.toList());
    }

    /**
     * GET  /queues/:id/queuereports -> get queuereports for the queue
     */
    @RequestMapping(value = "/queues/{id}/queuereports",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public QueueReportDTO getQueueReport(@PathVariable Long id,
                                         @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                         @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        log.debug("REST request to get queuereports for queue : {}", id);
        accessControl.hasQueuePermission(id);
        User user = accessControl.findCurrentLoggedInUser();
        return queueService.getQueueReport(id, from, to, user.getZoneId());
    }

    @RequestMapping(value = "/queues/{id}/initialmessage",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> uploadInitialMessage(@PathVariable Long id,
                                                         @RequestParam @Valid @NotNull MultipartFile file) {
        log.debug("REST request to upload initial message : {}", id);
        Queue queue = queueService.uploadInitialMessage(id, file);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/queues/{id}/initialmessage",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> deleteInitialMessage(@PathVariable Long id) {
        Queue queue = queueService.deleteInitialMessage(id);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/queues/{id}/musiconhold",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> uploadMusicOnHold(@PathVariable Long id,
                                                      @RequestParam @Valid @NotNull MultipartFile file) {
        log.debug("REST request to upload musiconhold : {}", id);
        Queue queue = queueService.uploadMusicOnHold(id, file);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/queues/{id}/musiconhold",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> deleteMusicOnHold(@PathVariable Long id) {
        Queue queue = queueService.deleteMusicOnHold(id);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/queues/{id}/comfortmessage",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> uploadComfortMessage(@PathVariable Long id,
                                                         @RequestParam @Valid @NotNull MultipartFile file) {
        log.debug("REST request to upload comfort message : {}", id);
        Queue queue = queueService.uploadComfortMessage(id, file);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/queues/{id}/comfortmessage",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<QueueDTO> deleteComfortMessage(@PathVariable Long id) {
        Queue queue = queueService.deleteComfortMessage(id);
        accessControl.hasCompanyPermission(queue);
        return QueueDTO.getDTO(queue)
            .map(dto -> ResponseEntity.ok().body(dto))
            .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    /**
     * GET  /queues/:id/initialmessage/download -> download the initialmessage for "id" queue.
     */
    @RequestMapping(value = "/queues/{id}/initialmessage/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<byte[]> downloadInitialMessage(@PathVariable Long id) {
        log.debug("REST request to download Queue initialmessage : {}", id);
        accessControl.hasQueuePermission(id);
        SoundFile soundFile = queueService.downloadInitialMessage(id);
        return MiscUtils.getDownload(soundFile.getBytes(), soundFile.getFileName(), MediaType.parseMediaType("audio/wave"));
    }

    /**
     * GET  /queues/:id/musiconhold/download -> download the musiconhold for "id" queue.
     */
    @RequestMapping(value = "/queues/{id}/musiconhold/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<byte[]> downloadMusicOnHold(@PathVariable Long id) {
        log.debug("REST request to download Queue musiconhold : {}", id);
        accessControl.hasQueuePermission(id);
        SoundFile soundFile = queueService.downloadMusicOnHold(id);
        return MiscUtils.getDownload(soundFile.getBytes(), soundFile.getFileName(), MediaType.parseMediaType("audio/wave"));
    }

    /**
     * GET  /queues/:id/comfortmessage/download -> download the comfortmessage for "id" queue.
     */
    @RequestMapping(value = "/queues/{id}/comfortmessage/download",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<byte[]> downloadComfortMessage(@PathVariable Long id) {
        log.debug("REST request to download Queue comfortmessage : {}", id);
        accessControl.hasQueuePermission(id);
        SoundFile soundFile = queueService.downloadComfortMessage(id);
        return MiscUtils.getDownload(soundFile.getBytes(), soundFile.getFileName(), MediaType.parseMediaType("audio/wave"));
    }
}
