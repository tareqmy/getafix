package com.telefonix.getafix.web.rest;

import com.telefonix.getafix.service.ServerSentEventService;
import com.telefonix.getafix.service.live.dto.*;
import com.telefonix.getfix.utils.AuthoritiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by tareqmy on 2019-05-22.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ServerSentEventResource {

    @Autowired
    private ServerSentEventService serverSentEventService;

    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    @RequestMapping(path = "/sse/time/live", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerTimeDTO> streamTime() {
        return serverSentEventService.streamTime();
    }

    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN})
    @RequestMapping(path = "/sse/system/live", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<SystemLiveDTO> streamSystemLive() {
        return serverSentEventService.streamSystemLive();
    }

    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    @RequestMapping(path = "/sse/callcenter/live", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<CallCenterLiveDTO> streamCallCenterLive() {
        return serverSentEventService.streamCallCenterLive();
    }

    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN})
    @RequestMapping(path = "/sse/queues/{uniqueQueueId}/live", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<QueueLiveDTO> streamQueuesLive(@PathVariable String uniqueQueueId) {
        return serverSentEventService.streamQueuesLive(uniqueQueueId);
    }

    @Secured({AuthoritiesConstants.SYSTEM_ADMIN, AuthoritiesConstants.GETAFIX_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER})
    @RequestMapping(path = "/sse/agents/live", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<AgentLiveDTO> streamAgentsLive() {
        return serverSentEventService.streamAgentsLive();
    }
}
