package com.telefonix.getafix.web.config;

import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Slf4j
@Configuration
public class AsteriskConfiguration {

    @Value(value = "${getafix.asterisk.ami.ip}")
    private String ip;

    @Value(value = "${getafix.asterisk.ami.port}")
    private int port;

    @Value(value = "${getafix.asterisk.ami.userName}")
    private String userName;

    @Value(value = "${getafix.asterisk.ami.password}")
    private String password;

    @Bean(name = "managerConnectionFactory")
    public ManagerConnectionFactory managerConnectionFactory() {
        log.debug("Creating managerConnectionFactory bean with {} {} {}", ip, port, userName);
        return new ManagerConnectionFactory(ip, port, userName, password);
    }
}
