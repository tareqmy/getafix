package com.telefonix.getafix.web.config;

import com.telefonix.getafix.web.aop.logging.LoggingAspect;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;

/**
 * Created by tmyousuf on 8/8/18.
 */
@Slf4j
@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfiguration {

    @Bean
    @Profile("development")
    public LoggingAspect loggingAspect() {
        return new LoggingAspect();
    }
}
