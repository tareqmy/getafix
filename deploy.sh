source .env
echo "Delete existing containers..."
docker rm -f $API $MINIO $MINIOMC $DATABASE $MAILHOG

docker-compose up -d

./logs.sh
