package ch.loway.oss.ari4java.generated.ari_4_0_0.actions;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
//    Generated on: Wed Jul 31 13:06:40 BDT 2019
// ----------------------------------------------------

import ch.loway.oss.ari4java.generated.Module;
import ch.loway.oss.ari4java.generated.*;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.BaseAriAction;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.HttpParam;
import ch.loway.oss.ari4java.tools.HttpResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import ch.loway.oss.ari4java.generated.ari_4_0_0.models.*;

/**********************************************************
 * 
 * Generated by: Apis
 *********************************************************/


public class ActionMailboxes_impl_ari_4_0_0 extends BaseAriAction  implements ActionMailboxes {
/**********************************************************
 * Mailboxes
 * 
 * List all mailboxes.
 *********************************************************/
private void buildList() {
reset();
url = "/mailboxes";
method = "GET";
}

@Override
public List<Mailbox> list() throws RestException {
buildList();
String json = httpActionSync();
return deserializeJsonAsAbstractList( json,
   new TypeReference<List<Mailbox_impl_ari_4_0_0>>() {} ); 
}

@Override
public void list(AriCallback<List<Mailbox>> callback) {
buildList();
httpActionAsync(callback, new TypeReference<List<Mailbox_impl_ari_4_0_0>>() {});
}

/**********************************************************
 * Mailbox state
 * 
 * Retrieve the current state of a mailbox.
 *********************************************************/
private void buildGet(String mailboxName) {
reset();
url = "/mailboxes/" + mailboxName + "";
method = "GET";
lE.add( HttpResponse.build( 404, "Mailbox not found") );
}

@Override
public Mailbox get(String mailboxName) throws RestException {
buildGet(mailboxName);
String json = httpActionSync();
return deserializeJson( json, Mailbox_impl_ari_4_0_0.class ); 
}

@Override
public void get(String mailboxName, AriCallback<Mailbox> callback) {
buildGet(mailboxName);
httpActionAsync(callback, Mailbox_impl_ari_4_0_0.class);
}

/**********************************************************
 * Mailbox state
 * 
 * Change the state of a mailbox. (Note - implicitly creates the mailbox).
 *********************************************************/
private void buildUpdate(String mailboxName, int oldMessages, int newMessages) {
reset();
url = "/mailboxes/" + mailboxName + "";
method = "PUT";
lParamQuery.add( HttpParam.build( "oldMessages", oldMessages) );
lParamQuery.add( HttpParam.build( "newMessages", newMessages) );
lE.add( HttpResponse.build( 404, "Mailbox not found") );
}

@Override
public void update(String mailboxName, int oldMessages, int newMessages) throws RestException {
buildUpdate(mailboxName, oldMessages, newMessages);
String json = httpActionSync();
}

@Override
public void update(String mailboxName, int oldMessages, int newMessages, AriCallback<Void> callback) {
buildUpdate(mailboxName, oldMessages, newMessages);
httpActionAsync(callback);
}

/**********************************************************
 * Mailbox state
 * 
 * Destroy a mailbox.
 *********************************************************/
private void buildDelete(String mailboxName) {
reset();
url = "/mailboxes/" + mailboxName + "";
method = "DELETE";
lE.add( HttpResponse.build( 404, "Mailbox not found") );
}

@Override
public void delete(String mailboxName) throws RestException {
buildDelete(mailboxName);
String json = httpActionSync();
}

@Override
public void delete(String mailboxName, AriCallback<Void> callback) {
buildDelete(mailboxName);
httpActionAsync(callback);
}

/** No missing signatures from interface */
};

