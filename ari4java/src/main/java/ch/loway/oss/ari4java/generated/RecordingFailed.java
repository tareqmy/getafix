package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
//    Generated on: Wed Jul 31 13:06:39 BDT 2019
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.tags.*;
import ch.loway.oss.ari4java.generated.Module;

/**********************************************************
 * 
 * Generated by: JavaInterface
 *********************************************************/


public interface RecordingFailed extends Event  {

// LiveRecording getRecording
/**********************************************************
 * Recording control object
 * 
 * @since ari_2_0_0
 *********************************************************/
 public LiveRecording getRecording();



// void setRecording LiveRecording
/**********************************************************
 * Recording control object
 * 
 * @since ari_2_0_0
 *********************************************************/
 public void setRecording(LiveRecording val );


}
;
