package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
//    Generated on: Wed Jul 31 13:06:39 BDT 2019
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.tags.*;
import ch.loway.oss.ari4java.generated.Module;

/**********************************************************
 * 
 * Generated by: JavaInterface
 *********************************************************/


public interface ChannelDialplan extends Event  {

// Channel getChannel
/**********************************************************
 * The channel that changed dialplan location.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public Channel getChannel();



// String getDialplan_app_data
/**********************************************************
 * The data to be passed to the application.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public String getDialplan_app_data();



// void setChannel Channel
/**********************************************************
 * The channel that changed dialplan location.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public void setChannel(Channel val );



// void setDialplan_app String
/**********************************************************
 * The application about to be executed.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public void setDialplan_app(String val );



// void setDialplan_app_data String
/**********************************************************
 * The data to be passed to the application.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public void setDialplan_app_data(String val );



// String getDialplan_app
/**********************************************************
 * The application about to be executed.
 * 
 * @since ari_2_0_0
 *********************************************************/
 public String getDialplan_app();


}
;
