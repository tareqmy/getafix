package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Repository
public interface PSEndpointRepository extends JpaRepository<PSEndpoint, String> {

    List<PSEndpoint> findByContext(String context);
}
