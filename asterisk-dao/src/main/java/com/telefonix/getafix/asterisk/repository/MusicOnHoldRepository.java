package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.MusicOnHold;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tareqmy on 15/9/19.
 */
@Repository
public interface MusicOnHoldRepository extends JpaRepository<MusicOnHold, String> {
}
