package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.AstConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tareqmy on 2019-01-29.
 */
@Repository
public interface AstConfigRepository extends JpaRepository<AstConfig, Long> {

    List<AstConfig> findByCategory(String category);

    AstConfig findFirstByCategoryOrderByVarMetricAsc(String category);

    AstConfig findFirstByCategoryOrderByVarMetricDesc(String category);

    //NOTE: this is a new requirement?
    @Transactional
    void deleteByCategory(String category);

    AstConfig findTopByOrderByCatMetricDesc();

    @Query("SELECT DISTINCT(a.category) FROM AstConfig a")
    List<String> findAllCategory();
}
