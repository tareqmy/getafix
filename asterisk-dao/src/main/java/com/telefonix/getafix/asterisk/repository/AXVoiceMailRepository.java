package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Repository
public interface AXVoiceMailRepository extends JpaRepository<AXVoiceMail, Long> {

    Optional<AXVoiceMail> findByContextAndMailbox(String context, String mailbox);
}
