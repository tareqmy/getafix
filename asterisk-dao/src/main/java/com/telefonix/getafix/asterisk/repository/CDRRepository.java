package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.CDR;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by tmyousuf on 8/8/18.
 */
@Repository
public interface CDRRepository extends JpaRepository<CDR, Long> {

    @Query("SELECT c FROM CDR c WHERE c.id > :after")
    List<CDR> findAfter(@Param("after") Long after);

    @Query("SELECT c FROM CDR c WHERE c.accountCode in :accountCodes OR c.peerAccount in :accountCodes")
    List<CDR> findAllByAccountCodes(@Param("accountCodes") List<String> accountCodes);

    @Query("SELECT c FROM CDR c WHERE (c.start BETWEEN :fromDate AND :toDate)")
    List<CDR> findAllByDate(@Param("fromDate") ZonedDateTime fromDate, @Param("toDate") ZonedDateTime toDate);

    @Query("SELECT c FROM CDR c WHERE c.accountCode like :accountCodeLike% OR c.peerAccount like :accountCodeLike%")
    List<CDR> findAllByAccountCodeLike(@Param("accountCodeLike") String accountCodeLike);

    List<CDR> findAllByLastAppAndLastData(String lastapp, String lastdata);

    List<CDR> findByChannelAndUniqueId(String channel, String uniqueId);
}
