package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getafix.asterisk.entity.AXVoiceMessageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Repository
public interface AXVoiceMessageRepository extends JpaRepository<AXVoiceMessage, AXVoiceMessageId> {

    List<AXVoiceMessage> findByMailboxUserAndMailboxContext(String mailboxUser, String mailboxContext);

    @Query("SELECT v FROM AXVoiceMessage v WHERE v.axVoiceMessageId.dir = :dir")
    List<AXVoiceMessage> findByDir(@Param("dir") String dir);

    Optional<AXVoiceMessage> findByMailboxUserAndMailboxContextAndMsgId(String mailboxUser, String mailboxContext, String msgId);
}
