package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Repository
public interface AXQueueLogRepository extends JpaRepository<AXQueueLog, Long> {

    Optional<AXQueueLog> findTopByQueueNameOrderByTimeAsc(String queueName);

    @Query("SELECT c FROM AXQueueLog c " +
        "WHERE (c.queueName = :queueName) " +
        "AND c.time BETWEEN :fromDate AND :toDate ORDER BY c.time DESC")
    List<AXQueueLog> findAllByQueueNameAndTimeBetweenOrderByTimeDesc(@Param("queueName") String queueName,
                                                                     @Param("fromDate") Instant fromDate,
                                                                     @Param("toDate") Instant toDate);

    @Query("SELECT c FROM AXQueueLog c " +
        "WHERE (c.agent = :agent) " +
        "AND c.time BETWEEN :fromDate AND :toDate ORDER BY c.time DESC")
    List<AXQueueLog> findAllByAgentAndTimeBetweenOrderByTimeDesc(@Param("agent") String agent,
                                                                 @Param("fromDate") Instant fromDate,
                                                                 @Param("toDate") Instant toDate);

    //could not find a better way. so using postgres native distinct on
    @Query(nativeQuery = true,
        value = "SELECT DISTINCT ON (agent) q.* FROM queue_log q " +
            "WHERE q.queuename = :queueName AND time < :time " +
            "ORDER BY agent, time DESC")
    List<AXQueueLog> findByQueueNameDistinctOnAgentOrderByTimeDesc(@Param("queueName") String queueName, @Param("time") Instant time);
}
