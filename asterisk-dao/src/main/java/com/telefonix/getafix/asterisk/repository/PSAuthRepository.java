package com.telefonix.getafix.asterisk.repository;

import com.telefonix.getafix.asterisk.entity.PSAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Repository
public interface PSAuthRepository extends JpaRepository<PSAuth, String> {
}
