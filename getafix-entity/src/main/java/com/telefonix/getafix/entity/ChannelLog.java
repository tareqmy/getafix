package com.telefonix.getafix.entity;

import com.telefonix.getafix.entity.enums.ChannelLogStatus;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by tareqmy on 2019-06-16.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"uniqueId"})
@Table(name = "channel_logs")
public class ChannelLog implements Comparable<ChannelLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "channel_logs_id_seq")
    @SequenceGenerator(name = "channel_logs_id_seq", sequenceName = "channel_logs_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "unique_id", length = 24, nullable = false)
    private String uniqueId;

    @Column(name = "account_code", length = 80, nullable = false)
    private String accountCode;

    @Column(name = "company_id", length = 12)
    private String companyId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_name", length = 52)
    private String userName;

    @Column(name = "extension_id")
    private Long extensionId;

    @Column(name = "base_extension")
    private String baseExtension;

    @Column(name = "phone_name")
    private String phoneName;

    @Column(name = "linked_id", length = 24, nullable = false)
    private String linkedId;

    @Column(name = "call_from", nullable = false)
    private String from;

    @Column(name = "call_to", nullable = false)
    private String to;

    @Column(name = "caller_name")
    private String callerName;

    @Column(name = "caller_number")
    private String callerNumber;

    @Column(name = "called_number")
    private String calledNumber;

    @Column(nullable = false)
    private boolean caller;

    @Column(name = "call_initiator", nullable = false)
    private boolean callInitiator;

    @Column(nullable = false)
    private boolean external;

    @Column(name = "call_start", nullable = false)
    private Instant start;

    @Column(name = "call_answer")
    private Instant answer;

    @Column(name = "call_end")
    private Instant end;

    @Column(name = "duration", nullable = false)
    private Long duration;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 24, nullable = false)
    private ChannelLogStatus status;

    @Column(name = "call_type", length = 24)
    private String callType;

    @Override
    public int compareTo(ChannelLog o) {
        return o.getStart().compareTo(getStart());
    }
}
