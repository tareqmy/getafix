package com.telefonix.getafix.entity.enums;

public enum AgentEvent {
    JOIN_CALL_CENTER,
    PAUSE_AGENT,
    RESUME_AGENT,
    LEAVE_CALL_CENTER,
    JOIN_QUEUE,
    LEAVE_QUEUE
}
