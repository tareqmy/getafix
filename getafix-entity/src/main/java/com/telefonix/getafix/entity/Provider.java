package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Comparator;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"providerId"})
@Table(name = "providers")
public class Provider implements Comparable<Provider> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "providers_id_seq")
    @SequenceGenerator(name = "providers_id_seq", sequenceName = "providers_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "provider_id", length = 50, nullable = false, unique = true)
    private String providerId;

    @Column(length = 50, nullable = false)
    private String host;

    @Override
    public String toString() {
        return "Provider{" +
            "id=" + id +
            ", providerId='" + providerId + '\'' +
            ", host='" + host + '\'' +
            '}';
    }

    @Override
    public int compareTo(Provider o) {
        return Comparator.comparing(Provider::getProviderId)
            .compare(this, o);
    }
}
