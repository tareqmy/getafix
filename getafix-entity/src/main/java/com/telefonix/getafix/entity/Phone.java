package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"company", "id"})
@Table(name = "phones")
public class Phone implements Comparable<Phone> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phones_id_seq")
    @SequenceGenerator(name = "phones_id_seq", sequenceName = "phones_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "phone_name", length = 50, nullable = false)
    private String phoneName;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OrderBy("id asc")
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "phone")
    private Set<PhoneLine> phoneLines = new HashSet<>();

    @Override
    public int compareTo(Phone o) {
        return getPhoneName().compareTo(o.getPhoneName());
    }

    @Override
    public String toString() {
        return "Phone{" +
            "id=" + id +
            ", phoneName='" + phoneName + '\'' +
            ", company=" + company.getCompanyId() +
            ", phoneLines=" + phoneLines +
            '}';
    }
}
