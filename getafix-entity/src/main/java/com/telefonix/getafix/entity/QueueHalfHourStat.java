package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.Comparator;

/**
 * Created by tareqmy on 1/10/19.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"start", "queueId"})
@Table(name = "queue_half_hour_stats", uniqueConstraints = @UniqueConstraint(columnNames = {"stat_start", "queue_id"}))
public class QueueHalfHourStat implements Comparable<QueueHalfHourStat> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queue_half_hour_stats_id_seq")
    @SequenceGenerator(name = "queue_half_hour_stats_id_seq", sequenceName = "queue_half_hour_stats_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "stat_start", nullable = false)
    private Instant start;

    @Column(name = "company_id", length = 12)
    private String companyId;

    @Column(name = "queue_id", nullable = false)
    private Long queueId;

    @Column(name = "queue_name", nullable = false)
    private String queueName;

    @Column(name = "continued_agents", nullable = false)
    private long continuedAgents;

    @Column(name = "max_joined_agents", nullable = false)
    private long maxJoinedAgents;

    @Column(name = "continued_callers", nullable = false)
    private long continuedCallers;

    @Column(name = "max_callers", nullable = false)
    private long maxCallers;

    @Override
    public String toString() {
        return "QueueHalfHourStat{" +
            "id=" + id +
            ", start=" + start +
            ", companyId='" + companyId + '\'' +
            ", queueId=" + queueId +
            ", queueName='" + queueName + '\'' +
            ", continuedAgents=" + continuedAgents +
            ", maxJoinedAgents=" + maxJoinedAgents +
            ", continuedCallers=" + continuedCallers +
            ", maxCallers=" + maxCallers +
            '}';
    }

    @Override
    public int compareTo(QueueHalfHourStat o) {
        return Comparator.comparing(QueueHalfHourStat::getCompanyId)
            .thenComparing(QueueHalfHourStat::getStart)
            .compare(this, o);
    }
}
