package com.telefonix.getafix.entity.enums;

public enum Strategy {
    ringall,
    leastrecent,
    fewestcalls,
    random,
    rrmemory,
    rrordered,
    linear,
    wrandom
}
