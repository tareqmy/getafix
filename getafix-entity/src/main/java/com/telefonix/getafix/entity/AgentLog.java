package com.telefonix.getafix.entity;

import com.telefonix.getafix.entity.enums.AgentEvent;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by tareqmy on 2019-08-11.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
@Table(name = "agent_logs")
public class AgentLog implements Comparable<AgentLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agent_logs_id_seq")
    @SequenceGenerator(name = "agent_logs_id_seq", sequenceName = "agent_logs_id_seq", allocationSize = 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "agent_event", nullable = false)
    private AgentEvent agentEvent;

    @Column(name = "event_instant", nullable = false)
    private Instant eventInstant;

    @Column(name = "company_id", length = 12, nullable = false)
    private String companyId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "queue_id")
    private Long queueId;

    @Column(name = "queue_name")
    private String queueName;

    @Column(name = "phone_line_id")
    private Long phoneLineId;

    @Column(name = "sip_user_name", nullable = false)
    private String sipUserName;

    @Override
    public int compareTo(AgentLog o) {
        return o.getEventInstant().compareTo(getEventInstant());
    }
}
