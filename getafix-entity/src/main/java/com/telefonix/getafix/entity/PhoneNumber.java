package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Comparator;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"number"})
@Table(name = "phone_numbers")
public class PhoneNumber implements Comparable<PhoneNumber> {

    @Id
    @Column(length = 20, nullable = false)
    @Pattern(regexp = "^\\d{11,14}$")
    private String number;

    @Column(length = 60, nullable = false)
    private String password;

    @ManyToOne()
    @JoinColumn(name = "provider_id")
    protected Provider provider;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToOne()
    @JoinColumn(name = "extension_id", nullable = false)
    private Extension extension;

    @Override
    public String toString() {
        return "PhoneNumber{" +
            "number='" + number + '\'' +
//            ", password=" + password +
            ", provider=" + provider.getId() +
            ", company=" + company.getCompanyId() +
            ", extension=" + extension.getBaseExtension() +
            '}';
    }

    @Override
    public int compareTo(PhoneNumber o) {
        return Comparator.comparing(PhoneNumber::getCompany)
            .thenComparing(PhoneNumber::getNumber)
            .compare(this, o);
    }
}
