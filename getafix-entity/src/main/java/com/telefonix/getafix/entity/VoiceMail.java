package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-23.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"extension"})
@Table(name = "voice_mails")
public class VoiceMail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voice_mails_id_seq")
    @SequenceGenerator(name = "voice_mails_id_seq", sequenceName = "voice_mails_id_seq", allocationSize = 1)
    private Long id;

    @OneToOne()
    @JoinColumn(name = "extension_id", nullable = false)
    private Extension extension;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "full_name", length = 80)
    private String fullName;

    @Column(name = "email", length = 80)
    private String email;

    public VoiceMail(Extension extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return "VoiceMail{" +
            "id=" + id +
            ", extension=" + extension.getBaseExtension() +
            ", enabled=" + enabled +
            ", fullName='" + fullName + '\'' +
            ", email='" + email + '\'' +
            '}';
    }
}
