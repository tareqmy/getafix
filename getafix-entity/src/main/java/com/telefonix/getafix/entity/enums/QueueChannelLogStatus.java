package com.telefonix.getafix.entity.enums;

public enum QueueChannelLogStatus {
    IN_PROGRESS,
    CALL_ABANDONED,
    CALL_ANSWERED,
    CALL_NOT_ANSWERED
}
