package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 2019-05-05.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"company", "autoAttendantId"})
@Table(name = "auto_attendants")
public class AutoAttendant implements Comparable<AutoAttendant> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auto_attendants_id_seq")
    @SequenceGenerator(name = "auto_attendants_id_seq", sequenceName = "auto_attendants_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 3, max = 12)
    @Column(name = "auto_attendant_id", length = 12, nullable = false, updatable = false)
    private String autoAttendantId;

    @Size(min = 5, max = 50)
    @Column(name = "auto_attendant_name", length = 50, nullable = false)
    private String autoAttendantName;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToOne()
    @JoinColumn(name = "extension_id", nullable = false)
    private Extension extension;

    @Column(name = "prompt_uploaded", nullable = false)
    private boolean promptUploaded;

    @ElementCollection
    @MapKeyColumn(name = "option")
    @Column(name = "destination")
    @CollectionTable(name = "auto_attendant_options", joinColumns = @JoinColumn(name = "auto_attendant_id"))
    private Map<String, String> optionsMap = new HashMap<>();

    @Override
    public String toString() {
        return "AutoAttendant{" +
            "id=" + id +
            ", autoAttendantId='" + autoAttendantId + '\'' +
            ", autoAttendantName='" + autoAttendantName + '\'' +
            ", company=" + company.getCompanyId() +
            ", extension=" + extension.getBaseExtension() +
            ", promptUploaded=" + promptUploaded +
            ", optionsMap=" + optionsMap +
            '}';
    }

    @Override
    public int compareTo(AutoAttendant o) {
        return Comparator.comparing(AutoAttendant::getCompany)
            .thenComparing(AutoAttendant::getAutoAttendantName)
            .compare(this, o);
    }
}
