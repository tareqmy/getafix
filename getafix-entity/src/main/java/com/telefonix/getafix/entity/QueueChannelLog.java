package com.telefonix.getafix.entity;

import com.telefonix.getafix.entity.enums.QueueChannelLogStatus;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 4/9/19.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"uniqueId"})
@Table(name = "queue_channel_logs")
public class QueueChannelLog implements Comparable<QueueChannelLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queue_channel_logs_id_seq")
    @SequenceGenerator(name = "queue_channel_logs_id_seq", sequenceName = "queue_channel_logs_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "unique_id", length = 24, nullable = false)
    private String uniqueId;

    @Column(name = "company_id", length = 12)
    private String companyId;

    @Column(name = "linked_id", length = 24, nullable = false)
    private String linkedId;

    @Column(name = "call_from", nullable = false)
    private String from;

    @Column(name = "call_to", nullable = false)
    private String to;

    @Column(name = "caller_name")
    private String callerName;

    @Column(name = "caller_number")
    private String callerNumber;

    @Column(name = "called_number")
    private String calledNumber;

    @Column(nullable = false)
    private boolean external;

    @Column(nullable = false)
    private boolean outbound;

    @Column(name = "call_start", nullable = false)
    private Instant start;

    @Column(name = "call_end")
    private Instant end;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 24)
    private QueueChannelLogStatus status;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "queueChannelLog")
    private List<QueueChannelQueueLog> queueChannelQueueLogs = new ArrayList<>();

    @Override
    public int compareTo(QueueChannelLog o) {
        return o.getStart().compareTo(getStart());
    }

    @Override
    public String toString() {
        return "QueueChannelLog{" +
            "id=" + id +
            ", uniqueId='" + uniqueId + '\'' +
            ", companyId='" + companyId + '\'' +
            ", linkedId='" + linkedId + '\'' +
            ", from='" + from + '\'' +
            ", to='" + to + '\'' +
            ", callerName='" + callerName + '\'' +
            ", callerNumber='" + callerNumber + '\'' +
            ", calledNumber='" + calledNumber + '\'' +
            ", external=" + external +
            ", outbound=" + outbound +
            ", start=" + start +
            ", end=" + end +
            ", status=" + status +
            ", queueChannelQueueLogs=" + queueChannelQueueLogs +
            '}';
    }
}
