package com.telefonix.getafix.entity;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tareqmy on 3/1/19.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"companyId"})
@Table(name = "companies")
public class Company implements Serializable, Comparable<Company> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "companies_id_seq")
    @SequenceGenerator(name = "companies_id_seq", sequenceName = "companies_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 3, max = 12)
    @Column(name = "company_id", length = 12, nullable = false, updatable = false, unique = true)
    private String companyId;

    @Size(min = 5, max = 50)
    @Column(name = "company_name", length = 50)
    private String companyName;

    @Size(max = 125)
    @Column(length = 125)
    private String description;

    @Column(nullable = false)
    private boolean enabled = true;

    //time zone
    @Column(name = "zone_id", nullable = false)
    private ZoneId zoneId;

    @Email
    @Size(max = 50)
    @Column(length = 50)
    private String email;

    @Size(max = 25)
    @Column(length = 25)
    private String phone;

    @Size(max = 255)
    private String notes;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<PhoneNumber> phoneNumbers = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<Phone> phones = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<Queue> queues = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<Extension> extensions = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<AutoAttendant> autoAttendants = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<User> users = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "company")
    private Set<Conference> conferences = new HashSet<>();

    @Override
    public String toString() {
        return "Company{" +
            "id=" + id +
            ", companyId='" + companyId + '\'' +
            ", companyName='" + companyName + '\'' +
            ", description='" + description + '\'' +
            ", enabled=" + enabled +
            ", zoneId=" + zoneId +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", notes='" + notes + '\'' +
            ", phoneNumbers=" + phoneNumbers +
            ", phones=" + phones +
            '}';
    }

    @Override
    public int compareTo(Company o) {
        return Comparator.comparing(Company::getCompanyId)
            .compare(this, o);
    }
}
