package com.telefonix.getafix.entity.enums;

/**
 * Created by tmyousuf on 8/8/18.
 */
public enum Role {
    ROLE_SYSTEM_ADMIN,
    ROLE_GETAFIX_ADMIN,
    ROLE_ADMIN,
    ROLE_USER
}
