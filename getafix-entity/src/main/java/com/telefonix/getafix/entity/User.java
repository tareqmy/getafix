package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"email"})
@Table(name = "users")
public class User implements UserDetails, Comparable<User> {

    private static final long serialVersionUID = -7941769011539363185L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    private Long id;

    @Column(length = 50, nullable = false, updatable = false, unique = true)
    private String email;

    @Column(name = "first_name", length = 25)
    private String firstName;

    @Column(name = "last_name", length = 25)
    private String lastName;

    @Column(length = 60, nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "user_role_id")
    private UserRole userRole;

    @Column(nullable = false)
    private boolean enabled;

    @Column(nullable = false)
    private boolean locked;

    //time zone
    @Column(name = "zone_id", nullable = false)
    private ZoneId zoneId;

    @Column(name = "email_confirmed", nullable = false)
    private boolean emailConfirmed;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    private Set<PasswordResetToken> passwordResetTokens = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    private Set<EmailConfirmToken> emailConfirmTokens = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Extension> extensions = new HashSet<>();

    @OneToOne()
    @JoinColumn(name = "phone_line_id")
    private PhoneLine joinedPhoneLine;

    public String getFullName() {
        return String.format("%s %s", this.firstName, this.lastName);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(userRole.getRole().toString());
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
//                ", password='" + password + '\'' +
            ", userRole=" + userRole +
            ", enabled=" + enabled +
            ", locked=" + locked +
            ", zoneId=" + zoneId +
            ", emailConfirmed=" + emailConfirmed +
            ", company=" + company.getCompanyId() +
            '}';
    }

    @Override
    public int compareTo(User o) {
        return Comparator.comparing(User::getCompany)
            .thenComparing(User::getFullName)
            .compare(this, o);
    }
}
