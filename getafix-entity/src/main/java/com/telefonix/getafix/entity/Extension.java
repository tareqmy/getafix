package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Comparator;

/**
 * Created by tareqmy on 2019-01-14.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"company", "baseExtension"})
@Table(name = "extensions")
public class Extension implements Comparable<Extension> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "extensions_id_seq")
    @SequenceGenerator(name = "extensions_id_seq", sequenceName = "extensions_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 2, max = 2)
    @Column(name = "base_extension", length = 2, nullable = false)
    private String baseExtension;

    @Column(name = "ring_duration")
    private Integer ringDuration;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(cascade = CascadeType.PERSIST, orphanRemoval = true, mappedBy = "extension")
    private VoiceMail voiceMail;

    @OneToOne(mappedBy = "extension")
    private Queue queue;

    @OneToOne(mappedBy = "extension")
    private PhoneLine phoneLine;

    @OneToOne(mappedBy = "extension")
    private PhoneNumber phoneNumber;

    @OneToOne(mappedBy = "extension")
    private AutoAttendant autoAttendant;

    @Override
    public String toString() {
        return "Extension{" +
            "id=" + id +
            ", baseExtension='" + baseExtension + '\'' +
            ", ringDuration=" + ringDuration +
            ", company=" + company.getCompanyId() +
//            ", user=" + user +
            '}';
    }

    @Override
    public int compareTo(Extension o) {
        return Comparator.comparing(Extension::getCompany)
            .thenComparing(Extension::getBaseExtension)
            .compare(this, o);
    }
}
