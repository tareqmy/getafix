package com.telefonix.getafix.entity;

import com.telefonix.getafix.entity.enums.Strategy;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Comparator;

/**
 * Created by tareqmy on 2019-01-28.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"company", "queueId"})
@Table(name = "queues")
public class Queue implements Comparable<Queue> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queues_id_seq")
    @SequenceGenerator(name = "queues_id_seq", sequenceName = "queues_id_seq", allocationSize = 1)
    private Long id;

    @Size(min = 3, max = 12)
    @Column(name = "queue_id", length = 12, nullable = false, updatable = false)
    private String queueId;

    @Size(min = 5, max = 50)
    @Column(name = "queue_name", length = 50)
    private String queueName;

    @Column(name = "email", length = 80)
    private String email;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToOne()
    @JoinColumn(name = "extension_id", nullable = false)
    private Extension extension;

    @Enumerated(EnumType.STRING)
    @Column(name = "strategy", length = 20)
    private Strategy strategy;

    @Column(name = "weight")
    private int weight;

    @Column(name = "service_level")
    private int serviceLevel;

    @Column(name = "max_length")
    private int maxLength;

    @Column(name = "play_initial_message")
    private boolean playInitialMessage;

    @Column(name = "initial_message_file_name", length = 48)
    private String initialMessageFileName;

    @Column(name = "play_moh")
    private boolean playMusicOnHold;

    @Column(name = "moh_file_name", length = 48)
    private String mohFileName;

    @Column(name = "play_comfort_message")
    private boolean playComfortMessage;

    @Column(name = "comfort_message_file_name", length = 48)
    private String comfortMessageFileName;

    @Column(name = "comfort_message_interval", nullable = false)
    private int comfortMessageInterval;

    @Column(name = "zero_escape_enabled")
    private boolean zeroEscapeEnabled;

    @Override
    public String toString() {
        return "Queue{" +
            "id=" + id +
            ", queueId='" + queueId + '\'' +
            ", queueName='" + queueName + '\'' +
            ", email='" + email + '\'' +
            ", company=" + company.getCompanyId() +
            ", extension=" + extension.getBaseExtension() +
            ", strategy=" + strategy +
            ", weight=" + weight +
            ", serviceLevel=" + serviceLevel +
            ", maxLength=" + maxLength +
            '}';
    }

    @Override
    public int compareTo(Queue o) {
        return Comparator.comparing(Queue::getCompany)
            .thenComparing(Queue::getQueueId)
            .compare(this, o);
    }
}
