package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Comparator;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"company", "leaderCode", "participantCode"})
@Table(name = "conferences")
public class Conference implements Comparable<Conference> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conferences_id_seq")
    @SequenceGenerator(name = "conferences_id_seq", sequenceName = "conferences_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name", length = 30, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    //server generated readonly
    @Column(name = "leader_code", length = 6, nullable = false)
    private String leaderCode;

    //server generated readonly
    @Column(name = "participant_code", length = 6, nullable = false)
    private String participantCode;

    @Override
    public String toString() {
        return "Conference{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", company=" + company.getCompanyId() +
            ", leaderCode='" + leaderCode + '\'' +
            ", participantCode='" + participantCode + '\'' +
            '}';
    }

    @Override
    public int compareTo(Conference o) {
        return Comparator.comparing(Conference::getCompany)
            .thenComparing(Conference::getName)
            .compare(this, o);
    }
}
