package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@Table(name = "systems")
public class System {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "systems_id_seq")
    @SequenceGenerator(name = "systems_id_seq", sequenceName = "systems_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "international_enabled", nullable = false)
    private boolean internationalEnabled;

    //NOTE: this is only set by liquibase script
    //this is needed when the dialplan schema is updated and needs to be reflected on all
    //the customers dialplan. so it does not make sense if triggered manually by a user.
    @Column(name = "rebuild_dialplan", nullable = false)
    private boolean rebuildDialplan;

    @Override
    public String toString() {
        return "System{" +
            "id=" + id +
            ", internationalEnabled=" + internationalEnabled +
            ", rebuildDialplan=" + rebuildDialplan +
            '}';
    }
}
