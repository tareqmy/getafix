package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

/**
 * Created by tmyousuf on 17/10/17.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"token"})
@Table(name = "password_reset_tokens")
public class PasswordResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "password_reset_tokens_id_seq")
    @SequenceGenerator(name = "password_reset_tokens_id_seq", sequenceName = "password_reset_tokens_id_seq", allocationSize = 1)
    private Long id;

    @Size(max = 20)
    @Column(name = "token", length = 20, nullable = false)
    private String token;

    @Column(name = "expiry_date", nullable = false)
    private ZonedDateTime expiryDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Override
    public String toString() {
        return "PasswordResetToken{" +
            "id=" + id +
            ", token='" + token + '\'' +
            ", expiryDate=" + expiryDate +
            ", user=" + user +
            '}';
    }
}
