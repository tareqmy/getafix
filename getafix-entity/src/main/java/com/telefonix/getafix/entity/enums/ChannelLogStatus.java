package com.telefonix.getafix.entity.enums;

/**
 * Created by tareqmy on 5/9/19.
 */
public enum ChannelLogStatus {
    IN_PROGRESS,
    CALL_MISSED,
    CALL_MADE,
    CALL_RECEIVED
}
