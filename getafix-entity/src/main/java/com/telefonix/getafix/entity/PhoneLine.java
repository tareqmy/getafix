package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"phone", "sipUserName"})
@Table(name = "phone_lines")
public class PhoneLine implements Comparable<PhoneLine> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_lines_id_seq")
    @SequenceGenerator(name = "phone_lines_id_seq", sequenceName = "phone_lines_id_seq", allocationSize = 1)
    private Long id;

    @Column(length = 50, nullable = false)
    private String description;

    @ManyToOne()
    @JoinColumn(name = "phone_id", nullable = false)
    private Phone phone;

    @Column(name = "caller_name", length = 20, nullable = false)
    private String callerName;

    @OneToOne()
    @JoinColumn(name = "extension_id", nullable = false)
    private Extension extension;

    @Column(name = "sip_user_name", length = 50, nullable = false)
    private String sipUserName;

    @Column(name = "sip_password", length = 50, nullable = false)
    private String sipPassword;

    @Column(name = "call_waiting", nullable = false)
    private boolean callWaitingEnabled;

    @OneToOne(mappedBy = "joinedPhoneLine")
    private User joinedUser;

    @Override
    public String toString() {
        return "PhoneLine{" +
            "id=" + id +
            ", description='" + description + '\'' +
            ", phone=" + phone.getPhoneName() +
            ", callerName='" + callerName + '\'' +
            ", extension=" + extension +
            ", sipUserName='" + sipUserName + '\'' +
            ", sipPassword='" + sipPassword + '\'' +
            ", callWaitingEnabled=" + callWaitingEnabled +
            '}';
    }

    @Override
    public int compareTo(PhoneLine o) {
        return getExtension().compareTo(o.getExtension());
    }
}
