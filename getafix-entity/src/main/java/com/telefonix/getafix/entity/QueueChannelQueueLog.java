package com.telefonix.getafix.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by tareqmy on 8/9/19.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"queueChannelLog", "queued"})
@Table(name = "queue_channel_queue_logs")
public class QueueChannelQueueLog implements Comparable<QueueChannelQueueLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queue_channel_queue_logs_id_seq")
    @SequenceGenerator(name = "queue_channel_queue_logs_id_seq", sequenceName = "queue_channel_queue_logs_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "queue_channel_log_id", nullable = false)
    private QueueChannelLog queueChannelLog;

    @Column(name = "queue_id", nullable = false)
    private Long queueId;

    @Column(name = "base_extension", nullable = false)
    private String baseExtension;

    @Column(name = "call_queued", nullable = false)
    private Instant queued;

    @Column(name = "call_left")
    private Instant left;

    //NOTE: abandoned is always before left queue
    @Column(name = "call_abandoned")
    private Instant abandoned;

    @Column(name = "call_handled")
    private Instant handled;

    @Override
    public String toString() {
        return "QueueChannelQueueLog{" +
            "id=" + id +
            ", queueChannelLog=" + queueChannelLog.getId() +
            ", queueId=" + queueId +
            ", baseExtension='" + baseExtension + '\'' +
            ", queued=" + queued +
            ", left=" + left +
            ", abandoned=" + abandoned +
            ", handled=" + handled +
            '}';
    }

    @Override
    public int compareTo(QueueChannelQueueLog o) {
        return o.getQueued().compareTo(getQueued());
    }
}
