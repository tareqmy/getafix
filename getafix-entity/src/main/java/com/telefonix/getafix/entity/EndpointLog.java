package com.telefonix.getafix.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by tareqmy on 2019-08-20.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
@Table(name = "endpoint_logs")
public class EndpointLog implements Comparable<EndpointLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "endpoint_logs_id_seq")
    @SequenceGenerator(name = "endpoint_logs_id_seq", sequenceName = "endpoint_logs_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "event_instant", nullable = false)
    private Instant eventInstant;

    @Column(name = "resource", length = 40, nullable = false)
    private String resource;

    @Column(name = "company_id", length = 12)
    private String companyId;

    @Column(name = "phone_line_id")
    private Long phoneLineId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_name", length = 52)
    private String userName;

    @Column(name = "endpoint_state", length = 12, nullable = false)
    private String endpointState;

    @Column(name = "device_state", length = 12, nullable = false)
    private String deviceState;

    @Override
    public int compareTo(EndpointLog o) {
        return o.getEventInstant().compareTo(getEventInstant());
    }
}
