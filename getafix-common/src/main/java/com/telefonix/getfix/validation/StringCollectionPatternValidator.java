package com.telefonix.getfix.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by tareqmy on 2019-05-06.
 */
public class StringCollectionPatternValidator implements ConstraintValidator<StringCollectionPattern, Iterable<String>> {

    private String regexp;

    @Override
    public void initialize(StringCollectionPattern constraintAnnotation) {
        this.regexp = constraintAnnotation.regexp();
    }

    @Override
    public boolean isValid(Iterable<String> items, ConstraintValidatorContext context) {
        if (items != null) {
            for (String next : items) {
                if (!next.matches(this.regexp)) {
                    return false;
                }
            }
        }

        return true;//Or throw exceptions when list is null
    }
}
