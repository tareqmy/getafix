package com.telefonix.getfix.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by tareqmy on 2019-05-06.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {StringCollectionPatternValidator.class})
@Documented
public @interface StringCollectionPattern {
    String regexp();

    String message() default "{com.telefonix.getfix.validation.StringCollectionPattern.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
