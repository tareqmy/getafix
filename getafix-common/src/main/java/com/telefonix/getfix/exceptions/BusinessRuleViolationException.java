package com.telefonix.getfix.exceptions;

/**
 * Created by tareqmy on 2019-05-06.
 */
public class BusinessRuleViolationException extends RuntimeException {

    public BusinessRuleViolationException(String message) {
        super(message);
    }

    public BusinessRuleViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessRuleViolationException(Throwable cause) {
        super(cause);
    }

    public BusinessRuleViolationException() {
        super("This action violates business rule!");
    }
}
