package com.telefonix.getfix.exceptions;

/**
 * Created by tareqmy on 2019-05-06.
 */
public class GenericGetafixException extends RuntimeException {

    public GenericGetafixException(String message) {
        super(message);
    }

    public GenericGetafixException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericGetafixException(Throwable cause) {
        super(cause);
    }
}
