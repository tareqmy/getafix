package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Company;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by tareqmy on 3/1/19.
 */
@Data
@NoArgsConstructor
public class CompanyDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9]+$")
    @Size(min = 3, max = 12)
    private String companyId;

    @NotNull
    @Size(min = 5, max = 50)
    private String companyName;

    @Size(max = 125)
    private String description;

    private boolean enabled = true;

    @NotNull
    private ZoneId zoneId;

    @Email
    @Size(max = 50)
    private String email;

    @Size(max = 25)
    private String phone;

    @Size(max = 255)
    private String notes;

    public CompanyDTO(Company company) {
        setId(company.getId());
        setCompanyId(company.getCompanyId());
        setCompanyName(company.getCompanyName());
        setDescription(company.getDescription());
        setEnabled(company.isEnabled());
        setZoneId(company.getZoneId());
        setEmail(company.getEmail());
        setPhone(company.getPhone());
        setNotes(company.getNotes());
    }

    public static Optional<CompanyDTO> getDTO(Company company) {
        if (company == null)
            return Optional.empty();
        CompanyDTO dto = new CompanyDTO(company);
        return Optional.of(dto);
    }
}
