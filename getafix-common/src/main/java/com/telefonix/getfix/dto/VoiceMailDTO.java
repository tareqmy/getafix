package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.VoiceMail;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-23.
 */
@Data
@NoArgsConstructor
public class VoiceMailDTO {

    @NotNull
    private Long id;

    private Long extensionId;

    private String baseExtension;

    private boolean enabled;

    private String fullName;

    @Email
    private String email;

    public VoiceMailDTO(VoiceMail voiceMail) {
        setId(voiceMail.getId());
        setExtensionId(voiceMail.getExtension().getId());
        setBaseExtension(voiceMail.getExtension().getBaseExtension());
        setEnabled(voiceMail.isEnabled());
        setFullName(voiceMail.getFullName());
        setEmail(voiceMail.getEmail());
    }

    public static Optional<VoiceMailDTO> getDTO(VoiceMail voiceMail) {
        if (voiceMail == null)
            return Optional.empty();
        VoiceMailDTO dto = new VoiceMailDTO(voiceMail);
        return Optional.of(dto);
    }
}
