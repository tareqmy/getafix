package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Conference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Data
@NoArgsConstructor
public class ConferenceDTO {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Long companyId;

    private String companyName;

    private String leaderCode;

    private String participantCode;

    public ConferenceDTO(Conference conference) {
        setId(conference.getId());
        setName(conference.getName());
        setCompanyId(conference.getCompany().getId());
        setCompanyName(conference.getCompany().getCompanyName());
        setLeaderCode(conference.getLeaderCode());
        setParticipantCode(conference.getParticipantCode());
    }

    public static Optional<ConferenceDTO> getDTO(Conference conference) {
        if (conference == null)
            return Optional.empty();
        ConferenceDTO dto = new ConferenceDTO(conference);
        return Optional.of(dto);
    }
}
