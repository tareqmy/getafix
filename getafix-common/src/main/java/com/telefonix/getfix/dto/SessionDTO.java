package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Created by tareqmy on 28/8/19.
 */
@Data
@NoArgsConstructor
public class SessionDTO {

    private LocalDateTime start;

    private LocalDateTime end;

    private long duration = 0;

    private String time = "00:00:00";
}
