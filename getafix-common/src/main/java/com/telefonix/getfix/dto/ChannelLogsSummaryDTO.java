package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.User;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-30.
 */
@Data
@NoArgsConstructor
public class ChannelLogsSummaryDTO {

    private Long extensionId;

    private String extension;

    private String userName;

    private String from;

    private String to;

    private Long calls;

    private String callsTime;

    private Long incomingCalls;

    private String incomingCallsTime;

    private Long outgoingCalls;

    private String outgoingCallsTime;

    public ChannelLogsSummaryDTO(Extension extension, List<ChannelLog> channelLogs, LocalDate from, LocalDate to) {
        long outCalls = 0;
        long inCalls = 0;
        long outTime = 0;
        long inTime = 0;
        for (ChannelLog channelLog : channelLogs) {

            if (!channelLog.getCallType().equals("Personal")) {
                if (!channelLog.isCallInitiator()) {
                    inCalls++;
                    inTime += channelLog.getDuration();
                } else {
                    outCalls++;
                    outTime += channelLog.getDuration();
                }
            } else {
                if (channelLog.isCaller()) {
                    outCalls++;
                    outTime += channelLog.getDuration();
                } else {
                    inCalls++;
                    inTime += channelLog.getDuration();
                }
            }
        }

        setExtensionId(extension.getId());
        setExtension(extension.getBaseExtension());
        User user = extension.getUser();
        if (user != null) {
            setUserName(user.getFullName());
        }

        setFrom(MiscUtils.formatDateTime(from.atStartOfDay()));
        setTo(MiscUtils.formatDateTime(to.plusDays(1).atStartOfDay()));

        setIncomingCalls(inCalls);
        setOutgoingCalls(outCalls);
        setIncomingCallsTime(MiscUtils.formatDuration(inTime));
        setOutgoingCallsTime(MiscUtils.formatDuration(outTime));
        setCalls(inCalls + outCalls);
        setCallsTime(MiscUtils.formatDuration(inTime + outTime));
    }
}
