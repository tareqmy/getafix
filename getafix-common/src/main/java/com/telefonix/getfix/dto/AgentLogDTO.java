package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-08-11.
 */
@Data
@NoArgsConstructor
public class AgentLogDTO {

    private Long id;

    private String agentEvent;

    private String eventDateTime;

    private String companyId;

    private Long userId;

    private String userName;

    private Long queueId;

    private String queueName;

    private Long phoneLineId;

    private String sipUserName;

    public AgentLogDTO(AgentLog agentLog, ZoneId zoneId) {
        setId(agentLog.getId());
        setAgentEvent(agentLog.getAgentEvent().name());
        setEventDateTime(MiscUtils.formatDateTime(agentLog.getEventInstant(), zoneId));
        setCompanyId(agentLog.getCompanyId());
        setUserId(agentLog.getUserId());
        setUserName(agentLog.getUserName());
        setQueueId(agentLog.getQueueId());
        setQueueName(agentLog.getQueueName());
        setPhoneLineId(agentLog.getPhoneLineId());
        setSipUserName(agentLog.getSipUserName());
    }

    public static Optional<AgentLogDTO> getDTO(AgentLog agentLog, ZoneId zoneId) {
        if (agentLog == null)
            return Optional.empty();
        AgentLogDTO dto = new AgentLogDTO(agentLog, zoneId);
        return Optional.of(dto);
    }
}
