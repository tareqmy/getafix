package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.EndpointLog;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-08-20.
 */
@Data
@NoArgsConstructor
public class EndpointLogDTO {

    private Long id;

    private String eventDateTime;

    private String resource;

    private String companyId;

    private Long phoneLineId;

    private Long userId;

    private String userName;

    private String endpointState;

    private String deviceState;

    public EndpointLogDTO(EndpointLog endpointLog, ZoneId zoneId) {
        setId(endpointLog.getId());
        setEventDateTime(MiscUtils.formatDateTime(endpointLog.getEventInstant(), zoneId));
        setResource(endpointLog.getResource());
        setCompanyId(endpointLog.getCompanyId());
        setPhoneLineId(endpointLog.getPhoneLineId());
        setUserId(endpointLog.getUserId());
        setUserName(endpointLog.getUserName());
        setEndpointState(StringUtils.upperCase(endpointLog.getEndpointState()));
        setDeviceState(endpointLog.getDeviceState());
    }

    public static Optional<EndpointLogDTO> getDTO(EndpointLog endpointLog, ZoneId zoneId) {
        if (endpointLog == null)
            return Optional.empty();
        EndpointLogDTO dto = new EndpointLogDTO(endpointLog, zoneId);
        return Optional.of(dto);
    }
}
