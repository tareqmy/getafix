package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by tmyousuf on 4/9/18.
 */
@Data
@NoArgsConstructor
public class LoginDTO {

    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String email;

    @NotNull
    @Size(min = 4, max = 100)
    private String password;

    private Boolean rememberMe;
}

