package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by tareqmy on 2019-08-04.
 */
@Data
@NoArgsConstructor
public class SoundFile {

    private String fileName;

    private byte[] bytes;

    @Override
    public String toString() {
        return "SoundFile{" +
            "fileName='" + fileName + '\'' +
            '}';
    }
}
