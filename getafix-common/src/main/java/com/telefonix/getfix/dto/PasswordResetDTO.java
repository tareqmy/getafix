package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PasswordResetDTO {

    private String email;
    private String token;
    private String newPassword;
}
