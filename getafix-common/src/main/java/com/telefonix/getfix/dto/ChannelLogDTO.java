package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.ChannelLog;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneId;

/**
 * Created by tareqmy on 2019-06-17.
 */
@Data
@NoArgsConstructor
public class ChannelLogDTO {

    private Long id;

    private String uniqueId;

    private String accountCode;

    private String companyId;

    private Long userId;

    private String userName;

    private Long extensionId;

    private String baseExtension;

    private String phoneName;

    private String linkedId;

    private String from;

    private String to;

    private String callerName;

    private String callerNumber;

    private String calledNumber;

    private boolean caller;

    private boolean callInitiator;

    private boolean external;

    private String dateTime;

    private String date;

    private String time;

    private String duration;

    private String status;

    private String callType;

    public ChannelLogDTO(ChannelLog channelLog, ZoneId zoneId) {
        setId(channelLog.getId());
        setUniqueId(channelLog.getUniqueId());
        setAccountCode(channelLog.getAccountCode());
        setCompanyId(channelLog.getCompanyId());
        setUserId(channelLog.getUserId());
        setUserName(channelLog.getUserName());
        setExtensionId(channelLog.getExtensionId());
        setBaseExtension(channelLog.getBaseExtension());
        setPhoneName(channelLog.getPhoneName());
        setLinkedId(channelLog.getLinkedId());
        setFrom(channelLog.getFrom());
        setTo(channelLog.getTo());
        setCallerName(channelLog.getCallerName());
        setCallerNumber(channelLog.getCallerNumber());
        setCalledNumber(channelLog.getCalledNumber());
        setCaller(channelLog.isCaller());
        setCallInitiator(channelLog.isCallInitiator());
        setExternal(channelLog.isExternal());
        setDateTime(MiscUtils.formatDateTime(channelLog.getStart(), zoneId));
        setDate(MiscUtils.formatDate(channelLog.getStart(), zoneId));
        setTime(MiscUtils.formatTime(channelLog.getStart(), zoneId));

        setDuration(MiscUtils.formatDuration(channelLog.getDuration()));
        setStatus(channelLog.getStatus().name());
        setCallType(channelLog.getCallType());
    }
}
