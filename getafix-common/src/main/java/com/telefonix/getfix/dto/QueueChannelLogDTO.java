package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.QueueChannelQueueLog;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.ZoneId;

/**
 * Created by tareqmy on 18/9/19.
 */
@Data
@NoArgsConstructor
public class QueueChannelLogDTO {

    private Long id;

    private String uniqueId;

    private String companyId;

    private Long queueId;

    private String linkedId;

    private String from;

    private String to;

    private String callerName;

    private String callerNumber;

    private String calledNumber;

    private boolean external;

    private boolean outbound;

    private String queuedDateTime;

    private String date;

    private String time;

    private String queuedDuration;

    private String status;

    public QueueChannelLogDTO(QueueChannelQueueLog queueChannelQueueLog, ZoneId zoneId) {
        setId(queueChannelQueueLog.getId());
        setUniqueId(queueChannelQueueLog.getQueueChannelLog().getUniqueId());
        setCompanyId(queueChannelQueueLog.getQueueChannelLog().getCompanyId());
        setQueueId(queueChannelQueueLog.getQueueId());
        setLinkedId(queueChannelQueueLog.getQueueChannelLog().getLinkedId());
        setFrom(queueChannelQueueLog.getQueueChannelLog().getFrom());
        setTo(queueChannelQueueLog.getQueueChannelLog().getTo());
        setCallerName(queueChannelQueueLog.getQueueChannelLog().getCallerName());
        setCallerNumber(queueChannelQueueLog.getQueueChannelLog().getCallerNumber());
        setCalledNumber(queueChannelQueueLog.getQueueChannelLog().getCalledNumber());
        setExternal(queueChannelQueueLog.getQueueChannelLog().isExternal());
        setOutbound(queueChannelQueueLog.getQueueChannelLog().isOutbound());

        setQueuedDateTime(MiscUtils.formatDateTime(queueChannelQueueLog.getQueued(), zoneId));
        setDate(MiscUtils.formatDate(queueChannelQueueLog.getQueued(), zoneId));
        setTime(MiscUtils.formatTime(queueChannelQueueLog.getQueued(), zoneId));

        Duration between = MiscUtils.getDuration(queueChannelQueueLog.getQueued(), queueChannelQueueLog.getLeft());
        setQueuedDuration(MiscUtils.formatDuration(between.getSeconds()));

        setStatus(queueChannelQueueLog.getQueueChannelLog().getStatus().name());
    }
}
