package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Phone;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Data
@NoArgsConstructor
public class PhoneDTO {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String phoneName;

    private int lineCount;

    @NotNull
    private Long companyId;

    private String companyName;

    public PhoneDTO(Phone phone) {
        setId(phone.getId());
        setPhoneName(phone.getPhoneName());
        setLineCount(phone.getPhoneLines().size());
        setCompanyId(phone.getCompany().getId());
        setCompanyName(phone.getCompany().getCompanyName());
    }

    public static Optional<PhoneDTO> getDTO(Phone phone) {
        if (phone == null)
            return Optional.empty();
        PhoneDTO dto = new PhoneDTO(phone);
        return Optional.of(dto);
    }
}
