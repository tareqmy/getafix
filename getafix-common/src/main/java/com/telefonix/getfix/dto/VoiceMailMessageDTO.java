package com.telefonix.getfix.dto;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import com.telefonix.getfix.utils.MiscUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RegExUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-07-28.
 */
@Data
@NoArgsConstructor
public class VoiceMailMessageDTO {

    private String dir;

    private String msgNum;

    private String context;

    private String macroContext;

    private String callerId;

    private String origTime;

    private String duration;

    private String flag;

    private String mailboxUser;

    private String mailboxContext;

    private String msgId;

    public VoiceMailMessageDTO(AXVoiceMessage axVoiceMessage, ZoneId zoneId) {
        String dir = axVoiceMessage.getAxVoiceMessageId().getDir();
        setDir(RegExUtils.removePattern(dir, "^/.*/"));
        setMsgNum(axVoiceMessage.getAxVoiceMessageId().getMsgNum());
        setContext(axVoiceMessage.getContext());
        setMacroContext(axVoiceMessage.getMacroContext());
        setCallerId(axVoiceMessage.getCallerId());
        if (axVoiceMessage.getOrigTime() != null) {
            setOrigTime(MiscUtils.formatDate(Instant.ofEpochSecond(Long.parseLong(axVoiceMessage.getOrigTime())), zoneId));
        }
        setDuration(axVoiceMessage.getDuration());
        setFlag(axVoiceMessage.getFlag());
        setMailboxUser(axVoiceMessage.getMailboxUser());
        setMailboxContext(axVoiceMessage.getMailboxContext());
        setMsgId(axVoiceMessage.getMsgId());
    }

    public static Optional<VoiceMailMessageDTO> getDTO(AXVoiceMessage axVoiceMessage, ZoneId zoneId) {
        if (axVoiceMessage == null)
            return Optional.empty();
        VoiceMailMessageDTO dto = new VoiceMailMessageDTO(axVoiceMessage, zoneId);
        return Optional.of(dto);
    }
}
