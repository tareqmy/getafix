package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 28/8/19.
 */
@Data
@NoArgsConstructor
public class AgentSessionDTO {

    private String sipUserName;

    private SessionDTO session = new SessionDTO();

    private List<SessionDTO> joined = new ArrayList<>();

    private List<SessionDTO> pauses = new ArrayList<>();

    private long joinedDuration;

    private long unJoinedDuration;

    private long pausedDuration;

    //in queue logs connect means answered
    private long acdCallsAnswered;

    //in queue logs completecaller
    private long acdCallsEndByCaller;

    //in queue logs completeagent
    private long acdCallsEndByAgent;

    //in queue logs blindtransfer
    private long acdCallsTransferred;

    //in queue logs ringnoanswer
    private long acdCallsFailed;
}
