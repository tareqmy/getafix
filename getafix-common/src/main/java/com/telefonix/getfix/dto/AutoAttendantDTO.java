package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.AutoAttendant;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-05.
 */
@Data
@NoArgsConstructor
public class AutoAttendantDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9]+$")
    @Size(min = 3, max = 12)
    private String autoAttendantId;

    @NotNull
    @Size(min = 5, max = 50)
    private String autoAttendantName;

    @NotNull
    private Long extensionId;

    private String baseExtension;

    @NotNull
    private Long companyId;

    private String companyName;

    private Map<String, String> optionsMap = new HashMap<>();

    public AutoAttendantDTO(AutoAttendant autoAttendant) {
        setId(autoAttendant.getId());
        setAutoAttendantId(autoAttendant.getAutoAttendantId());
        setAutoAttendantName(autoAttendant.getAutoAttendantName());
        setExtensionId(autoAttendant.getExtension().getId());
        setBaseExtension(autoAttendant.getExtension().getBaseExtension());
        setCompanyId(autoAttendant.getCompany().getId());
        setCompanyName(autoAttendant.getCompany().getCompanyName());
        setOptionsMap(autoAttendant.getOptionsMap());
    }

    public static Optional<AutoAttendantDTO> getDTO(AutoAttendant autoAttendant) {
        if (autoAttendant == null)
            return Optional.empty();
        AutoAttendantDTO dto = new AutoAttendantDTO(autoAttendant);
        return Optional.of(dto);
    }
}
