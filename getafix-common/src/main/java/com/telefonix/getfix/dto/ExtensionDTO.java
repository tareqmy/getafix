package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Extension;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-14.
 */
@Data
@NoArgsConstructor
public class ExtensionDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[0-9]{2}$")
    private String baseExtension;

    @Range(min = 6, max = 99)
    private Integer ringDuration;

    @NotNull
    private Long companyId;

    private String companyName;

    private Long userId;

    private String userName;

    private Long voiceMailId;

    private Long phoneLineId;

    private String sipUserName;

    public ExtensionDTO(Extension extension) {
        setId(extension.getId());
        setBaseExtension(extension.getBaseExtension());
        setRingDuration(extension.getRingDuration());
        setCompanyId(extension.getCompany().getId());
        setCompanyName(extension.getCompany().getCompanyName());
        if (extension.getUser() != null) {
            setUserId(extension.getUser().getId());
            setUserName(extension.getUser().getFullName());
        }
        setVoiceMailId(extension.getVoiceMail().getId());

        if (extension.getPhoneLine() != null) {
            setPhoneLineId(extension.getPhoneLine().getId());
            setSipUserName(extension.getPhoneLine().getSipUserName());
        }
    }

    public static Optional<ExtensionDTO> getDTO(Extension extension) {
        if (extension == null)
            return Optional.empty();
        ExtensionDTO dto = new ExtensionDTO(extension);
        return Optional.of(dto);
    }
}
