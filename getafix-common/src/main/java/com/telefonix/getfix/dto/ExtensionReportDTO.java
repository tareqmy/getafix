package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by tareqmy on 2019-07-08.
 */
@Data
@NoArgsConstructor
public class ExtensionReportDTO {

    private String dayString;

    private Long callCount;
    private Long incomingCallCount;
    private Long outgoingCallCount;

    private Long callDuration;
    private Long incomingCallDuration;
    private Long outgoingCallDuration;
}
