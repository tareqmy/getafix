package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by tmyousuf on 22/11/17.
 */
@Data
@NoArgsConstructor
public class ApplicationDTO {

    private String applicationVersion;

    private String activeProfile;

    private String developerEmail;

    private String developerName;

    private String developerWeb;
}
