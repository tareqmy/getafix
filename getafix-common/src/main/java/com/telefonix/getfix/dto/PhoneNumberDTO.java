package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.PhoneNumber;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Data
@NoArgsConstructor
public class PhoneNumberDTO {

    @NotNull
    @Pattern(regexp = "^\\d{11,14}$")
    private String number;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;

    @NotNull
    private Long providerId;

    private String providerName;

    @NotNull
    private Long companyId;

    private String companyName;

    @NotNull
    private Long extensionId;

    private String baseExtension;

    public PhoneNumberDTO(PhoneNumber phoneNumber) {
        setNumber(phoneNumber.getNumber());
        setPassword(phoneNumber.getPassword());
        setProviderId(phoneNumber.getProvider().getId());
        setProviderName(phoneNumber.getProvider().getProviderId());
        setCompanyId(phoneNumber.getCompany().getId());
        setCompanyName(phoneNumber.getCompany().getCompanyName());
        setExtensionId(phoneNumber.getExtension().getId());
        setBaseExtension(phoneNumber.getExtension().getBaseExtension());
    }

    public static Optional<PhoneNumberDTO> getDTO(PhoneNumber phoneNumber) {
        if (phoneNumber == null)
            return Optional.empty();
        PhoneNumberDTO dto = new PhoneNumberDTO(phoneNumber);
        return Optional.of(dto);
    }
}
