package com.telefonix.getfix.dto;

/**
 * Created by tmyousuf on 4/9/18.
 */
public class JWTToken {

    private String token;

    public JWTToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
