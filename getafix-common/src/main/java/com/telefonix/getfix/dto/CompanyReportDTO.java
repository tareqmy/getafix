package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 24/10/19.
 */
@Data
@NoArgsConstructor
public class CompanyReportDTO {

    private List<QueueReportDTO> queueReports = new ArrayList<>();

    private List<AgentReportDTO> agentReports = new ArrayList<>();

    private long totalCalls;

    private long totalOverflowed;

    private long totalReceived;

    private long totalAnswered;

    private long totalTransferred;

    private long totalUnanswered;

    private long totalAbandoned;

    private String percentAbandoned = "0";

    private long totalAgents;

    private long totalSessions;

    private long averageSessionDuration;

    private String averageSessionTime;

    private long shortestSessionDuration;

    private String shortestSessionTime;

    private long longestSessionDuration;

    private String longestSessionTime;

    private long totalSessionDuration;

    private String totalSessionTime;
}
