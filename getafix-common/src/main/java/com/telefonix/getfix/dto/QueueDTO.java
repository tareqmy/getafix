package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Queue;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-28.
 */
@Data
@NoArgsConstructor
public class QueueDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9]+$")
    @Size(min = 3, max = 12)
    private String queueId;

    @NotNull
    @Size(min = 5, max = 50)
    private String queueName;

    @Email
    private String email;

    @NotNull
    private Long extensionId;

    private String baseExtension;

    @NotNull
    private Long companyId;

    private String companyName;

    @NotNull
    private String strategy;

    @Range(min = 0, max = 100)
    private int weight;

    @Range(min = 30, max = 300)
    private int serviceLevel;

    @Range(min = 0, max = 100)
    private int maxLength;

    private boolean playInitialMessage;

    private String initialMessageFileName;

    private boolean playMusicOnHold;

    private String mohFileName;

    private boolean playComfortMessage;

    private String comfortMessageFileName;

    @Range(min = 15, max = 120)
    private Integer comfortMessageInterval;

    private boolean zeroEscapeEnabled;

    public QueueDTO(Queue queue) {
        setId(queue.getId());
        setQueueId(queue.getQueueId());
        setQueueName(queue.getQueueName());
        setEmail(queue.getEmail());
        setExtensionId(queue.getExtension().getId());
        setBaseExtension(queue.getExtension().getBaseExtension());
        setCompanyId(queue.getCompany().getId());
        setCompanyName(queue.getCompany().getCompanyName());
        setStrategy(queue.getStrategy().name());
        setWeight(queue.getWeight());
        setServiceLevel(queue.getServiceLevel());
        setMaxLength(queue.getMaxLength());
        setPlayInitialMessage(queue.isPlayInitialMessage());
        setInitialMessageFileName(queue.getInitialMessageFileName());
        setPlayMusicOnHold(queue.isPlayMusicOnHold());
        setMohFileName(queue.getMohFileName());
        setPlayComfortMessage(queue.isPlayComfortMessage());
        setComfortMessageFileName(queue.getComfortMessageFileName());
        setComfortMessageInterval(queue.getComfortMessageInterval());
        setZeroEscapeEnabled(queue.isZeroEscapeEnabled());
    }

    public static Optional<QueueDTO> getDTO(Queue queue) {
        if (queue == null)
            return Optional.empty();
        QueueDTO dto = new QueueDTO(queue);
        return Optional.of(dto);
    }
}
