package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.PhoneLine;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Data
@NoArgsConstructor
public class PhoneLineDTO {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String description;

    @NotNull
    private Long phoneId;

    private String phoneName;

    @Size(max = 20)
    private String callerName;

    @NotNull
    private Long extensionId;

    private String baseExtension;

    @Size(max = 50)
    private String sipUserName;

    @NotNull
    @Size(max = 50)
    @Pattern(regexp = "^[A-Za-z0-9\\\\!$(),-.@^_{|}~\\[\\]]+$")
    private String sipPassword;

    @NotNull
    private boolean callWaitingEnabled;

    public PhoneLineDTO(PhoneLine phoneLine) {
        setId(phoneLine.getId());
        setDescription(phoneLine.getDescription());
        setPhoneId(phoneLine.getPhone().getId());
        setPhoneName(phoneLine.getPhone().getPhoneName());
        setCallerName(phoneLine.getCallerName());
        setExtensionId(phoneLine.getExtension().getId());
        setBaseExtension(phoneLine.getExtension().getBaseExtension());
        setSipUserName(phoneLine.getSipUserName());
        setSipPassword(phoneLine.getSipPassword());
        setCallWaitingEnabled(phoneLine.isCallWaitingEnabled());
    }

    public static Optional<PhoneLineDTO> getDTO(PhoneLine phoneLine) {
        if (phoneLine == null)
            return Optional.empty();
        PhoneLineDTO dto = new PhoneLineDTO(phoneLine);
        return Optional.of(dto);
    }
}
