package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.*;

/**
 * Created by tareqmy on 4/9/19.
 */
@Data
@NoArgsConstructor
public class QueueReportDTO {

    private Long queueId;

    private String uniqueQueueId;

    private String queueName;

    private Instant start;

    private Instant end;

    private ZoneId zoneId;

    private LocalDateTime from;

    private LocalDateTime to;

    private Period period;

    private Duration duration;

    //queue call treatment
    private long total; //received + overflowed

    private long overflowed; //full + joinempty

    private long received; //answered + unanswered + transferred + abandoned //enterqueue

    private long answered; //completecaller + completeagent

    private long transferred; //transferred

    private long unanswered; //timeout + leaveempty

    private long abandoned; //abandoned

    private String percentAnswered = "0";

    private String percentTransferred = "0";

    private String percentUnanswered = "0";

    private String percentAbandoned = "0";

    private long logins;

    private long pauses;

    private long resumes;

    private long logoffs;

    private long maxCallers;

    private long maxJoinedAgents;

    private long callsWithinSLA;

    private float percentSLA;

    private long totalWaitDuration;

    private String totalWaitTime;

    private long totalTalkDuration;

    private String totalTalkTime;

    private float averageWaitDuration;

    private String averageWaitTime;

    private float averageTalkDuration;

    private String averageTalkTime;

    private long totalOutbound;

    private long answeredOutbound;

    private long unansweredOutbound;

    private long totalRingOutboundDuration;

    private String totalRingOutboundTime;

    private long totalTalkOutboundDuration;

    private String totalTalkOutboundTime;

    private float averageRingOutboundDuration;

    private String averageRingOutboundTime;

    private float averageTalkOutboundDuration;

    private String averageTalkOutboundTime;

    private String percentAnsweredOutbound = "0";

    private String percentUnansweredOutbound = "0";
}
