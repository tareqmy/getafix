package com.telefonix.getfix.dto;

import com.telefonix.getafix.entity.Provider;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Data
@NoArgsConstructor
public class ProviderDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    @Size(min = 3, max = 50)
    private String providerId;

    @NotNull
    @Pattern(regexp = "^([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}$")
    private String host;

    public ProviderDTO(Provider provider) {
        setId(provider.getId());
        setProviderId(provider.getProviderId());
        setHost(provider.getHost());
    }

    public static Optional<ProviderDTO> getDTO(Provider provider) {
        if (provider == null)
            return Optional.empty();
        ProviderDTO dto = new ProviderDTO(provider);
        return Optional.of(dto);
    }
}
