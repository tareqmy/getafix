package com.telefonix.getfix.dto;


import com.telefonix.getafix.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by tmyousuf on 7/8/18.
 */
@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;

    @Email
    @Size(min = 5, max = 50)
    private String email;

    @NotNull
    @Size(min = 6, max = 32)
    private String password;

    @NotNull
    @Size(min = 2, max = 25)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 25)
    private String lastName;

    private String fullName;

    private boolean enabled;

    private boolean locked;

    @NotNull
    private ZoneId zoneId;

    private boolean emailConfirmed;

    @NotNull
    private String userRole;

    @NotNull
    private Long companyId;

    private String companyName;

    public UserDTO(User user) {
        setId(user.getId());
        setEmail(user.getEmail());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setFullName(user.getFullName());
//        setPassword(user.getPassword());
        setEnabled(user.isEnabled());
        setLocked(user.isLocked());
        setZoneId(user.getZoneId());
        setEmailConfirmed(user.isEmailConfirmed());
        setUserRole(user.getUserRole().getRole().toString());
        setCompanyId(user.getCompany().getId());
        setCompanyName(user.getCompany().getCompanyName());
    }

    public static Optional<UserDTO> getDTO(User user) {
        if (user == null)
            return Optional.empty();
        UserDTO dto = new UserDTO(user);
        return Optional.of(dto);
    }
}
