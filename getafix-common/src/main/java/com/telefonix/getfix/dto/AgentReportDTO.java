package com.telefonix.getfix.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 28/8/19.
 */
@Data
@NoArgsConstructor
public class AgentReportDTO {

    private Long userId;

    private String userName;

    private ZoneId zoneId;

    private LocalDateTime from;

    private LocalDateTime to;

    private Period period;

    private Duration duration;

    //agent availability

    private long totalSessionDuration;

    private String totalSessionTime;

    private long totalJoinedDuration;

    private String totalJoinedTime;

    private long totalUnJoinedDuration;

    private String totalUnJoinedTime;

    //session = notjoined + joined ( idle + paused + ringtime + talktime )

    private long totalPausedDuration;

    private String totalPausedTime;

    private long totalCallDuration;

    private String totalCallTime;

    private long totalQueueCallRingDuration;

    private String totalQueueCallRingTime;

    private long totalQueueCallTalkDuration;

    private String totalQueueCallTalkTime;

    private long totalIdleDuration;

    private String totalIdleTime;

    private String percentSession;

    private long totalCalls;

    private long totalQueueCalls;

    private long totalQueueCallsInbound;

    private long totalQueueCallsOutbound;

    private long totalQueueCallsAnswered;

    private long totalQueueCallsUnanswered;

    //call durations/call count
    private float averageHandleDuration;

    private String averageHandleTime;

    //agent session and pause durations

    private long sessions;

    private long joined;

    private long pauses;

    private float averageSessionDuration;

    private String averageSessionTime;

    private float averageJoinedDuration;

    private String averageJoinedTime;

    private float averagePausedDuration;

    private String averagePausedTime;

    private String percentJoined = "0";

    private String percentPaused = "0";

    private String pausesPerSession = "0";

    //call disposition

    private long totalACDCallsAnswered;

    private long totalACDCallsEndByCaller;

    private long totalACDCallsEndByAgent;

    private long totalACDCallsTransferred;

    private long totalACDCallsFailed;

    private List<AgentSessionDTO> agentSessions = new ArrayList<>();
}
