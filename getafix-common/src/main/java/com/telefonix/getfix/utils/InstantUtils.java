package com.telefonix.getfix.utils;

import lombok.Getter;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by tareqmy on 11/9/19.
 */
public class InstantUtils {

    private InstantUtils() {
    }

    public static long maxConcurrent(List<Instant> starts, List<Instant> ends) {
        long max = 0;
        List<SessionInstant> sessionInstants = new ArrayList<>();
        starts.forEach(instant -> {
            sessionInstants.add(new SessionInstant(instant, InstantType.start));
        });
        ends.forEach(instant -> {
            sessionInstants.add(new SessionInstant(instant, InstantType.end));
        });

        long current = 0;
        List<SessionInstant> sorted = sessionInstants.stream().sorted().collect(Collectors.toList());

        for (SessionInstant sessionInstant : sorted) {
            switch (sessionInstant.getInstantType()) {
                case start:
                    current++;
                    break;
                case end:
                    current--;
                    break;
            }
            if (current > max) {
                max = current;
            }
        }
        return max;
    }

    public static Instant getCurrentFloorHalfHour() {
        Instant now = Instant.now();
        Instant floorHour = now.truncatedTo(ChronoUnit.HOURS);
        Duration between = Duration.between(floorHour, now);
        if (between.toMinutes() > 30) {
            return floorHour.plus(30, ChronoUnit.MINUTES);
        }
        return floorHour;
    }

    public static Instant getFloorHalfHour(Instant now) {
        //if already a half hour send it back
        if (now.atZone(ZoneId.systemDefault()).getMinute() % 30 == 0) {
            return now;
        }

        Instant floorHour = now.truncatedTo(ChronoUnit.HOURS);
        Duration between = Duration.between(floorHour, now);
        if (between.toMinutes() > 30) {
            return floorHour.plus(30, ChronoUnit.MINUTES);
        }
        return floorHour;
    }

    public static List<Instant> getHalfHourInstants(Instant fromInclusive, Instant toInclusive) {
        Instant start = fromInclusive;
        Instant end = toInclusive;
        if (fromInclusive.isAfter(toInclusive)) {
            start = toInclusive;
            end = fromInclusive;
        }
        start = getFloorHalfHour(start);
        end = getFloorHalfHour(end);

        List<Instant> instants = new ArrayList<>();
        if (start.equals(end)) {
            instants.add(start);
            return instants;
        }

        Instant intermediate = start;
        instants.add(start);
        while (!intermediate.equals(end)) {
            intermediate = intermediate.plus(30, ChronoUnit.MINUTES);
            instants.add(intermediate);
        }

        return instants;
    }

    private enum InstantType {
        start,
        end
    }

    @Getter
    private static class SessionInstant implements Comparable<SessionInstant> {

        private Instant instant;

        private InstantType instantType;

        SessionInstant(Instant instant, InstantType instantType) {
            this.instant = instant;
            this.instantType = instantType;
        }

        @Override
        public int compareTo(SessionInstant o) {
            return instant.compareTo(o.instant);
        }
    }
}
