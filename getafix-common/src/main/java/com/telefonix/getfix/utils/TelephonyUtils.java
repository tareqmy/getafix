package com.telefonix.getfix.utils;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tareqmy on 2019-01-23.
 */
public class TelephonyUtils {

    private final static String baseExtensionRegex = "^(\\d{2})$";

    private final static String internalEntityRegex = "^(.*?)_(\\d{2})$";

    private final static String callerIdRegex = "^\"?(.*?)\"?\\s?<(.*?)>$";

    private final static String mailBoxRegex = "^(\\d{2})@([a-zA-Z0-9]+)(,([bdgsuUP]+))*$";

    private final static String ipAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

    private final static String domainNameRegex = "^([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}$";

    private final static String queueNameRegex = "^(.*?)_(.*?)$";

    private final static String interfaceRegex = "^(.*?)/(.*?)$";

    //PJSIP/getafix_11-00000001
    private final static String localChannelRegex = "^PJSIP/(.*?)_(\\d{2})-(\\w){8}$";

    private final static String internalDialRegex = "^PJSIP/(.*?)_(\\d{2})$";

    //Local/11@vantage-00000000;2
    private final static String serverOriginatedChannelRegex = "^Local/(\\d){2}@(\\w){3,50}-(\\w){8};[12]$";

    private final static Pattern internalEntityPattern = Pattern.compile(internalEntityRegex);

    private final static Pattern ipAddressPattern = Pattern.compile(ipAddressRegex);

    private final static Pattern domainNamePattern = Pattern.compile(domainNameRegex);

    private final static Pattern localChannelPattern = Pattern.compile(localChannelRegex);

    private final static Pattern internalDialPattern = Pattern.compile(internalDialRegex);

    private final static Pattern serverOriginatedChannelPattern = Pattern.compile(serverOriginatedChannelRegex);

    private final static Pattern queueNamePattern = Pattern.compile(queueNameRegex);

    private final static Pattern interfacePattern = Pattern.compile(interfaceRegex);

    private final static String separator = "_";

    private TelephonyUtils() {
    }

    public static boolean isValidIpAddress(String ipAddress) {
        return ipAddress != null && ipAddressPattern.matcher(ipAddress).matches();
    }

    public static boolean isValidDomainName(String domainName) {
        return domainName != null && domainNamePattern.matcher(domainName).matches();
    }

    public static boolean isValidExtension(String extension) {
        return !StringUtils.isEmpty(extension) && extension.matches(baseExtensionRegex);
    }

    public static boolean isLocalExtensionChannel(String channel) {
        return channel != null && localChannelPattern.matcher(channel).matches();
    }

    public static boolean isServerGenerated(String channel) {
        return channel != null && serverOriginatedChannelPattern.matcher(channel).matches();
    }

    public static boolean isInternalDial(String dialString) {
        return dialString != null && internalDialPattern.matcher(dialString).matches();
    }

    public static String getCompanyId(String fullExtension) {
        return matchPattern(fullExtension, 1);
    }

    public static String getBaseExtension(String fullExtension) {
        return matchPattern(fullExtension, 2);
    }

    private static String matchPattern(String target, int group) {
        if (StringUtils.isEmpty(target)) {
            return null;
        }

        Matcher matcher = internalEntityPattern.matcher(target);
        if (matcher.find()) {
            return matcher.group(group);
        }
        return null;
    }

    public static boolean isValidInternalEntity(String fullExtension) {
        return fullExtension != null && fullExtension.matches(internalEntityRegex);
    }

    public static String getFullExtension(Extension extension) {
        return getFullExtension(extension.getCompany(), extension.getBaseExtension());
    }

    public static String getFullExtension(Company company, String baseExtension) {
        String companyId = company.getCompanyId();
        return getFullExtension(companyId, baseExtension);
    }

    private static String getFullExtension(String companyId, String baseExtension) {
        return companyId + separator + baseExtension;
    }

    public static String getUniqueQueueId(String companyId, String queueId) {
        return companyId + separator + queueId;
    }

    /**
     * sip callerid examples
     * "Name" <number>
     * "Name <number>
     * Name" <number>
     * Name<number>
     *
     * @param callerId
     * @return Name
     */
    public static String getCallerName(String callerId) {
        if (StringUtils.isEmpty(callerId)) {
            return "";
        }
        return getMatchGroup(callerIdRegex, callerId, 1);
    }

    /**
     * sip callerid examples
     * "Name" <number>
     * "Name <number>
     * Name" <number>
     * Name<number>
     *
     * @param callerId
     * @return number
     */
    public static String getCallerNumber(String callerId) {
        if (StringUtils.isEmpty(callerId)) {
            return "";
        }
        return getMatchGroup(callerIdRegex, callerId, 2);
    }

    public static String getCallerId(String callerName, String callerNumber) {
        return "\"" + callerName + "\"" + " <" + callerNumber + ">";
    }

    public static boolean isValidMailbox(String mailbox) {
        return mailbox != null && mailbox.matches(mailBoxRegex);
    }

    public static String getMailContext(String mailbox) {
        if (StringUtils.isEmpty(mailbox)) {
            return "";
        }
        return getMatchGroup(mailBoxRegex, mailbox, 2);
    }

    public static String getMailExtension(String mailbox) {
        if (StringUtils.isEmpty(mailbox)) {
            return "";
        }
        return getMatchGroup(mailBoxRegex, mailbox, 1);
    }

    public static String getMailBox(String mailBox) {
        return getMailBox(getMailExtension(mailBox), getMailContext(mailBox));
    }

    public static String getMailBox(String extension, String context) {
        return extension + "@" + context;
    }

    private static String getMatchGroup(String regex, String target, int group) {
        Pattern pattern = Pattern.compile(regex);
        return getMatchGroup(pattern, target, group);
    }

    private static String getMatchGroup(Pattern pattern, String target, int group) {
        Matcher matcher = pattern.matcher(target);
        while (matcher.find()) {
            return matcher.group(group);
        }

        return "";
    }

    public static String getCompanyIdFromQueueName(String queueName) {
        if (StringUtils.isEmpty(queueName)) {
            return null;
        }

        Matcher matcher = queueNamePattern.matcher(queueName);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static String getQueueIdFromQueueName(String queueName) {
        if (StringUtils.isEmpty(queueName)) {
            return null;
        }

        Matcher matcher = queueNamePattern.matcher(queueName);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }

    public static String getSipUserName(String resource) {
        if (StringUtils.isEmpty(resource)) {
            return null;
        }

        Matcher matcher = interfacePattern.matcher(resource);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }
}
