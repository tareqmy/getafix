package com.telefonix.getfix.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by tmyousuf on 2/15/16.
 * Utility class for generating random Strings.
 */
public final class RandomUtils {

    private static final int DEF_COUNT = 20;

    private RandomUtils() {
    }

    /**
     * Generates a password.
     *
     * @return the generated password
     */
    public static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }

    /**
     * Generates an activation key.
     *
     * @return the generated activation key
     */
    public static String generateActivationKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    /**
     * Generates a reset key.
     *
     * @return the generated reset key
     */
    public static String generateResetKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    /**
     * Numeric only
     *
     * @param count length of the code
     * @return the generated random number of length
     */
    public static String generateConferenceCode(int count) {
        return RandomStringUtils.randomNumeric(count);
    }

    public static String generateActionId() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    public static String generateDynamicConferenceId() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }
}
