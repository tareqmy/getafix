package com.telefonix.getfix.utils;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.concurrent.TimeUnit;

/**
 * Created by tareqmy on 2019-01-23.
 */
public class MiscUtils {

    private MiscUtils() {
    }

    public static String getCurrentTimestamp(ZoneId zoneId) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        ZonedDateTime dateTime = ZonedDateTime.now(zoneId);
        return dateTime.format(formatter);
    }

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignored) {

        }
    }

    public static ResponseEntity<byte[]> getDownload(byte[] content, String filename, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(content.length);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        headers.set(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
        headers.setContentType(mediaType);

        return new ResponseEntity<>(content, headers, HttpStatus.OK);
    }

    public static String formatDuration(Long durationSecs) {
        if (durationSecs == null || durationSecs < 0) {
            return "00:00:00";
        }
        return DurationFormatUtils.formatDuration(durationSecs * 1000, "HH:mm:ss");
    }

    public static String formatDateTime(LocalDateTime dateTime) {
        if (dateTime == null) {
            dateTime = LocalDateTime.now();
        }
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public static String formatDateTime(Instant instant, ZoneId zoneId) {
        if (instant == null) {
            instant = Instant.now();
        }
        return instant.atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public static String formatDate(Instant instant, ZoneId zoneId) {
        if (instant == null) {
            instant = Instant.now();
        }
        return instant.atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static String formatTime(Instant instant, ZoneId zoneId) {
        if (instant == null) {
            instant = Instant.now();
        }
        return instant.atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public static Duration getDuration(Instant start, Instant end) {
        if (start == null) {
            start = Instant.now();
        }

        if (end == null) {
            end = Instant.now();
        }
        return Duration.between(start, end);
    }

}
