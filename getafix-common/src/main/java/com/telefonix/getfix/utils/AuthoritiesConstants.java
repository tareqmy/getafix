package com.telefonix.getfix.utils;

/**
 * Created by tmyousuf on 8/8/18.
 */
public class AuthoritiesConstants {

    public static final String SYSTEM_ADMIN = "ROLE_SYSTEM_ADMIN";

    public static final String GETAFIX_ADMIN = "ROLE_GETAFIX_ADMIN";

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {

    }
}
