package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.System;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Repository
public interface SystemRepository extends JpaRepository<System, Long> {
}
