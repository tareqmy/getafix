package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {

    List<Phone> findByCompany(Company company);
}
