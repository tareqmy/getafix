package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.AutoAttendant;
import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-05.
 */
@Repository
public interface AutoAttendantRepository extends JpaRepository<AutoAttendant, Long> {

    List<AutoAttendant> findByCompany(Company company);

    Optional<AutoAttendant> findByExtension(Extension extension);
}
