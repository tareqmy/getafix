package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.QueueHalfHourStat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 1/10/19.
 */
@Repository
public interface QueueHalfHourStatRepository extends JpaRepository<QueueHalfHourStat, Long> {

    Optional<QueueHalfHourStat> findByQueueIdAndStart(Long queueId, Instant start);

    Optional<QueueHalfHourStat> findTopByQueueIdOrderByStartDesc(Long queueId);

    @Query("SELECT x FROM QueueHalfHourStat x " +
        "WHERE (x.queueId = :queueId) " +
        "AND x.start BETWEEN :fromDate AND :toDate ORDER BY x.start DESC")
    List<QueueHalfHourStat> findByQueueIdAndStartBetween(@Param("queueId") Long queueId,
                                                         @Param("fromDate") Instant fromDate,
                                                         @Param("toDate") Instant toDate);
}
