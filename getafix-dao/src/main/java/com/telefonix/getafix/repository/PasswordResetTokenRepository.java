package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.PasswordResetToken;
import com.telefonix.getafix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by tmyousuf on 17/10/17.
 */
@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    List<PasswordResetToken> findAllByUserAndToken(User user, String token);

    List<PasswordResetToken> findAllByUser(User user);

    void deleteByUserAndToken(User user, String token);

    void deleteAllByUser(User user);

    void deleteAllByExpiryDateBefore(ZonedDateTime zonedDateTime);
}
