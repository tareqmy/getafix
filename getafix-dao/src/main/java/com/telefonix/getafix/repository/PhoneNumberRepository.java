package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-05-01.
 */
@Repository
public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, String> {

    List<PhoneNumber> findByCompany(Company company);

    Optional<PhoneNumber> findByExtension(Extension extension);
}
