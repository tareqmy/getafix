package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.AgentLog;
import com.telefonix.getafix.entity.enums.AgentEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-08-11.
 */
@Repository
public interface AgentLogRepository extends JpaRepository<AgentLog, Long> {

    @Query("SELECT c FROM AgentLog c " +
        "WHERE (c.userId = :userId) " +
        "AND c.eventInstant BETWEEN :fromDate AND :toDate ORDER BY c.eventInstant DESC")
    List<AgentLog> findByUserIdAndEventInstantBetweenOrderByEventInstantDesc(@Param("userId") Long userId,
                                                                             @Param("fromDate") Instant fromDate,
                                                                             @Param("toDate") Instant toDate);

    @Query("SELECT c FROM AgentLog c " +
        "WHERE (c.userId = :userId) " +
        "AND c.eventInstant BETWEEN :fromDate AND :toDate ORDER BY c.eventInstant ASC")
    List<AgentLog> findByUserIdAndEventInstantBetweenOrderByEventInstantAsc(@Param("userId") Long userId,
                                                                            @Param("fromDate") Instant fromDate,
                                                                            @Param("toDate") Instant toDate);

    AgentLog getTopByUserIdAndEventInstantBeforeOrderByEventInstantDesc(Long userId, Instant eventInstant);

    AgentLog getTopByUserIdAndAgentEventAndEventInstantBeforeOrderByEventInstantDesc(Long userId, AgentEvent agentEvent, Instant eventInstant);

    @Query("SELECT c FROM AgentLog c " +
        "WHERE (c.userId = :userId) AND c.queueName IS NOT NULL " +
        "AND c.eventInstant BETWEEN :fromDate AND :toDate ORDER BY c.eventInstant DESC")
    List<AgentLog> findByUserIdAndQueueNameNotNullAndEventInstantBetweenOrderByEventInstantDesc(@Param("userId") Long userId,
                                                                                                @Param("fromDate") Instant fromDate,
                                                                                                @Param("toDate") Instant toDate);

    Optional<AgentLog> findTopBySipUserNameOrderByEventInstantDesc(String sipUserName);

    Optional<AgentLog> findTopBySipUserNameAndQueueNameOrderByEventInstantDesc(String sipUserName, String queueName);
}
