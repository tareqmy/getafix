package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.EndpointLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-08-20.
 */
@Repository
public interface EndpointLogRepository extends JpaRepository<EndpointLog, Long> {

    List<EndpointLog> findByCompanyIdOrderByEventInstantDesc(String companyId);

    List<EndpointLog> findByCompanyIdNullOrderByEventInstantDesc();

    Optional<EndpointLog> findTopByResourceOrderByEventInstantDesc(String resource);

    @Query("SELECT e FROM EndpointLog e " +
        "WHERE (e.resource = :resource) " +
        "AND e.eventInstant BETWEEN :fromDate AND :toDate ORDER BY e.eventInstant DESC")
    List<EndpointLog> findByResourceAndStartBetween(@Param("resource") String resource,
                                                    @Param("fromDate") Instant fromDate,
                                                    @Param("toDate") Instant toDate);

    @Query("SELECT e FROM EndpointLog e " +
        "WHERE (e.resource IN :resources) " +
        "AND e.eventInstant BETWEEN :fromDate AND :toDate ORDER BY e.eventInstant DESC")
    List<EndpointLog> findByResourceInAndStartBetween(@Param("resources") List<String> resources,
                                                      @Param("fromDate") Instant fromDate,
                                                      @Param("toDate") Instant toDate);

    @Query("SELECT e FROM EndpointLog e " +
        "WHERE (e.userId = :userId) " +
        "AND e.eventInstant BETWEEN :fromDate AND :toDate ORDER BY e.eventInstant DESC")
    List<EndpointLog> findByUserIdAndStartBetween(@Param("userId") Long userId,
                                                  @Param("fromDate") Instant fromDate,
                                                  @Param("toDate") Instant toDate);
}
