package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.ChannelLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Created by tareqmy on 2019-06-16.
 */
@Repository
public interface ChannelLogRepository extends JpaRepository<ChannelLog, Long> {

    List<ChannelLog> findByCompanyId(String companyId);

    @Query("SELECT c FROM ChannelLog c " +
        "WHERE (c.extensionId = :extensionId) " +
        "AND c.start BETWEEN :fromDate AND :toDate ORDER BY c.start DESC")
    List<ChannelLog> findByExtensionIdAndStartBetween(@Param("extensionId") Long extensionId,
                                                      @Param("fromDate") Instant fromDate,
                                                      @Param("toDate") Instant toDate);

    @Query("SELECT c FROM ChannelLog c " +
        "WHERE (c.extensionId IN :extensionIds) " +
        "AND c.start BETWEEN :fromDate AND :toDate ORDER BY c.start DESC")
    List<ChannelLog> findByExtensionIdInAndStartBetween(@Param("extensionIds") List<Long> extensionIds,
                                                        @Param("fromDate") Instant fromDate,
                                                        @Param("toDate") Instant toDate);

    @Query("SELECT c FROM ChannelLog c " +
        "WHERE (c.userId IN :userId) " +
        "AND c.start BETWEEN :fromDate AND :toDate ORDER BY c.start DESC")
    List<ChannelLog> findByUserIdAndStartBetween(@Param("userId") Long userId,
                                                 @Param("fromDate") Instant fromDate,
                                                 @Param("toDate") Instant toDate);

    List<ChannelLog> findByEndIsNull();
}
