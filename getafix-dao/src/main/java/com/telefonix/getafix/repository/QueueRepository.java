package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Queue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-28.
 */
@Repository
public interface QueueRepository extends JpaRepository<Queue, Long> {

    List<Queue> findByCompany(Company company);

    Optional<Queue> findByExtension(Extension extension);

    @Query("SELECT e FROM Queue e WHERE e.queueId = :queueId AND e.company.companyId = :companyId")
    Optional<Queue> findByCompanyIdAndQueueId(@Param("companyId") String companyId, @Param("queueId") String queueId);
}
