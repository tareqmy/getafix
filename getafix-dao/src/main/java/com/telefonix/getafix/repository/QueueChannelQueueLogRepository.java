package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.QueueChannelQueueLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Created by tareqmy on 8/9/19.
 */
@Repository
public interface QueueChannelQueueLogRepository extends JpaRepository<QueueChannelQueueLog, Long> {

    @Query("SELECT c FROM QueueChannelQueueLog c " +
        "WHERE (c.queueId = :queueId) " +
        "AND c.queued BETWEEN :fromDate AND :toDate ORDER BY c.queued DESC")
    List<QueueChannelQueueLog> findByQueueIdAndQueuedBetween(@Param("queueId") Long queueId,
                                                             @Param("fromDate") Instant fromDate,
                                                             @Param("toDate") Instant toDate);

    @Query("SELECT c FROM QueueChannelQueueLog c " +
        "WHERE (c.queueId = :queueId) " +
        "AND (c.queued BETWEEN :fromDate AND :toDate OR c.abandoned BETWEEN :fromDate AND :toDate OR c.handled BETWEEN :fromDate AND :toDate) " +
        "ORDER BY c.queued DESC")
    List<QueueChannelQueueLog> findByQueueIdAndQueuedBetweenOrDoneBetween(@Param("queueId") Long queueId,
                                                                          @Param("fromDate") Instant fromDate,
                                                                          @Param("toDate") Instant toDate);
}
