package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.EmailConfirmToken;
import com.telefonix.getafix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by tmyousuf on 03/12/18.
 */
@Repository
public interface EmailConfirmTokenRepository extends JpaRepository<EmailConfirmToken, Long> {

    List<EmailConfirmToken> findAllByUserAndToken(User user, String token);

    List<EmailConfirmToken> findAllByUser(User user);

    void deleteByUserAndToken(User user, String token);

    void deleteAllByUser(User user);

    void deleteAllByExpiryDateBefore(ZonedDateTime zonedDateTime);
}
