package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.Phone;
import com.telefonix.getafix.entity.PhoneLine;
import com.telefonix.getafix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-03-25.
 */
@Repository
public interface PhoneLineRepository extends JpaRepository<PhoneLine, Long> {

    List<PhoneLine> findByExtensionOrderBySipUserNameAsc(Extension extension);

    List<PhoneLine> findByPhone(Phone phone);

    Optional<PhoneLine> findBySipUserName(String sipUserName);

    List<PhoneLine> findByJoinedUserNotNull();

    Optional<PhoneLine> findByJoinedUser(User joinedUser);
}
