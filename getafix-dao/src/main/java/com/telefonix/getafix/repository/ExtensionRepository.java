package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-14.
 */
@Repository
public interface ExtensionRepository extends JpaRepository<Extension, Long> {

    List<Extension> findByCompany(Company company);

    List<Extension> findByUser(User user);

    @Query("SELECT e FROM Extension e WHERE e.baseExtension = :baseExtension AND e.company.companyId = :companyId")
    Optional<Extension> findByCompanyIdAndBaseExtension(@Param("companyId") String companyId, @Param("baseExtension") String baseExtension);
}
