package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.QueueChannelLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tareqmy on 4/9/19.
 */
@Repository
public interface QueueChannelLogRepository extends JpaRepository<QueueChannelLog, Long> {

    List<QueueChannelLog> findByCompanyId(String companyId);
}
