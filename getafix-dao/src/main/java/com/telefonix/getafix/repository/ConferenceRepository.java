package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tareqmy on 2019-05-14.
 */
@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {

    List<Conference> findByCompany(Company company);

    @Query("SELECT c FROM Conference c WHERE c.company.companyId = :companyId")
    List<Conference> findByCompanyId(@Param("companyId") String companyId);
}
