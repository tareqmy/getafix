package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Extension;
import com.telefonix.getafix.entity.VoiceMail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-23.
 */
@Repository
public interface VoiceMailRepository extends JpaRepository<VoiceMail, Long> {

    Optional<VoiceMail> findByExtension(Extension extension);

    List<VoiceMail> findAllByEnabledTrue();
}
