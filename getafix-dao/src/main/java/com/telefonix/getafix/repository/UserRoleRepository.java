package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.UserRole;
import com.telefonix.getafix.entity.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by tmyousuf on 8/8/18.
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    Optional<UserRole> findOneByRole(Role role);
}
