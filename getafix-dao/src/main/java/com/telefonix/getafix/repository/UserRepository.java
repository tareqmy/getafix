package com.telefonix.getafix.repository;

import com.telefonix.getafix.entity.Company;
import com.telefonix.getafix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by tmyousuf on 6/8/18.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByEmail(String email);

    List<User> findByCompany(Company company);

    List<User> findByJoinedPhoneLineNotNull();
}
