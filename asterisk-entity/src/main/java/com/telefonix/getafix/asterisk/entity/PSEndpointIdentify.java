package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by tareqmy on 2019-01-21.
 * type=identify
 * <p>
 * trunk sample
 * id;endpoint;match
 * support;support;sip.nextfone.com.bd
 * <p>
 * <p>
 * this blog explains about registration line support --- https://blogs.asterisk.org/2016/01/27/the-pjsip-outbound-registration-line-option/
 * but my itsp does not support this i guess. if it did i didnt need to add ps_endpoint_id_ips
 * meaning dont need the line=yes. identify with phone number dialed for incoming call. use single endpoint for itsp detection
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_endpoint_id_ips")
public class PSEndpointIdentify {

    @Id
    @Column(length = 40, nullable = false)
    private String id;

    //matched with endpoint with this
    @Column(name = "endpoint", length = 40)
    private String endpoint;

    @Column(name = "match", length = 80)
    private String match;
}

//    - createTable:
//        columns:
//        - column:
//        name: srv_lookups
//        type: VARCHAR(20)
//        - column:
//        name: match_header
//        type: VARCHAR(255)
//        tableName: ps_endpoint_id_ips
