package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.QueueLogEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

/**
 * Created by tareqmy on 2019-01-21.
 * note: server-generated
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "queue_log")
public class AXQueueLog implements Comparable<AXQueueLog> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queue_log_id_seq") //queue_log_pkey??
    @SequenceGenerator(name = "queue_log_id_seq", sequenceName = "queue_log_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "callid", length = 40, nullable = false)
    private String callid;

    @Column(name = "queuename", length = 40, nullable = false)
    private String queueName;

    @Column(name = "agent", length = 40, nullable = false)
    private String agent;

    @Enumerated(EnumType.STRING)
    @Column(name = "event", length = 40, nullable = false)
    private QueueLogEvent event;

    @Column(name = "data1", length = 128, nullable = false)
    private String data1;

    @Column(name = "data2", length = 128, nullable = false)
    private String data2;

    @Column(name = "data3", length = 128, nullable = false)
    private String data3;

    @Column(name = "data4", length = 128, nullable = false)
    private String data4;

    @Column(name = "data5", length = 128, nullable = false)
    private String data5;

    //NOTE: queue_log_realtime_use_gmt=yes in logger.conf
    @Column(name = "time", length = 40)
    private Instant time;

    @Override
    public int compareTo(AXQueueLog o) {
        return o.getTime().compareTo(getTime());
    }
}
