package com.telefonix.getafix.asterisk.entity.enums;

public enum YesNoLimitMore {
    yes,
    no,
    limit,
    more
}
