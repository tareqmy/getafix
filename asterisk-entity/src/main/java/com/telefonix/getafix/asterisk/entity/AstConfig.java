package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-29.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "ast_config")
public class AstConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ast_config_id_seq")
    @SequenceGenerator(name = "ast_config_id_seq", sequenceName = "ast_config_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "cat_metric", nullable = false)
    private int catMetric;

    @Column(name = "var_metric", nullable = false)
    private int varMetric;

    @Column(length = 30, nullable = false)
    private String filename;

    @Column(length = 60, nullable = false)
    private String category;

    @Column(name = "var_name", length = 30, nullable = false)
    private String varName;

    @Column(name = "var_val", length = 250, nullable = false)
    private String varValue;

    @Column(nullable = false)
    private int commented = 0;
}
