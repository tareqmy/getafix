package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.AuthType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * type=auth
 * <p>
 * endpoint sample
 * id;auth_type;username;password
 * 1001;userpass;1001;1001
 * <p>
 * trunk outbound sample
 * id;auth_type;username;password
 * support_auth;userpass;09614500737;8dw307vs5x8y95pg
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_auths")
public class PSAuth {

    @Id
    @Column(length = 40, nullable = false)
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "auth_type", length = 10)
    private AuthType authType = AuthType.userpass;

    @Column(name = "nonce_lifetime")
    private int nonceLifetime;

    @Column(name = "md5_cred", length = 40)
    private String md5Cred;

    @Column(name = "password", length = 80)
    private String password;

    @Column(name = "realm", length = 40)
    private String realm;

    @Column(name = "username", length = 40)
    private String username;
}
