package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * <p>
 * context;mailbox;password;fullname;email
 * example;1001;1001;Jon Doe;jond@example.com
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"context", "mailbox"})
@Table(name = "voicemail")
public class AXVoiceMail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voicemail_uniqueid_seq") //voicemail_pkey??
    @SequenceGenerator(name = "voicemail_uniqueid_seq", sequenceName = "voicemail_uniqueid_seq", allocationSize = 1)
    private Long uniqueid;

    @Column(name = "context", length = 80, nullable = false)
    private String context;

    @Column(name = "mailbox", length = 80, nullable = false)
    private String mailbox;

    @Column(name = "password", length = 80, nullable = false)
    private String password;

    @Column(name = "fullname", length = 80)
    private String fullName;

    @Column(name = "email", length = 80)
    private String email;
}
