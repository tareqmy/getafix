package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Created by tmyousuf on 8/8/18.
 * https://wiki.asterisk.org/wiki/display/AST/CDR+Fields
 * This entity must not create, update or remove.
 * Only for retreive actions.
 * Asterisk enters this.
 * NOTE: GETAFIX MUST NOT DO ANYTHING OTHER THAN VIEW
 * <p>
 * note: server-generated
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "cdr")
public class CDR {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "accountcode", length = 20)
    private String accountCode;

    @Column(name = "src", length = 80)
    private String src;

    @Column(name = "dst", length = 80)
    private String dst;

    @Column(name = "dcontext", length = 80)
    private String dContext;

    @Column(name = "clid", length = 80)
    private String clId;

    @Column(name = "channel", length = 80)
    private String channel;

    @Column(name = "dstchannel", length = 80)
    private String dstChannel;

    @Column(name = "lastapp", length = 80)
    private String lastApp;

    @Column(name = "lastdata", length = 80)
    private String lastData;

    //NOTE: this time is the localtime of the asterisk server
    @Column(name = "callstart")
    private ZonedDateTime start;

    //NOTE: this time is the localtime of the asterisk server
    @Column(name = "callanswer")
    private ZonedDateTime answer;

    //NOTE: this time is the localtime of the asterisk server
    @Column(name = "callend")
    private ZonedDateTime end;

    @Column(name = "duration")
    private int duration;

    @Column(name = "billsec")
    private int billSec;

    //ANSWERED, NO ANSWER, BUSY
    @Column(name = "disposition", length = 45)
    private String disposition;

    //DOCUMENTATION, BILL, IGNORE etc
    @Column(name = "amaflags", length = 45)
    private String amaFlags;

    //Set(CDR(userfield)=Value).
    @Column(name = "userfield", length = 256)
    private String userField;

    @Column(name = "uniqueid", length = 150)
    private String uniqueId;

    @Column(name = "linkedid", length = 150)
    private String linkedId;

    @Column(name = "peeraccount", length = 20)
    private String peerAccount;

    @Column(name = "sequence")
    private int sequence;
}
