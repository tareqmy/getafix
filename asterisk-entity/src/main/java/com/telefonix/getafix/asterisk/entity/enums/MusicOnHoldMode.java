package com.telefonix.getafix.asterisk.entity.enums;

public enum MusicOnHoldMode {
    custom,
    files,
    mp3nb,
    quietmp3nb,
    quietmp3
}
