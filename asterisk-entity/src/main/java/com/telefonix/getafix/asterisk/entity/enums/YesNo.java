package com.telefonix.getafix.asterisk.entity.enums;

public enum YesNo {
    yes,
    no
}
