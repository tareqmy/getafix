package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.MusicOnHoldMode;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by tareqmy on 15/9/19.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "musiconhold")
public class MusicOnHold {

    @Id
    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "mode")
    private MusicOnHoldMode mode;

    @Column(name = "directory")
    private String directory;

    @Column(name = "application")
    private String application;

    @Column(name = "digit")
    private String digit;

    @Column(name = "sort")
    private String sort;

    @Column(name = "format")
    private String format;

    @Column(name = "stamp")
    private LocalDateTime stamp;
}
