package com.telefonix.getafix.asterisk.entity.enums;

public enum YesNoOnce {
    yes,
    no,
    once
}
