package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * <p>
 * trunk registration
 * id;transport;outbound_auth;server_uri;client_uri;contact_user;retry_interval;forbidden_retry_interval;fatal_retry_interval;expiration;line;endpoint
 * support;transport-udp;support_auth;sip:sip.nextfone.com.bd;sip:09614500737@sip.nextfone.com.bd;09614500737;60;600;300;3600;yes;support
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_registrations")
public class PSRegistration {

    @Id
    @Column(length = 40, nullable = false)
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "auth_rejection_permanent", length = 5)
    private YesNo authRejectionPermanent;

    @Column(name = "client_uri", length = 255)
    private String clientUri;

    @Column(name = "contact_user", length = 40)
    private String contactUser;

    @Column(name = "expiration")
    private int expiration;

    @Column(name = "max_retries")
    private int maxRetries;

    @Column(name = "outbound_auth", length = 40)
    private String outboundAuth;

    @Column(name = "outbound_proxy", length = 40)
    private String outboundProxy;

    @Column(name = "retry_interval")
    private int retryInterval;

    @Column(name = "forbidden_retry_interval")
    private int forbiddenRetryInterval;

    @Column(name = "server_uri", length = 255)
    private String serverUri;

    @Column(name = "transport", length = 40)
    private String transport;

    @Enumerated(EnumType.STRING)
    @Column(name = "support_path", length = 5)
    private YesNo supportPath;

    @Column(name = "fatal_retry_interval")
    private int fatalRetryInterval;

    @Enumerated(EnumType.STRING)
    @Column(name = "line", length = 5)
    private YesNo line;

    @Column(name = "endpoint", length = 40)
    private String endpoint;
}
