package com.telefonix.getafix.asterisk.entity.enums;

/**
 * Created by tareqmy on 4/9/19.
 * https://wiki.asterisk.org/wiki/display/AST/Queue+Logs
 */
public enum QueueLogEvent {
    ABANDON,
    ADDMEMBER,
    AGENTDUMP, //redundant
    AGENTLOGIN, //redundant
    AGENTCALLBACKLOGIN, //redundant
    AGENTLOGOFF, //redundant
    AGENTCALLBACKLOGOFF, //redundant
    ATTENDEDTRANSFER,
    BLINDTRANSFER,
    COMPLETEAGENT,
    COMPLETECALLER,
    CONNECT,
    ENTERQUEUE,
    EXITEMPTY,
    EXITWITHKEY, //The caller elected to use a menu key to exit the queue. hangup/transfer/park
    EXITWITHTIMEOUT,
    FULL, //GXEVENT
    JOINEMPTY, //GXEVENT
    PAUSE,
    QUEUESTART,
    REMOVEMEMBER,
    RINGNOANSWER,
    RINGCANCELED,
    SYSCOMPAT,
    TRANSFER,
    UNPAUSE
}
