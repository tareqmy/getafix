package com.telefonix.getafix.asterisk.entity.enums;

public enum QueueEmptyOption {
    paused,
    penalty,
    inuse,
    ringing,
    unavailable,
    invalid,
    unknown,
    wrapup
}
