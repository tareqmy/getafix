package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "queue_rules")
public class AXQueueRule {

    @Id
    @Column(name = "ruleName", length = 80, nullable = false)
    private String ruleName;

    @Column(name = "time", length = 32, nullable = false)
    private String time;

    @Column(name = "min_penalty", length = 32, nullable = false)
    private String minPenalty;

    @Column(name = "max_penalty", length = 32, nullable = false)
    private String maxPenalty;
}
