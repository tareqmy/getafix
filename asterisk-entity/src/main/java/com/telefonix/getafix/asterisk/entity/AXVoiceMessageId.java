package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"dir", "msgNum"})
@Embeddable
public class AXVoiceMessageId implements Serializable {

    //if new it ends with 'INBOX'
    //if read it ends with 'Old'
    @Column(name = "dir", length = 255, nullable = false)
    private String dir;

    @Column(name = "msgnum", length = 40, nullable = false)
    private String msgNum;
}
