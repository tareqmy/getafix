package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_contacts",
    uniqueConstraints = @UniqueConstraint(name = "ps_contacts_uq", columnNames = {"id", "reg_server"}))
public class PSContact {

    @Id
    @Column(length = 255, nullable = false)
    private String id;

    @Column(length = 255)
    private String uri;

    @Column(name = "expiration_time", length = 40)
    private String expirationTime;

    @Column(name = "qualify_frequency", length = 40)
    private String qualifyFrequency;

    @Column(name = "outbound_proxy", length = 40)
    private String outboundProxy;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    @Column(name = "user_agent", length = 255)
    private String userAgent;

    @Column(name = "qualify_timeout", length = 40)
    private String qualifyTimeout;

    @Column(name = "reg_server", length = 20, nullable = false)
    private String regServer;

    @Column(name = "authenticate_qualify", length = 20)
    private String authenticateQualify;

    @Column(name = "via_addr", length = 40)
    private String viaAddr;

    @Column(name = "via_port", length = 20)
    private String viaPort;

    @Column(name = "call_id", length = 255)
    private String callId;

    @Column(name = "endpoint", length = 40)
    private String endpoint;

    @Column(name = "prune_on_boot", length = 20)
    private String pruneOnBoot;
}

//    - createTable:
//        columns:
//        - column:
//        constraints:
//        nullable: false
//        name: id
//        type: VARCHAR(255)
//        - column:
//        name: uri
//        type: VARCHAR(255)
//        - column:
//        name: expiration_time
//        type: VARCHAR(40)
//        - column:
//        name: qualify_frequency
//        type: VARCHAR(40)
//        - column:
//        name: outbound_proxy
//        type: VARCHAR(40)
//        - column:
//        name: path
//        type: TEXT
//        - column:
//        name: user_agent
//        type: VARCHAR(255)
//        - column:
//        name: qualify_timeout
//        type: VARCHAR(40)
//        - column:
//        name: reg_server
//        type: VARCHAR(20)
//        - column:
//        name: authenticate_qualify
//        type: VARCHAR(20)
//        - column:
//        name: via_addr
//        type: VARCHAR(40)
//        - column:
//        name: via_port
//        type: VARCHAR(20)
//        - column:
//        name: call_id
//        type: VARCHAR(255)
//        - column:
//        name: endpoint
//        type: VARCHAR(40)
//        - column:
//        name: prune_on_boot
//        type: VARCHAR(20)
//        tableName: ps_contacts
//    - addUniqueConstraint:
//        columnNames: id
//        constraintName: ps_contacts_id_key
//        tableName: ps_contacts
//        - addUniqueConstraint:
//        columnNames: id, reg_server
//        constraintName: ps_contacts_uq
//        tableName: ps_contacts
//        - createIndex:
//        columns:
//        - column:
//        name: qualify_frequency
//        - column:
//        name: expiration_time
//        indexName: ps_contacts_qualifyfreq_exp
//        tableName: ps_contacts
