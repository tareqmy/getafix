package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * type=aor
 * <p>
 * endpoint sample
 * id;mailboxes;max_contacts;remove_existing;default_expiration;maximum_expiration
 * 1001;1001@example;1;yes;100;100
 * <p>
 * trunk sample
 * id;contact
 * support;sip:sip.nextfone.com.bd:5060
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_aors")
public class PSAor {

    @Id
    @Column(length = 40, nullable = false)
    private String id;

    @Column(length = 255)
    private String contact;

    @Column(name = "default_expiration")
    private int defaultExpiration;

    @Column(name = "mailboxes", length = 80)
    private String mailBoxes;

    @Column(name = "max_contacts")
    private int maxContacts = 1;

    @Column(name = "minimum_expiration")
    private int minimumExpiration;

    @Enumerated(EnumType.STRING)
    @Column(name = "remove_existing", length = 5)
    private YesNo removeExisting;

    @Column(name = "qualify_frequency")
    private int qualifyFrequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "authenticate_qualify", length = 5)
    private YesNo authenticateQualify;

    @Column(name = "maximum_expiration")
    private int maximumExpiration;

    @Column(name = "outbound_proxy", length = 40)
    private String outboundProxy;

    @Enumerated(EnumType.STRING)
    @Column(name = "support_path", length = 5)
    private YesNo supportPath;

    @Column(name = "qualify_timeout")
    private int qualifyTimeout;
}
