package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.YesNo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * type=endpoint
 * <p>
 * endpoint sample
 * id;transport;context;disallow;allow;auth;aors;callerid;accountcode;set_var;direct_media;rtp_symmetric;force_rport;device_state_busy_at
 * 1001;transport-udp;local;all;ulaw;1001;1001;Jon Doe <1001>;1001@example.com;Queues=support^sales;no;yes;yes;1
 * <p>
 * trunk sample
 * id;transport;context;disallow;allow;outbound_auth;aors;force_rport;direct_media;ice_support
 * support;transport-udp;trunk-incoming;all;ulaw;support_auth;support;yes;no;yes
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "ps_endpoints")
public class PSEndpoint {

    @Id
    @Column(length = 40, nullable = false)
    private String id;

    @Column(name = "transport", length = 40)
    private String transport;

    @Column(name = "context", length = 40, nullable = false)
    private String context;

    //e.g 'all'
    @Column(name = "disallow", length = 200)
    private String disallow;

    //e.g 'ulaw,alaw'
    @Column(name = "allow", length = 200)
    private String allow;

    @Column(name = "auth", length = 40)
    private String auth;

    //comma separated list of aors?
    @Column(name = "aors", length = 200)
    private String aors;

    @Column(name = "callerid", length = 40)
    private String callerId;

    @Column(name = "accountcode", length = 40)
    private String accountCode;

    @Column(name = "set_var", columnDefinition = "text")
    private String setVar;

    @Enumerated(EnumType.STRING)
    @Column(name = "direct_media", length = 5)
    private YesNo directMedia = YesNo.no;

    @Enumerated(EnumType.STRING)
    @Column(name = "ice_support", length = 5)
    private YesNo iceSupport = YesNo.no;

    @Enumerated(EnumType.STRING)
    @Column(name = "rtp_symmetric", length = 5)
    private YesNo rtpSymmetric;

    @Enumerated(EnumType.STRING)
    @Column(name = "force_rport", length = 5)
    private YesNo forceRport;

    @Column(name = "outbound_auth", length = 40)
    private String outboundAuth;

    @Column(name = "device_state_busy_at")
    private int deviceStateBusyAt;
}

//
//    - createTable:
//    columns:

//    - column:
//    name: connected_line_method
//    type: VARCHAR(20)
//    - column:
//    name: direct_media_method
//    type: VARCHAR(20)
//    - column:
//    name: direct_media_glare_mitigation
//    type: VARCHAR(20)
//    - column:
//    name: disable_direct_media_on_nat
//    type: VARCHAR(20)
//    - column:
//    name: dtmf_mode
//    type: VARCHAR(20)
//    - column:
//    name: external_media_address
//    type: VARCHAR(40)


//    - column:
//    name: identify_by
//    type: VARCHAR(20)
//    - column:
//    name: mailboxes
//    type: VARCHAR(40)
//    - column:
//    name: moh_suggest
//    type: VARCHAR(40)

//    - column:
//    name: outbound_proxy
//    type: VARCHAR(40)
//    - column:
//    name: rewrite_contact
//    type: VARCHAR(20)
//    - column:
//    name: rtp_ipv6
//    type: VARCHAR(20)

//    - column:
//    name: send_diversion
//    type: VARCHAR(20)
//    - column:
//    name: send_pai
//    type: VARCHAR(20)
//    - column:
//    name: send_rpid
//    type: VARCHAR(20)
//    - column:
//    name: timers_min_se
//    type: INT
//    - column:
//    name: timers
//    type: VARCHAR(20)
//    - column:
//    name: timers_sess_expires
//    type: INT

//    - column:
//    name: callerid_privacy
//    type: VARCHAR(20)
//    - column:
//    name: callerid_tag
//    type: VARCHAR(40)
//    - column:
//    name: 100rel
//    type: VARCHAR(20)
//    - column:
//    name: aggregate_mwi
//    type: VARCHAR(20)
//    - column:
//    name: trust_id_inbound
//    type: VARCHAR(20)
//    - column:
//    name: trust_id_outbound
//    type: VARCHAR(20)
//    - column:
//    name: use_ptime
//    type: VARCHAR(20)
//    - column:
//    name: use_avpf
//    type: VARCHAR(20)
//    - column:
//    name: media_encryption
//    type: VARCHAR(20)
//    - column:
//    name: inband_progress
//    type: VARCHAR(20)
//    - column:
//    name: call_group
//    type: VARCHAR(40)
//    - column:
//    name: pickup_group
//    type: VARCHAR(40)
//    - column:
//    name: named_call_group
//    type: VARCHAR(40)
//    - column:
//    name: named_pickup_group
//    type: VARCHAR(40)

//    - column:
//    name: fax_detect
//    type: VARCHAR(20)
//    - column:
//    name: t38_udptl
//    type: VARCHAR(20)
//    - column:
//    name: t38_udptl_ec
//    type: VARCHAR(20)
//    - column:
//    name: t38_udptl_maxdatagram
//    type: INT
//    - column:
//    name: t38_udptl_nat
//    type: VARCHAR(20)
//    - column:
//    name: t38_udptl_ipv6
//    type: VARCHAR(20)
//    - column:
//    name: tone_zone
//    type: VARCHAR(40)
//    - column:
//    name: language
//    type: VARCHAR(40)
//    - column:
//    name: one_touch_recording
//    type: VARCHAR(20)
//    - column:
//    name: record_on_feature
//    type: VARCHAR(40)
//    - column:
//    name: record_off_feature
//    type: VARCHAR(40)
//    - column:
//    name: rtp_engine
//    type: VARCHAR(40)
//    - column:
//    name: allow_transfer
//    type: VARCHAR(20)
//    - column:
//    name: allow_subscribe
//    type: VARCHAR(20)
//    - column:
//    name: sdp_owner
//    type: VARCHAR(40)
//    - column:
//    name: sdp_session
//    type: VARCHAR(40)
//    - column:
//    name: tos_audio
//    type: VARCHAR(10)
//    - column:
//    name: tos_video
//    type: VARCHAR(10)
//    - column:
//    name: sub_min_expiry
//    type: INT
//    - column:
//    name: from_domain
//    type: VARCHAR(40)
//    - column:
//    name: from_user
//    type: VARCHAR(40)
//    - column:
//    name: mwi_from_user
//    type: VARCHAR(40)
//    - column:
//    name: dtls_verify
//    type: VARCHAR(40)
//    - column:
//    name: dtls_rekey
//    type: VARCHAR(40)
//    - column:
//    name: dtls_cert_file
//    type: VARCHAR(200)
//    - column:
//    name: dtls_private_key
//    type: VARCHAR(200)
//    - column:
//    name: dtls_cipher
//    type: VARCHAR(200)
//    - column:
//    name: dtls_ca_file
//    type: VARCHAR(200)
//    - column:
//    name: dtls_ca_path
//    type: VARCHAR(200)
//    - column:
//    name: dtls_setup
//    type: VARCHAR(20)
//    - column:
//    name: srtp_tag_32
//    type: VARCHAR(20)
//    - column:
//    name: media_address
//    type: VARCHAR(40)
//    - column:
//    name: redirect_method
//    type: VARCHAR(20)

//    - column:
//    name: cos_audio
//    type: INT
//    - column:
//    name: cos_video
//    type: INT
//    - column:
//    name: message_context
//    type: VARCHAR(40)
//    - column:
//    name: force_avp
//    type: VARCHAR(20)
//    - column:
//    name: media_use_received_transport
//    type: VARCHAR(20)

//    - column:
//    name: user_eq_phone
//    type: VARCHAR(20)
//    - column:
//    name: moh_passthrough
//    type: VARCHAR(20)
//    - column:
//    name: media_encryption_optimistic
//    type: VARCHAR(20)
//    - column:
//    name: rpid_immediate
//    type: VARCHAR(20)
//    - column:
//    name: g726_non_standard
//    type: VARCHAR(20)
//    - column:
//    name: rtp_keepalive
//    type: INT
//    - column:
//    name: rtp_timeout
//    type: INT
//    - column:
//    name: rtp_timeout_hold
//    type: INT
//    - column:
//    name: bind_rtp_to_media_address
//    type: VARCHAR(20)
//    - column:
//    name: voicemail_extension
//    type: VARCHAR(40)
//    - column:
//    name: mwi_subscribe_replaces_unsolicited
//    type: INT
//    - column:
//    name: deny
//    type: VARCHAR(95)
//    - column:
//    name: permit
//    type: VARCHAR(95)
//    - column:
//    name: acl
//    type: VARCHAR(40)
//    - column:
//    name: contact_deny
//    type: VARCHAR(95)
//    - column:
//    name: contact_permit
//    type: VARCHAR(95)
//    - column:
//    name: contact_acl
//    type: VARCHAR(40)
//    - column:
//    name: subscribe_context
//    type: VARCHAR(40)
//    - column:
//    name: fax_detect_timeout
//    type: INT
//    - column:
//    name: contact_user
//    type: VARCHAR(80)
//    - column:
//    name: preferred_codec_only
//    type: VARCHAR(20)
//    - column:
//    name: asymmetric_rtp_codec
//    type: VARCHAR(20)
//    - column:
//    name: rtcp_mux
//    type: VARCHAR(20)
//    - column:
//    name: allow_overlap
//    type: VARCHAR(20)
//    - column:
//    name: refer_blind_progress
//    type: VARCHAR(20)
//    - column:
//    name: notify_early_inuse_ringing
//    type: VARCHAR(20)
//    - column:
//    name: max_audio_streams
//    type: INT
//    - column:
//    name: max_video_streams
//    type: INT
//    - column:
//    name: webrtc
//    type: VARCHAR(20)
//    - column:
//    name: dtls_fingerprint
//    type: VARCHAR(20)
//    - column:
//    name: incoming_mwi_mailbox
//    type: VARCHAR(40)
//    - column:
//    name: bundle
//    type: VARCHAR(20)
//    - column:
//    name: dtls_auto_generate_cert
//    type: VARCHAR(20)
//    tableName: ps_endpoints
