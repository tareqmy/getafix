package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"queueName", "interFace"})
@Embeddable
public class AXQueueMemberId implements Serializable {

    @Column(name = "queue_name", length = 80, nullable = false)
    private String queueName;

    @Column(name = "interface", length = 80, nullable = false)
    private String interFace;
}
