package com.telefonix.getafix.asterisk.entity;

import com.telefonix.getafix.asterisk.entity.enums.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * <p>
 * queue sample
 * name;weight;musiconhold;strategy;ringinuse;autopause;maxlen;setinterfacevar;setqueueentryvar;setqueuevar;timeout;retry;timeoutpriority;announce_frequency;min_announce_frequency;periodic_announce_frequency;random_periodic_announce;relative_periodic_announce;announce_holdtime;announce_position;announce_position_limit;announce_round_seconds;joinempty;leavewhenempty
 * support;10;default;rrmemory;no;no;20;yes;yes;yes;10;5;app;30;30;45;no;yes;once;limit;10;30;unavailable,invalid,unknown;unavailable,invalid,unknown
 * sales;5;default;rrmemory;no;no;20;yes;yes;yes;10;5;app;30;30;45;no;yes;once;limit;10;30;unavailable,invalid,unknown;unavailable,invalid,unknown
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"name"})
@Table(name = "queues")
public class AXQueue {

    @Id
    @Column(length = 128, nullable = false)
    private String name;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "musicclass", length = 128)
    private String musicclass;

    @Enumerated(EnumType.STRING)
    @Column(name = "strategy", length = 20)
    private Strategy strategy;

    @Column(name = "context", length = 128)
    private String context;

    @Enumerated(EnumType.STRING)
    @Column(name = "ringinuse", length = 5)
    private YesNo ringinuse;

    @Enumerated(EnumType.STRING)
    @Column(name = "autopause", length = 5)
    private YesNo autopause;

    @Column(name = "maxlen")
    private Integer maxlen;

    @Enumerated(EnumType.STRING)
    @Column(name = "setinterfacevar", length = 5)
    private YesNo setinterfacevar;

    @Enumerated(EnumType.STRING)
    @Column(name = "setqueueentryvar", length = 5)
    private YesNo setqueueentryvar;

    @Enumerated(EnumType.STRING)
    @Column(name = "setqueuevar", length = 5)
    private YesNo setqueuevar;

    @Column(name = "timeout")
    private Integer timeout;

    @Column(name = "wrapuptime")
    private Integer wrapuptime;

    @Column(name = "retry")
    private Integer retry;

    @Enumerated(EnumType.STRING)
    @Column(name = "timeoutpriority", length = 5)
    private TimeoutPriority timeoutpriority = TimeoutPriority.app;

    @Column(name = "announce_frequency")
    private Integer announceFrequency;

    @Column(name = "min_announce_frequency")
    private Integer minAnnounceFrequency;

    @Column(name = "periodic_announce_frequency")
    private Integer periodicAnnounceFrequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "random_periodic_announce", length = 5)
    private YesNo randomPeriodicAnnounce;

    @Enumerated(EnumType.STRING)
    @Column(name = "relative_periodic_announce", length = 5)
    private YesNo relativePeriodicAnnounce;

    @Enumerated(EnumType.STRING)
    @Column(name = "announce_holdtime", length = 5)
    private YesNoOnce announceHoldtime;

    @Enumerated(EnumType.STRING)
    @Column(name = "announce_position", length = 5)
    private YesNoLimitMore announcePosition;

    @Column(name = "announce_position_limit")
    private Integer announcePositionLimit;

    @Column(name = "announce_round_seconds")
    private Integer announceRoundSeconds;

    @Column(name = "periodic_announce", length = 50)
    private String periodicAnnounce;

    @Column(name = "joinempty", length = 128)
    private String joinempty; //comma-separated QueueEmptyOption

    @Column(name = "leavewhenempty", length = 128)
    private String leavewhenempty; //comma-separated QueueEmptyOption

    @Column(name = "servicelevel")
    private Integer servicelevel;

}

//    - createTable:
//        columns:

//        - column:
//        name: announce
//        type: VARCHAR(128)


//        - column:
//        name: monitor_format
//        type: VARCHAR(8)
//        - column:
//        name: membermacro
//        type: VARCHAR(512)
//        - column:
//        name: membergosub
//        type: VARCHAR(512)
//        - column:
//        name: queue_youarenext
//        type: VARCHAR(128)
//        - column:
//        name: queue_thereare
//        type: VARCHAR(128)
//        - column:
//        name: queue_callswaiting
//        type: VARCHAR(128)
//        - column:
//        name: queue_quantity1
//        type: VARCHAR(128)
//        - column:
//        name: queue_quantity2
//        type: VARCHAR(128)
//        - column:
//        name: queue_holdtime
//        type: VARCHAR(128)
//        - column:
//        name: queue_minutes
//        type: VARCHAR(128)
//        - column:
//        name: queue_minute
//        type: VARCHAR(128)
//        - column:
//        name: queue_seconds
//        type: VARCHAR(128)
//        - column:
//        name: queue_thankyou
//        type: VARCHAR(128)
//        - column:
//        name: queue_callerannounce
//        type: VARCHAR(128)
//        - column:
//        name: queue_reporthold
//        type: VARCHAR(128)

//        - column:
//        name: announce_to_first_user
//        type: VARCHAR(20)

//        - column:
//        name: penaltymemberslimit
//        type: INT
//        - column:
//        name: autofill
//        type: VARCHAR(20)
//        - column:
//        name: monitor_type
//        type: VARCHAR(128)

//        - column:
//        name: autopausedelay
//        type: INT
//        - column:
//        name: autopausebusy
//        type: VARCHAR(20)
//        - column:
//        name: autopauseunavail
//        type: VARCHAR(20)


//        - column:
//        name: reportholdtime
//        type: VARCHAR(20)
//        - column:
//        name: memberdelay
//        type: INT

//        - column:
//        name: timeoutrestart
//        type: VARCHAR(20)
//        - column:
//        name: defaultrule
//        type: VARCHAR(128)

//        tableName: queues
