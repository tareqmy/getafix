package com.telefonix.getafix.asterisk.entity.enums;

public enum Strategy {
    ringall,
    leastrecent,
    fewestcalls,
    random,
    rrmemory,
    rrordered,
    linear,
    wrandom
}
