package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by tareqmy on 2019-01-21.
 */
@Entity
@Data
@EqualsAndHashCode(of = {"axQueueMemberId"})
@NoArgsConstructor
@Table(name = "queue_members")
public class AXQueueMember {

    @EmbeddedId
    private AXQueueMemberId axQueueMemberId;

    @Column(name = "membername", length = 80)
    private String membername;

    @Column(name = "state_interface", length = 80)
    private String stateInterface;

    @Column(name = "penalty")
    private int penalty;

    @Column(name = "paused")
    private int paused;

    @Column(name = "uniqueid", nullable = false, unique = true)
    private int uniqueid;
}
