package com.telefonix.getafix.asterisk.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by tareqmy on 2019-01-21.
 * note: server-generated
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"axVoiceMessageId"})
@Table(name = "voicemessages")
public class AXVoiceMessage {

    @EmbeddedId
    private AXVoiceMessageId axVoiceMessageId;

    @Column(name = "context", length = 80)
    private String context;

    @Column(name = "macrocontext", length = 80)
    private String macroContext;

    @Column(name = "callerid", length = 80)
    private String callerId;

    //NOTE: this time is an instant
    @Column(name = "origtime", length = 40)
    private String origTime;

    @Column(name = "duration", length = 20)
    private String duration;

    @Column(name = "flag", length = 8)
    private String flag;

    @Column(name = "mailboxuser", length = 30)
    private String mailboxUser;

    @Column(name = "mailboxcontext", length = 30)
    private String mailboxContext;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "recording")
    private byte[] recording;

    @Column(name = "msg_id", length = 40)
    private String msgId;

    @Override
    public String toString() {
        return "AXVoiceMessage{" +
            "axVoiceMessageId=" + axVoiceMessageId +
            ", context='" + context + '\'' +
            ", macroContext='" + macroContext + '\'' +
            ", callerId='" + callerId + '\'' +
            ", origTime='" + origTime + '\'' +
            ", duration='" + duration + '\'' +
            ", flag='" + flag + '\'' +
            ", mailboxUser='" + mailboxUser + '\'' +
            ", mailboxContext='" + mailboxContext + '\'' +
//            ", recording=" + Arrays.toString(recording) +
            ", msgId='" + msgId + '\'' +
            '}';
    }
}
