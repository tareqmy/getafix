FROM openjdk:11-jre-slim
LABEL creator="Tareq Mohammad Yousuf"
LABEL email="tareq.y@gmail.com"

# install ffmpeg
RUN apt-get update \
    && apt-get install -y ffmpeg postgresql-client

# creates the folder if doesnt exist
WORKDIR /getafix
COPY entrypoint.sh /opt/
COPY wait-for-db.sh /opt/
RUN chmod +x /opt/*.sh

COPY getafix-web/build/libs/getafix-web-1.0.0-SNAPSHOT.jar /getafix/

EXPOSE 8080

CMD ["/opt/entrypoint.sh"]
