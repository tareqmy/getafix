package com.telefonix.getafix.asterisk.ari.enums;

/**
 * Created by tmyousuf on 4/11/16.
 */
public enum HangupReason {
    normal,
    busy,
    congestion,
    no_answer
}
