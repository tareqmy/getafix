package com.telefonix.getafix.asterisk.ami;

import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ManagerConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Slf4j
@Component
public class AMIConnectionManager {

    @Value(value = "${getafix.asterisk.enabled}")
    private boolean enabled;

    @Value(value = "${getafix.asterisk.ami.connect.retryInMili}")
    private int retryInMili = 5000;

    @Value(value = "${getafix.asterisk.ami.timeoutInMili}")
    private int amiConnectionTimeoutInMili;

    private AsteriskManagerConnection asteriskManagerConnection;

    @Autowired
    public AMIConnectionManager(AsteriskManagerConnection asteriskManagerConnection) {
        this.asteriskManagerConnection = asteriskManagerConnection;
    }

    @PostConstruct
    public void init() {
        if (!enabled) {
            return;
        }
        log.debug("init ->");
        Runnable r = () -> {
            while (!connect()) {
                try {
                    Thread.sleep(retryInMili);
                } catch (InterruptedException e) {
                    // ignore the exception
                }
            }
        };

        //cant use @Async because at this time async will not be functional
        //so if for some reason asterisk is down. it block the startup of the whole application
        Thread t = new Thread(r);
        t.start();
    }

    private boolean connect() {
        try {
            ManagerConnection connection = asteriskManagerConnection.getManagerConnectionFactory().createManagerConnection();
            connection.login();
            log.debug("Connected successfully to asterisk manager interface....{}", connection.getHostname());
            asteriskManagerConnection.setManagerConnection(connection);
            asteriskManagerConnection.addEventListener();
        } catch (Exception e) {
            log.error("Could not Initialize ConnectionManager! " + e);
            return false;
        }
        return true;
    }

    @PreDestroy
    public void destroy() {
        if (!enabled) {
            return;
        }
        log.debug("destroy ->");
        try {
            asteriskManagerConnection.getManagerConnection().logoff();
        } catch (IllegalStateException e) {
            log.warn("trying to logoff failed: " + e);
        }
    }
}
