package com.telefonix.getafix.asterisk.ari;

import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Endpoint;
import ch.loway.oss.ari4java.generated.Variable;
import ch.loway.oss.ari4java.tools.RestException;

import java.util.List;

/**
 * Created by tareqmy on 2019-02-07.
 */
public interface AsteriskResourceService {

    void reloadModule(String moduleName) throws RestException;

    void setChannelVar(String channelId, String varName, String varValue) throws RestException;

    void holdChannel(String channelId) throws RestException;

    void continueChannelInDialplan(String channelId, String context, String extension, int priority, String label) throws RestException;

    void hangupChannel(String channelId, String hangupReason) throws RestException;

    Endpoint getEndpoint(String tech, String resource) throws RestException;

    List<Endpoint> getAllEndpoints() throws RestException;

    List<Channel> getAllChannels() throws RestException;

    Channel getChannel(String channelId) throws RestException;

    Variable getChannelVar(String channelId, String varName) throws RestException;

    List<Bridge> getAllBridges() throws RestException;

    Bridge getBridge(String bridgeId) throws RestException;

    void addChannel(String bridgeId, String channelId) throws RestException;
}
