package com.telefonix.getafix.asterisk.ami;

import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ResponseEvents;
import org.asteriskjava.manager.action.*;
import org.asteriskjava.manager.response.GetVarResponse;
import org.asteriskjava.manager.response.ManagerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by tareqmy on 2019-02-07.
 */
@Slf4j
@Service
public class AsteriskManagerCommandService {

    private AsteriskManagerConnection asteriskManagerConnection;

    @Autowired
    public AsteriskManagerCommandService(AsteriskManagerConnection asteriskManagerConnection) {
        this.asteriskManagerConnection = asteriskManagerConnection;
    }

    public String originate(String channel, String callerId, String application, String data, Map<String, String> variables) {
        log.debug("originate {} {} {} {} {}", channel, callerId, application, data, variables);
        OriginateAction action = new OriginateAction();
        action.setChannel(channel);
        action.setCallerId(callerId);
        action.setApplication(application);
        action.setData(data);
        action.setVariables(variables);
        action.setAsync(true);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String setVariable(String channel, String name, String value) {
        SetVarAction action = new SetVarAction();
        action.setChannel(channel);
        action.setVariable(name);
        action.setValue(value);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public void setVariables(String channel, Map<String, String> variables) {
        variables.forEach((name, value) -> {
            SetVarAction action = new SetVarAction();
            action.setChannel(channel);
            action.setVariable(name);
            action.setValue(value);
            asteriskManagerConnection.sendAction(action);
        });
    }

    public String getVariable(String channel, String name) {
        GetVarAction action = new GetVarAction();
        action.setChannel(channel);
        action.setVariable(name);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        log.debug("managerResponse {}", managerResponse);
        if (managerResponse != null) {
            GetVarResponse getVarResponse = (GetVarResponse) managerResponse;
            return getVarResponse.getValue();
        }
        return null;
    }

    public String queueAdd(String queueName, String iface, String memberName) {
        QueueAddAction action = new QueueAddAction();
        action.setInterface(iface);
        action.setMemberName(memberName);
        action.setQueue(queueName);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String queueRemove(String queueName, String iface) {
        QueueRemoveAction action = new QueueRemoveAction();
        action.setInterface(iface);
        action.setQueue(queueName);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String queuePause(String queueName, String iface) {
        return queuePause(queueName, iface, true);
    }

    private String queuePause(String queueName, String iface, boolean b) {
        QueuePauseAction action = new QueuePauseAction();
        action.setInterface(iface);
        action.setQueue(queueName);
        action.setPaused(b);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String queueUnpause(String queueName, String iface) {
        return queuePause(queueName, iface, false);
    }

    public ResponseEvents queueStatus() {
        QueueStatusAction action = new QueueStatusAction();
        return asteriskManagerConnection.sendEventGeneratingAction(action);
    }

    public ResponseEvents queueStatus(String queueName) {
        QueueStatusAction action = new QueueStatusAction();
        action.setQueue(queueName);
        return asteriskManagerConnection.sendEventGeneratingAction(action);
    }

    public ResponseEvents queueSummary(String queueName) {
        QueueSummaryAction action = new QueueSummaryAction();
        action.setQueue(queueName);
        return asteriskManagerConnection.sendEventGeneratingAction(action);
    }

    public String blindTransfer(String channel, String context, String exten) {
        log.debug("blindTransfer {} to {}:{}", channel, context, exten);
        BlindTransferAction action = new BlindTransferAction();
        action.setChannel(channel);
        action.setContext(context);
        action.setExten(exten);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String redirectChannel(String channel, String context, String exten) {
        log.debug("redirectChannel {} to {}:{}", channel, context, exten);
        RedirectAction action = new RedirectAction();
        action.setChannel(channel);
        action.setContext(context);
        action.setExten(exten);
        action.setPriority(1);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String bridgeChannels(String channel1, String channel2) {
        log.debug("bridgeChannels {} and {}", channel1, channel2);
        BridgeAction action = new BridgeAction();
        action.setChannel1(channel1);
        action.setChannel2(channel2);
//        action.setTone(true);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(action);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }

    public String voicemailRefresh(String context, String mailbox) {
        MWIUpdateAction mwiUpdateAction = new MWIUpdateAction();
        mwiUpdateAction.setMailbox(mailbox + "@" + context);
        log.debug("voicemailRefresh {} / {}", context, mailbox);
        VoicemailRefreshAction action = new VoicemailRefreshAction();
        action.setContext(context);
        action.setMailbox(mailbox);
        ManagerResponse managerResponse = asteriskManagerConnection.sendAction(mwiUpdateAction);
        if (managerResponse != null) {
            return managerResponse.getResponse();
        }
        return null;
    }
}
