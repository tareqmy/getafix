package com.telefonix.getafix.asterisk.ami;

/**
 * Created by tareqmy on 2019-02-07.
 */
public interface ManagerEventNotificationService {

    void registerManagerEventListener(ManagerEventListener managerEventListener);

    void unregisterManagerEventListener(ManagerEventListener managerEventListener);
}
