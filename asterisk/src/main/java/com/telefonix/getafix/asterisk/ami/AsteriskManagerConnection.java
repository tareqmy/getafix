package com.telefonix.getafix.asterisk.ami;

import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.*;
import org.asteriskjava.manager.action.EventGeneratingAction;
import org.asteriskjava.manager.action.ManagerAction;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.response.ManagerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by tareqmy on 2019-02-06.
 */
@Slf4j
@Component
public class AsteriskManagerConnection implements ManagerEventListener {

    private ManagerConnectionFactory managerConnectionFactory;

    private ManagerConnection managerConnection;

    private ManagerEventProcessor managerEventProcessor;

    @Autowired
    public AsteriskManagerConnection(ManagerConnectionFactory managerConnectionFactory,
                                     ManagerEventProcessor managerEventProcessor) {
        this.managerConnectionFactory = managerConnectionFactory;
        this.managerEventProcessor = managerEventProcessor;
    }

    @Override
    public void onManagerEvent(ManagerEvent event) {
        managerEventProcessor.onManagerEvent(event);
    }

    ManagerResponse sendAction(ManagerAction managerAction) {
        try {
            return managerConnection.sendAction(managerAction);
        } catch (IOException | TimeoutException e) {
            log.error("Send action failed {}", e.getMessage());
        }
        return null;
    }

    ResponseEvents sendEventGeneratingAction(EventGeneratingAction eventGeneratingAction) {
        try {
            return managerConnection.sendEventGeneratingAction(eventGeneratingAction);
        } catch (IOException | TimeoutException e) {
            log.error("Send action failed {}", e.getMessage());
        }
        return null;
    }

    void addEventListener() {
        managerConnection.addEventListener(this);
    }

    ManagerConnectionFactory getManagerConnectionFactory() {
        return managerConnectionFactory;
    }

    ManagerConnection getManagerConnection() {
        return managerConnection;
    }

    void setManagerConnection(ManagerConnection managerConnection) {
        this.managerConnection = managerConnection;
    }
}
