package com.telefonix.getafix.asterisk.ari;

import ch.loway.oss.ari4java.generated.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by tareqmy on 2019-02-10.
 */
@Slf4j
@Component
public class MessageQueueHandler implements ResourceMessageNotificationService {

    private boolean messageQueueEnabled = true;

    private Set<ResourceMessageListener> resourceMessageListeners = new HashSet<>();

    @Async("asteriskTaskExecutor")
    @Override
    public void onResourceMessage(Message message) {
        if (messageQueueEnabled) {
            resourceMessageListeners.forEach(resourceMessageListener -> resourceMessageListener.onResourceMessage(message));
        }
    }

    @PreDestroy
    public void shutDown() {
        this.messageQueueEnabled = false;
    }

    @Override
    public void registerResourceMessageListener(ResourceMessageListener resourceMessageListener) {
        log.debug("##########registering for resource message {}", resourceMessageListener.getClass());
        resourceMessageListeners.add(resourceMessageListener);
    }

    @Override
    public void unregisterResourceMessageListener(ResourceMessageListener resourceMessageListener) {
        resourceMessageListeners.remove(resourceMessageListener);
    }
}
