package com.telefonix.getafix.asterisk.ari;

import ch.loway.oss.ari4java.generated.Message;

/**
 * Created by tareqmy on 2019-02-11.
 */
public interface ResourceMessageNotificationService {

    void onResourceMessage(Message message);

    void registerResourceMessageListener(ResourceMessageListener resourceMessageListener);

    void unregisterResourceMessageListener(ResourceMessageListener resourceMessageListener);
}
