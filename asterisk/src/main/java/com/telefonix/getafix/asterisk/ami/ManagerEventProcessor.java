package com.telefonix.getafix.asterisk.ami;

import lombok.extern.slf4j.Slf4j;
import org.asteriskjava.manager.event.ManagerEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by tareqmy on 2019-02-07.
 * ; all       - All event classes below (including any we may have missed).
 * ; system    - General information about the system and ability to run system
 * ;             management commands, such as Shutdown, Restart, and Reload. This
 * ;             class also includes dialplan manipulation actions such as
 * ;             DialplanExtensionAdd and DialplanExtensionRemove.
 * ; call      - Information about channels and ability to set information in a
 * ;             running channel.
 * ; log       - Logging information.  Read-only. (Defined but not yet used.)
 * ; verbose   - Verbose information.  Read-only. (Defined but not yet used.)
 * ; agent     - Information about queues and agents and ability to add queue
 * ;             members to a queue.
 * ; user      - Permission to send and receive UserEvent.
 * ; config    - Ability to read and write configuration files.
 * ; command   - Permission to run CLI commands.  Write-only.
 * ; dtmf      - Receive DTMF events.  Read-only.
 * ; reporting - Ability to get information about the system.
 * ; cdr       - Output of cdr_manager, if loaded.  Read-only.
 * ; dialplan  - Receive NewExten and VarSet events.  Read-only.
 * ; originate - Permission to originate new calls.  Write-only.
 * ; agi       - Output AGI commands executed.  Input AGI command to execute.
 * ; cc        - Call Completion events.  Read-only.
 * ; aoc       - Permission to send Advice Of Charge messages and receive Advice
 * ;           - Of Charge events.
 * ; test      - Ability to read TestEvent notifications sent to the Asterisk Test
 * ;             Suite.  Note that this is only enabled when the TEST_FRAMEWORK
 * ;             compiler flag is defined.
 * ; security  - Security Events.  Read-only.
 * ; message   - Permissions to send out of call messages. Write-only
 * ;
 * ;read = system,call,log,verbose,agent,user,config,dtmf,reporting,cdr,dialplan
 * ;write = system,call,agent,user,config,command,reporting,originate,message
 */
@Slf4j
@Component
public class ManagerEventProcessor implements ManagerEventNotificationService {

    private Set<ManagerEventListener> managerEventListeners = new HashSet<>();

    @Async("asteriskTaskExecutor")
    void onManagerEvent(ManagerEvent event) {
        if (event == null) {
            log.info("null found!");
            return;
        }
        log.trace("ManagerEvent received {}", event);
        //note: privilege is null for many cases... unreliable
        managerEventListeners.forEach(managerEventListener -> managerEventListener.onManagerEvent(event));
    }

    @Override
    public void registerManagerEventListener(ManagerEventListener managerEventListener) {
        managerEventListeners.add(managerEventListener);
    }

    @Override
    public void unregisterManagerEventListener(ManagerEventListener managerEventListener) {
        managerEventListeners.remove(managerEventListener);
    }
}
