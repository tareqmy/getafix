package com.telefonix.getafix.asterisk.ari;

import ch.loway.oss.ari4java.ARI;
import ch.loway.oss.ari4java.AriVersion;
import ch.loway.oss.ari4java.generated.ActionEvents;
import ch.loway.oss.ari4java.generated.Application;
import ch.loway.oss.ari4java.generated.AsteriskInfo;
import ch.loway.oss.ari4java.generated.Message;
import ch.loway.oss.ari4java.tools.ARIException;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by tareqmy on 2019-02-07.
 */
@Slf4j
@Component
public class ARIConnectionManager {

    @Value(value = "${getafix.asterisk.enabled}")
    private boolean enabled;

    @Value(value = "${getafix.asterisk.ari.url}")
    private String url;

    @Value(value = "${getafix.asterisk.ari.version}")
    private String version;

    @Value(value = "${getafix.asterisk.ari.username}")
    private String username;

    @Value(value = "${getafix.asterisk.ari.password}")
    private String password;

    @Value(value = "${getafix.asterisk.ari.applicationName}")
    private String applicationName;

    @Value(value = "${getafix.asterisk.ari.connect.retryInMili}")
    private int retryInMili = 5000;

    private ARI ari;

    private ActionEvents liveActionEvent = null;

    private AsteriskResourceServiceImpl asteriskResourceService;

    private MessageQueueHandler messageQueueHandler;

    public ARIConnectionManager(AsteriskResourceServiceImpl asteriskResourceService,
                                MessageQueueHandler messageQueueHandler) {
        this.asteriskResourceService = asteriskResourceService;
        this.messageQueueHandler = messageQueueHandler;
    }

    @PostConstruct
    public void init() {
        Runnable r = () -> {
            boolean success = false;
            if (enabled) {
                while (!success) {
                    try {
                        connect();
                    } catch (ARIException e) {
                        log.error("Failed to connect to asterisk from ARI app ... {}", toString());
                    }
                    success = true;
                    try {
                        Thread.sleep(retryInMili);
                    } catch (InterruptedException e) {
                        // ignore the exception
                    }
                }
            }
        };

        //cant use @Async because at this time async will not be functional
        //so if for some reason asterisk is down. it block the startup of the whole application
        Thread t = new Thread(r);
        t.start();
    }

    /**
     * @throws ARIException
     */
    private void connect() throws ARIException {
        log.debug("Connecting to: " + url + " as " + username);
        ari = ARI.build(url, applicationName, username, password, AriVersion.fromVersionString(version));
        log.debug("Connected through ARI: " + ari.getVersion());
        this.asteriskResourceService.setAri(ari);

        // let's raise an exeption if the connection is not valid
        AsteriskInfo ai = ari.asterisk().getInfo("");
        log.debug("Hey! We're connected! Asterisk Version: " + ai.getSystem().getVersion());

        // register the AE so we can disconnectWs it when the erorr goes down
        liveActionEvent = ari.events();
        liveActionEvent.eventWebsocket(applicationName, true, new AriCallback<>() {

            @Override
            public void onSuccess(Message result) {
                messageQueueHandler.onResourceMessage(result);
            }

            @Override
            public void onFailure(RestException e) {
                log.warn("Err:" + e.getMessage());
            }
        });
    }

    @Scheduled(initialDelayString = "${getafix.asterisk.ari.websocket.pollConnection.initialDelay}",
        fixedRateString = "${getafix.asterisk.ari.websocket.pollConnection.fixedRate}")
    public void pollConnection() {
        if (enabled) {
            boolean isAlive = false;
            try {
                Application application = ari.applications().get(applicationName);
                if (application != null) {
                    isAlive = true;
                }
            } catch (RestException e) {
                log.warn("Connection failed --- {}", e.getMessage());
            }

            if (!isAlive) {
                reconnect();
            }
        }
    }

    private void reconnect() {
        shutDown();
        init();
    }

    @PreDestroy
    public void shutDown() {
        try {
            if (enabled) {
                if (liveActionEvent != null)
                    ari.closeAction(liveActionEvent);
                ari.cleanup();
            }
        } catch (ARIException e) {
            log.warn("Failed to shutdown ARI application...");
        }
    }
}
