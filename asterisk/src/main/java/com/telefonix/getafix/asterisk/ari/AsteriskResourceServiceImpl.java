package com.telefonix.getafix.asterisk.ari;

import ch.loway.oss.ari4java.ARI;
import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.Channel;
import ch.loway.oss.ari4java.generated.Endpoint;
import ch.loway.oss.ari4java.generated.Variable;
import ch.loway.oss.ari4java.tools.RestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tareqmy on 2019-02-07.
 */
@Slf4j
@Service
public class AsteriskResourceServiceImpl implements AsteriskResourceService {

    private ARI ari;

    @Override
    public void reloadModule(String moduleName) throws RestException {
        this.ari.asterisk().reloadModule(moduleName);
    }

    @Override
    public void setChannelVar(String channelId, String varName, String varValue) throws RestException {
        this.ari.channels().setChannelVar(channelId, varName, varValue);
    }

    @Override
    public void holdChannel(String channelId) throws RestException {
        this.ari.channels().hold(channelId);
    }

    @Override
    public void continueChannelInDialplan(String channelId, String context, String extension, int priority, String label) throws RestException {
        this.ari.channels().continueInDialplan(channelId, context, extension, priority, label);
    }

    @Override
    public void hangupChannel(String channelId, String hangupReason) throws RestException {
        this.ari.channels().hangup(channelId, hangupReason);
    }

    @Override
    public Endpoint getEndpoint(String tech, String resource) throws RestException {
        return this.ari.endpoints().get(tech, resource);
    }

    @Override
    public List<Endpoint> getAllEndpoints() throws RestException {
        return this.ari.endpoints().list();
    }

    @Override
    public List<Channel> getAllChannels() throws RestException {
        return this.ari.channels().list();
    }

    @Override
    public Channel getChannel(String channelId) throws RestException {
        return this.ari.channels().get(channelId);
    }

    @Override
    public Variable getChannelVar(String channelId, String varName) throws RestException {
        return this.ari.channels().getChannelVar(channelId, varName);
    }

    @Override
    public List<Bridge> getAllBridges() throws RestException {
        return this.ari.bridges().list();
    }

    @Override
    public Bridge getBridge(String bridgeId) throws RestException {
        return this.ari.bridges().get(bridgeId);
    }

    @Override
    public void addChannel(String bridgeId, String channelId) throws RestException {
        this.ari.bridges().addChannel(bridgeId, channelId, "", false, false);
    }

    public void setAri(ARI ari) {
        this.ari = ari;
    }
}
