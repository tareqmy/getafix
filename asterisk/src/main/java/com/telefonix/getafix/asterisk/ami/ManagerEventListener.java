package com.telefonix.getafix.asterisk.ami;

import org.asteriskjava.manager.event.ManagerEvent;

/**
 * Created by tareqmy on 2019-02-11.
 */
public interface ManagerEventListener {

    void onManagerEvent(ManagerEvent event);
}
