count=0
while [ "$( psql -h $DATABASE -p $DB_PORT $DB_NAME $DB_USER -lqt | grep $DB_NAME | wc -l )" = '0' ]
do
  sleep 1
  count=$((count + 1))
  echo "count: $count waiting....$(date)"
  if [ "$count" -eq 100 ]; then
    exit 1;
  fi
done
