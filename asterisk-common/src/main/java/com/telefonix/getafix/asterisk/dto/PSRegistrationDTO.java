package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSRegistration;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSRegistrationDTO {

    @NotNull
    @Size(max = 40)
    private String id;

    @Size(max = 5)
    private String authRejectionPermanent;

    @Size(max = 255)
    private String clientUri;

    @Size(max = 40)
    private String contactUser;

    private int expiration;

    private int maxRetries;

    @Size(max = 40)
    private String outboundAuth;

    @Size(max = 40)
    private String outboundProxy;

    private int retryInterval;

    private int forbiddenRetryInterval;

    @Size(max = 255)
    private String serverUri;

    @Size(max = 40)
    private String transport;

    @Size(max = 5)
    private String supportPath;

    private int fatalRetryInterval;

    @Size(max = 5)
    private String line;

    @Size(max = 40)
    private String endpoint;

    public PSRegistrationDTO(PSRegistration psRegistration) {
        setId(psRegistration.getId());
        setAuthRejectionPermanent(psRegistration.getAuthRejectionPermanent().name());
        setClientUri(psRegistration.getClientUri());
        setContactUser(psRegistration.getContactUser());
        setExpiration(psRegistration.getExpiration());
        setMaxRetries(psRegistration.getMaxRetries());
        setOutboundAuth(psRegistration.getOutboundAuth());
        setOutboundProxy(psRegistration.getOutboundProxy());
        setRetryInterval(psRegistration.getRetryInterval());
        setForbiddenRetryInterval(psRegistration.getForbiddenRetryInterval());
        setServerUri(psRegistration.getServerUri());
        setTransport(psRegistration.getTransport());
        setSupportPath(psRegistration.getSupportPath().name());
        setFatalRetryInterval(psRegistration.getFatalRetryInterval());
        setLine(psRegistration.getLine().name());
        setEndpoint(psRegistration.getEndpoint());
    }
}
