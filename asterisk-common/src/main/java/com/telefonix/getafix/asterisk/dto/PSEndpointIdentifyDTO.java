package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSEndpointIdentify;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSEndpointIdentifyDTO {

    @NotNull
    @Size(max = 40)
    private String id;

    @Size(max = 40)
    private String endpoint;

    @Size(max = 80)
    private String match;

    public PSEndpointIdentifyDTO(PSEndpointIdentify psEndpointIdentify) {
        setId(psEndpointIdentify.getId());
        setEndpoint(psEndpointIdentify.getEndpoint());
        setMatch(psEndpointIdentify.getMatch());
    }
}
