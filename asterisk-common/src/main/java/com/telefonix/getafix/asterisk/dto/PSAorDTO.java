package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSAor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSAorDTO {

    @NotNull
    @Size(max = 40)
    private String id;

    @Size(max = 255)
    private String contact;

    private int defaultExpiration;

    @Size(max = 80)
    private String mailBoxes;

    private int maxContacts;

    private int minimumExpiration;

    @Size(max = 5)
    private String removeExisting;

    private int qualifyFrequency;

    @Size(max = 5)
    private String authenticateQualify;

    private int maximumExpiration;

    @Size(max = 40)
    private String outboundProxy;

    @Size(max = 5)
    private String supportPath;

    private int qualifyTimeout;

    public PSAorDTO(PSAor psAor) {
        setId(psAor.getId());
        setContact(psAor.getContact());
        setDefaultExpiration(psAor.getDefaultExpiration());
        setMailBoxes(psAor.getMailBoxes());
        setMaxContacts(psAor.getMaxContacts());
        setMinimumExpiration(psAor.getMinimumExpiration());
        setRemoveExisting(psAor.getRemoveExisting().name());
        setQualifyFrequency(psAor.getQualifyFrequency());
        setAuthenticateQualify(psAor.getAuthenticateQualify().name());
        setMaximumExpiration(psAor.getMaximumExpiration());
        setOutboundProxy(psAor.getOutboundProxy());
        setSupportPath(psAor.getSupportPath().name());
        setQualifyTimeout(psAor.getQualifyTimeout());
    }
}
