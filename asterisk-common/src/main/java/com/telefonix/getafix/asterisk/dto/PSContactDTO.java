package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSContact;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSContactDTO {

    @NotNull
    @Size(max = 255)
    private String id;

    @Size(max = 255)
    private String uri;

    @Size(max = 40)
    private String expirationTime;

    @Size(max = 40)
    private String qualifyFrequency;

    @Size(max = 40)
    private String outboundProxy;

    private String path;

    @Size(max = 255)
    private String userAgent;

    @Size(max = 40)
    private String qualifyTimeout;

    @NotNull
    @Size(max = 20)
    private String regServer;

    @Size(max = 20)
    private String authenticateQualify;

    @Size(max = 40)
    private String viaAddr;

    @Size(max = 20)
    private String viaPort;

    @Size(max = 255)
    private String callId;

    @Size(max = 40)
    private String endpoint;

    @Size(max = 20)
    private String pruneOnBoot;

    public PSContactDTO(PSContact psContact) {
        setId(psContact.getId());
        setUri(psContact.getUri());
        setExpirationTime(psContact.getExpirationTime());
        setQualifyFrequency(psContact.getQualifyFrequency());
        setOutboundProxy(psContact.getOutboundProxy());
        setPath(psContact.getPath());
        setUserAgent(psContact.getUserAgent());
        setQualifyTimeout(psContact.getQualifyTimeout());
        setRegServer(psContact.getRegServer());
        setAuthenticateQualify(psContact.getAuthenticateQualify());
        setViaAddr(psContact.getViaAddr());
        setViaPort(psContact.getViaPort());
        setCallId(psContact.getCallId());
        setEndpoint(psContact.getEndpoint());
        setPruneOnBoot(psContact.getPruneOnBoot());
    }
}
