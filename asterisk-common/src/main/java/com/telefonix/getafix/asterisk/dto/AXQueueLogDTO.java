package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXQueueLog;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by tareqmy on 2019-01-22.
 * note: server-generated
 */
@Data
@NoArgsConstructor
public class AXQueueLogDTO {

    private Long id;

    private String callid;

    private String queueName;

    private String agent;

    private String event;

    private String data1;

    private String data2;

    private String data3;

    private String data4;

    private String data5;

    private String time;

    public AXQueueLogDTO(AXQueueLog axQueueLog, ZoneId zoneId) {
        setId(axQueueLog.getId());
        setCallid(axQueueLog.getCallid());
        setQueueName(axQueueLog.getQueueName());
        setAgent(axQueueLog.getAgent());
        setEvent(axQueueLog.getEvent().name());
        setData1(axQueueLog.getData1());
        setData2(axQueueLog.getData2());
        setData3(axQueueLog.getData3());
        setData4(axQueueLog.getData4());
        setData5(axQueueLog.getData5());
        setTime(axQueueLog.getTime()
            .atZone(zoneId)
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    public static Optional<AXQueueLogDTO> getDTO(AXQueueLog axQueueLog, ZoneId zoneId) {
        if (axQueueLog == null)
            return Optional.empty();
        AXQueueLogDTO dto = new AXQueueLogDTO(axQueueLog, zoneId);
        return Optional.of(dto);
    }
}
