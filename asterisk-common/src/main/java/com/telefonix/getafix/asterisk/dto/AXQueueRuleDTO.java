package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXQueueRule;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class AXQueueRuleDTO {

    @NotNull
    @Size(max = 80)
    private String ruleName;

    @NotNull
    @Size(max = 32)
    private String time;

    @NotNull
    @Size(max = 32)
    private String minPenalty;

    @NotNull
    @Size(max = 32)
    private String maxPenalty;

    public AXQueueRuleDTO(AXQueueRule axQueueRule) {
        setRuleName(axQueueRule.getRuleName());
        setTime(axQueueRule.getTime());
        setMinPenalty(axQueueRule.getMinPenalty());
        setMaxPenalty(axQueueRule.getMaxPenalty());
    }
}
