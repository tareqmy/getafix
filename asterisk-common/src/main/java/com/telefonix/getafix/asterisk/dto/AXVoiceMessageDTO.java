package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXVoiceMessage;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by tareqmy on 2019-01-22.
 * note: server-generated
 */
@Data
@NoArgsConstructor
public class AXVoiceMessageDTO {

    private String dir;

    private String msgNum;

    private String context;

    private String macroContext;

    private String callerId;

    private String origTime;

    private String duration;

    private String flag;

    private String mailboxUser;

    private String mailboxContext;

    private byte[] recording;

    private String msgId;

    public AXVoiceMessageDTO(AXVoiceMessage axVoiceMessage) {
        setDir(axVoiceMessage.getAxVoiceMessageId().getDir());
        setMsgNum(axVoiceMessage.getAxVoiceMessageId().getMsgNum());
        setContext(axVoiceMessage.getContext());
        setMacroContext(axVoiceMessage.getMacroContext());
        setCallerId(axVoiceMessage.getCallerId());
        setCallerId(axVoiceMessage.getCallerId());
        setOrigTime(axVoiceMessage.getOrigTime());
        setDuration(axVoiceMessage.getDuration());
        setFlag(axVoiceMessage.getFlag());
        setMailboxUser(axVoiceMessage.getMailboxUser());
        setMailboxContext(axVoiceMessage.getMailboxContext());
        setRecording(axVoiceMessage.getRecording());
        setMsgId(axVoiceMessage.getMsgId());
    }
}
