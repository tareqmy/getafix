package com.telefonix.getafix.asterisk.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tareqmy on 22/10/19.
 */
public class AsteriskTelephonyUtils {

    private final static String mailBoxRegex = "^(\\d{2})@([a-zA-Z0-9]+)(,([bdgsuUP]+))*$";

    private AsteriskTelephonyUtils() {
    }

    public static String getMailBox(String extension, String context) {
        return extension + "@" + context;
    }

    public static boolean isValidMailbox(String mailbox) {
        return mailbox != null && mailbox.matches(mailBoxRegex);
    }

    public static String getMailContext(String mailbox) {
        if (StringUtils.isEmpty(mailbox)) {
            return "";
        }
        return getMatchGroup(mailBoxRegex, mailbox, 2);
    }

    public static String getMailExtension(String mailbox) {
        if (StringUtils.isEmpty(mailbox)) {
            return "";
        }
        return getMatchGroup(mailBoxRegex, mailbox, 1);
    }

    private static String getMatchGroup(String regex, String target, int group) {
        Pattern pattern = Pattern.compile(regex);
        return getMatchGroup(pattern, target, group);
    }

    private static String getMatchGroup(Pattern pattern, String target, int group) {
        Matcher matcher = pattern.matcher(target);
        while (matcher.find()) {
            return matcher.group(group);
        }

        return "";
    }
}
