package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXQueueMember;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class AXQueueMemberDTO {

    @NotNull
    @Size(max = 80)
    private String queueName;

    @NotNull
    @Size(max = 80)
    private String interFace;

    @Size(max = 80)
    private String membername;

    @Size(max = 80)
    private String stateInterface;

    private int penalty;

    private int paused;

    @NotNull
    private int uniqueid;

    public AXQueueMemberDTO(AXQueueMember axQueueMember) {
        setQueueName(axQueueMember.getAxQueueMemberId().getQueueName());
        setInterFace(axQueueMember.getAxQueueMemberId().getInterFace());
        setMembername(axQueueMember.getMembername());
        setStateInterface(axQueueMember.getStateInterface());
        setPenalty(axQueueMember.getPenalty());
        setPaused(axQueueMember.getPaused());
        setUniqueid(axQueueMember.getUniqueid());
    }
}
