package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSEndpoint;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSEndpointDTO {

    @NotNull
    @Size(max = 40)
    private String id;

    @Size(max = 40)
    private String transport;

    @NotNull
    @Size(max = 40)
    private String context;

    @Size(max = 200)
    private String disallow;

    @Size(max = 200)
    private String allow;

    @Size(max = 40)
    private String auth;

    @Size(max = 200)
    private String aors;

    @Size(max = 40)
    private String callerId;

    @Size(max = 40)
    private String accountCode;

    private String setVar;

    @Size(max = 5)
    private String directMedia;

    @Size(max = 5)
    private String rtpSymmetric;

    @Size(max = 5)
    private String forceRport;

    private int deviceStateBusyAt;

    public PSEndpointDTO(PSEndpoint psEndpoint) {
        setId(psEndpoint.getId());
        setTransport(psEndpoint.getTransport());
        setContext(psEndpoint.getContext());
        setDisallow(psEndpoint.getDisallow());
        setAllow(psEndpoint.getAllow());
        setAuth(psEndpoint.getAuth());
        setAors(psEndpoint.getAors());
        setCallerId(psEndpoint.getCallerId());
        setAccountCode(psEndpoint.getAccountCode());
        setSetVar(psEndpoint.getSetVar());
        setDirectMedia(psEndpoint.getDirectMedia().name());
        setRtpSymmetric(psEndpoint.getRtpSymmetric().name());
        setForceRport(psEndpoint.getForceRport().name());
        setDeviceStateBusyAt(psEndpoint.getDeviceStateBusyAt());
    }
}
