package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXQueue;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class AXQueueDTO {

    @NotNull
    @Size(max = 128)
    private String name;

    private int weight;

    @Size(max = 128)
    private String musicclass;

    @Size(max = 20)
    private String strategy;

    @Size(max = 5)
    private String ringinuse;

    @Size(max = 5)
    private String autopause;

    private int maxlen;

    @Size(max = 5)
    private String setinterfacevar;

    @Size(max = 5)
    private String setqueueentryvar;

    @Size(max = 5)
    private String setqueuevar;

    private int timeout;

    private int retry;

    private String timeoutpriority;

    private int announceFrequency;

    private int minAnnounceFrequency;

    private int periodicAnnounceFrequency;

    @Size(max = 128)
    private String randomPeriodicAnnounce;

    @Size(max = 128)
    private String relativePeriodicAnnounce;

    @Size(max = 128)
    private String announceHoldtime;

    @Size(max = 128)
    private String announcePosition;

    private int announcePositionLimit;

    private int announceRoundSeconds;

    @Size(max = 128)
    private String joinempty;

    @Size(max = 128)
    private String leavewhenempty;

    public AXQueueDTO(AXQueue axQueue) {
        setName(axQueue.getName());
        setWeight(axQueue.getWeight());
        setMusicclass(axQueue.getMusicclass());
        setStrategy(axQueue.getStrategy().name());
        setRinginuse(axQueue.getRinginuse().name());
        setAutopause(axQueue.getAutopause().name());
        setMaxlen(axQueue.getMaxlen());
        setSetinterfacevar(axQueue.getSetinterfacevar().name());
        setSetqueueentryvar(axQueue.getSetqueueentryvar().name());
        setSetqueuevar(axQueue.getSetqueuevar().name());
        setTimeout(axQueue.getTimeout());
        setRetry(axQueue.getRetry());
        setTimeoutpriority(axQueue.getTimeoutpriority().name());
        setAnnounceFrequency(axQueue.getAnnounceFrequency());
        setMinAnnounceFrequency(axQueue.getMinAnnounceFrequency());
        setPeriodicAnnounceFrequency(axQueue.getPeriodicAnnounceFrequency());
        setRandomPeriodicAnnounce(axQueue.getRandomPeriodicAnnounce().name());
        setRelativePeriodicAnnounce(axQueue.getRelativePeriodicAnnounce().name());
        setAnnounceHoldtime(axQueue.getAnnounceHoldtime().name());
        setAnnouncePosition(axQueue.getAnnouncePosition().name());
        setAnnouncePositionLimit(axQueue.getAnnouncePositionLimit());
        setAnnounceRoundSeconds(axQueue.getAnnounceRoundSeconds());
        setJoinempty(axQueue.getJoinempty());
        setLeavewhenempty(axQueue.getLeavewhenempty());
    }
}
