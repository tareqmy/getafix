package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.AXVoiceMail;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class AXVoiceMailDTO {

    private Long uniqueid;

    @NotNull
    @Size(max = 80)
    private String context;

    @NotNull
    @Size(max = 80)
    private String mailbox;

    @NotNull
    @Size(max = 80)
    private String password;

    @Size(max = 80)
    private String fullName;

    @Size(max = 80)
    private String email;

    public AXVoiceMailDTO(AXVoiceMail axVoiceMail) {
        setUniqueid(axVoiceMail.getUniqueid());
        setContext(axVoiceMail.getContext());
        setMailbox(axVoiceMail.getMailbox());
        setPassword(axVoiceMail.getPassword());
        setFullName(axVoiceMail.getFullName());
        setEmail(axVoiceMail.getEmail());
    }
}
