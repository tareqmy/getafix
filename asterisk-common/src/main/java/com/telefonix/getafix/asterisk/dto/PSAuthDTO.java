package com.telefonix.getafix.asterisk.dto;

import com.telefonix.getafix.asterisk.entity.PSAuth;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tareqmy on 2019-01-22.
 */
@Data
@NoArgsConstructor
public class PSAuthDTO {

    @NotNull
    @Size(max = 40)
    private String id;

    @Size(max = 10)
    private String authType;

    private int nonceLifetime;

    @Size(max = 40)
    private String md5Cred;

    @Size(max = 80)
    private String password;

    @Size(max = 40)
    private String realm;

    @Size(max = 40)
    private String username;

    public PSAuthDTO(PSAuth psAuth) {
        setId(psAuth.getId());
        setAuthType(psAuth.getAuthType().name());
        setNonceLifetime(psAuth.getNonceLifetime());
        setMd5Cred(psAuth.getMd5Cred());
        setPassword(psAuth.getPassword());
        setRealm(psAuth.getRealm());
        setUsername(psAuth.getUsername());
    }
}
