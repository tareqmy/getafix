package com.telefonix.getafix.asterisk.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.telefonix.getafix.asterisk.entity.CDR;
import com.telefonix.getafix.asterisk.utils.ZonedDateTimeSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

/**
 * Created by tareqmy on 2019-01-22.
 * note: server-generated
 */
@Data
@NoArgsConstructor
public class CDRDTO {

    private Long id;

    private String accountCode;

    private String src;

    private String dst;

    private String dContext;

    private String clId;

    private String channel;

    private String dstChannel;

    private String lastApp;

    private String lastData;

    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime start;

    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime answer;

    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime end;

    private int duration;

    private int billSec;

    private String disposition;

    private String amaFlags;

    private String userField;

    private String uniqueId;

    private String linkedId;

    private String peerAccount;

    private int sequence;

    public CDRDTO(CDR cdr) {
        setId(cdr.getId());
        setAccountCode(cdr.getAccountCode());
        setSrc(cdr.getSrc());
        setDst(cdr.getDst());
        setDContext(cdr.getDContext());
        setClId(cdr.getClId());
        setChannel(cdr.getChannel());
        setDstChannel(cdr.getDstChannel());
        setLastApp(cdr.getLastApp());
        setLastData(cdr.getLastData());
        setStart(cdr.getStart());
        setAnswer(cdr.getAnswer());
        setEnd(cdr.getEnd());
        setDuration(cdr.getDuration());
        setBillSec(cdr.getBillSec());
        setDisposition(cdr.getDisposition());
        setAmaFlags(cdr.getAmaFlags());
        setUserField(cdr.getUserField());
        setUniqueId(cdr.getUniqueId());
        setLinkedId(cdr.getLinkedId());
        setPeerAccount(cdr.getPeerAccount());
        setSequence(cdr.getSequence());
    }
}
