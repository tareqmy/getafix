#!/bin/bash

echo "check for database connection..."
/opt/wait-for-db.sh
echo "database connection checked.."

## Start Application
java -jar /getafix/getafix-web-1.0.0-SNAPSHOT.jar
