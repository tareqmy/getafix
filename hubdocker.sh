source .env

gradle clean build

docker build -t getafix:latest .

docker tag getafix:latest tareqmy/getafix:latest
docker push tareqmy/getafix:latest
docker rmi tareqmy/getafix:latest
